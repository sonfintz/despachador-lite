import {Component, ViewChild, OnInit, Renderer, Input} from '@angular/core';

/**
 * Generated class for the AccordionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'accordion',
  templateUrl: 'accordion.html'
})
export class AccordionComponent implements  OnInit{

  text: string;
  accordionExapanded = false;
  @ViewChild("cc") cardContent : any;
  @Input() Data: string;
  @Input() _BASE_URL: string;
  constructor( public renderer:Renderer) {
    console.log('Hello AccordionComponent Component');
    this.text = 'Hello World';
  }
  ngOnInit(){
    this.renderer.setElementStyle(this.cardContent.nativeElement,"webkitTransition","max-height 500ms, padding 500ms");
  }

  toogleAcordion(){ console.log('F accordion');
    if(this.accordionExapanded){
        this.renderer.setElementStyle(this.cardContent.nativeElement,"max-height","0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement,"padding","0px 16px");
    }else{
      this.renderer.setElementStyle(this.cardContent.nativeElement,"max-height","500px");
      this.renderer.setElementStyle(this.cardContent.nativeElement,"padding","13px 16px");
    }
    this.accordionExapanded = !this.accordionExapanded;
  }
}
