import {/*ChangeDetectorRef, */Component, Input, EventEmitter, Output} from '@angular/core';
import {rulesTypeahead} from "./rules_typeahead";

/**
 * Generated class for the TypeaheadComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'typeahead',
  templateUrl: 'typeahead.html'
})
export class TypeaheadComponent {

   public result:any = [];
   public  ItemSearch='';
   public inputVal:any;
   private focus:boolean;
  @Input() Data = [];
  @Input() Rules: rulesTypeahead;
  @Output('ValueOutput') ValueOutput = new EventEmitter();
  constructor(/*private changeDetector : ChangeDetectorRef*/) {
    //---/console.log('Hello TypeaheadComponent Component', this.Data);

  }
  itemfocus(/*$event:any*/){
    ////---/console.log('Focus',$event)
    this.focus = true;
  }
  itemblur(/*$event:any*/){
    ////---/console.log('Blur',$event)
    this.focus= false;
  }
  eventHandler($value){
    ////---/console.log('HANDLER:',$value,'FOCUS:',this.focus);
    //this.changeDetector.detectChanges();
    this.ItemSearch = this.Rules.search_for_Item;
    if($value!=''&& this.focus === true){
      this.result = this.getResults($value);
    }else {this.result = [];

    }

    if($value =='' && this.focus === true){
      this.ValueOutput.emit({data: null});
    }
    ////---/console.log('Result',this.result);
  }

  getResults(keyword:any){
    ////---/console.log('Funtion->',this.Data, this.Rules);

    return this.Data.filter(item=>

      //item[this.Rules.search_for_Item].toLowerCase().startsWith(keyword.toLowerCase())).slice(0,3);
        item[this.Rules.search_for_Item].toLowerCase().includes(keyword.toLowerCase())).slice(0,3);

  }
  _selectedItem(valueName,valueItem){
    this.inputVal = valueName;
    this.ValueOutput.emit({data: valueItem});
    //---/console.log('Value Selected',valueItem);
  }
}
