var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { /*ChangeDetectorRef, */ Component, Input, EventEmitter, Output } from '@angular/core';
/**
 * Generated class for the TypeaheadComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
let TypeaheadComponent = class TypeaheadComponent {
    constructor() {
        //---/console.log('Hello TypeaheadComponent Component', this.Data);
        this.result = [];
        this.ItemSearch = '';
        this.Data = [];
        this.ValueOutput = new EventEmitter();
    }
    itemfocus() {
        ////---/console.log('Focus',$event)
        this.focus = true;
    }
    itemblur() {
        ////---/console.log('Blur',$event)
        this.focus = false;
    }
    eventHandler($value) {
        ////---/console.log('HANDLER:',$value,'FOCUS:',this.focus);
        //this.changeDetector.detectChanges();
        this.ItemSearch = this.Rules.search_for_Item;
        if ($value != '' && this.focus === true) {
            this.result = this.getResults($value);
        }
        else {
            this.result = [];
        }
        if ($value == '' && this.focus === true) {
            this.ValueOutput.emit({ data: null });
        }
        ////---/console.log('Result',this.result);
    }
    getResults(keyword) {
        ////---/console.log('Funtion->',this.Data, this.Rules);
        return this.Data.filter(item => 
        //item[this.Rules.search_for_Item].toLowerCase().startsWith(keyword.toLowerCase())).slice(0,3);
        item[this.Rules.search_for_Item].toLowerCase().includes(keyword.toLowerCase())).slice(0, 3);
    }
    _selectedItem(valueName, valueItem) {
        this.inputVal = valueName;
        this.ValueOutput.emit({ data: valueItem });
        //---/console.log('Value Selected',valueItem);
    }
};
__decorate([
    Input(),
    __metadata("design:type", Object)
], TypeaheadComponent.prototype, "Data", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], TypeaheadComponent.prototype, "Rules", void 0);
__decorate([
    Output('ValueOutput'),
    __metadata("design:type", Object)
], TypeaheadComponent.prototype, "ValueOutput", void 0);
TypeaheadComponent = __decorate([
    Component({
        selector: 'typeahead',
        templateUrl: 'typeahead.html'
    }),
    __metadata("design:paramtypes", [])
], TypeaheadComponent);
export { TypeaheadComponent };
//# sourceMappingURL=typeahead.js.map