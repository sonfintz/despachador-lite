import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
/**
 * Generated class for the LogrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import * as moment from "moment";
import {AppModule} from "../../app/app.module";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";

@Component({
  selector: 'page-logros',
  templateUrl: 'logros.html',
})
export class LogrosPage {

  get NotificationsSetting() {
    return DataglobalProvider.NotificacionesSettings;
  }

  private Logros: any;
  private LogrosTotal: any;

  constructor(public navCtrl: NavController,
              public Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    console.log(this.navParams.get('value').retos);
    this.Logros = this.navParams.get('data');
    this.LogrosTotal = this.navParams.get('value').logros;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogrosPage');
  }

  imagenLogro(imagen) {
    return AppModule.BASE_URL + '/' + imagen
  }

  _formatDate(date) {
    if (date) { return moment(date, "DD-MM-YYYY HH:mm:ss").format('YYYY/MM/DD HH:mm a') }
    else {return ''}
  }

  infoNotf(e) {
      const ref = e.design_codigo;
      // const Puntos = this.TailProcesCuentas[0].punto;
      let btn, mss, Puntos;
      let Tabulador
      let titlePrimary;
      btn = [{
        text: "Entendido",
        handler: () => {
        },
        cssClass: "button-green"

      }];
      try {
        Tabulador = this.NotificationsSetting.find(i => i.design_codigo == ref);
        console.log(Tabulador);
        let conversion = Tabulador['design_puntos'] || 1000;
        Puntos = Tabulador['design_moneda'] * conversion;
        const spanPuntos = `<span class="puntos">+ ${Puntos} MP</span>`;
        let spanPuntosTitle = `<span class="punto-in-title">&nbsp;+${Puntos} MP</span>`;
        mss = `${Tabulador.design_detalle}`;
        console.log(spanPuntos);

        titlePrimary = `${Tabulador.design_tipo}${spanPuntosTitle}`
      } catch (e) {
        console.error('Error en el find ')
      }

      this.Complementos.showAlert(titlePrimary, `${Tabulador.design_titulo}`, mss, btn, 'alertConfig_6_css');
    }
}
