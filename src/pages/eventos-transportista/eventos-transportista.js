var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { FormBuilder, Validators } from "@angular/forms";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let EventosTransportistaPage = class EventosTransportistaPage {
    constructor(navCtrl, navParams, events, fb, Complementos, api) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.fb = fb;
        this.Complementos = Complementos;
        this.api = api;
        this.TRANSPORTISTA = this.navParams.get('data');
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad EventosTransportistaPage');
    }
    ngOnInit() {
        this._Ubicaciones();
        this.api._GET('/tipoevento').subscribe((data) => {
            console.log('eventos', data);
            this.eventos = data;
        });
        this.initForm();
    }
    initForm() {
        this.form = this.fb.group({
            id_tipoevento: [null, [Validators.required]],
            observaciones: [null, [Validators.required]],
            id_usuario: [this.TRANSPORTISTA.usuario_id],
            id_solicitud: [this.TRANSPORTISTA.solicitudes_id],
            latitud: [0],
            longitud: [0],
            extra: null,
            id_oferta: null
        });
    }
    _UbicacionOutput(datos) {
        console.log(datos.data);
        if (datos.data) {
            console.log('Output ubicacion', datos.data.latitud, datos.data.longitud);
            this.form.get('latitud').setValue(datos.data.latitud);
            this.form.get('longitud').setValue(datos.data.longitud);
        }
        else {
            this.form.get('latitud').setValue(0);
            this.form.get('longitud').setValue(0);
        }
    }
    _Ubicaciones() {
        this.api._GET('/municipio')
            .subscribe((data) => {
            data = data.map((i) => {
                i.coordenadas = {
                    latitud: i.latitud,
                    longitud: i.longitud
                };
                return i;
            });
            console.log('_Municipios:', data);
            this._Municipios = data;
        }, () => {
        }, () => {
        });
    }
    saveData() {
        console.log('guardando evento', this.form.value);
        this.api._POST('/solicitudeseventos', this.form.value).subscribe(() => {
            this.Complementos._MessageToast(`Evento creado exitosamente`);
            this.navCtrl.pop();
            this.events.publish('evento-created');
        });
    }
};
EventosTransportistaPage = __decorate([
    Component({
        selector: 'page-eventos-transportista',
        templateUrl: 'eventos-transportista.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Events,
        FormBuilder,
        ComplementosviewsProvider,
        ApiProvider])
], EventosTransportistaPage);
export { EventosTransportistaPage };
//# sourceMappingURL=eventos-transportista.js.map