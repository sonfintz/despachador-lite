import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-eventos-transportista',
  templateUrl: 'eventos-transportista.html',
})
export class EventosTransportistaPage {
  private TRANSPORTISTA: any;
  public eventos: any;
  form: FormGroup;
  public _Municipios: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public events: Events,
              public fb: FormBuilder,
              private Complementos: ComplementosviewsProvider,
              public api: ApiProvider) {
    this.TRANSPORTISTA = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventosTransportistaPage');
  }

  ngOnInit() {
    this._Ubicaciones();
    this.api._GET('/tipoevento').subscribe((data) => {
      console.log('eventos', data);
      this.eventos = data;
    });
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      id_tipoevento: [null, [Validators.required]],
      observaciones: [null, [Validators.required]],
      id_usuario: [this.TRANSPORTISTA.usuario_id],
      id_solicitud: [this.TRANSPORTISTA.solicitudes_id],
      latitud: [0],
      longitud: [0],
      extra: null,
      id_oferta: null
    })
  }

  _UbicacionOutput(datos) {
    console.log(datos.data);
    if (datos.data) {
      console.log('Output ubicacion', datos.data.latitud, datos.data.longitud);
      this.form.get('latitud').setValue(datos.data.latitud);
      this.form.get('longitud').setValue(datos.data.longitud);
    } else {
      this.form.get('latitud').setValue(0);
      this.form.get('longitud').setValue(0);
    }
  }

  _Ubicaciones() {
    this.api._GET('/municipio')
      .subscribe(
        (data) => {
          data = data.map((i) => {
            i.coordenadas = {
              latitud: i.latitud,
              longitud: i.longitud
            };
            return i;
          });
          console.log('_Municipios:', data);
          this._Municipios = data;
        },
        () => {
        },
        () => {
        },
      );
  }

  saveData() {
    console.log('guardando evento', this.form.value);
    this.api._POST('/solicitudeseventos', this.form.value).subscribe(() => {
      this.Complementos._MessageToast(`Evento creado exitosamente`);
      this.navCtrl.pop();
      this.events.publish('evento-created')
    })
  }


}
