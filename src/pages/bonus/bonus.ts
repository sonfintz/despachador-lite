import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
/**
 * Generated class for the BonusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import * as moment from "moment";
import {AppModule} from "../../app/app.module";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-bonus',
  templateUrl: 'bonus.html',
})
export class BonusPage {

  get NotificationsSetting() {
    return DataglobalProvider.NotificacionesSettings;
  }

  private Bonus: any;
  private BonusTotal: any;

  /*Bonus: Array<any> = [{
    alcanzado: "NO",
    design_codigo: 100,
    design_detalle: "Comparte tu PIN a nuevos transportistas y gana MP.",
    design_puntos: 1000,
    design_tipo: "Bonus MP",
    design_titulo: "¡Referiste a un Transportista!",
    fecha: "",
    icono: "/img/iconos/ico_bonus_off.svg"
  }]*/

  constructor(public navCtrl: NavController,
              public Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    console.log(this.navParams.get('value').bonus);
    this.Bonus = this.navParams.get('data');
    this.BonusTotal = this.navParams.get('value').bonus
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BonusPage');
  }

  imagenLogro(imagen) {
    return AppModule.BASE_URL + '/' + imagen
  }

  _formatDate(date) {
    if (date) { return moment(date, "DD-MM-YYYY HH:mm:ss").format('YYYY/MM/DD HH:mm a') }
    else {return ''}
  }

  infoNotf(e) {
      const ref = e.design_codigo;
      // const Puntos = this.TailProcesCuentas[0].punto;
      let btn, mss, Puntos;
      let Tabulador
      let titlePrimary;
      btn = [{
        text: "Entendido",
        handler: () => {
        },
        cssClass: "button-green"

      }];
      try {
        Tabulador = this.NotificationsSetting.find(i => i.design_codigo == ref);
        let conversion = Tabulador['design_puntos'] || 1000;
        Puntos = Tabulador['design_moneda'] * conversion;
        const spanPuntos = `<span class="puntos">+ ${Puntos} MP</span>`;
        let spanPuntosTitle = `<span class="punto-in-title">&nbsp;+${Puntos} MP</span>`;
        mss = `${Tabulador.design_detalle}`;
        console.log(spanPuntos);

        titlePrimary = `${Tabulador.design_tipo}${spanPuntosTitle}`
      } catch (e) {
        console.error('Error en el find ')
      }

      this.Complementos.showAlert(titlePrimary, `${Tabulador.design_titulo}`, mss, btn, 'alertConfig_6_css');
    }
}
