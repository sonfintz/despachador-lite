import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

@Component({
  selector: 'page-remesamanifiesto',
  templateUrl: 'remesamanifiesto.html',
})
export class RemesamanifiestoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RemesamanifiestoPage');
  }

}
