var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, NavController, NavParams, ToastController } from 'ionic-angular';
import { AppModule } from "../../app/app.module";
import { Camera } from "@ionic-native/camera";
import { FormBuilder, Validators } from "@angular/forms";
import { ConferenceApp } from "../../app/app.component";
import { ApiProvider } from "../../providers/api/api";
import { LoginPage } from "../login/login";
import { GpsProvider } from "../../providers/gps/gps";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { EmpresaaddPage } from "../empresaadd/empresaadd";
import { PerfileditPage } from "../perfiledit/perfiledit";
import { CuentasasociadasPage } from "../cuentasasociadas/cuentasasociadas";
import { DeleteAcountPage } from "../delete-acount/delete-acount";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
import { SocialSharing } from "@ionic-native/social-sharing";
import { FirebaseProvider } from "../../providers/firebase/firebase";
let PerfilPage = class PerfilPage {
    constructor(navCtrl, navParams, camera, Complementos, socialSharing, formBuilder, toastCtrl, firebase, events, alertCtrl, Api) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.Complementos = Complementos;
        this.socialSharing = socialSharing;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.firebase = firebase;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.Api = Api;
        this._Edit = true;
        this._IMG = false;
        this._Municipios = [];
        this._User = localStorage.getItem('pin');
        this.ErrorConexion = false;
        this._VerificarUsuario();
        console.log('djklasdsalhdsa', DataglobalProvider.dataPerfil);
        ConferenceApp.rand = Math.random() * 10;
        this._Edit = true;
        this.myForm = this.createMyForm();
    }
    /** ALMACENA LOS DATOS DEL USUARIO LOGHEADO**/
    get _star() {
        return JSON.parse(localStorage.getItem("star"));
    }
    get _user() {
        return this.USER = {
            usuario_email: localStorage.getItem("emailUserTRUCK"),
            usuario_nombre: localStorage.getItem("nameUserTRUCK"),
            usuario_id: localStorage.getItem("idUserTRUCK")
        };
    }
    get _BASE_URL() {
        return AppModule.BASE_URL;
    }
    _IMG_DEFAULT(event) {
        event.target.src = 'assets/imgs/Imagen_Perfil.svg';
    }
    get _PERFIL() {
        return DataglobalProvider.dataPerfil;
    }
    getUrl() {
        let idMunicipio = localStorage.getItem("idMunicUserTRUCK");
        let ruta = `url('${this._BASE_URL}/img/municipios/${idMunicipio}.jpg')`;
        return ruta + ",url('assets/imgs/cucuta.png')";
    }
    get ImagenPerfil() {
        return DataglobalProvider.ImagenPerfil;
    }
    get BadConection() {
        return GpsProvider.Band_Conexion;
    }
    _DEFAULT_ImagenPerfil(event) {
        event.target.src = 'assets/imgs/icon/m_f_perfil.svg';
    }
    validacionRapida() {
        this.Api._GET('/validacionrapida').subscribe((data) => {
            this.Complementos._MessageToast('Solicitud enviada');
            console.log(data);
        }, (err) => {
            this.Complementos._MessageToast(`${err.error.mensaje}`);
        });
    }
    /*DATA PERFIL*/
    doRefresh(refresher) {
        this.DataPerfil();
        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
    DataPerfil() {
        this.Api._GET(`/despachador/${localStorage.getItem('idUserMUEVE')}`).subscribe((data) => {
            console.log('data del perfil', data);
            DataglobalProvider.dataPerfil = data;
            this.Perfil = data.usuario_verificado;
        }, (err) => {
            console.group("PETICION DE PERFIL"); //1fedcdda-210b-40fa-b379-5bf6358c4f8b    8644d682-cf77-4928-b4ff-4d0a334b942e
            console.log(err);
            console.groupEnd();
        });
    }
    /*VERIFICAR USUARIO*/
    _VerificarUsuario() {
        console.log('PERFIL', 1);
        this.Api._GET(`/despachador/${localStorage.getItem('idUserMUEVE')}`).subscribe((data) => {
            this.ErrorConexion = false;
            this.Perfil = data.usuario_verificado;
            console.log('VERIFICADO?----------', this.Perfil);
        }, (err) => {
            this.ErrorConexion = true;
            console.group("PETICION DE PERFIL"); //1fedcdda-210b-40fa-b379-5bf6358c4f8b    8644d682-cf77-4928-b4ff-4d0a334b942e
            console.log(err);
            console.groupEnd();
        });
    }
    Perfilclose() {
        this.navCtrl.pop();
    }
    /*CUENTAS ASOCIADAS (FACEBOOK O GOOGLE)*/
    _cuentasAsociadas() {
        this.navCtrl.push(CuentasasociadasPage);
    }
    /**+--------------------------------+
     *  Outputs typeahead
     */
    _UbicacionOutput(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.myForm.get('id_municipio').setValue(datos.data);
        }
        else {
            this.myForm.get('id_municipio').setValue('');
        }
    }
    /**+--------------------------------+
     * Fin  Outputs typeahead
     */
    /**----------------------------------------------
     *
     *                 FORMULARIO
     *
     *----------------------------------------------- **/
    /**-------/CREAR FORMULARIO **/
    createMyForm() {
        return this.formBuilder.group({
            login: ['', [Validators.required], [this._ValidateLogin.bind(this)]],
            password: ['', Validators.required],
            passwordConfirmation: ['', [Validators.required], [this._PASSWORDS_MATCH2.bind(this)]],
            telefono: ['', Validators.required],
            id_municipio: ['', Validators.required],
        });
    }
    /**+++++++++++++++++++++++++++++++++++++++++++++++++
     * +-------/  VALIDACIONES  FORMULARIO
     * --> VALIDAR LOGIN
     * ------------------------------------------------+ **/
    _ValidateLogin(control) {
        return new Promise(resolve => {
            this.Api._GET(`/consultar/login/${control.value}`)
                .subscribe(data => {
                if (data[0].existe === 'SI') {
                    return resolve({ loginTaken: true });
                }
                else {
                    return resolve(null);
                }
            }, error2 => {
                console.log(error2);
            }, () => {
            });
        });
    }
    // telefono celular (2)
    telefonoCelular2(telefono) {
        if (telefono === '0000000') {
            return '';
        }
        else {
            return telefono;
        }
    }
    /**
     * --> VALIDAR CONTRASEÑA
     * ------------------------------------------------+ **/
    _PASSWORDS_MATCH2(control, group) {
        console.log(group);
        return new Promise(resolve => {
            let firstPassword = this.myForm.get('password').value;
            if (firstPassword != control.value) {
                return resolve({ MatchPassword: true });
            }
            else {
                return resolve(null);
            }
        });
    }
    _EditarForm(opc) {
        console.log('OPc:', opc);
        if (opc === 1) {
            this._Edit = false;
        }
        else {
            this._Edit = true;
        }
    }
    /**----------------------------------------------
     *
     *                FIN  FORMULARIO
     *
     *----------------------------------------------- **/
    /*NUEVA ALERTA*/
    _Select_PictureSourceType() {
        // let actionSheet = this.alertCtrl.create({
        //     title: 'Camara',
        //     message: 'Seleciona ',
        //     buttons: [{
        //         text: 'Cargar de la Galeria',
        //         handler: () => {
        //             this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        //         }
        //     }, {
        //         text: 'Tomar foto',
        //         handler: () => {
        //             this.getPicture(this.camera.PictureSourceType.CAMERA);
        //         }
        //     }, {
        //         text: 'Cancel',
        //         role: 'cancel'
        //     }]
        // });
        // actionSheet.present();
        let Btns = [{
                text: 'Galeria',
                cssClass: "botton1",
                handler: () => {
                    this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            }, {
                text: 'Tomar foto',
                cssClass: "botton2",
                handler: () => {
                    this.getPicture(this.camera.PictureSourceType.CAMERA);
                }
            }];
        let mss = "Selecciona";
        this.Complementos.showAlert("Camara", "", mss, Btns, "alertCustomCssPicture", true);
    }
    /**-----------------------------------------------------
     *              METODO    CAPTURAR IMAGENES
     *  ----------------------------------------------------**/
    /* _Select_PictureSourceType() {
       let actionSheet = this.alertCtrl.create({
         title: 'Camara',
         buttons: [{
           text: 'Cargar de la Galeria',
           cssClass: 'botton1',
           handler: () => {
             this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
           }
         }, {
           text: 'Tomar foto',
           cssClass: 'botton1',
           handler: () => {
             this.getPicture(this.camera.PictureSourceType.CAMERA);
           }
         }, {
           text: 'Cancel',
           role: 'cancel'
         }],
         cssClass: 'alertCustomCssFoto' // <- added this
       });
       actionSheet.present();
     }*/
    getPicture(selectedSourceType) {
        let options = {
            sourceType: selectedSourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 300,
            targetHeight: 300,
            quality: 70,
            saveToPhotoAlbum: true
        };
        this.camera.getPicture(options)
            .then(imagenData => {
            this.image = `data:image/jpeg;base64,${imagenData}`;
            if (this.image) {
                this._IMG = true;
                this.savePicture();
            }
        })
            .catch(error => {
            console.log(error);
        });
    }
    savePicture() {
        let obj = {
            data: { img: this.image },
        };
        this.Api._POST('/despachador/uploadimg/', obj)
            .subscribe(data => {
            console.log('Actualizado:', obj, data);
            this._MensajeToast("¡Datos Actualizados!");
            this.events.publish('imagen-upload', true);
        }, error1 => {
            if (error1) {
                console.info('error', error1);
                this._MensajeToast('Error con el servicio..');
            }
        });
    }
    /**-----------------------------------------------------
     *        FIN      METODO    CAPTURAR IMAGENES
     *  ----------------------------------------------------**/
    /** MENSAJE TOAST**/
    _MensajeToast(mensaje) {
        let toast = this.toastCtrl.create({
            message: mensaje,
            duration: 6000
        });
        toast.present().then(() => {
        });
    }
    /**------------------------------------------
     *                                          @
     *       CONSULTAS API BACKEND              @
     *                                          @
     * ------------------------------------------
     */
    _Ubicaciones() {
        this.Api._GET('/municipio')
            .subscribe((data) => {
            console.log('_Municipios:', data);
            this._Municipios = data;
        }, () => {
        }, () => {
        });
    }
    /** DATOS DEL USUARIO **/
    _DatosUser() {
        this.Api._GET(`/perfil/${localStorage.getItem("idUserMUEVE")}`)
            .subscribe((data) => {
            console.log('La data del usuario:', data);
            this.DataUser = data;
        }, (err) => {
            console.log(err);
        }, () => {
        });
    }
    /** FIN DEL METODO **/
    save_FORM() {
        let Obj = Object.assign(this.myForm.value, { imagen: this.image, id: localStorage.getItem("idUserTRUCK") });
        console.log('Datos del Formulario:', Obj);
        this.Api._POST('/perfil', Obj)
            .subscribe((data) => {
            console.log('Actualizado:', data);
            if (data.update === true) {
                this._MensajeToast("¡Datos Actualizados!");
            }
        }, (error) => {
            if (error) {
                this._MensajeToast('Error con el servicio..');
            }
        }, () => {
        });
    }
    addEmpresa() {
        this.navCtrl.push(EmpresaaddPage, { data: this.dataPerfil });
    }
    _LogOut() {
        let actionSheet = this.alertCtrl.create({
            title: 'Cerrar sesión',
            buttons: [
                {
                    text: '¡Espera no!',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: 'Cerrar sesión',
                    cssClass: 'botton2',
                    handler: () => {
                        this.Api._GET('/session/cerrar').subscribe(data => console.log(data));
                        try {
                            FCMPlugin.unsubscribeFromTopic(`notificacion${localStorage.getItem('idUserMUEVE')}`);
                        }
                        catch (e) {
                            console.log(e);
                        }
                        localStorage.clear();
                        AppModule.My_Token = '';
                        this._CloseGPS();
                        this._DestroySocket();
                        this.Complementos._MessageToast('¡Fin de la sesion!');
                        this.navCtrl.setRoot(LoginPage);
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        actionSheet.present();
    }
    /**-- Destruir SOCKET --**/
    _DestroySocket() {
        GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
        GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
        if (GpsProvider.EMIT != undefined) {
            console.log(' Disconect Socket', GpsProvider.EMIT);
            GpsProvider.EMIT.disconnect();
            console.log('1 Disconect Socket', GpsProvider.EMIT);
        }
    }
    /** CERRAR GPS **/
    _CloseGPS() {
        console.group('CERRANDO EL EMIT');
        console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
        console.groupEnd();
        // GpsProvider.Band_Conexion=false;
        clearInterval(GpsProvider.intervalSocket);
        clearInterval(GpsProvider.antiFakeGPS);
        let D = clearInterval(GpsProvider.intervalSocket);
        GpsProvider.intervalSocket = 0;
        console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);
    }
    /*EDITAR DATOS DE PERFIL*/
    editPerfil(campo) {
        this.navCtrl.push(PerfileditPage, { data: DataglobalProvider.dataPerfil, value: campo });
    }
    /*ELIMINAR CUENTA ASOCIADA*/
    _DeleteAcount() {
        this.navCtrl.push(DeleteAcountPage);
    }
    Compartir(shortid) {
        console.log(shortid);
        this.socialSharing.share(`Mi pin de referido ${shortid}`, 'Mueve Logistica', '', 'https://play.google.com/store/apps/details?id=com.mueve_despachadorlite&hl=en')
            .then(() => {
        })
            .catch(err => {
            console.error('Error al compartir: ', err);
            this.Complementos._MessageToast('Error al compartir');
        });
    }
};
PerfilPage = __decorate([
    Component({
        selector: 'page-perfil',
        templateUrl: 'perfil.html',
        providers: [ApiProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Camera,
        ComplementosviewsProvider,
        SocialSharing,
        FormBuilder,
        ToastController,
        FirebaseProvider,
        Events,
        AlertController,
        ApiProvider])
], PerfilPage);
export { PerfilPage };
//# sourceMappingURL=perfil.js.map