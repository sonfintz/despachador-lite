import {Component} from '@angular/core';
import {AlertController, Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {AppModule} from "../../app/app.module";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConferenceApp} from "../../app/app.component";
import {ApiProvider} from "../../providers/api/api";
import {LoginPage} from "../login/login";
import {GpsProvider} from "../../providers/gps/gps";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {EmpresaaddPage} from "../empresaadd/empresaadd";
import {PerfileditPage} from "../perfiledit/perfiledit";
import {CuentasasociadasPage} from "../cuentasasociadas/cuentasasociadas";
import {DeleteAcountPage} from "../delete-acount/delete-acount";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {SocialSharing} from "@ionic-native/social-sharing";
import {FirebaseProvider} from "../../providers/firebase/firebase";

declare let FCMPlugin;

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
  providers: [ApiProvider]
})
export class PerfilPage {
  image: any;
  DataUser: any;
  public _Edit: boolean = true;
  public _IMG: boolean = false;
  public _Municipios: any = [];
  myForm: FormGroup;
  rand: any;
  public USER: {};
  public dataPerfil: any;
  public _User = localStorage.getItem('pin');
  public Perfil: any;
  private ErrorConexion: boolean = false;

  /** ALMACENA LOS DATOS DEL USUARIO LOGHEADO**/
  public get _star(): any {
    return JSON.parse(localStorage.getItem("star"));
  }

  public get _user(): {} {
    return this.USER = {
      usuario_email: localStorage.getItem("emailUserTRUCK"),
      usuario_nombre: localStorage.getItem("nameUserTRUCK"),
      usuario_id: localStorage.getItem("idUserTRUCK")
    };
  }

  public get _BASE_URL() {
    return AppModule.BASE_URL;
  }

  public _IMG_DEFAULT(event) {
    event.target.src = 'assets/imgs/Imagen_Perfil.svg';
  }

  get _PERFIL() {
    return DataglobalProvider.dataPerfil
  }

  getUrl() {
    let idMunicipio = localStorage.getItem("idMunicUserTRUCK");
    let ruta = `url('${this._BASE_URL}/img/municipios/${idMunicipio}.jpg')`;
    return ruta + ",url('assets/imgs/cucuta.png')";
  }

  public get ImagenPerfil() {
    return DataglobalProvider.ImagenPerfil;
  }

  get BadConection() {
    return GpsProvider.Band_Conexion;
  }

  public _DEFAULT_ImagenPerfil(event) {
    event.target.src = 'assets/imgs/icon/m_f_perfil.svg';
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private camera: Camera,
              private Complementos: ComplementosviewsProvider,
              private socialSharing: SocialSharing,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController,
              public firebase: FirebaseProvider,
              public events: Events,
              public alertCtrl: AlertController,
              private Api: ApiProvider) {
    this._VerificarUsuario();
    console.log('djklasdsalhdsa', DataglobalProvider.dataPerfil);
    ConferenceApp.rand = Math.random() * 10;
    this._Edit = true;
    this.myForm = this.createMyForm();
  }

  validacionRapida() {
    this.Api._GET('/validacionrapida').subscribe(
      (data) => {
        this.Complementos._MessageToast('Solicitud enviada');
        console.log(data)
      }, (err) => {
        this.Complementos._MessageToast(`${err.error.mensaje}`)
      })
  }

  /*DATA PERFIL*/
  doRefresh(refresher) {
    this.DataPerfil();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


  DataPerfil() {
    this.Api._GET(`/despachador/${localStorage.getItem('idUserMUEVE')}`).subscribe(
      (data) => {
        console.log('data del perfil', data);
        DataglobalProvider.dataPerfil = data;
        this.Perfil = data.usuario_verificado;
      }, (err) => {
        console.group("PETICION DE PERFIL"); //1fedcdda-210b-40fa-b379-5bf6358c4f8b    8644d682-cf77-4928-b4ff-4d0a334b942e
        console.log(err);
        console.groupEnd()
      });
  }

  /*VERIFICAR USUARIO*/
  _VerificarUsuario() {
    console.log('PERFIL', 1);
    this.Api._GET(`/despachador/${localStorage.getItem('idUserMUEVE')}`).subscribe(
      (data) => {
        this.ErrorConexion = false;
        this.Perfil = data.usuario_verificado;
        console.log('VERIFICADO?----------', this.Perfil)
      }, (err) => {
        this.ErrorConexion = true;
        console.group("PETICION DE PERFIL"); //1fedcdda-210b-40fa-b379-5bf6358c4f8b    8644d682-cf77-4928-b4ff-4d0a334b942e
        console.log(err);
        console.groupEnd()
      })
  }

  Perfilclose() {
    this.navCtrl.pop();
  }

  /*CUENTAS ASOCIADAS (FACEBOOK O GOOGLE)*/
  _cuentasAsociadas() {
    this.navCtrl.push(CuentasasociadasPage);
  }

  /**+--------------------------------+
   *  Outputs typeahead
   */
  _UbicacionOutput(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.myForm.get('id_municipio').setValue(datos.data);
    } else {
      this.myForm.get('id_municipio').setValue('');
    }
  }

  /**+--------------------------------+
   * Fin  Outputs typeahead
   */

  /**----------------------------------------------
   *
   *                 FORMULARIO
   *
   *----------------------------------------------- **/
  /**-------/CREAR FORMULARIO **/
  private createMyForm() {

    return this.formBuilder.group({
      login: ['', [Validators.required], [this._ValidateLogin.bind(this)]],
      password: ['', Validators.required],
      passwordConfirmation: ['', [Validators.required], [this._PASSWORDS_MATCH2.bind(this)]],
      telefono: ['', Validators.required],
      id_municipio: ['', Validators.required],
    });
  }

  /**+++++++++++++++++++++++++++++++++++++++++++++++++
   * +-------/  VALIDACIONES  FORMULARIO
   * --> VALIDAR LOGIN
   * ------------------------------------------------+ **/
  _ValidateLogin(control: AbstractControl) {
    return new Promise(resolve => {
      this.Api._GET(`/consultar/login/${control.value}`)
      /* TODO Eliminar instancia del servicio
      this.Auth._ValidateLogin2(control.value) */
        .subscribe(
          data => {
            if (data[0].existe === 'SI') {
              return resolve({loginTaken: true});
            } else {
              return resolve(null);
            }
          },
          error2 => {
            console.log(error2)
          },
          () => {
          }
        );
    });

  }

  // telefono celular (2)
  telefonoCelular2(telefono) {
    if (telefono === '0000000') {
      return '';
    } else {
      return telefono
    }
  }

  /**
   * --> VALIDAR CONTRASEÑA
   * ------------------------------------------------+ **/
  _PASSWORDS_MATCH2(control: AbstractControl, group: FormGroup) {
    console.log(group);
    return new Promise(resolve => {

      let firstPassword = this.myForm.get('password').value;

      if (firstPassword != control.value) {

        return resolve({MatchPassword: true});
      } else {
        return resolve(null);
      }
    });

  }

  _EditarForm(opc: number) {
    console.log('OPc:', opc);
    if (opc === 1) {
      this._Edit = false;
    } else {
      this._Edit = true;
    }

  }

  /**----------------------------------------------
   *
   *                FIN  FORMULARIO
   *
   *----------------------------------------------- **/


  /*NUEVA ALERTA*/
  _Select_PictureSourceType() {
    // let actionSheet = this.alertCtrl.create({
    //     title: 'Camara',
    //     message: 'Seleciona ',
    //     buttons: [{
    //         text: 'Cargar de la Galeria',
    //         handler: () => {
    //             this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
    //         }
    //     }, {
    //         text: 'Tomar foto',
    //         handler: () => {
    //             this.getPicture(this.camera.PictureSourceType.CAMERA);
    //         }
    //     }, {
    //         text: 'Cancel',
    //         role: 'cancel'
    //     }]
    // });
    // actionSheet.present();
    let Btns = [{
      text: 'Galeria',
      cssClass:"botton1",
      handler: () => {
        this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
      }
    }, {
      text: 'Tomar foto',
      cssClass:"botton2",
      handler: () => {
        this.getPicture(this.camera.PictureSourceType.CAMERA);
      }
    }];
    let mss = "Selecciona";
    this.Complementos.showAlert("Camara","",mss,Btns,"alertCustomCssPicture",true)
  }


  /**-----------------------------------------------------
   *              METODO    CAPTURAR IMAGENES
   *  ----------------------------------------------------**/
 /* _Select_PictureSourceType() {
    let actionSheet = this.alertCtrl.create({
      title: 'Camara',
      buttons: [{
        text: 'Cargar de la Galeria',
        cssClass: 'botton1',
        handler: () => {
          this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Tomar foto',
        cssClass: 'botton1',
        handler: () => {
          this.getPicture(this.camera.PictureSourceType.CAMERA);
        }
      }, {
        text: 'Cancel',
        role: 'cancel'
      }],
      cssClass: 'alertCustomCssFoto' // <- added this
    });
    actionSheet.present();
  }*/

  getPicture(selectedSourceType: number) {
    let options: CameraOptions = {
      sourceType: selectedSourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 300,
      targetHeight: 300,
      quality: 70,
      saveToPhotoAlbum: true
    };

    this.camera.getPicture(options)
      .then(imagenData => {
        this.image = `data:image/jpeg;base64,${imagenData}`;
        if (this.image) {
          this._IMG = true;
          this.savePicture();
        }

      })
      .catch(error => {
        console.log(error);
      });
  }

  savePicture() {
    let obj: any = {
      data: {img: this.image},
    };
    this.Api._POST('/despachador/uploadimg/', obj)
      .subscribe(
        data => {
          console.log('Actualizado:', obj, data);
          this._MensajeToast("¡Datos Actualizados!");
          this.events.publish('imagen-upload', true);
        },
        error1 => {
          if (error1) {
            console.info('error', error1);
            this._MensajeToast('Error con el servicio..');
          }
        }
      )
  }

  /**-----------------------------------------------------
   *        FIN      METODO    CAPTURAR IMAGENES
   *  ----------------------------------------------------**/


  /** MENSAJE TOAST**/
  _MensajeToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 6000
    });

    toast.present().then(() => {
    });
  }

  /**------------------------------------------
   *                                          @
   *       CONSULTAS API BACKEND              @
   *                                          @
   * ------------------------------------------
   */
  _Ubicaciones() {
    this.Api._GET('/municipio')
      .subscribe(
        (data) => {
          console.log('_Municipios:', data);
          this._Municipios = data;
        },
        () => {
        },
        () => {
        },
      );
  }

  /** DATOS DEL USUARIO **/
  _DatosUser() {
    this.Api._GET(`/perfil/${localStorage.getItem("idUserMUEVE")}`)
      .subscribe(
        (data) => {
          console.log('La data del usuario:', data);
          this.DataUser = data;
        },
        (err) => {
          console.log(err)
        },
        () => {
        }
      );
  }

  /** FIN DEL METODO **/

  save_FORM() {

    let Obj: any = Object.assign(this.myForm.value, {imagen: this.image, id: localStorage.getItem("idUserTRUCK")});
    console.log('Datos del Formulario:', Obj);
    this.Api._POST('/perfil', Obj)
      .subscribe(
        (data) => {
          console.log('Actualizado:', data);
          if (data.update === true) {
            this._MensajeToast("¡Datos Actualizados!")
          }
        },
        (error) => {
          if (error) {
            this._MensajeToast('Error con el servicio..')
          }
        },
        () => {
        }
      );
  }

  addEmpresa() {
    this.navCtrl.push(EmpresaaddPage, {data: this.dataPerfil})
  }

  _LogOut() {
    let actionSheet = this.alertCtrl.create({
      title: 'Cerrar sesión',
      buttons: [
        {
          text: '¡Espera no!',
          role: 'cancel',
          cssClass: 'botton1',
        }, {
          text: 'Cerrar sesión',
          cssClass: 'botton2',
          handler: () => {
            this.Api._GET('/session/cerrar').subscribe(data => console.log(data));
            try {
              FCMPlugin.unsubscribeFromTopic(`notificacion${localStorage.getItem('idUserMUEVE')}`);
            } catch (e) {
              console.log(e)
            }
            localStorage.clear();
            AppModule.My_Token = '';
            this._CloseGPS();
            this._DestroySocket();
            this.Complementos._MessageToast('¡Fin de la sesion!');
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ],
      cssClass: 'alertCustomCss' // <- added this
    });
    actionSheet.present();
  }

  /**-- Destruir SOCKET --**/
  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      console.log(' Disconect Socket', GpsProvider.EMIT);
      GpsProvider.EMIT.disconnect();
      console.log('1 Disconect Socket', GpsProvider.EMIT);
    }

  }

  /** CERRAR GPS **/
  _CloseGPS() {
    console.group('CERRANDO EL EMIT');
    console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
    console.groupEnd();
    // GpsProvider.Band_Conexion=false;
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }

  /*EDITAR DATOS DE PERFIL*/
  editPerfil(campo: string) {
    this.navCtrl.push(PerfileditPage, {data: DataglobalProvider.dataPerfil, value: campo})
  }

  /*ELIMINAR CUENTA ASOCIADA*/
  _DeleteAcount() {
    this.navCtrl.push(DeleteAcountPage)
  }

  Compartir(shortid) {
    console.log(shortid);
    this.socialSharing.share(`Mi pin de referido ${shortid}`, 'Mueve Logistica', '', 'https://play.google.com/store/apps/details?id=com.mueve_despachadorlite&hl=en')
      .then(() => {
      })
      .catch(err => {
        console.error('Error al compartir: ', err);
        this.Complementos._MessageToast('Error al compartir')
      })
  }
}
