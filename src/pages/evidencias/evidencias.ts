import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {Camera, CameraOptions} from "@ionic-native/camera";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {ApiProvider} from "../../providers/api/api";

@Component({
  selector: 'page-evidencias',
  templateUrl: 'evidencias.html',
  providers: [ApiProvider]
})
export class EvidenciasPage {

  image = [];
  Datos: any;
  success: boolean;
  DisableSave: boolean = false;
  private CallbackFunction: any;
  private BandCapture: boolean = false;

  public ImagesBtn: Object = {
    btn_subir: 'assets/imgs/icon/subir_fotografia.svg',
    btn_tomar: 'assets/imgs/icon/tomar_fotografia.svg'
  };

  public btn_subir = this.ImagesBtn['btn_subir'];

  public btn_tomar = this.ImagesBtn['btn_tomar'];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private camera: Camera,
              private Complementos: ComplementosviewsProvider) {
    this.CallbackFunction = this.navParams.get('callback');
  }

  ionViewDidLoad() {
    //--/ console.log('ionViewDidLoad EvidenciasPage');
  }

  ionViewCanLeave(): boolean {
    if (!this.BandCapture) {
      this.CallbackFunction(null)
    }
    return true;
  }

  public _IMG_DEFAULT_SLIDER(event, opc) {
    if (opc == 1) {
      event.target.src = 'assets/imgs/imagen_registro1.jpg';
    } else {
      event.target.src = 'assets/imgs/documento.jpg';
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  PHOTOLIBRARY() {
    this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);

  }

  CAMERA() {
    this.getPicture(this.camera.PictureSourceType.CAMERA);

  }

  /**---------------------------------------------
   *             CAPTURAR IMAGENES
   *--------------------------------------------**/
  getPicture(selectedSourceType) {
    let options: CameraOptions = {
      sourceType: selectedSourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 500,
      targetHeight: 500,
      quality: 70,
      saveToPhotoAlbum: true
    };

    this.camera.getPicture(options)
      .then(imagenData => {
        let image = `data:image/jpeg;base64,${imagenData}`;
        this.image.push(image);

      })
      .catch(error => {
        console.log(error);
      });
  }

  /**
   * -----------/ METODO PARA ELIMINAR LA IMAGEN EN EL ARRAY **/

  _DeleteImage(index) {
    this.image.splice(index, 1);
    this.Complementos._MessageToast('¡Imagen carga eliminada!');

  }

  /**                   FIN DEL METODO
   * --------------------------------------------------------**/
  /** -----------------------------------------------------------
   *                  FIN DE CAPTURA IMAGENES
   * -------------------------------------------------------- **/

  /**------------------------------------------
   *                                          @
   *       CONSULTAS API BACKEND              @
   *                                          @
   * ------------------------------------------
   */
  /**
   * -----------/ METODO PARA GUARDAR LOS DATOS DE LA EIDENCIAS **/

  GuardarImages() {
    if (this.image.length > 0) {
      this.BandCapture = true;
      console.log('Images', this.image)
      this.CallbackFunction(this.image).then(() => {
        this.navCtrl.pop();
      });
    } else {
      this.Complementos.showAlert("No hay imagenes", "", "Debes cargar al menos una imagen", ['Entiendo'], 'alertCustomCss')
    }

  }
}
