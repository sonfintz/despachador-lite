var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
let CalificartransportistaPage = class CalificartransportistaPage {
    constructor(navCtrl, fb, api, alertController, complemento, events, navParams) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.api = api;
        this.alertController = alertController;
        this.complemento = complemento;
        this.events = events;
        this.navParams = navParams;
        this.segment = 'excelente';
        console.log(this.navParams.get('data'));
        this.initForm();
    }
    get NotificationsSetting() {
        return DataglobalProvider.NotificacionesSettings;
    }
    RepuTrans(value) {
        this.segment = value;
        console.log(this.segment);
        if (this.segment === 'excelente') {
            this.Form.get('observacion').setValue('Excelente, todo estuvo bien.');
        }
        if (this.segment === 'bueno') {
            this.Form.get('observacion').setValue(null);
        }
        if (this.segment === 'aceptable') {
            this.Form.get('observacion').setValue(null);
        }
        if (this.segment === 'regular') {
            this.Form.get('observacion').setValue(null);
        }
    }
    initForm() {
        this.Form = this.fb.group({
            observacion: ['Excelente, todo estuvo bien.', Validators.required]
        });
    }
    _Calificar() {
        let Obj = {
            descripcion: this.Form.get('observacion').value,
            estado: this.segment.toUpperCase(),
            usuario_id: this.navParams.get('data').usuario_id,
            id_oferta: this.navParams.get('data').id_oferta ? this.navParams.get('data').id_oferta : this.navParams.get('data').ofertaid,
        };
        this.evento = this.CalificacionSegment(this.segment);
        console.log('EVENTO CONSEGUIDO', this.evento);
        const alert = this.alertController.create({
            title: `${this.evento.design_titulo_confirmacion}`,
            subTitle: `${this.evento.design_descripcion_confirmacion}`,
            message: `+ ${this.evento.design_puntos} MP`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: '¡Confirmar!',
                    cssClass: 'botton2',
                    handler: () => {
                        this.api._POST('/clasificacion', Obj).subscribe((data) => {
                            console.log(data);
                            this.complemento._MessageToast('Usuario Calificado');
                            this.navCtrl.pop();
                            this.events.publish('calificado', true);
                            console.log('Calificando transportista', Obj);
                        });
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
    CalificacionSegment(data) {
        let evento;
        switch (data) {
            case 'excelente':
                evento = this.NotificationsSetting.find(i => i.design_codigo === 75);
                break;
            case 'bueno':
                evento = this.NotificationsSetting.find(i => i.design_codigo === 76);
                break;
            case 'aceptable':
                evento = this.NotificationsSetting.find(i => i.design_codigo === 77);
                break;
            case 'regular':
                evento = this.NotificationsSetting.find(i => i.design_codigo === 78);
                break;
        }
        return evento;
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad CalificartransportistaPage');
    }
};
CalificartransportistaPage = __decorate([
    Component({
        selector: 'page-calificartransportista',
        templateUrl: 'calificartransportista.html',
    }),
    __metadata("design:paramtypes", [NavController,
        FormBuilder,
        ApiProvider,
        AlertController,
        ComplementosviewsProvider,
        Events,
        NavParams])
], CalificartransportistaPage);
export { CalificartransportistaPage };
//# sourceMappingURL=calificartransportista.js.map