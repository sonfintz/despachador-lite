import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {TransportistasListPage} from "../transportistas-list/transportistas-list";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";

@Component({
  selector: 'page-trailer',
  templateUrl: 'trailer.html',
})
export class TrailerPage {
  form: FormGroup;
  placa_repetida = false;
  /*data_combustible: any[];
  data_carroceria: any[];
  data_color: any[];
  data_marca: any[];
  data_unidadmedida: any[];*/
  data_tipotrailer: any[];
  private TRANSPORTISTA: any;

  get data_combustible() {
    return this.dataGlobal.Combustible();
  }

  get data_carroceria() {
    return this.dataGlobal.Carroceria()
  }

  get data_color() {
    return this.dataGlobal.Color()
  }

  get data_marca() {
    return this.dataGlobal.Marca()
  }

  get data_unidadmedida() {
    return this.dataGlobal.UnidadMedida()
  }

  constructor(public navCtrl: NavController,
              public fb: FormBuilder,
              public Api: ApiProvider,
              public events: Events,
              public dataGlobal: DataglobalProvider,
              private Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    this.TRANSPORTISTA = this.navParams.get('data');
    console.log('Data del transportista', this.TRANSPORTISTA);
  }

  ngOnInit() {
    // this.getHeads();
    this.loadData();
    this.initForm();
    // console.log(this.data)
    // this.setVehiculoId(this.data);
  }

  /*ionViewDidLoad() {
    console.log('ionViewDidLoad TrailerPage');
  }*/

  /*FORMULARIO*/
  initForm(): void {
    this.form = this.fb.group({
      placa: [null, [Validators.required]],
      id_carroceria: [null, [Validators.required]],
      id_tipotrailer: [null, [Validators.required]],
      id_marcatrailer: [null, [Validators.required]],
      id_color: [null, [Validators.required]],
      id_unidadmedida: [null, [Validators.required]],
      peso: [null, [Validators.required]],
      capacidad: [null, [Validators.required]],
      fabricacion: [null, [Validators.required]],
      id_combustible: [null, [Validators.required]],
      vehiculo_id: [this.TRANSPORTISTA.vehiculo_id],
      // vehiculo_id: [null],
    });
  }


  /*
  * CARGANDO TODA LA DATA PARA
  * LOS TYPEAHEAD
  * */
  loadData() {
    /* this.Api._GET('/color').subscribe(color => {
       this.data_color = color;
       // console.log(color)
     });
     this.Api._GET('/combustible').subscribe(combustible => {
       this.data_combustible = combustible;
       // console.log('combustible', combustible)

     });
     this.Api._GET('/carroceria').subscribe(carroceria => {
       this.data_carroceria = carroceria;
       // console.log('carroceria', carroceria)

     });
     this.Api._GET('/marca').subscribe(marca => {
       this.data_marca = marca;
       // console.log('MARCA TRAILER', marca)

     });
     this.Api._GET('/unidadmedida').subscribe(unidad => {
       this.data_unidadmedida = unidad;*/
    // console.log(unidad)

    // });
    this.Api._GET(`/tipotrailer/${this.TRANSPORTISTA.tipovehiculo_id}`).subscribe(trailer => {
      this.data_tipotrailer = trailer;
      // console.log('trailer', trailer)

    });
  }

  /* METODO GUARDAR */
  saveData() {
    console.log(this.form.value);
    this.Api._POST('/trailer', this.form.value).subscribe(
      () => {
        this.navCtrl.setRoot(TransportistasListPage);
        this.events.publish('list-transport');
        this.Complementos._MessageToast(`Trailer creado exitosamente`);
      },
      (e) => {
        this.Complementos._MessageToast(`${e.error.mensaje}`);
      })
  }

  /*TYPEAHEAD*/
  _Carroceria(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_carroceria').setValue(datos.data);
    } else {
      this.form.get('id_carroceria').setValue('');
    }
  }

  _MarcaTrailer(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_marcatrailer').setValue(datos.data);
    } else {
      this.form.get('id_marcatrailer').setValue('');
    }
  }

  _ColorTrailer(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_color').setValue(datos.data);
    } else {
      this.form.get('id_color').setValue('');
    }
  }

  /*COMPROBAR CAMPO PLACA*/
  comprobarCampo(campo: string): void {
    switch (campo) {
      case 'placa':
        const placa = this.form.value.placa;
        if (placa !== null) {
          this.Api._GET(`/trailer/placa/${placa}`).subscribe(
            (item) => {
              console.log(item);
              if (item.existe === 'SI') {
                console.log('Aqui el login igual');
                this.Complementos._MessageToast('Placa existente');
                this.placa_repetida = !this.placa_repetida;
                this.form.get('placa').reset();
              } else {
                console.log('no iguales');
                this.placa_repetida = false;
              }
            }
          );
        } else {
          this.Complementos._MessageToast('Debes ingresar una placa');
        }
        break;
    }
  }

}
