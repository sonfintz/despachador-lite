export interface Vehiculo {
  id: number;
  descripcion: string;
  codigo: number;
  orden: number;
  capacidad: number;
}
