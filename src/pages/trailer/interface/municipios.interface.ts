export interface Municipios {
  id_municipio: number;
  id_departamento: number;
  departamento_nombre: string;
  latitud: string;
  longitud: string;
  municipio_nombre: string;
  municipio_codigo: string;
  municipio_descripcion: any;
}
