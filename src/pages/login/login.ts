import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';

import {Events, MenuController, NavController, ToastController} from 'ionic-angular';

import {UserData} from '../../providers/user-data';
import {SignupPage} from '../signup/signup';
import {ApiProvider} from "../../providers/api/api";
import {AppModule} from "../../app/app.module";
import {GpsProvider} from "../../providers/gps/gps";
import {UserOptions} from "../../interfaces/user-options";
import {TutorialPage} from "../tutorial/tutorial";
import {ForgetAcountPage} from "../forget-acount/forget-acount";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

declare var sha256;

@Component({
  selector: 'login-page',
  templateUrl: 'login.html',
  providers: [ApiProvider, GpsProvider]
})
export class LoginPage {
  login: UserOptions = {username: '', password: ''};
  submitted = false;
  Error: string;
  private Logged: boolean = false;
  passwordType: string = 'password';
  passwordShown: boolean = false;

  constructor(
    public navCtrl: NavController,
    public userData: UserData,
    private toast: ToastController,
    private Api: ApiProvider,
    private menuCtrl: MenuController,
    private Gps: GpsProvider,
    public events: Events,
    public Complementos: ComplementosviewsProvider,
  ) {
  }

  public togglePassword() {
    this.passwordShown ? this.passwordType = 'password' : this.passwordType = 'text';
    this.passwordShown = !this.passwordShown;
  }

  forgetPassword() {
    this.navCtrl.push(ForgetAcountPage)
  }

  rootPages() {
    console.log('cliqueando');
    this.navCtrl.setRoot(TutorialPage);
  }

  ionViewWillEnter() {
    this.menuCtrl.close('loggedOutMenu');
    this.menuCtrl.enable(false, 'loggedOutMenu');
    this.menuCtrl.swipeEnable(false, 'leftMenu');

    /* this.menuCtrl.close('rightMenu');
     this.menuCtrl.swipeEnable(false,'loggedOutMenu');
     this.menuCtrl.swipeEnable(false,'rightMenu');*/
  }

  ionViewWillLeave() {
    this.menuCtrl.enable(true, 'loggedOutMenu');
    this.menuCtrl.swipeEnable(true, 'loggedOutMenu');
    this.menuCtrl.swipeEnable(true, 'rightMenu');
  }

  onLogin(form: NgForm) {
    this.Complementos._PresentLoading();
    this.submitted = true;
    this.Error = '';
    if (form.valid) {
      console.log('this.login.username', this.login.username, this.login.password, sha256);
      this.Api._Login(sha256(this.login.username.toUpperCase()), sha256(this.login.password))
        .subscribe(
          (data) => {
            console.group('API SESION');
            console.log(data);
            console.groupEnd();
            if (data.body.error) {
              this.Error = data.body.mensaje;
            } else {
              this.Logged = true;
              window.localStorage.setItem("tipo", data.body.tipo);
              window.localStorage.setItem("token", data.body.usuario_token);
              window.localStorage.setItem("idUserMUEVE", data.body.usuario_id);
              window.localStorage.setItem("nameUserMUEVE", data.body.persona_nombre);
              window.localStorage.setItem("empresa", data.body.empresa_nombre);
              window.localStorage.setItem("idMunicUserMUEVE", data.body.usuario_idmunicipio);
              window.localStorage.setItem("pin", data.body.shortid);
              window.localStorage.setItem("perfil", data.body.perfil_descripcion);
              AppModule.My_Token = window.localStorage.getItem("token");
              window.localStorage.setItem("canales", JSON.stringify(data.body.canales));
              window.localStorage.setItem("puntos", JSON.stringify(data.body.puntos));
              window.localStorage.setItem("tiempo", JSON.stringify(data.body.tiempo));
              window.localStorage.setItem("despachos", JSON.stringify(data.body.despachos));
              window.localStorage.setItem("categoria", data.body.categoria);
              window.localStorage.setItem("verificado", data.body.verificado);

              this.events.publish('user:sessions', true);
              this.events.publish('user:NotificationsSQL', true);
            }
          }, (e) => {
            this.Complementos._DismissPresentLoading();
            if (e.status !== 0) {
              console.log('Error login', e.error.mensaje);
              const toast = this.toast.create({
                message: `${e.error.mensaje}`,
                duration: 2000,
                position: 'bottom',
              });
              toast.present();
            } else {
              const toast = this.toast.create({
                message: `No es posible conectarse con Mueve Logística en este momento, intenta de nuevo más tarde.`,
                duration: 2000,
                position: 'bottom',
              });
              toast.present();
            }
          },
          () => {
            this.Complementos._DismissPresentLoading();
            console.log('Completado');
            if (this.Logged) {
              this.events.publish('sesionuser');
              //  this.dataglobal._Perfil();
              this.Gps._GPS();
            }
          });
    }
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }

  _VerificaLogin() {
    if (window.localStorage.getItem("idUserMUEVE") && localStorage.getItem("idUserMUEVE") != '') {
      console.log('PASO POR AQUI');
      this.events.publish('user:NotificationsSQL', true);
      this.Gps._GPS();
    }
  }
}
