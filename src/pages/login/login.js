var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, MenuController, NavController, ToastController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { SignupPage } from '../signup/signup';
import { ApiProvider } from "../../providers/api/api";
import { AppModule } from "../../app/app.module";
import { GpsProvider } from "../../providers/gps/gps";
import { TutorialPage } from "../tutorial/tutorial";
import { ForgetAcountPage } from "../forget-acount/forget-acount";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let LoginPage = class LoginPage {
    constructor(navCtrl, userData, toast, Api, menuCtrl, Gps, events, Complementos) {
        this.navCtrl = navCtrl;
        this.userData = userData;
        this.toast = toast;
        this.Api = Api;
        this.menuCtrl = menuCtrl;
        this.Gps = Gps;
        this.events = events;
        this.Complementos = Complementos;
        this.login = { username: '', password: '' };
        this.submitted = false;
        this.Logged = false;
        this.passwordType = 'password';
        this.passwordShown = false;
    }
    togglePassword() {
        this.passwordShown ? this.passwordType = 'password' : this.passwordType = 'text';
        this.passwordShown = !this.passwordShown;
    }
    forgetPassword() {
        this.navCtrl.push(ForgetAcountPage);
    }
    rootPages() {
        console.log('cliqueando');
        this.navCtrl.setRoot(TutorialPage);
    }
    ionViewWillEnter() {
        this.menuCtrl.close('loggedOutMenu');
        this.menuCtrl.enable(false, 'loggedOutMenu');
        this.menuCtrl.swipeEnable(false, 'leftMenu');
        /* this.menuCtrl.close('rightMenu');
         this.menuCtrl.swipeEnable(false,'loggedOutMenu');
         this.menuCtrl.swipeEnable(false,'rightMenu');*/
    }
    ionViewWillLeave() {
        this.menuCtrl.enable(true, 'loggedOutMenu');
        this.menuCtrl.swipeEnable(true, 'loggedOutMenu');
        this.menuCtrl.swipeEnable(true, 'rightMenu');
    }
    onLogin(form) {
        this.Complementos._PresentLoading();
        this.submitted = true;
        this.Error = '';
        if (form.valid) {
            console.log('this.login.username', this.login.username, this.login.password, sha256);
            this.Api._Login(sha256(this.login.username.toUpperCase()), sha256(this.login.password))
                .subscribe((data) => {
                console.group('API SESION');
                console.log(data);
                console.groupEnd();
                if (data.body.error) {
                    this.Error = data.body.mensaje;
                }
                else {
                    this.Logged = true;
                    window.localStorage.setItem("tipo", data.body.tipo);
                    window.localStorage.setItem("token", data.body.usuario_token);
                    window.localStorage.setItem("idUserMUEVE", data.body.usuario_id);
                    window.localStorage.setItem("nameUserMUEVE", data.body.persona_nombre);
                    window.localStorage.setItem("empresa", data.body.empresa_nombre);
                    window.localStorage.setItem("idMunicUserMUEVE", data.body.usuario_idmunicipio);
                    window.localStorage.setItem("pin", data.body.shortid);
                    window.localStorage.setItem("perfil", data.body.perfil_descripcion);
                    AppModule.My_Token = window.localStorage.getItem("token");
                    window.localStorage.setItem("canales", JSON.stringify(data.body.canales));
                    window.localStorage.setItem("puntos", JSON.stringify(data.body.puntos));
                    window.localStorage.setItem("tiempo", JSON.stringify(data.body.tiempo));
                    window.localStorage.setItem("despachos", JSON.stringify(data.body.despachos));
                    window.localStorage.setItem("categoria", data.body.categoria);
                    window.localStorage.setItem("verificado", data.body.verificado);
                    this.events.publish('user:sessions', true);
                    this.events.publish('user:NotificationsSQL', true);
                }
            }, (e) => {
                this.Complementos._DismissPresentLoading();
                if (e.status !== 0) {
                    console.log('Error login', e.error.mensaje);
                    const toast = this.toast.create({
                        message: `${e.error.mensaje}`,
                        duration: 2000,
                        position: 'bottom',
                    });
                    toast.present();
                }
                else {
                    const toast = this.toast.create({
                        message: `No es posible conectarse con Mueve Logística en este momento, intenta de nuevo más tarde.`,
                        duration: 2000,
                        position: 'bottom',
                    });
                    toast.present();
                }
            }, () => {
                this.Complementos._DismissPresentLoading();
                console.log('Completado');
                if (this.Logged) {
                    this.events.publish('sesionuser');
                    //  this.dataglobal._Perfil();
                    this.Gps._GPS();
                }
            });
        }
    }
    onSignup() {
        this.navCtrl.push(SignupPage);
    }
    _VerificaLogin() {
        if (window.localStorage.getItem("idUserMUEVE") && localStorage.getItem("idUserMUEVE") != '') {
            console.log('PASO POR AQUI');
            this.events.publish('user:NotificationsSQL', true);
            this.Gps._GPS();
        }
    }
};
LoginPage = __decorate([
    Component({
        selector: 'login-page',
        templateUrl: 'login.html',
        providers: [ApiProvider, GpsProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        UserData,
        ToastController,
        ApiProvider,
        MenuController,
        GpsProvider,
        Events,
        ComplementosviewsProvider])
], LoginPage);
export { LoginPage };
//# sourceMappingURL=login.js.map