var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams, PopoverController, ToastController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';
let GenerateCumplidoManifiestoPage = class GenerateCumplidoManifiestoPage {
    constructor(navCtrl, navParams, popoverCtrl, events, fb, Api, Complementos, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.fb = fb;
        this.Api = Api;
        this.Complementos = Complementos;
        this.toastCtrl = toastCtrl;
        this.solicitud = this.navParams.get('data');
    }
    getCurrentTime() {
        return moment().format('YYYY-MM-DD');
    }
    ngOnInit() {
        this.initForm();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad GenerateCumplidoManifiestoPage');
    }
    initForm() {
        this.form = this.fb.group({
            fecha: [null, [Validators.required]],
            adicionalcargue: [0, [Validators.required]],
            id_manifiesto: [this.solicitud.id_manifiesto],
            descuento: [0, [Validators.required]],
            adicionalanticipo: [0, [Validators.required]],
        });
    }
    saveData() {
        this.Complementos._MessageToast('Generando Cumplido Manifiesto');
        this.Api._POST(`/orden/cumplirmanifiesto`, this.form.value).subscribe((data) => {
            console.log(data);
            this.Complementos._MessageToast('Cumplido Inicial Generado');
            this.navCtrl.pop();
            this.events.publish('despachado');
        }, (e) => this.Complementos._MessageToast(`${e.error.mensaje}`));
    }
};
GenerateCumplidoManifiestoPage = __decorate([
    Component({
        selector: 'page-generate-cumplido-manifiesto',
        templateUrl: 'generate-cumplido-manifiesto.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        PopoverController,
        Events,
        FormBuilder,
        ApiProvider,
        ComplementosviewsProvider,
        ToastController])
], GenerateCumplidoManifiestoPage);
export { GenerateCumplidoManifiestoPage };
//# sourceMappingURL=generate-cumplido-manifiesto.js.map