import {Component} from '@angular/core';
import {Events, NavController, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';

@Component({
  selector: 'page-generate-cumplido-manifiesto',
  templateUrl: 'generate-cumplido-manifiesto.html',
})
export class GenerateCumplidoManifiestoPage {
  public solicitud: any;
  public form: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              public events: Events,
              public fb: FormBuilder,
              private Api: ApiProvider,
              private Complementos: ComplementosviewsProvider,
              public toastCtrl: ToastController) {
    this.solicitud = this.navParams.get('data');
  }

  getCurrentTime() {
    return moment().format('YYYY-MM-DD')
  }

  ngOnInit() {
    this.initForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenerateCumplidoManifiestoPage');
  }

  initForm() {
    this.form = this.fb.group({
      fecha: [null, [Validators.required]],
      adicionalcargue: [0, [Validators.required]],
      id_manifiesto: [this.solicitud.id_manifiesto],
      descuento: [0, [Validators.required]],
      adicionalanticipo: [0, [Validators.required]],
    })
  }

  saveData() {
    this.Complementos._MessageToast('Generando Cumplido Manifiesto');
    this.Api._POST(`/orden/cumplirmanifiesto`, this.form.value).subscribe(
      (data) => {
        console.log(data);
        this.Complementos._MessageToast('Cumplido Inicial Generado');
        this.navCtrl.pop();
        this.events.publish('despachado');
      },
      (e) => this.Complementos._MessageToast(`${e.error.mensaje}`));
  }
}
