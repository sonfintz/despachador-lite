var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { OrdenCargaPage } from "../orden-carga/orden-carga";
let AcceptPopoverPage = class AcceptPopoverPage {
    constructor(navCtrl, api, events, alertController, navParams) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.events = events;
        this.alertController = alertController;
        this.navParams = navParams;
        this.TRANSPORTISTA = this.navParams.get('data');
        console.log('ASIGNADOS', this.TRANSPORTISTA);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad AcceptPopoverPage');
    }
    _Aceptar() {
        let solicitudes_id = this.TRANSPORTISTA.solicitudes_id;
        let usuario_id = this.TRANSPORTISTA.usuario_id;
        console.log('Aceptar transportista', solicitudes_id, usuario_id);
        const alert = this.alertController.create({
            title: '¿Aceptar transportista?',
            message: '<strong>Confirme su opcion</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah', blah);
                    }
                }, {
                    text: 'Aceptar',
                    handler: () => {
                        let Obj = {
                            usuario_id: usuario_id,
                            despacho_estado: "DESPACHADO"
                        };
                        console.log('SOLICITUD', solicitudes_id, 'USUARIO', usuario_id, Obj);
                        this.api._PUT(`/despachos/${solicitudes_id}`, Obj).subscribe((data) => {
                            console.log(data);
                            this.events.publish('despachado');
                            this.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
        // return this.presentAlertConfirm(solicitudes_id, usuario_id);
    }
    OrdenCarga(id, data) {
        console.log('ID_SOLICITUD', id, 'DATA', data);
        this.navCtrl.push(OrdenCargaPage, { data: this.TRANSPORTISTA });
        /*const Obj = {
          solicitudes_id: id,
          solicitudes_fechacarga: data.solicitudes_fechacarga,
          solicitudes_fechaentrega: data.solicitudes_fechaentrega,
          producto_id: data.producto_id,
          unidadmedida_id: data.unidadmedida_id,
          sede_id: data.sede_id
        };*/
    }
};
AcceptPopoverPage = __decorate([
    Component({
        selector: 'page-accept-popover',
        templateUrl: 'accept-popover.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        Events,
        AlertController,
        NavParams])
], AcceptPopoverPage);
export { AcceptPopoverPage };
//# sourceMappingURL=accept-popover.js.map