import {Component, ViewChild} from '@angular/core';

import {Events, MenuController, NavController, Slides} from 'ionic-angular';

import {Storage} from '@ionic/storage';
import {LoginPage} from "../login/login";
import {SignupPage} from "../signup/signup";
import {GpsProvider} from "../../providers/gps/gps";

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
  providers: [GpsProvider]
})

export class TutorialPage {
  showSkip = true;
  TextInfo = Array();
  indexText: number = 0;
  printText: String = ``;
  @ViewChild('slides') slides: Slides;

  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    public storage: Storage,
    public Gps: GpsProvider,
    public events: Events,
  ) {
    this.TextInfo[0] = `La manera más fácil de encontrar transportistas, ahorrar tiempo y lo mejor, es gratis.`;
    this.TextInfo[1] = `Ahorrar tiempo, y lo mejor, es totalmente gratis.`;
    this.TextInfo[2] = `Pensamos en ti, redime tus Mueve Puntos`;
    this.printText = this.TextInfo[0];
    this._VerificaLogin();
  }

  startApp() {
    this.navCtrl.push(LoginPage).then(() => {
      this.storage.set('hasSeenTutorial', 'true');
    })
  }

  onSlideChangeStart(slider: Slides) {
    this.showSkip = !slider.isEnd();
  }

  ctrl() {
    this.navCtrl.push(SignupPage)
  }

  ctrl2() {
    this.navCtrl.push(LoginPage)
  }

  /*ionViewWillEnter() {
    this.slides.update();
  }*/

  ionViewDidEnter() {
    this.menu.enable(false);
    this.menu.close('leftMenu');
    this.menu.close('rightMenu');
    this.menu.swipeEnable(false, 'leftMenu');
    this.menu.swipeEnable(false, 'rightMenu');
  }

  ionViewDidLeave() {
    this.menu.enable(true);
  }

  _VerificaLogin() {
    if (window.localStorage.getItem("idUserMUEVE")) {
      this.events.publish("user:NotificationsSQL", true);
      console.log('PASO POR AQUI _VerificaLogin()');
      this.events.publish("user:NotificationsSQL", true);
      this.events.publish('user:sessions', true);
      this.Gps._GPS();
    }
  }

  swipe(event) {
    console.log(' swipe: ', JSON.stringify(event), event.offsetDirection, '| ', this.indexText)
    if (event.offsetDirection === 2) {
      (this.indexText < this.TextInfo.length) ? this.indexText++ : 0;

    } else if (event.offsetDirection === 4) {
      (this.indexText >= 0) ? this.indexText-- : 0;

    }

    if (this.indexText >= 0 && this.indexText < this.TextInfo.length) {
      this.printText = this.TextInfo[this.indexText];
    }
  }

}
