var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, NavController, Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from "../login/login";
import { SignupPage } from "../signup/signup";
import { GpsProvider } from "../../providers/gps/gps";
let TutorialPage = class TutorialPage {
    constructor(navCtrl, menu, storage, Gps, events) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.storage = storage;
        this.Gps = Gps;
        this.events = events;
        this.showSkip = true;
        this.TextInfo = Array();
        this.indexText = 0;
        this.printText = ``;
        this.TextInfo[0] = `La manera más fácil de encontrar transportistas, ahorrar tiempo y lo mejor, es gratis.`;
        this.TextInfo[1] = `Ahorrar tiempo, y lo mejor, es totalmente gratis.`;
        this.TextInfo[2] = `Pensamos en ti, redime tus Mueve Puntos`;
        this.printText = this.TextInfo[0];
        this._VerificaLogin();
    }
    startApp() {
        this.navCtrl.push(LoginPage).then(() => {
            this.storage.set('hasSeenTutorial', 'true');
        });
    }
    onSlideChangeStart(slider) {
        this.showSkip = !slider.isEnd();
    }
    ctrl() {
        this.navCtrl.push(SignupPage);
    }
    ctrl2() {
        this.navCtrl.push(LoginPage);
    }
    /*ionViewWillEnter() {
      this.slides.update();
    }*/
    ionViewDidEnter() {
        this.menu.enable(false);
        this.menu.close('leftMenu');
        this.menu.close('rightMenu');
        this.menu.swipeEnable(false, 'leftMenu');
        this.menu.swipeEnable(false, 'rightMenu');
    }
    ionViewDidLeave() {
        this.menu.enable(true);
    }
    _VerificaLogin() {
        if (window.localStorage.getItem("idUserMUEVE")) {
            this.events.publish("user:NotificationsSQL", true);
            console.log('PASO POR AQUI _VerificaLogin()');
            this.events.publish("user:NotificationsSQL", true);
            this.events.publish('user:sessions', true);
            this.Gps._GPS();
        }
    }
    swipe(event) {
        console.log(' swipe: ', JSON.stringify(event), event.offsetDirection, '| ', this.indexText);
        if (event.offsetDirection === 2) {
            (this.indexText < this.TextInfo.length) ? this.indexText++ : 0;
        }
        else if (event.offsetDirection === 4) {
            (this.indexText >= 0) ? this.indexText-- : 0;
        }
        if (this.indexText >= 0 && this.indexText < this.TextInfo.length) {
            this.printText = this.TextInfo[this.indexText];
        }
    }
};
__decorate([
    ViewChild('slides'),
    __metadata("design:type", Slides)
], TutorialPage.prototype, "slides", void 0);
TutorialPage = __decorate([
    Component({
        selector: 'page-tutorial',
        templateUrl: 'tutorial.html',
        providers: [GpsProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        MenuController,
        Storage,
        GpsProvider,
        Events])
], TutorialPage);
export { TutorialPage };
//# sourceMappingURL=tutorial.js.map