import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-perfiledit',
  templateUrl: 'perfiledit.html',
})
export class PerfileditPage {
  public form: FormGroup;
  public PERFIL: any;
  public Campo: any;
  passwordType: string = 'password';
  passwordType1: string = 'password';
  passwordShown: boolean = false;
  passwordShown1: boolean = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public Api: ApiProvider,
              public events: Events,
              private Complementos: ComplementosviewsProvider,
              public fb: FormBuilder) {
    console.log('el campo clickeado', this.navParams.get('value'));
    this.Campo = this.navParams.get('value');
    this.PERFIL = this.navParams.get('data');
    console.log('Data del transportista', this.PERFIL);
    this.initForm();
  }

  Title(value) {
    if (value == 'personanombre') {
      return 'Nombre completo'
    }
    if (value == 'login') {
      return 'Nombre de usuario'
    }
    if (value == 'password') {
      return 'Contraseña'
    }
    if (value == 'personaidentificacion') {
      return 'Identificacion'
    }
    if (value == 'personaemail') {
      return 'Correo electronico'
    }
    if (value == 'personatelefono') {
      return 'Telefono celular'
    }
    if (value == 'persona_telefono2') {
      return 'Telefono celular (2)'
    }
  }

  public togglePassword() {
    this.passwordShown ? this.passwordType = 'password' : this.passwordType = 'text';
    this.passwordShown = !this.passwordShown;
  }

  public togglePassword1() {
    this.passwordShown1 ? this.passwordType1 = 'password' : this.passwordType1 = 'text';
    this.passwordShown1 = !this.passwordShown1;
  }

  initForm() {
    // const clave = this.PERFIL.usuario_password;
    this.form = this.fb.group({
      personanombre: [this.PERFIL.persona_nombre, Validators.required],
      login: [this.PERFIL.usuario_login, Validators.required],
      password: [null],
      confirmpassword: [null],
      personaidentificacion: [this.PERFIL.persona_identificacion],
      personaemail: [this.PERFIL.persona_email, Validators.required],
      personatelefono: [this.PERFIL.persona_telefono, Validators.required],
      personatelefono2: [this.PERFIL.persona_telefono2],
    });
    this.form.get('confirmpassword').valueChanges.subscribe((data) => {
      if (data !== this.form.get('password').value) {
        console.log('no coincide', data);
        this.form.get('confirmpassword').setErrors({invalidNet: `No coincide`});
      }
    });
    this.form.get('password').valueChanges.subscribe((data) => {
      console.log(data);
      this.form.get('confirmpassword').reset()
    });
    this.form.get('personatelefono2').valueChanges.subscribe((data) => {
      if (data == this.form.get('personatelefono').value) {
        this.form.get('personatelefono2').setErrors({invalidNet: `telefono duplicado`});
        this.form.get('personatelefono2').reset();
        this.Complementos._MessageToast('Teléfono duplicado')
      }
    })
  }

  _ClearInput(value) {
    console.log('reseteando formulario', value);
    this.form.get(`${value}`).reset();
  }

  savePerfil() {
    let form = this.form.value;
    let Obj = {
      personanombre: form.personanombre,
      login: form.login,
      password: form.password,
      personaidentificacion: form.personaidentificacion,
      personaemail: form.personaemail,
      personatelefono: form.personatelefono,
      personatelefono2: form.personatelefono2,
    };
    console.log('actualizando perfil', this.form.value);
    this.Api._PUT(`/despachador/${localStorage.getItem('idUserMUEVE')}`, Obj).subscribe(
      () => {
        this.navCtrl.pop();
        this.events.publish('user:sessions');
        this.Complementos._MessageToast('Perfil actualizado', 800);
      }, (err) => {
        console.log(err);
        this.Complementos._MessageToast(`${err.error.mensaje}`, 1500);
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfileditPage');
  }
}
