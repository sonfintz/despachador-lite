import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

@Component({
  selector: 'page-cuentasasociadas',
  templateUrl: 'cuentasasociadas.html',
})
export class CuentasasociadasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CuentasasociadasPage');
  }

}
