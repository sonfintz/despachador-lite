var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let ReporttransportistaPage = class ReporttransportistaPage {
    constructor(navCtrl, fb, api, alertController, complemento, Api, navParams) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.api = api;
        this.alertController = alertController;
        this.complemento = complemento;
        this.Api = Api;
        this.navParams = navParams;
        console.log(this.navParams.get('data'));
        this.loadData();
        this.initForm();
    }
    initForm() {
        this.Form = this.fb.group({
            observacion: [null, Validators.required],
            id_tema: [null, Validators.required]
        });
    }
    loadData() {
        this.Api._GET('/temas').subscribe((tema) => {
            console.log(tema);
            tema = tema.filter(i => i.tipo === 'REPORTE DE USUARIO');
            this.temas = tema;
        });
    }
    confirmReport() {
        let Obj = {
            denuncia: this.Form.value.observacion,
            id_tema: this.Form.value.id_tema,
            id_denunciado: this.navParams.get('data').usuario_id
        };
        const alert = this.alertController.create({
            title: '¿Reportar?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: '¡Confirmar!',
                    cssClass: 'botton2',
                    handler: () => {
                        console.log('Objeto', Obj);
                        this.api._POST('/despachador/denuncias', Obj).subscribe((data) => {
                            console.log(data);
                            this.complemento._MessageToast('Usuario denunciado');
                            this.Form.reset();
                            this.navCtrl.pop();
                            this.navCtrl.pop();
                        });
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ReporttransportistaPage');
    }
};
ReporttransportistaPage = __decorate([
    Component({
        selector: 'page-reporttransportista',
        templateUrl: 'reporttransportista.html',
    }),
    __metadata("design:paramtypes", [NavController,
        FormBuilder,
        ApiProvider,
        AlertController,
        ComplementosviewsProvider,
        ApiProvider,
        NavParams])
], ReporttransportistaPage);
export { ReporttransportistaPage };
//# sourceMappingURL=reporttransportista.js.map