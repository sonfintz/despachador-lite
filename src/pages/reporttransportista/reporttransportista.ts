import {Component} from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-reporttransportista',
  templateUrl: 'reporttransportista.html',
})
export class ReporttransportistaPage {
  public Form: FormGroup;
  private temas: any;

  constructor(public navCtrl: NavController,
              public fb: FormBuilder,
              public api: ApiProvider,
              public alertController: AlertController,
              public complemento: ComplementosviewsProvider,
              private  Api: ApiProvider,
              public navParams: NavParams) {
    console.log(this.navParams.get('data'));
    this.loadData();
    this.initForm();
  }

  initForm() {
    this.Form = this.fb.group({
      observacion: [null, Validators.required],
      id_tema: [null, Validators.required]
    })
  }

  loadData() {
    this.Api._GET('/temas').subscribe((tema) => {
      console.log(tema);
      tema = tema.filter(i => i.tipo === 'REPORTE DE USUARIO');
      this.temas = tema;
    })
  }

  confirmReport() {
    let Obj = {
      denuncia: this.Form.value.observacion,
      id_tema: this.Form.value.id_tema,
      id_denunciado: this.navParams.get('data').usuario_id
    };
    const alert = this.alertController.create({
      title: '¿Reportar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'botton1',
        }, {
          text: '¡Confirmar!',
          cssClass: 'botton2',
          handler: () => {
            console.log('Objeto', Obj);
            this.api._POST('/despachador/denuncias', Obj).subscribe((data) => {
              console.log(data);
              this.complemento._MessageToast('Usuario denunciado');
              this.Form.reset();
              this.navCtrl.pop();
              this.navCtrl.pop()
            });
          }
        }
      ],
      cssClass: 'alertCustomCss' // <- added this
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReporttransportistaPage');
  }

}
