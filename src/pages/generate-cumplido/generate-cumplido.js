var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, NavController, NavParams, PopoverController, ToastController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { GenerateCumplidoRemesaPage } from "../generate-cumplido-remesa/generate-cumplido-remesa";
import { GenerateCumplidoManifiestoPage } from "../generate-cumplido-manifiesto/generate-cumplido-manifiesto";
import { ReporteMrPage } from "../reporte-mr/reporte-mr";
let GenerateCumplidoPage = class GenerateCumplidoPage {
    constructor(navCtrl, navParams, popoverCtrl, events, alertController, Api, Complementos, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.alertController = alertController;
        this.Api = Api;
        this.Complementos = Complementos;
        this.toastCtrl = toastCtrl;
        this.solicitud = this.navParams.get('data');
        console.log('la data de la solicitud', this.solicitud);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad GenerateCumplidoPage');
    }
    cumplidoInicial() {
        const alert = this.alertController.create({
            title: '¿Generar cumplido inicial?',
            message: '<strong>Confirme su opcion</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah', blah);
                    }
                }, {
                    text: 'Aceptar',
                    handler: () => {
                        this.Complementos._MessageToast('Generando Cumplido Inicial');
                        this.Api._GET(`/orden/cumplidoinicial/${this.solicitud.id_remesa}`).subscribe((data) => {
                            console.log(data);
                            this.Complementos._MessageToast('Cumplido Inicial Generado');
                            this.navCtrl.pop();
                        }, (e) => this.Complementos._MessageToast(`${e.error}`));
                    }
                }
            ]
        });
        alert.present();
    }
    cumplidoRemesa() {
        this.navCtrl.push(GenerateCumplidoRemesaPage, { data: this.solicitud });
    }
    viewORM() {
        this.navCtrl.push(ReporteMrPage, { data: this.solicitud });
    }
    cumplidoManifiesto() {
        this.navCtrl.push(GenerateCumplidoManifiestoPage, { data: this.solicitud });
    }
};
GenerateCumplidoPage = __decorate([
    Component({
        selector: 'page-generate-cumplido',
        templateUrl: 'generate-cumplido.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        PopoverController,
        Events,
        AlertController,
        ApiProvider,
        ComplementosviewsProvider,
        ToastController])
], GenerateCumplidoPage);
export { GenerateCumplidoPage };
//# sourceMappingURL=generate-cumplido.js.map