import {Component} from '@angular/core';
import {AlertController, Events, NavController, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {GenerateCumplidoRemesaPage} from "../generate-cumplido-remesa/generate-cumplido-remesa";
import {GenerateCumplidoManifiestoPage} from "../generate-cumplido-manifiesto/generate-cumplido-manifiesto";
import {ReporteMrPage} from "../reporte-mr/reporte-mr";

@Component({
  selector: 'page-generate-cumplido',
  templateUrl: 'generate-cumplido.html',
})
export class GenerateCumplidoPage {
  public solicitud: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              public events: Events,
              public alertController: AlertController,
              private Api: ApiProvider,
              private Complementos: ComplementosviewsProvider,
              public toastCtrl: ToastController) {
    this.solicitud = this.navParams.get('data');
    console.log('la data de la solicitud', this.solicitud);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenerateCumplidoPage');
  }

  cumplidoInicial() {
    const alert = this.alertController.create({
      title: '¿Generar cumplido inicial?',
      message: '<strong>Confirme su opcion</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah', blah);
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.Complementos._MessageToast('Generando Cumplido Inicial');
            this.Api._GET(`/orden/cumplidoinicial/${this.solicitud.id_remesa}`).subscribe(
              (data) => {
                console.log(data);
                this.Complementos._MessageToast('Cumplido Inicial Generado');
                this.navCtrl.pop();
              },
              (e) => this.Complementos._MessageToast(`${e.error}`));
          }
        }
      ]
    });
    alert.present();
  }

  cumplidoRemesa() {
    this.navCtrl.push(GenerateCumplidoRemesaPage, {data: this.solicitud})
  }

  viewORM() {
    this.navCtrl.push(ReporteMrPage, {data: this.solicitud})
  }

  cumplidoManifiesto() {
    this.navCtrl.push(GenerateCumplidoManifiestoPage, {data: this.solicitud})
  }

}
