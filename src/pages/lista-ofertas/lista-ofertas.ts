import {Component} from '@angular/core';
import {
  ActionSheet,
  AlertController,
  App,
  Config,
  Events,
  NavController,
  PopoverController,
  ViewController
} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import * as moment from 'moment';
import {CreateOfertPage} from "../create-ofert/create-ofert";
import {ActivityPage} from "../activity/activity";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {GpsProvider} from "../../providers/gps/gps";
import {OfertanewdetallePage} from "../ofertanewdetalle/ofertanewdetalle";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {Descompilar} from "../../providers/descopilar/descompilar";

@Component({
  selector: 'page-speaker-list',
  templateUrl: 'lista-ofertas.html',
  providers: [ApiProvider]
})
export class ListaOfertasPage {

  actionSheet: ActionSheet;
  speakers: any[] = [];
  queryText = '';
  public LIST_OFERT: any;
  public LIST_OFERT_2: any;
  public LIST_OFERT_3: Array<any> = Array();
  public items: Array<any> = Array();
  public segment2 = 'publicadas';
  public SOLICITUD: any;
  public Perfil: any;
  private ErrorConexion: boolean = false;
  private IndexArrayOrder: number;
  private ListOferta: any;
  private ofertasocket = false;

  // private paginaActual = 1;

  get Notificacion() {
    return DataglobalProvider.notificacion
  }

  get BadConection() {
    return GpsProvider.Band_Conexion;
  }

  get Verificado() {
    return DataglobalProvider.Verificado;
  }

  constructor(
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public config: Config,
    public alertController: AlertController,
    private Api: ApiProvider,
    public app: App,
    public complements: ComplementosviewsProvider,
    public viewCtrl: ViewController,
    public events: Events,
    public descompilar: Descompilar,
  ) {
    this.events.subscribe('reloadofertas', (data) => {
      if (this.ofertasocket === false) {
        console.log('llegó por socket', data);
        this.ofertasocket = true;
        this.getOfertaPaginada(this.segment2)
      }
    });
    this.events.subscribe('cuenta-activada', () => {
      this._VerificarUsuario();
    });
    this.events.publish('tipoUsuario', 'FORANEO');
  }

  doInfinite(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        let i = this.IndexArrayOrder + 1;
        let max = i + 10;
        for (i; i < max; i++) {
          if (this.LIST_OFERT[i]) {
            this.IndexArrayOrder = i;
            this.ListOferta.push(this.LIST_OFERT[i]);
          }
        }
        resolve();
      }, 500);
    })
  }

  ionViewWillLeave() {
    console.log('Unsubscribe cuenta-activada');
    this.events.unsubscribe('cuenta-activada')
  }

  ionViewWillEnter() {
    this.getOfertaPaginada(this.segment2);
  }

  getOfertaPaginada(segment) {
    this.Api._GET('/oferta/pagina/0').subscribe(
      (data) => {
        if (data) {
          this.ErrorConexion = false;
          data = this.descompilar.desc(data);
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          console.log('data descomprimida', data);
          this.LIST_OFERT_3 = data;
          this.LIST_OFERT_2 = data;
          this.newFilter(segment)
        }
      }, (err) => {
        if (err.status == 0) {
          this.ErrorConexion = true;
        } else {
          this.complements._ErrorRequest(`${err.status}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
        }
        console.log(err);
      });
  }

  _VerificarUsuario() {
    console.log('verificando usuario...');
    this.events.publish('perfilreload', true)
  }

  /*filterSolic2(value) {
    console.log('NUEVO GET A OFERTAS');
    this.Api._GET('/oferta').subscribe(
      (data) => {
        if (Array.isArray(data) && data) {
          this.ErrorConexion = false;
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          this.LIST_OFERT_3 = data;
          this.LIST_OFERT_2 = data;
          this.newFilter(value)
        }
      },
      (err) => {
        if (err.status == 0) {
          this.ErrorConexion = true;
        } else {
          this.complements._ErrorRequest(`${err.status}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
        }
        console.log(err);
      }, () => {
      }
    );
  }*/

  /* METODO FILTER SOLICITUDES*/
  filterSolic(value) {
    // this.complements._PresentLoading();
    this.segment2 = value;
    if (value === 'publicadas') {
      this.Api._GET('/oferta').subscribe(
        (data) => {
          // this.complements._DismissPresentLoading();
          this.ErrorConexion = false;
          data = data.filter(i => i.oferta_estado === 'PUBLICADA');
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          this.LIST_OFERT = data;
          this.LIST_OFERT_2 = data;
        },
        (err) => {
          // this.complements._DismissPresentLoading();
          if (err.status == 0) {
            this.ErrorConexion = true;
          } else {
            this.complements._ErrorRequest(`${err.status}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
          }
          console.log(err);
        }, () => {
        }
      );
    }
    if (value === 'full cupo') {
      this.Api._GET('/oferta').subscribe(
        (data) => {
          this.ErrorConexion = false;
          data = data.filter(i => i.oferta_estado === 'COMPLETADA');
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          this.LIST_OFERT = data;
          this.LIST_OFERT_2 = data;
        },
        (err) => {
          if (err.status == 0) {
            this.ErrorConexion = true;
          } else {
            if (err.error.mensaje)
              this.complements._ErrorRequest(`${err.statusText}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
          }
        }, () => {
        }
      );
    }
    if (value === 'expiradas') {
      this.Api._GET('/oferta').subscribe(
        (data) => {
          this.ErrorConexion = false;
          data = data.filter(i => i.oferta_estado === 'VENCIDA');
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          this.LIST_OFERT = data;
          this.LIST_OFERT_2 = data;
        },
        (err) => {
          if (err.status == 0) {
            this.ErrorConexion = true;
          } else {
            this.complements._ErrorRequest(`${err.status}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
          }
        }, () => {
        }
      );
    }
    if (value === 'canceladas') {
      this.Api._GET('/oferta').subscribe(
        (data) => {
          data = data.filter(i => i.oferta_estado === 'CANCELADAS');
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          this.LIST_OFERT = data;
          this.LIST_OFERT_2 = data;
        },
        (err) => {
          if (err.status == 0) {
            this.ErrorConexion = true;
          } else {
            this.complements._ErrorRequest(`${err.statusText}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
          }
        }, () => {
        }
      );
    }
    if (value === 'cerradas') {
      this.Api._GET('/oferta').subscribe(
        (data) => {
          data = data.filter(i => i.oferta_estado === 'CERRADA');
          data.map((i) => {
            i.fechacarga = moment(i.fechacarga).format('YYYY-MM-DD');
            i.fechaentrega = moment(i.fechaentrega).format('YYYY-MM-DD');
            i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm: a');
          });
          this.LIST_OFERT = data;
          this.LIST_OFERT_2 = data;
        },
        (err) => {
          if (err.status == 0) {
            this.ErrorConexion = true;
          } else {
            this.complements._ErrorRequest(`${err.status}`, `${err.error.mensaje}`, 'Ha ocurrido un error')
          }
        }, () => {
        }
      );
    }
  }

  newFilter(value) {
    this.segment2 = value;
    console.log('segment', value);
    switch (value) {
      case 'publicadas' :
        this.LIST_OFERT = this.LIST_OFERT_3.filter(i => i.oferta_estado === 'PUBLICADA');
        this.LIST_OFERT_2 = this.LIST_OFERT;
        break;
      case 'full cupo' :
        this.LIST_OFERT = this.LIST_OFERT_3.filter(i => i.oferta_estado === 'COMPLETADA');
        this.LIST_OFERT_2 = this.LIST_OFERT;
        break;
      case 'expiradas' :
        this.LIST_OFERT = this.LIST_OFERT_3.filter(i => i.oferta_estado === 'VENCIDA');
        this.LIST_OFERT_2 = this.LIST_OFERT;
        break;
      case 'canceladas' :
        this.LIST_OFERT = this.LIST_OFERT_3.filter(i => i.oferta_estado === 'CANCELADAS');
        this.LIST_OFERT_2 = this.LIST_OFERT;
        break;
      case 'cerradas' :
        this.LIST_OFERT = this.LIST_OFERT_3.filter(i => i.oferta_estado === 'CERRADA');
        this.LIST_OFERT_2 = this.LIST_OFERT;
        break;
    }
    this.ListOferta = [];
    for (let i = 0; i < 3; i++) {
      if (this.LIST_OFERT[i]) {
        this.IndexArrayOrder = i;
        this.ListOferta.push(this.LIST_OFERT[i]);
      }

    }
    console.group('OFERTA LIST');
    console.log(this.LIST_OFERT);
    console.log(this.LIST_OFERT_2);
    console.log(this.ListOferta);
    console.groupEnd();
    this.ofertasocket = false;
  }

  updateSchedule() {
    if (this.queryText) {
      this.ListOferta =
        this.LIST_OFERT_2.filter(item => item.empresa_nombre.toLowerCase().includes(this.queryText.toLowerCase())
          || item.oferta_producto.toLowerCase().includes(this.queryText.toLowerCase())
          || item.tipovehiculo_nombre.toLowerCase().includes(this.queryText.toLowerCase())
          || item.destino_nombre.toLowerCase().includes(this.queryText.toLowerCase())
          || item.fechacarga.toLowerCase().includes(this.queryText.toLowerCase())
          || item.fechaentrega.toLowerCase().includes(this.queryText.toLowerCase())
          || item.etiqueta.toLowerCase().includes(this.queryText.toLowerCase())
          || item.origen_nombre.toLowerCase().includes(this.queryText.toLowerCase()))
    } else {
      this.ListOferta = this.LIST_OFERT_2
    }
  }


  doRefresh(refresher) {
    this.getOfertaPaginada(this.segment2);
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  validacionRapida() {
    //this.complements._PresentLoading();
    this.Api._GET('/validacionrapida').subscribe(
      () => {
        //this.complements._DismissPresentLoading();
        this.complements._MessageToast('Solicitud enviada');
      }, (err) => {
        //this.complements._DismissPresentLoading();
        this.complements._MessageToast(`${err.error.mensaje}`)
      })
  }

  goToOfertaDetail(oferta) {
    this.app.getRootNav().push(OfertanewdetallePage, {data: oferta, value: oferta.oferta_estado});
  }

  _Notificaciones() {
    this.events.publish('clear-notificaciones');
    this.navCtrl.push(ActivityPage);
  }

  /*DESTRUIR SOCKET*/
  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      GpsProvider.EMIT.disconnect();
    }

  }

  break_text(text) {
    let b = text.split(' ');
    if (b.length >= 4) {
      // return `${b[0]} ${b[1]} <br> ${b[2]} ${b[3]}`
      return `${b[0]} ${b[1]}`
    }
    if (b.length >= 3) {
      // return `${b[0]} ${b[1]} <br> ${b[2]}`
      return `${b[0]} ${b[1]}`
    }
    if (b.length >= 2) {
      return `${b[0]} ${b[1]}`
    }
    return b.join(`<br>`)

  }

  /*CERRAR GPS*/
  _CloseGPS() {
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }

  /**------------------------------------------
   *                                          @
   *       CONSULTAS API BACKEND              @
   *                                          @
   * ------------------------------------------
   */

  createOferta() {
    this.Api._GET(`/oferta/restricciones`).subscribe(
      (data) => {
        if (data) {
          if (data.cupo < 1) {
            const alert = this.alertController.create({
              title: `¡Lo siento!`,
              message: `TU CUPO ACTUAL ES DE ${data.cupo}`,
              buttons: [
                {
                  text: '¡Entendido!',
                  cssClass: 'botton3',
                  handler: () => {
                  }
                }
              ],
              cssClass: 'alertCustomCss3' // <- added this
            });
            alert.present();
          } else {
            this.navCtrl.push(CreateOfertPage, {
              data: data,
            })
          }
        }
      }, (err) => {
        if (err.status == 0) {
          this.ErrorConexion = true;
        } else {
          this.complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error')
        }
      });
  }

  _VehiculoRequerido() {
  }

  /*RECORRIENDO LOS POSTULADOS*/
  _Postulados(data: Array<any> = Array()) {
    data = data.filter(i => i.postulacion_estado === 'PENDIENTE');
    return data.length
  }

  _Activos(data: Array<any> = Array()) {
    data = data.filter(i => i.postulacion_estado === 'ACEPTADA');
    return data.length
  }

  _Culminados(data: Array<any> = Array()) {
    data = data.filter(i => i.postulacion_estado === 'CULMINADA');
    return data.length
  }
}
