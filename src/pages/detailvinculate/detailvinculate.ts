import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

@Component({
  selector: 'page-detailvinculate',
  templateUrl: 'detailvinculate.html',
})
export class DetailvinculatePage {
  public DETALLE: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
    this.DETALLE = this.navParams.get('data');
    console.log('EL DETALLE ', this.DETALLE)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailvinculatePage');
  }

}
