import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import * as moment from 'moment';
import {Vehiculo} from "../trailer/interface/vehiculo.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {distinctUntilChanged, filter, switchMap} from 'rxjs/operators';
import {Observable} from "rxjs";
import {Linea} from "../trailer/interface/linea.interface";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {TransportistasListPage} from "../transportistas-list/transportistas-list";

@Component({
  selector: 'page-vehiculo',
  templateUrl: 'vehiculo.html',
})
export class VehiculoPage {
  form: FormGroup;
  placa_repetida = false;
  poliza_repetida = false;
  vehiculo: Vehiculo[];
  data_vehiculo: any[];
  data_combustible: any[];
  data_carroceria: any[];
  data_color: any[];
  data_marca: any[];
  data_aseguradora: any[];
  data_linea: any[];
  myMoment: moment.Moment = moment();
  public TRANSPORTISTA: any;

  constructor(public navCtrl: NavController,
              private readonly fb: FormBuilder,
              public api: ApiProvider,
              public events: Events,
              private Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    this.TRANSPORTISTA = this.navParams.get('data');

  }

  getCurrentTime() {
    return moment().format('YYYY-MM-DD')
  }

  ngOnInit() {
    // this.getHeads();
    this.loadData();
    this.initForm();
    console.log(this.TRANSPORTISTA);
    // this.setVehiculoId(this.data);
  }

  initForm(): void {
    this.form = this.fb.group({
      propietarioidentificacion: [null, [Validators.required]],
      propietarionombre: [null, [Validators.required]],
      placa: [null, [Validators.required]],
      id_tipovehiculo: [null, [Validators.required]],
      id_carroceria: [null, [Validators.required]],
      id_color: [null, [Validators.required]],
      id_marca: [null, [Validators.required]],
      id_linea: [null, [Validators.required]],
      peso: [null, [Validators.required]],
      id_aseguradora: [null, [Validators.required]],
      fechavencesoat: [null, [Validators.required]],
      poliza: [null],
      fabricacion: [null, [Validators.required]],
      id_combustible: [1],
      id_usuario: [this.TRANSPORTISTA.usuario_id]
    });
    this.formValueChanges();
  }

  formValueChanges() {
    this.form.get('id_marca').valueChanges.pipe(
      distinctUntilChanged(),
      filter((i) => {
        return Boolean(i);
      }),
      switchMap((v) => {
        return this.listarLinea(v);
      })
    ).subscribe((data) => {
      console.log('SI LLEGO!!!', data);
      this.data_linea = data;
    });
    this.form.get('id_marca').valueChanges
      .pipe(distinctUntilChanged()).subscribe(() => {
      this.form.get('id_linea').reset();
    });
  }

  listarLinea(id: number): Observable<Linea[]> {
    return this.api._GET(`/linea/${id}`);
  }

  loadData() {
    this.api._GET('/tipovehiculo').subscribe(vehiculo => {
      this.data_vehiculo = vehiculo;
      console.log('vehiculo', vehiculo);
    });
    this.api._GET('/aseguradora').subscribe(aseguradora => {
      this.data_aseguradora = aseguradora;
      console.log('aseguradora', aseguradora);
    });
    this.api._GET('/color').subscribe(color => {
      this.data_color = color;
      console.log('color', color);
    });
    this.api._GET('/combustible').subscribe(combustible => {
      this.data_combustible = combustible;
      console.log('combustible', combustible);
    });
    this.api._GET('/carroceria').subscribe(carroceria => {
      this.data_carroceria = carroceria;
      console.log('carroceria', carroceria);
    });
    this.api._GET('/marca').subscribe(marca => {
      this.data_marca = marca;
      console.log('marca', marca);
    });
  }

  /*TYPEAHEAD*/
  _Carroceria(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_carroceria').setValue(datos.data);
    } else {
      this.form.get('id_carroceria').setValue('');
    }
  }

  _ColorTrailer(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_color').setValue(datos.data);
    } else {
      this.form.get('id_color').setValue('');
    }
  }

  _MarcaVehiculo(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_marca').setValue(datos.data);
    } else {
      this.form.get('id_marca').setValue('');
    }
  }

  _TipoLinea(datos) {
    if (datos.data != null) {
      console.log('Output ubicacion', datos);
      this.form.get('id_linea').setValue(datos.data);
    } else {
      this.form.get('id_linea').setValue('');
    }
  }

  _Aseguradora(datos) {
    if (datos.data != null) {
      console.log('Output aseguradora', datos);
      this.form.get('id_aseguradora').setValue(datos.data);
    } else {
      this.form.get('id_aseguradora').setValue('');
    }
  }

  saveData() {
    console.log(this.form.value);
    this.api._POST('/vehiculo', this.form.value).subscribe(
      () => {
        this.events.publish('list-transport');
        this.navCtrl.setRoot(TransportistasListPage);
        this.Complementos._MessageToast(`Vehiculo creado exitosamente`);
      },
      (e) => {
        this.Complementos._MessageToast(`${e.error.mensaje}`);
      })
  }


  /*
  * COMPROBAR CAMPO PLACA Y POLIZA
  * */
  comprobarCampo(campo: string): void {
    switch (campo) {
      case 'placa':
        const placa = this.form.value.placa;
        if (placa !== null) {
          this.api._GET(`/vehiculo/placa/${placa}`).subscribe(
            (item) => {
              if (item.existe === 'SI') {
                this.Complementos._MessageToast('Placa existente');
                console.log('Aqui el login igual');
                this.form.get('placa').reset();
                this.placa_repetida = true;
              } else {
                this.placa_repetida = false;
                console.log('no iguales');
              }
            }
          );
        } else {
          this.Complementos._MessageToast('Debes ingresar una placa');
        }
        break;
      case 'poliza':
        const poliza = this.form.value.poliza;
        if (poliza !== null) {
          this.api._GET(`/vehiculo/poliza/${poliza}`).subscribe(
            (item) => {
              if (item.existe === 'SI') {
                console.log('Aqui la licencia igual');
                this.poliza_repetida = true;
                this.Complementos._MessageToast('Poliza existente');
                this.form.get('poliza').reset();
              } else {
                this.poliza_repetida = false;
                console.log('no iguales');
              }
            }
          );
        } else {
          this.Complementos._MessageToast('Debes ingresar una poliza');
        }
        break;
    }
  }

  /*ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculoPage');
  }*/

}
