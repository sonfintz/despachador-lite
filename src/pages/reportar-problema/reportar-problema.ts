import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {EvidenciasPage} from "../evidencias/evidencias";

// import {EvidenciasPage} from "../evidencias/evidencias";

/**
 * Generated class for the ReportarProblemaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reportar-problema',
  templateUrl: 'reportar-problema.html',
})
export class ReportarProblemaPage {
  formEventos: FormGroup;
  private images: any;
  private temas: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              private  Api: ApiProvider,
              private Complementos: ComplementosviewsProvider) {
    this.loadData();
    this.formEventos = this.initForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportarProblemaPage');
  }

  loadData() {
    this.Api._GET('/temas').subscribe((tema) => {
      console.log(tema);
      tema = tema.filter(i => i.tipo === 'REPORTE DE PROBLEMA');
      this.temas = tema;
    })
  }

  initForm() {
    return this.formBuilder.group({
      descripcion: [null, [Validators.required]],
      id_tema: [null, [Validators.required]],
      extra: [null]
    });
  }

  Evidendia_fotos() {
    console.log('EvidenciasPage');
    this.navCtrl.push(EvidenciasPage, {callback: this.myCallbackFunction})
  }

  myCallbackFunction = (_params) => {
    return new Promise((resolve) => {
      console.log("Imagenes Evidencia", _params);
      this.images = _params;
      resolve()
    });
  };

  SendApi() {
    this.Complementos.PresentLoading('Cargando', 200);
    this.formEventos.value.extra = this.images || null;
    this.Api._POST('/usuario/fallas', this.formEventos.value)
      .subscribe(
        data => {
          console.log(data);
          if (data.hasOwnProperty("success") && data.success == true) {
            this.Complementos._MessageToast('Gracias por reportar', 5000);
            this.navCtrl.pop();
          }
        },
        err => {
          console.log(err);
        },
        () => {
        }
      );
  }

}
