var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, App, Events, NavController, NavParams } from 'ionic-angular';
import { OfertaeditPage } from "../ofertaedit/ofertaedit";
import { ApiProvider } from "../../providers/api/api";
import { ListaOfertasPage } from "../lista-ofertas/lista-ofertas";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
let PopovereditPage = class PopovereditPage {
    constructor(navCtrl, alertController, Api, events, app, navParams) {
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.Api = Api;
        this.events = events;
        this.app = app;
        this.navParams = navParams;
        this.navParams.get('data');
        this.navParams.get('value');
        console.log(this.navParams.get('data'), this.navParams.get('value'));
    }
    get NotificationsSetting() {
        return DataglobalProvider.NotificacionesSettings;
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad PopovereditPage');
    }
    culminarOferta() {
        const alert = this.alertController.create({
            title: '¿Cerrar Oferta?',
            message: 'La oferta dejará de estar publicada' +
                ' pero continuarás con el seguimiento a tus vehículos.',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: '¡Confirmar!',
                    cssClass: 'botton2',
                    handler: () => {
                        console.log(this.navParams.get('data').oferta_id);
                        this.Api._PUT(`/oferta/completar/${this.navParams.get('data').oferta_id}`, {}).subscribe((data) => {
                            console.log(data);
                            this.navCtrl.pop();
                            this.app.getRootNav().setRoot(ListaOfertasPage);
                        });
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
    rechazarOferta() {
        let evento = this.NotificationsSetting.find(i => i.design_codigo === 3);
        console.log('EVENTO CONSEGUIDO', evento);
        const alert = this.alertController.create({
            title: `${evento.design_titulo_confirmacion}`,
            subTitle: `${evento.design_descripcion_confirmacion}`,
            message: `- ${evento.design_puntos ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: '¡Confirmar!',
                    cssClass: 'botton2',
                    handler: () => {
                        console.log(this.navParams.get('data').oferta_id);
                        this.Api._PUT(`/oferta/anular/${this.navParams.get('data').oferta_id}`, {}).subscribe((data) => {
                            console.log(data);
                            this.navCtrl.pop();
                            this.app.getRootNav().setRoot(ListaOfertasPage);
                        });
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
    _editarOferta() {
        this.app.getRootNav().push(OfertaeditPage, { data: this.navParams.get('data'), value: this.navParams.get('value') });
        this.navCtrl.pop();
    }
    postuladosaceptados(data) {
        if (data.length > 0) {
            let val = data.find((i) => i.postulacion_estado == 'ACEPTADA' || i.postulacion_estado == 'CULMINADA');
            if (val !== undefined) {
                return val.postulacion_estado;
            }
            else {
                return 'PENDIENTE';
            }
        }
    }
};
PopovereditPage = __decorate([
    Component({
        selector: 'page-popoveredit',
        templateUrl: 'popoveredit.html',
    }),
    __metadata("design:paramtypes", [NavController,
        AlertController,
        ApiProvider,
        Events,
        App,
        NavParams])
], PopovereditPage);
export { PopovereditPage };
//# sourceMappingURL=popoveredit.js.map