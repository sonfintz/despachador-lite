import {Component} from '@angular/core';
import {AlertController, App, Events, NavController, NavParams} from 'ionic-angular';
import {OfertaeditPage} from "../ofertaedit/ofertaedit";
import {ApiProvider} from "../../providers/api/api";
import {ListaOfertasPage} from "../lista-ofertas/lista-ofertas";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";

@Component({
  selector: 'page-popoveredit',
  templateUrl: 'popoveredit.html',
})
export class PopovereditPage {
  get NotificationsSetting() {
    return DataglobalProvider.NotificacionesSettings;
  }

  constructor(public navCtrl: NavController,
              public alertController: AlertController,
              public Api: ApiProvider,
              public events: Events,
              private app: App,
              public navParams: NavParams) {
    this.navParams.get('data');
    this.navParams.get('value');
    console.log(this.navParams.get('data'), this.navParams.get('value'))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopovereditPage');
  }

  culminarOferta() {
    const alert = this.alertController.create({
      title: '¿Cerrar Oferta?',
      message: 'La oferta dejará de estar publicada' +
        ' pero continuarás con el seguimiento a tus vehículos.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'botton1',
        }, {
          text: '¡Confirmar!',
          cssClass: 'botton2',
          handler: () => {
            console.log(this.navParams.get('data').oferta_id);
            this.Api._PUT(`/oferta/completar/${this.navParams.get('data').oferta_id}`, {}).subscribe((data) => {
              console.log(data);
              this.navCtrl.pop();
              this.app.getRootNav().setRoot(ListaOfertasPage);
            })
          }
        }
      ],
      cssClass: 'alertCustomCss' // <- added this
    });
    alert.present();
  }

  rechazarOferta() {
    let evento = this.NotificationsSetting.find(i => i.design_codigo === 3);
    console.log('EVENTO CONSEGUIDO', evento);
    const alert = this.alertController.create({
      title: `${evento.design_titulo_confirmacion}`,
      subTitle: `${evento.design_descripcion_confirmacion}`,
      message: `- ${evento.design_puntos ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'botton1',
        }, {
          text: '¡Confirmar!',
          cssClass: 'botton2',
          handler: () => {
            console.log(this.navParams.get('data').oferta_id);
            this.Api._PUT(`/oferta/anular/${this.navParams.get('data').oferta_id}`, {}).subscribe((data) => {
              console.log(data);
              this.navCtrl.pop();
              this.app.getRootNav().setRoot(ListaOfertasPage);
            })
          }
        }
      ],
      cssClass: 'alertCustomCss' // <- added this
    });
    alert.present();
  }

  _editarOferta() {
    this.app.getRootNav().push(OfertaeditPage, {data: this.navParams.get('data'), value: this.navParams.get('value')});
    this.navCtrl.pop();
  }

  postuladosaceptados(data) {
    if (data.length > 0) {
      let val = data.find((i) => i.postulacion_estado == 'ACEPTADA' || i.postulacion_estado == 'CULMINADA');
      if (val !== undefined) {
        return val.postulacion_estado
      } else {
        return 'PENDIENTE'
      }
    }
  }

}
