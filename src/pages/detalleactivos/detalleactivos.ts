import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {TimelinetransportistaPage} from "../timelinetransportista/timelinetransportista";
import {DetalletransportistaPage} from "../detalletransportista/detalletransportista";
import {ApiProvider} from "../../providers/api/api";
import * as moment from "moment";
import {GpsProvider} from "../../providers/gps/gps";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-detalleactivos',
  templateUrl: 'detalleactivos.html',
})
export class DetalleactivosPage {
  public queryText: string;
  public Listado: Array<any> = Array();
  public Listado2: Array<any> = Array();
  public fechaCarga: any;
  public fechaEntrega: any;

  constructor(public navCtrl: NavController,
              public Api: ApiProvider,
              public complements: ComplementosviewsProvider,
              public events: Events,
              public navParams: NavParams) {
    this.SubscribeEvents();
  }

  SubscribeEvents() {
    this.events.subscribe('detalleactivos', (data) => {
      console.log(data);
      this._Detalleactivo();
    })
  }

  ionViewWillLeave() {
    console.log('Unsubscribe detalleactivos');
    this.events.unsubscribe('detalleactivos')
  }

  _Detalleactivo() {
    console.log(this.navParams.get('value'));
    console.log('LA DATA', this.navParams.get('data'));
    this.Api._GET(`/oferta/estados/${this.navParams.get('value').toUpperCase()}/${this.navParams.get('data')[0].id_oferta}`).subscribe(
      (data) => {
        if (data) {
          console.log('NUEVA RUTA', data);
          this.fechaCarga = moment(data.fechacarga).format('YYYY-MM-DD HH:mm a');
          this.fechaEntrega = moment(data.fechaentrega).format('YYYY-MM-DD HH:mm a');
          this.Listado = data;
          this.Listado2 = data;
        }
      }, (err) => {
        this.complements._MessageToast(`${err.error.mensaje}`)
      });
  }

  detalleTransportista(data) {
    //this.complements._PresentLoading();
    console.log('TRANSPORTISTA', data);
    let data1 = data;
    this.Api._GET(`/transportista/detalle/${data.usuario_id}`).subscribe(
      (data) => {
        //this.complements._DismissPresentLoading();
        if (data) {
          let Obj = Object.assign(data, data1);
          this.navCtrl.push(DetalletransportistaPage, {data: Obj, value: 0});
          console.log(data);
        }
      }, (err) => {
        //this.complements._DismissPresentLoading();
        this.complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error')
      })
  }

  Search_placa_name() {
    if (this.queryText) {
      console.log(this.queryText);
      this.Listado = this.Listado2.filter(i => i.vehiculo_placa.toLowerCase().includes(this.queryText.toLowerCase()) ||
        i.persona_nombre.toLowerCase().includes(this.queryText.toLowerCase()));
      console.log(this.Listado2);

    } else {
      this.Listado = this.Listado2;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleactivosPage');
  }

  ionViewWillEnter() {
    this._Detalleactivo();
  }

  break_text(text) {
    let b = text.split(' ');
    if (b.length >= 4) {
      return `${b[0]} ${b[1]} <br> ${b[2]} ${b[3]}`
    }
    if (b.length >= 3) {
      return `${b[0]} ${b[1]} <br> ${b[2]}`
    }
    if (b.length >= 2) {
      return `${b[0]} ${b[1]}`
    }
    return b.join(`<br>`)
  }

  timeLine(data, value) {
    data.map((i) => {
      i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm a');
      return i;
    });
    console.log('TIMELINE', data);
    console.log('VALUE', value);
    this.navCtrl.push(TimelinetransportistaPage, {data: data, value: value})
  }

  /*DESTRUIR SOCKET*/
  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      console.log(' Disconect Socket', GpsProvider.EMIT);
      GpsProvider.EMIT.disconnect();
      console.log('1 Disconect Socket', GpsProvider.EMIT);
    }

  }

  /*CERRAR GPS*/
  _CloseGPS() {
    console.group('CERRANDO EL EMIT');
    console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
    console.groupEnd();
    // GpsProvider.Band_Conexion=false;
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }

}
