var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AsignarTransportistasPage } from "../asignar-transportistas/asignar-transportistas";
import { OfertasPage } from "../ofertas/ofertas";
import { DespacharTrasportistasPage } from "../despachar-trasportistas/despachar-trasportistas";
import { EstadisticasPage } from "../estadisticas/estadisticas";
import { ApiProvider } from "../../providers/api/api";
let PopoverbuttonspagePage = class PopoverbuttonspagePage {
    constructor(navCtrl, navParams, Api) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Api = Api;
        this.SOLICITUD = this.navParams.get('data');
    }
    get _DisableOfertaBTN() {
        return (this.SOLICITUD.id_oferta) ? true : false;
    }
    ionViewDidLoad() {
        console.log('SOLICITUD', this.SOLICITUD);
    }
    _Transportistas() {
        console.log('Transportistas');
        this.navCtrl.push(AsignarTransportistasPage, { data: this.navParams.get('data') });
    }
    _Despachadores() {
        console.log('_Despachadores');
    }
    _CrearOferta() {
        console.log('_CrearOferta');
        this.Api._GET(`/solicitudes/preoferta/${this.SOLICITUD.solicitudes_id}`).subscribe((data) => {
            console.log('PREOFERTA', data);
            this.navCtrl.push(OfertasPage, { data: data });
        });
        // this.navCtrl.push(OfertasPage, {data: this.navParams.get('data')});
    }
    _Estadisticas() {
        console.log('_Estadisticas');
        this.navCtrl.push(EstadisticasPage, { data: this.navParams.get('data') });
    }
    _EnviarTransportistas() {
        console.log('_EnviarTransportistas');
        this.navCtrl.push(DespacharTrasportistasPage, { data: this.navParams.get('data') });
    }
};
PopoverbuttonspagePage = __decorate([
    Component({
        selector: 'page-popoverbuttonspage',
        templateUrl: 'popoverbuttonspage.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        ApiProvider])
], PopoverbuttonspagePage);
export { PopoverbuttonspagePage };
//# sourceMappingURL=popoverbuttonspage.js.map