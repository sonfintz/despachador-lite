import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AsignarTransportistasPage} from "../asignar-transportistas/asignar-transportistas";
import {OfertasPage} from "../ofertas/ofertas";
import {DespacharTrasportistasPage} from "../despachar-trasportistas/despachar-trasportistas";
import {EstadisticasPage} from "../estadisticas/estadisticas";
import {ApiProvider} from "../../providers/api/api";

@Component({
  selector: 'page-popoverbuttonspage',
  templateUrl: 'popoverbuttonspage.html',
})
export class PopoverbuttonspagePage {
  private SOLICITUD: any;

  public get _DisableOfertaBTN() {
    return (this.SOLICITUD.id_oferta) ? true : false;

  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private Api: ApiProvider,
  ) {
    this.SOLICITUD = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('SOLICITUD', this.SOLICITUD);

  }

  _Transportistas() {
    console.log('Transportistas');
    this.navCtrl.push(AsignarTransportistasPage, {data: this.navParams.get('data')});
  }

  _Despachadores() {
    console.log('_Despachadores');
  }

  _CrearOferta() {
    console.log('_CrearOferta');
    this.Api._GET(`/solicitudes/preoferta/${this.SOLICITUD.solicitudes_id}`).subscribe((data) => {
      console.log('PREOFERTA', data);
      this.navCtrl.push(OfertasPage, {data: data});
    });
    // this.navCtrl.push(OfertasPage, {data: this.navParams.get('data')});
  }

  _Estadisticas() {
    console.log('_Estadisticas');
    this.navCtrl.push(EstadisticasPage, {data: this.navParams.get('data')});
  }

  _EnviarTransportistas() {
    console.log('_EnviarTransportistas');
    this.navCtrl.push(DespacharTrasportistasPage, {data: this.navParams.get('data')});
  }


}
