import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ReporttransportistaPage} from "../reporttransportista/reporttransportista";

@Component({
  selector: 'page-popoverreport',
  templateUrl: 'popoverreport.html',
})
export class PopoverreportPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverreportPage');
  }

  _Reportar() {
    this.navCtrl.push(ReporttransportistaPage, {data: this.navParams.get('data')})
  }

}
