import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
/**
 * Generated class for the RetosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import * as moment from "moment";
import {AppModule} from "../../app/app.module";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-retos',
  templateUrl: 'retos.html',
})
export class RetosPage {
  private Retos: any;
  private RetosTotal: any;

  constructor(public navCtrl: NavController,
              public Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    console.log(this.navParams.get('value').retos);
    this.Retos = this.navParams.get('data');
    this.RetosTotal = this.navParams.get('value').retos;
  }

  get NotificationsSetting() {
    return DataglobalProvider.NotificacionesSettings;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RetosPage');
  }

  imagenLogro(imagen) {
    return AppModule.BASE_URL + '/' + imagen
  }

  _formatDate(date) {
    if (date) { return moment(date, "DD-MM-YYYY HH:mm:ss").format('YYYY/MM/DD HH:mm a') }
    else {return ''}
  }

  infoNotf(e) {
      const ref = e.design_codigo;
      // const Puntos = this.TailProcesCuentas[0].punto;
      let btn, mss, Puntos;
      let Tabulador
      let titlePrimary;
      btn = [{
        text: "Entendido",
        handler: () => {
        },
        cssClass: "button-green"

      }];
      try {
        Tabulador = this.NotificationsSetting.find(i => i.design_codigo == ref);
        let conversion = Tabulador['design_puntos'] || 1000;
        Puntos = Tabulador['design_moneda'] * conversion;
        const spanPuntos = `<span class="puntos">+ ${Puntos} MP</span>`;
        let spanPuntosTitle = `<span class="punto-in-title">&nbsp;+${Puntos} MP</span>`;
        mss = `${Tabulador.design_detalle}`;
        console.log(spanPuntos);

        titlePrimary = `${Tabulador.design_tipo}${spanPuntosTitle}`
      } catch (e) {
        console.error('Error en el find ')
      }

      this.Complementos.showAlert(titlePrimary, `${Tabulador.design_titulo}`, mss, btn, 'alertConfig_6_css');
    }
}
