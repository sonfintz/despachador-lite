var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { ApiProvider } from "../../providers/api/api";
import { PopoverreportPage } from "../popoverreport/popoverreport";
import { CalificartransportistaPage } from "../calificartransportista/calificartransportista";
import { CallNumber } from "@ionic-native/call-number";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
import { DetailcalificacionPage } from "../detailcalificacion/detailcalificacion";
/**
 * Generated class for the DetalletransportistaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let DetalletransportistaPage = class DetalletransportistaPage {
    constructor(navCtrl, Api, callNumber, popoverCtrl, alertController, events, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.Api = Api;
        this.callNumber = callNumber;
        this.popoverCtrl = popoverCtrl;
        this.alertController = alertController;
        this.events = events;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.events.subscribe('calificado', (data) => {
            console.log(data);
            this.Api._GET(`/transportista/detalle/${this._Detalle.usuario_id}`).subscribe((data) => {
                this._Detalle = data;
            });
        });
        if (this.navParams.get('value') === 3) {
            this._Detalle = this.navParams.get('data');
        }
        else {
            this._Detalle = this.navParams.get('data');
        }
        if (this.navParams.get('value') == 0 || this.navParams.get('value') == 3) {
            this._Value = null;
            this.ofertadetalle = null;
            console.log('DESDE OFERTA ACTIVOS', this._Value);
        }
        else {
            this._Value = this.navParams.get('value');
            this.ofertadetalle = this.navParams.get('ofertadetalle');
            console.group('DESDE OFERTA DETALLE');
            console.log('_Value', this._Value);
            console.log('Ofertadetalle', this.ofertadetalle);
            console.log('_Detalle', this._Detalle);
            console.groupEnd();
        }
    }
    get NotificationsSetting() {
        return DataglobalProvider.NotificacionesSettings;
    }
    popoverEdit(myEvent) {
        let popover = this.popoverCtrl.create(PopoverreportPage, { data: this.navParams.get('data'), value: 0 });
        popover.present({
            ev: myEvent,
            animate: true
        });
    }
    PhoneCall(Number, data, value) {
        console.log(Number, data);
        console.log(value);
        this.Api._GET(`/oferta/llamada/${value.oferta_id}/${data.usuario_id}`).subscribe(() => {
            this.callNumber.callNumber(Number, true)
                .then(res => console.log('Launched dialer!', res))
                .catch(err => console.log('Error launching dialer', err));
        }, (err) => {
            this.Complementos._MessageToast(`${err.error.mensaje}`);
        });
    }
    ionViewWillEnter() {
        if (this.navParams.get('data').id_oferta !== undefined) {
            this.Api._GET(`/transportista/postulado/${this.navParams.get('data').usuario_id}/oferta/${this.navParams.get('data').id_oferta}`).subscribe((data) => {
                console.log('DETALLE TRANSPORTISTA', data);
                this._Calificado = data;
            });
        }
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DetalletransportistaPage');
    }
    SeeCalification() {
        console.log('CALIFICADO', this._Calificado);
        this.navCtrl.push(DetailcalificacionPage, {
            data: this._Calificado
        });
    }
    calificarTransportista() {
        let Obj;
        if (this._Detalle.id_oferta) {
            Obj = this._Detalle;
        }
        else {
            Obj = this._Value;
        }
        this.navCtrl.push(CalificartransportistaPage, { data: Obj });
        console.log('Calificar transportista');
    }
    aceptarTransportistas(id, data) {
        console.log('el id', id.usuario_id, 'la data', data);
        if (data.oferta_registrado >= data.oferta_cantidad) {
            this.Complementos._MessageToast('Cupo de vehiculos completo');
        }
        else {
            let evento = this.NotificationsSetting.find(i => i.design_codigo === 6);
            console.log('EVENTO CONSEGUIDO', evento);
            console.log('TRASPORTISTA', id);
            const Obj = {
                id_usuario: id.usuario_id,
                estado: 'aceptada'
            };
            const alert = this.alertController.create({
                title: `${evento.design_titulo_confirmacion}`,
                subTitle: `${evento.design_descripcion_confirmacion}`,
                message: `+ ${evento.design_puntos ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'botton1',
                    }, {
                        text: 'Confirmar',
                        cssClass: 'botton2',
                        handler: () => {
                            this.Api._PUT(`/postulacion/${data.oferta_id}`, Obj)
                                .subscribe((data) => {
                                console.log('ACEPTADO:', data);
                                if (data.success === true) {
                                    this.Complementos._MessageToast('¡Transportista aceptado!');
                                    this.navCtrl.pop();
                                    console.log('refresh');
                                }
                            }, (err) => {
                                console.log('ERROR:', err);
                                this.Complementos._MessageToast(`${err.error.mensaje}`);
                            }, () => {
                            });
                        }
                    }
                ],
                cssClass: 'alertCustomCss' // <- added this
            });
            alert.present();
        }
    }
    rechazarTransportistas(id, data) {
        const evento = this.NotificationsSetting.find(i => i.design_codigo === 4);
        console.log('EVENTO CONSEGUIDO', evento);
        console.log('TRASPORTISTA', id.usuario_id);
        const Obj = {
            id_usuario: id.usuario_id,
            estado: 'cancelada'
        };
        const alert = this.alertController.create({
            title: '¿Rechazar Transportista?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: 'Confirmar',
                    cssClass: 'botton2',
                    handler: () => {
                        this.Api._PUT(`/postulacion/${data.oferta_id}`, Obj)
                            .subscribe((data) => {
                            console.log('RECHAZADO:', data);
                            if (data.success === true) {
                                this.Complementos._MessageToast('¡TRANSPORTISTA RECHAZADO!');
                                this.navCtrl.pop();
                                console.log('refresh');
                            }
                        }, (err) => {
                            console.log('ERROR:', err);
                        }, () => {
                        });
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
    rechazarTransportistas1(id, data) {
        console.log('TRASPORTISTA', id, data);
        const Obj = {
            id_usuario: id.usuario_id,
            estado: 'cancelada'
        };
        let evento = this.NotificationsSetting.find(i => i.design_codigo === 4);
        console.log('EVENTO CONSEGUIDO', evento);
        const alert = this.alertController.create({
            title: `${evento.design_titulo_confirmacion}`,
            subTitle: `${evento.design_descripcion_confirmacion}`,
            message: `- ${evento.design_puntos ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: 'Confirmar',
                    cssClass: 'botton2',
                    handler: () => {
                        this.Api._PUT(`/postulacion/${data.oferta_id}`, Obj)
                            .subscribe((data) => {
                            console.log('RECHAZADO:', data);
                            if (data.success === true) {
                                this.Complementos._MessageToast('¡TRANSPORTISTA RECHAZADO!');
                                this.navCtrl.pop();
                                console.log('refresh');
                            }
                        }, (err) => {
                            console.log('ERROR:', err);
                            this.Complementos._MessageToast(`${err.error.mensaje}`, 2000);
                        }, () => {
                        });
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
};
DetalletransportistaPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-detalletransportista',
        templateUrl: 'detalletransportista.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        CallNumber,
        PopoverController,
        AlertController,
        Events,
        ComplementosviewsProvider,
        NavParams])
], DetalletransportistaPage);
export { DetalletransportistaPage };
//# sourceMappingURL=detalletransportista.js.map