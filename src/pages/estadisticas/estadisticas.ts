import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";

@Component({
  selector: 'page-estadisticas',
  templateUrl: 'estadisticas.html',
})
export class EstadisticasPage {
  public sol: any = 50;
  public Variable: any;
  public Estadistica_data: any = {asignado: '', despachado: '', ruta: '', culminado: ''};
  public ESTADISTICA: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private Api: ApiProvider,) {
    this.ESTADISTICA = this.navParams.get('value');
    console.log(this.ESTADISTICA);
  }

  ngOnInit() {
    this.Api._GET(`/solicitudes/estadisticas/${this.navParams.get('data')}`)
      .subscribe(
        (data) => {
          this.Estadistica_data = data.resumen;
        }
      );
  }

  /* ionViewDidLoad() {
     console.log('ionViewDidLoad EstadisticasPage');
   }*/

  /* _Cambio($event) {
     console.log('Event:', $event, 'VAR:', this.Variable)
   }*/
}
