var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { TransportistasListPage } from "../transportistas-list/transportistas-list";
let EditartrailerPage = class EditartrailerPage {
    constructor(navCtrl, fb, Api, events, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.Api = Api;
        this.events = events;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.placa_repetida = false;
        this.TRANSPORTISTA = this.navParams.get('data');
        console.log('Data del transportista trailer', this.TRANSPORTISTA);
        this.comprobarCampo('placa');
        this.loadData();
    }
    ngOnInit() {
        // this.getHeads();
        this.initForm();
        // console.log(this.data)
        // this.setVehiculoId(this.data);
    }
    /*FORMULARIO*/
    initForm() {
        this.form = this.fb.group({
            placa: [this.TRANSPORTISTA.placa, [Validators.required]],
            id_carroceria: [this.TRANSPORTISTA.id_carroceria, [Validators.required]],
            id_tipotrailer: ['', [Validators.required]],
            id_marcatrailer: [this.TRANSPORTISTA.id_marcatrailer, [Validators.required]],
            id_color: [this.TRANSPORTISTA.id_color, [Validators.required]],
            id_unidadmedida: [this.TRANSPORTISTA.id_unidadmedida, [Validators.required]],
            peso: [this.TRANSPORTISTA.peso, [Validators.required]],
            capacidad: [this.TRANSPORTISTA.capacidad, [Validators.required]],
            fabricacion: [this.TRANSPORTISTA.fabricacion, [Validators.required]],
            id_combustible: [this.TRANSPORTISTA.id_combustible, [Validators.required]],
        });
        console.log('FORMULARIO', this.form.value);
    }
    loadData() {
        this.Api._GET('/color').subscribe(color => {
            this.data_color = color;
            // console.log(color)
        });
        this.Api._GET('/combustible').subscribe(combustible => {
            this.data_combustible = combustible;
            // console.log('combustible', combustible)
        });
        this.Api._GET('/carroceria').subscribe(carroceria => {
            this.data_carroceria = carroceria;
            // console.log('carroceria', carroceria)
        });
        this.Api._GET('/marca').subscribe(marca => {
            this.data_marca = marca;
            // console.log('MARCA TRAILER', marca)
        });
        this.Api._GET('/unidadmedida').subscribe(unidad => {
            this.data_unidadmedida = unidad;
            // console.log(unidad)
        });
    }
    loadTrailer(item) {
        this.Api._GET(`/tipotrailer/${item.id_tipovehiculo}`).subscribe(trailer => {
            this.data_tipotrailer = trailer;
            console.log('TRAILER', trailer);
            // console.log('trailer', trailer)
        });
    }
    /* METODO GUARDAR */
    saveData() {
        console.log(this.form.value);
        this.Api._PUT(`/trailer/${this.TRANSPORTISTA.id}`, this.form.value).subscribe(() => {
            this.navCtrl.setRoot(TransportistasListPage);
            this.events.publish('list-transport');
            this.Complementos._MessageToast(`Trailer editado exitosamente`);
        }, (e) => {
            this.Complementos._MessageToast(`${e.error.mensaje}`);
        });
    }
    /*TYPEAHEAD*/
    _Carroceria(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_carroceria').setValue(datos.data);
        }
        else {
            this.form.get('id_carroceria').setValue('');
        }
    }
    _MarcaTrailer(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_marcatrailer').setValue(datos.data);
        }
        else {
            this.form.get('id_marcatrailer').setValue('');
        }
    }
    _ColorTrailer(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_color').setValue(datos.data);
        }
        else {
            this.form.get('id_color').setValue('');
        }
    }
    /*COMPROBAR CAMPO PLACA*/
    comprobarCampo(campo) {
        switch (campo) {
            case 'placa':
                const placa = this.TRANSPORTISTA.placa;
                console.log('CASE PLACA', placa);
                if (placa !== null) {
                    this.Api._GET(`/trailer/placa/${placa}`).subscribe((item) => {
                        console.log('TRAILER DE LA PLACA-TRAILER', item);
                        this.loadTrailer(item);
                        if (item.existe === 'SI') {
                            // console.log('Aqui el login igual');
                            this.Complementos._MessageToast('Placa existente');
                            this.placa_repetida = !this.placa_repetida;
                            this.form.get('placa').reset();
                        }
                        else {
                            // console.log('no iguales');
                            this.placa_repetida = false;
                        }
                    });
                }
                else {
                    this.Complementos._MessageToast('Debes ingresar una placa');
                }
                break;
        }
    }
};
EditartrailerPage = __decorate([
    Component({
        selector: 'page-editartrailer',
        templateUrl: 'editartrailer.html',
    }),
    __metadata("design:paramtypes", [NavController,
        FormBuilder,
        ApiProvider,
        Events,
        ComplementosviewsProvider,
        NavParams])
], EditartrailerPage);
export { EditartrailerPage };
//# sourceMappingURL=editartrailer.js.map