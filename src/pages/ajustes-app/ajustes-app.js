var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, MenuController, NavController, NavParams } from 'ionic-angular';
import { PerfilPage } from "../perfil/perfil";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
import { ActivityPage } from "../activity/activity";
import { CreateOfertPage } from "../create-ofert/create-ofert";
import { ApiProvider } from "../../providers/api/api";
import { ReportarProblemaPage } from "../reportar-problema/reportar-problema";
import { SocialSharing } from "@ionic-native/social-sharing";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { MueveputosPage } from "../mueveputos/mueveputos";
import { EtiquetaPage } from "../etiqueta/etiqueta";
import { LinksRedirect } from "../../providers/linksRedirect";
/**
 * Generated class for the AjustesAppPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let AjustesAppPage = class AjustesAppPage {
    constructor(navCtrl, navParams, alertController, Complementos, socialSharing, Api, events, linksgo, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertController = alertController;
        this.Complementos = Complementos;
        this.socialSharing = socialSharing;
        this.Api = Api;
        this.events = events;
        this.linksgo = linksgo;
        this.menuCtrl = menuCtrl;
        this._User = localStorage.getItem('pin');
    }
    get Notificacion() {
        return DataglobalProvider.notificacion;
    }
    get VersionApp() {
        return DataglobalProvider.VersionApp;
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad AjustesAppPage');
    }
    ionViewWillEnter() {
        this.menuCtrl.close('leftMenu');
    }
    Account() {
        this.navCtrl.push(PerfilPage);
    }
    _Notificaciones() {
        this.events.publish('clear-notificaciones');
        this.navCtrl.push(ActivityPage);
    }
    createOferta() {
        this.Api._GET(`/oferta/restricciones`).subscribe((data) => {
            console.log('RESTRICCIONES', data);
            console.log('RESTRICCIONES', data.cupo);
            if (data.cupo < 1) {
                const alert = this.alertController.create({
                    title: `¡Lo siento!`,
                    message: `TU CUPO ACTUAL ES DE ${data.cupo}`,
                    buttons: [
                        {
                            text: '¡Entendido!',
                            cssClass: 'botton3',
                            handler: () => {
                            }
                        }
                    ],
                    cssClass: 'alertCustomCss3' // <- added this
                });
                alert.present();
            }
            else {
                this.navCtrl.push(CreateOfertPage, {
                    data: data,
                });
            }
        }, (err) => {
            this.Complementos._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error');
        });
    }
    ReportProblem() {
        this.navCtrl.push(ReportarProblemaPage);
    }
    goMuevePuntos() {
        this.navCtrl.push(MueveputosPage);
    }
    Compartir(shortid) {
        console.log(shortid);
        this.socialSharing.share(`Mi pin de referido ${shortid}`, 'Mueve Logistica', '', 'https://play.google.com/store/apps/details?id=com.mueve_despachadorlite&hl=en')
            .then(() => {
        })
            .catch(err => {
            console.error('Error al compartir: ', err);
            this.Complementos._MessageToast('Error al compartir');
        });
    }
    sincronizateData() {
        console.log('Sincronizate Data');
        const alert = this.alertController.create({
            title: '¿Sincronizar data?',
            message: `Será descargada la ultima informacion del servidor`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'botton1',
                }, {
                    text: 'Confirmar',
                    cssClass: 'botton2',
                    handler: () => {
                        console.log('Descargando data');
                        this.events.publish('ReloadNotifications', true);
                        this.Complementos.PresentLoading('Descargando', 2000);
                        setTimeout(() => {
                            this.Complementos._MessageToast('Informacion descargada');
                        }, 2000);
                    }
                }
            ],
            cssClass: 'alertCustomCss' // <- added this
        });
        alert.present();
    }
    createEtiqueta() {
        this.navCtrl.push(EtiquetaPage);
    }
};
AjustesAppPage = __decorate([
    Component({
        selector: 'page-ajustes-app',
        templateUrl: 'ajustes-app.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        AlertController,
        ComplementosviewsProvider,
        SocialSharing,
        ApiProvider,
        Events,
        LinksRedirect,
        MenuController])
], AjustesAppPage);
export { AjustesAppPage };
//# sourceMappingURL=ajustes-app.js.map