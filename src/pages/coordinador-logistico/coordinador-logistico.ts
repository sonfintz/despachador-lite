import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {DetailvinculatePage} from "../detailvinculate/detailvinculate";

@Component({
  selector: 'page-coordinador-logistico',
  templateUrl: 'coordinador-logistico.html',
})
export class CoordinadorLogisticoPage {
  public form: FormGroup;
  public invitados: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public fb: FormBuilder,
              private Complementos: ComplementosviewsProvider,
              public Api: ApiProvider) {
    this.initForm();
    this.loadInvitados();
  }

  loadInvitados() {
    this.Api._GET('/despachador/invitados').subscribe((data) => {
      console.log('invitados', data);
      this.invitados = data;
    })
  }

  initForm() {
    this.form = this.fb.group({
      invitacion: [null, Validators.required]
    })
  }

  _ClearInput(value) {
    console.log('reseteando formulario', value);
    this.form.get(`${value}`).reset();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoordinadorLogisticoPage');
  }

  _DetailVinculado(data, idx) {
    let newData = data[idx];
    console.log('lo que se va a ir ', newData);
    this.navCtrl.push(DetailvinculatePage, {data: data})
  }


  _SendInvitacion() {
    console.log('enviando invitacion');
    this.Api._GET(`/despachador/invitar/${this.form.get('invitacion').value}`).subscribe((data) => {
      console.log(data);
      this.Complementos._MessageToast('Invitacion Enviada');
      this.form.get('invitacion').reset();
      this.loadInvitados();
    })
  }

}

