var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { FormBuilder, Validators } from "@angular/forms";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { DetailvinculatePage } from "../detailvinculate/detailvinculate";
let CoordinadorLogisticoPage = class CoordinadorLogisticoPage {
    constructor(navCtrl, navParams, fb, Complementos, Api) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.Complementos = Complementos;
        this.Api = Api;
        this.initForm();
        this.loadInvitados();
    }
    loadInvitados() {
        this.Api._GET('/despachador/invitados').subscribe((data) => {
            console.log('invitados', data);
            this.invitados = data;
        });
    }
    initForm() {
        this.form = this.fb.group({
            invitacion: [null, Validators.required]
        });
    }
    _ClearInput(value) {
        console.log('reseteando formulario', value);
        this.form.get(`${value}`).reset();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad CoordinadorLogisticoPage');
    }
    _DetailVinculado(data, idx) {
        let newData = data[idx];
        console.log('lo que se va a ir ', newData);
        this.navCtrl.push(DetailvinculatePage, { data: data });
    }
    _SendInvitacion() {
        console.log('enviando invitacion');
        this.Api._GET(`/despachador/invitar/${this.form.get('invitacion').value}`).subscribe((data) => {
            console.log(data);
            this.Complementos._MessageToast('Invitacion Enviada');
            this.form.get('invitacion').reset();
            this.loadInvitados();
        });
    }
};
CoordinadorLogisticoPage = __decorate([
    Component({
        selector: 'page-coordinador-logistico',
        templateUrl: 'coordinador-logistico.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        FormBuilder,
        ComplementosviewsProvider,
        ApiProvider])
], CoordinadorLogisticoPage);
export { CoordinadorLogisticoPage };
//# sourceMappingURL=coordinador-logistico.js.map