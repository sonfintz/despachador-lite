var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, ModalController, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { ReporteMrPage } from "../reporte-mr/reporte-mr";
let PopoverDespachoPage = class PopoverDespachoPage {
    constructor(navCtrl, Api, modalController, alertController, events, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.Api = Api;
        this.modalController = modalController;
        this.alertController = alertController;
        this.events = events;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.aux = false;
        this.Despacho = this.navParams.get('data');
        console.log('POPOVER', this.Despacho);
    }
    DespacharTransportista(id, data) {
        console.log('ID_SOLICITUD', id, 'DATA', data);
        const alert = this.alertController.create({
            title: '¿Aceptar transportista?',
            message: '<strong>Confirme su opcion</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah', blah);
                    }
                }, {
                    text: 'Aceptar',
                    handler: () => {
                        const Obj = {
                            ordenbascula: 0,
                            turnobascula: 0,
                            pesoentradabascula: 0,
                            pesosalidabascula: 0,
                            observacionesbascula: 0
                        };
                        this.Api._PUT(`/despachos/ruta/${this.Despacho.solicitudes_id},${data.usuario_id}`, Obj)
                            .subscribe((data) => {
                            if (data.success === true) {
                                this.Complementos._MessageToast('Transportista Despachado', 5000);
                                this.events.publish('despachado');
                                this.navCtrl.pop();
                                // --/Eliminar De la Lista.
                            }
                        }, (err) => {
                            console.log('Error:', err);
                        }, () => {
                        });
                    }
                }
            ]
        });
        alert.present();
        /*console.log('OBJ:', Obj);
        this.Api._PUT(`/despachos/${id}`, Obj)
          .subscribe(
            (data) => {
              console.log('Despachar', data);
              if (data.success === true) {
                this.Complementos._MessageToast('Transportista Despachado', 5000);
                // --/Eliminar De la Lista.
              }
            },
            (err) => {
              console.log('Error:', err);
            },
            () => {
            }
          );*/
    }
    GenerateRyM(data) {
        const alert = this.alertController.create({
            title: '¿Generar Remesa y Manifiesto?',
            message: '<strong>Confirme su opcion</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah', blah);
                    }
                }, {
                    text: 'Aceptar',
                    handler: () => {
                        this.GenerateR(data);
                    }
                }
            ]
        });
        alert.present();
    }
    GenerateR(data) {
        this.Complementos._MessageToast('Generando Remesa');
        console.log('Generando Remesa y Manifiesto', data);
        this.Api._GET(`/orden/generarremesa/${data.id_remesa}`).subscribe((data) => {
            console.log('Generado', data);
            this.Complementos._MessageToast('Remesa Generada');
            this.REMESA = data;
        }, (e) => {
            this.Complementos._MessageToast(`${e.error}`);
        }, () => {
            this.GenerateM(data);
            // return this.api.get(`orden/imprimirmanifiesto/transportista/${data.usuario_id}`);
        });
    }
    GenerateM(data) {
        this.Complementos._MessageToast('Generando Manifiesto');
        this.Api._GET(`/orden/generarmanifiesto/${data.id_manifiesto}`).subscribe((data) => {
            console.log('Generado', data);
            this.Complementos._MessageToast('Manifiesto Generado');
            this.MANIFIESTO = data;
        }, (e) => {
            this.Complementos._MessageToast(`${e.error.mensaje}`);
        }, () => {
            console.log('REMESA', this.REMESA, 'MANIFIESTO', this.MANIFIESTO);
            if (this.REMESA.success === true && this.MANIFIESTO.success === true) {
                this.aux = true;
            }
        });
    }
    ReporteRyM(data) {
        this.navCtrl.push(ReporteMrPage, { data: data });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad PopoverDespachoPage');
    }
};
PopoverDespachoPage = __decorate([
    Component({
        selector: 'page-popover-despacho',
        templateUrl: 'popover-despacho.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        ModalController,
        AlertController,
        Events,
        ComplementosviewsProvider,
        NavParams])
], PopoverDespachoPage);
export { PopoverDespachoPage };
//# sourceMappingURL=popover-despacho.js.map