import {Component, ViewChild} from '@angular/core';

import {
  AlertController,
  App,
  Events,
  ItemSliding,
  List,
  ModalController,
  NavController,
  PopoverController,
  Refresher
} from 'ionic-angular';
import {UserData} from '../../providers/user-data';

import {SolicitudDetallePage} from '../solicitud-detalle/solicitud-detalle';
import {ScheduleFilterPage} from '../schedule-filter/schedule-filter';
import {OfertasPage} from "../ofertas/ofertas";
import {ApiProvider} from "../../providers/api/api";
import {PopoverbuttonspagePage} from "../popoverbuttonspage/popoverbuttonspage";
import {DatabaseProvider} from "../../providers/database/database";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import * as moment from 'moment';

/*
  To learn how to use third party libs in an
  Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
*/

// import moment from 'moment';


@Component({
  selector: 'page-schedule',
  templateUrl: 'solicitudes.html',
  providers: [ApiProvider, DatabaseProvider]
})
export class SolicitudesPage {
  // the list is a child of the solicitudes page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  @ViewChild('scheduleList', {read: List}) scheduleList: List;

  Search = '';
  public segment = 'all';
  public segment2 = 'todas';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  private SOLICITUD_2: any;
  public SOLICITUD: any;
  public sFavorites: any = Array();
  private sFavorites2: any = Array();

  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public user: UserData,
    private Api: ApiProvider,
    public popoverCtrl: PopoverController,
    public events: Events,
    private serviceDB: DatabaseProvider,
    private Complementos: ComplementosviewsProvider
  ) {
    events.publish('tipoUsuario', localStorage.getItem("tipo"));
  }

  ionViewWillEnter() {
    this._SOLICITUDList();
  }

  /*
    ionViewDidLoad() {
      this.app.setTitle('Schedule');
      this._SOLICITUDList();
    }
  */
  /**---------------------------
   * METODO REFRESCAR PAGINAS VISTA
   */
  doRefresh(refresher: Refresher) {
    this._SOLICITUDList();
    setTimeout(() => {
      refresher.complete();

    }, 1000);

  }

  /**-----------------
   *  METODO CAPTURA SEGMENTO
   * -----------------
   */
  updateSegment() {
    if (this.segment == 'all') {
    }
    else {
      this.listFavorite();
    }
  }


  ngOnInit() {
    this.events.subscribe('list-solicitud', () => {
      this._SOLICITUDList();
    });
  }

  /**-----------------------
   * METODO POPOVER
   * ----------------------
   */
  presentPopover(myEvent, data: any) {
    let popover = this.popoverCtrl.create(PopoverbuttonspagePage, {data: data});
    popover.present({
      ev: myEvent
    });
  }

  /**--------------------------------
   *  METODO SEARCH SOLICITUDES
   * -------------------------------
   */
  SearchSolicitud() {
    if (this.segment == 'all') {
      console.log('SEGMENT', this.segment);
      if (this.Search) {
        console.log('SEARCH', this.Search);
        this.SOLICITUD = this.SOLICITUD_2.filter(item => item.producto_nombre.toLowerCase().includes(this.Search.toLowerCase()) ||
          item.destino_nombre.toLowerCase().includes(this.Search.toLowerCase()) ||
          item.origen_nombre.toLowerCase().includes(this.Search.toLowerCase()) ||
          item.solicitudes_codigo.toLowerCase().includes(this.Search.toLowerCase()));
      } else {
        this.SOLICITUD = this.SOLICITUD_2;
      }
    }
    /*else if (this.segment == 'favorites') {
      if (this.Search) {
        this.sFavorites = this.sFavorites2.filter(item =>
          item.cliente_nombre.toLowerCase().startsWith(this.Search.toLowerCase())).slice(0, 5);

      } else {
        this.sFavorites = this.sFavorites2;
      }
    }*/
  }

  /* METODO FILTER SOLICITUDES*/
  filterSolic(value) {
    this.segment2 = value;
    if (value === 'todas') {
      this.Api._GET('/solicitudes').subscribe(
        (data) => {
          data.map((i) => {
            i.solicitudes_fechahoraregistro = moment(i.solicitudes_fechahoraregistro).format('MM/DD/YYYY HH:mm a');
            i.solicitudes_fechacarga = moment(i.solicitudes_fechacarga).format('DD-MM-YYYY');
            i.solicitudes_fechaentrega = moment(i.solicitudes_fechaentrega).format('DD-MM-YYYY');
            i.solicitudes_fechahoraupdate = moment(i.solicitudes_fechahoraupdate).format('DD-MM-YYYY');
          });
          this.SOLICITUD_2 = data;
          this.SOLICITUD = data;
          console.log('SOLICITUDES:', this.SOLICITUD);
        },
        (err) => {
          if (err.status === 401) {
            // this.events.publish('sesionDuplicada', err)
          } else {
            console.error(err)
          }
        }, () => {
          // this.Complementos.loader.dissmiss();
        }
      );
    }
    if (value === 'pendiente') {
      this.Api._GET('/solicitudes').subscribe(
        (data) => {
          data = data.filter(i => i.solicitudes_estado === 'PENDIENTE');
          data.map((i) => {
            i.solicitudes_fechahoraregistro = moment(i.solicitudes_fechahoraregistro).format('MM/DD/YYYY HH:mm a');
            i.solicitudes_fechacarga = moment(i.solicitudes_fechacarga).format('DD-MM-YYYY');
            i.solicitudes_fechaentrega = moment(i.solicitudes_fechaentrega).format('DD-MM-YYYY');
            i.solicitudes_fechahoraupdate = moment(i.solicitudes_fechahoraupdate).format('DD-MM-YYYY');
          });
          this.SOLICITUD_2 = data;
          this.SOLICITUD = data;
          console.log('SOLICITUDES:', this.SOLICITUD);
        },
        (err) => {
          if (err.status === 401) {
            // this.events.publish('sesionDuplicada', err)
          } else {
            console.error(err)
          }
        }, () => {
          // this.Complementos.loader.dissmiss();
        }
      );
    }
    if (value === 'proceso') {
      this.Api._GET('/solicitudes').subscribe(
        (data) => {
          data = data.filter(i => i.solicitudes_estado === 'PROCESANDO');
          data.map((i) => {
            i.solicitudes_fechahoraregistro = moment(i.solicitudes_fechahoraregistro).format('MM/DD/YYYY HH:mm a');
            i.solicitudes_fechacarga = moment(i.solicitudes_fechacarga).format('DD-MM-YYYY');
            i.solicitudes_fechaentrega = moment(i.solicitudes_fechaentrega).format('DD-MM-YYYY');
            i.solicitudes_fechahoraupdate = moment(i.solicitudes_fechahoraupdate).format('DD-MM-YYYY');
          });
          this.SOLICITUD_2 = data;
          this.SOLICITUD = data;
          console.log('SOLICITUDES:', this.SOLICITUD);
        },
        (err) => {
          if (err.status === 401) {
            // this.events.publish('sesionDuplicada', err)
          } else {
            console.error(err)
          }
        }, () => {
          // this.Complementos.loader.dissmiss();
        }
      );
    }
    if (value === 'concluidas') {
      this.Api._GET('/solicitudes').subscribe(
        (data) => {
          data = data.filter(i => i.solicitudes_estado === 'CULMINADO');
          data.map((i) => {
            i.solicitudes_fechahoraregistro = moment(i.solicitudes_fechahoraregistro).format('MM/DD/YYYY HH:mm a');
            i.solicitudes_fechacarga = moment(i.solicitudes_fechacarga).format('DD-MM-YYYY');
            i.solicitudes_fechaentrega = moment(i.solicitudes_fechaentrega).format('DD-MM-YYYY');
            i.solicitudes_fechahoraupdate = moment(i.solicitudes_fechahoraupdate).format('DD-MM-YYYY');
          });
          this.SOLICITUD_2 = data;
          this.SOLICITUD = data;
          console.log('SOLICITUDES:', this.SOLICITUD);
        },
        (err) => {
          if (err.status === 401) {
            // this.events.publish('sesionDuplicada', err)
          } else {
            console.error(err)
          }
        }, () => {
          // this.Complementos.loader.dissmiss();
        }
      );
    }
  }


  /**------------------------------------------
   *                                          @
   *       CONSULTAS API BACKEND              @
   *                                          @
   * ------------------------------------------
   */
  /**---/ Consultar Solicitudes **/
  _SOLICITUDList() {
    // this.Complementos.PresentLoading('¡Buscando Solicitudes!', 200);
    this.Api._GET('/solicitudes').subscribe(
      (data) => {
        data.map((i) => {
          i.solicitudes_fechahoraregistro = moment(i.solicitudes_fechahoraregistro).format('MM/DD/YYYY HH:mm a');
          i.solicitudes_fechacarga = moment(i.solicitudes_fechacarga).format('DD-MM-YYYY');
          i.solicitudes_fechaentrega = moment(i.solicitudes_fechaentrega).format('DD-MM-YYYY');
          i.solicitudes_fechahoraupdate = moment(i.solicitudes_fechahoraupdate).format('DD-MM-YYYY');
        });
        this.SOLICITUD_2 = data;
        this.SOLICITUD = data;
        console.log('SOLICITUDES:', this.SOLICITUD);
      },
      (err) => {
        if (err.status === 401) {
          // this.events.publish('sesionDuplicada', err)
        } else {
          console.error(err)
        }
      }, () => {
        // this.Complementos.loader.dissmiss();
      }
    );
  }

  /**-------------------------------------
   *      Fin de las consultas BACKEND
   *---------------------------------------
   */




  presentFilter() {
    let modal = this.modalCtrl.create(ScheduleFilterPage, this.excludeTracks);
    modal.present();

    modal.onWillDismiss((data: any[]) => {
      if (data) {
        this.excludeTracks = data;

      }
    });

  }


  /**-----------------------------------------------------------------------------------------
   *            CRUD  DATABASE SQLITE
   */

  /** AGREGAR FAVORITO **/
  addFavorite(slidingItem: ItemSliding, item: any) {

    this.serviceDB.insertSolicitudFavorite(JSON.stringify(item), parseInt(localStorage.getItem('idUserMUEVE')))
      .then(
        (data) => {
          console.log('FUNCION addFavorite:', data);
          this.Complementos._MessageToast('¡Agregado a Favoritos!');
          slidingItem.close();
        },
        (err) => {
          console.log('ERR FUNCION addFavorite:', err);
          this.Complementos._MessageToast('¡No se a podido añadir a Favoritos!');
          slidingItem.close();
        }
      );

  }

  /** LISTAR FAVORITOS **/

  listFavorite() {
    this.sFavorites.splice(0, this.sFavorites.length);
    this.sFavorites2.splice(0, this.sFavorites2.length);
    this.serviceDB.listSolicitudFavorite(parseInt(localStorage.getItem('idUserMUEVE')))
      .then(
        (data: any) => {
          console.log('FUNCION ListFavorite:', data);
          data.forEach((e) => {
            let a = JSON.parse(e.solicitud);
            a.id_sql = e.id;
            this.sFavorites.push(a);
            this.sFavorites2.push(a);
          });


        },
        (err) => {
          console.log('ERR FUNCION ListFavorite:', err);
        }
      );
  }

  /** ELIMINAR TODOS LOS FAVORITOS **/
  DeleteAll() {
    this.serviceDB.DeleteAllFavorites(localStorage.getItem('idUserMUEVE'))
      .then(
        (data) => {
          console.log('SUCCES DELETE ALL:', data)
        },
        (err) => {
          console.log('ERROR DELETE ALL:', err)
        }
      )
  }

  /** REMOVER UN FAVORITO **/
  removeFavorite(slidingItem: ItemSliding, id_sql) {
    let alert = this.alertCtrl.create({
      title: 'Seguro que decea Eliminar',
      message: '¡Esta solicitud se eliminara de la lista de  favoritos!',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            // they clicked the cancel button, do not remove the session
            // close the sliding item and hide the option buttons
            slidingItem.close();
          }
        },
        {
          text: 'Remove',
          handler: () => {
            this.serviceDB.DeleteFavorites(id_sql, localStorage.getItem('idUserMUEVE'))
              .then(
                (data) => {
                  console.log('DELETE FAVORITE DATA:', data);
                  this.listFavorite();
                },
                (err) => {
                  console.log('Error favorite data', err);
                }
              );
          }
        }
      ]
    });
    // now present the alert on top of all other content
    alert.present();
  }

  /**-----------------------------------------------------------------------------------------
   *        FIN    CRUD  DATABASE SQLITE
   */


  /** --------------------------------------------------
   *          METODOS PUSH PARA CARGAR NUEVA VISTA
   *
   */
  /** --/ DETALLE DE LA SOLICITUD **/
  goToSolicitudDetail(Data: any) {
    this.Api._GET(`/solicitudes/despachador/${Data.solicitudes_id}`).subscribe(
      (data) => this.navCtrl.push(SolicitudDetallePage, {data: Data, detalle: data[0]}),
      (err) => {
        if (err.status === 401) {
          // this.events.publish('sesionDuplicada', err)
        } else {
          console.error(err)
        }
      }
    )
  }

  /** --/ CREAR UNA OFERTA **/
  _CrearOferta() {
    this.navCtrl.push(OfertasPage, {});
  }


}
