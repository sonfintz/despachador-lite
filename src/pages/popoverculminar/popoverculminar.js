var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, Events, NavController, NavParams } from 'ionic-angular';
import { EventosTransportistaPage } from "../eventos-transportista/eventos-transportista";
import { ApiProvider } from "../../providers/api/api";
import { ReporteMrPage } from "../reporte-mr/reporte-mr";
let PopoverculminarPage = class PopoverculminarPage {
    constructor(navCtrl, api, events, alertController, navParams) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.events = events;
        this.alertController = alertController;
        this.navParams = navParams;
        this.TRANSPORTISTA = this.navParams.get('data');
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad PopoverculminarPage');
    }
    _Evento() {
        this.navCtrl.push(EventosTransportistaPage, { data: this.navParams.get('data') });
        console.log('creando evento');
    }
    _VerORM() {
        this.navCtrl.push(ReporteMrPage, { data: this.TRANSPORTISTA });
    }
    _Culminar() {
        let solicitudes_id = this.TRANSPORTISTA.solicitudes_id;
        let usuario_id = this.TRANSPORTISTA.usuario_id;
        console.log('Culminar transportista', solicitudes_id, usuario_id);
        const alert = this.alertController.create({
            title: '¿Culminar transportista?',
            message: '<strong>Confirme su opcion</strong>!!!',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah', blah);
                    }
                }, {
                    text: 'Aceptar',
                    handler: () => {
                        let Obj = {
                            "ordenpuerto": 0,
                            "pesopuerto": 0
                        };
                        console.log('SOLICITUD', solicitudes_id, 'USUARIO', usuario_id);
                        this.api._PUT(`/despachos/culminado/${solicitudes_id},${usuario_id}`, Obj).subscribe((data) => {
                            console.log(data);
                            this.events.publish('evento-created');
                            this.navCtrl.pop();
                        });
                    }
                }
            ]
        });
        alert.present();
        // return this.presentAlertConfirm(solicitudes_id, usuario_id);
    }
};
PopoverculminarPage = __decorate([
    Component({
        selector: 'page-popoverculminar',
        templateUrl: 'popoverculminar.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        Events,
        AlertController,
        NavParams])
], PopoverculminarPage);
export { PopoverculminarPage };
//# sourceMappingURL=popoverculminar.js.map