import {Component} from '@angular/core';
import {AlertController, Events, NavController, NavParams} from 'ionic-angular';
import {EventosTransportistaPage} from "../eventos-transportista/eventos-transportista";
import {ApiProvider} from "../../providers/api/api";
import {ReporteMrPage} from "../reporte-mr/reporte-mr";

@Component({
  selector: 'page-popoverculminar',
  templateUrl: 'popoverculminar.html',
})
export class PopoverculminarPage {
  private TRANSPORTISTA: any;

  constructor(public navCtrl: NavController,
              public api: ApiProvider,
              public events: Events,
              public alertController: AlertController,
              public navParams: NavParams) {
    this.TRANSPORTISTA = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverculminarPage');
  }

  _Evento() {
    this.navCtrl.push(EventosTransportistaPage, {data: this.navParams.get('data')});
    console.log('creando evento')
  }

  _VerORM() {
    this.navCtrl.push(ReporteMrPage, {data: this.TRANSPORTISTA})
  }

  _Culminar() {
    let solicitudes_id = this.TRANSPORTISTA.solicitudes_id;
    let usuario_id = this.TRANSPORTISTA.usuario_id;
    console.log('Culminar transportista', solicitudes_id, usuario_id);
    const alert = this.alertController.create({
      title: '¿Culminar transportista?',
      message: '<strong>Confirme su opcion</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah', blah);
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            let Obj = {
              "ordenpuerto": 0,
              "pesopuerto": 0
            };
            console.log('SOLICITUD', solicitudes_id, 'USUARIO', usuario_id);
            this.api._PUT(`/despachos/culminado/${solicitudes_id},${usuario_id}`, Obj).subscribe((data) => {
              console.log(data);
              this.events.publish('evento-created')
              this.navCtrl.pop();
            })
          }
        }
      ]
    });
    alert.present();
    // return this.presentAlertConfirm(solicitudes_id, usuario_id);
  }

}
