var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Events, NavController, NavParams, PopoverController, Searchbar, ToastController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { PopoverDespachoPage } from "../popover-despacho/popover-despacho";
import { PopoverculminarPage } from "../popoverculminar/popoverculminar";
import { AcceptPopoverPage } from "../accept-popover/accept-popover";
import { GenerateCumplidoPage } from "../generate-cumplido/generate-cumplido";
import { AsignarTransportistasPage } from "../asignar-transportistas/asignar-transportistas";
let DespacharTrasportistasPage = class DespacharTrasportistasPage {
    constructor(navCtrl, navParams, popoverCtrl, events, Api, Complementos, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.Api = Api;
        this.Complementos = Complementos;
        this.toastCtrl = toastCtrl;
        this.LIST_OF_TRASPORTISTS = Array();
        this.LIST_OF_TRASPORTISTS_2 = Array();
        this.TRASPORTISTAS_ENTURNADOS = Array();
        this.TRASPORTISTAS_ENTURNADOS_2 = Array();
        this.segment = 'despachar';
        // this._TRASPORTISTAS_EN_ENTURNAMIENTO();
        this.solicitud = this.navParams.get('data');
        this._ListaTrasportistasDespachados('asignados');
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DespacharTrasportistasPage');
    }
    ionViewWillEnter() {
        this._ListaTrasportistasDespachados(this.segment);
    }
    doRefresh(refresher) {
        this._ListaTrasportistasDespachados(this.segment);
        setTimeout(() => {
            refresher.complete();
        }, 1000);
    }
    /*LISTAR TRANSPORTISTAS*/
    _ListaTrasportistasDespachados(value) {
        this.segment = value;
        // this.Complementos.PresentLoading('¡Cargando Transportistas!', 200);
        if (value === 'asignados') {
            // this.Api._GET(`/despachos/${localStorage.getItem('idUserMUEVE')}`)
            this.Api._GET(`/despachos/asignados/${this.solicitud.solicitudes_id}`)
                .subscribe((data) => {
                console.log('Despachar', data);
                /* console.group('Lista de VEHICULOS DESPACHADOS');
                 console.groupEnd();*/
                this.LIST_OF_TRASPORTISTS = data;
                this.LIST_OF_TRASPORTISTS_2 = data;
            });
        }
        if (value === 'despachar') {
            this.Api._GET(`/despachos/listado/${this.solicitud.solicitudes_id}`)
                .subscribe((data) => {
                data = data.filter(i => i.despacho_estado === 'DESPACHADO');
                console.log('Despachados', data);
                /* console.group('Lista de VEHICULOS DESPACHADOS');
                 console.groupEnd();*/
                this.LIST_OF_TRASPORTISTS = data;
                this.LIST_OF_TRASPORTISTS_2 = data;
            });
        }
        if (value === 'ruta') {
            this.Api._GET(`/despachos/listado/${this.solicitud.solicitudes_id}`)
                .subscribe((data) => {
                data = data.filter(i => i.despacho_estado === 'EN RUTA');
                console.log('En ruta', data);
                /* console.group('Lista de VEHICULOS DESPACHADOS');
                 console.groupEnd();*/
                this.LIST_OF_TRASPORTISTS = data;
                this.LIST_OF_TRASPORTISTS_2 = data;
            });
        }
        if (value === 'culminado') {
            this.Api._GET(`/despachos/listado/${this.solicitud.solicitudes_id}`)
                .subscribe((data) => {
                data = data.filter(i => i.despacho_estado === 'CULMINADO');
                console.log('CULMINADO', data);
                /* console.group('Lista de VEHICULOS DESPACHADOS');
                 console.groupEnd();*/
                this.LIST_OF_TRASPORTISTS = data;
                this.LIST_OF_TRASPORTISTS_2 = data;
            });
            // this.Api._GET(`/despachos/culminado/`)
            // this.Api._GET(`/despachos/culminado`)
            //   .subscribe(
            //     (data) => {
            //       console.log('Culminados', data);
            //       /* console.group('Lista de VEHICULOS DESPACHADOS');
            //        console.groupEnd();*/
            //       this.LIST_OF_TRASPORTISTS = data;
            //       this.LIST_OF_TRASPORTISTS_2 = data;
            //     }
            //   );
        }
    }
    /*_TRASPORTISTAS_EN_ENTURNAMIENTO(){
      this.Api._POST('/enturnamiento',{id:this.navParams.get('data').id})
        .subscribe((data)=>{
          console.log('TRASPORTISTAS ENTURNADOS-->',data);
          this.TRASPORTISTAS_ENTURNADOS = data;
          this.TRASPORTISTAS_ENTURNADOS_2 = data;
  
        });
    }
    /!** //--------------Metodo para Refrescar la vista **!/
    _RefrescarData() {
      this._TRASPORTISTAS_EN_ENTURNAMIENTO();
    }*/
    /*
    * BUSQUEDA POR PLACA
    * */
    Search_for_placa() {
        if (this.segment === 'asignados') {
            if (this.SearchPlaca) {
                console.log('despachar', this.SearchPlaca);
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2.filter(item => item.vehiculo_placa.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.origen_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.producto_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_telefono.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.destino_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()));
            }
            else {
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2;
            }
        }
        if (this.segment === 'despachar') {
            if (this.SearchPlaca) {
                console.log('despachados', this.SearchPlaca);
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2.filter(item => item.vehiculo_placa.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.producto_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.origen_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_telefono.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.destino_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()));
            }
            else {
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2;
            }
        }
        if (this.segment === 'ruta') {
            if (this.SearchPlaca) {
                console.log('En ruta', this.SearchPlaca);
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2.filter(item => item.vehiculo_placa.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.origen_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_telefono.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.producto_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.destino_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()));
            }
            else {
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2;
            }
        }
        if (this.segment === 'culminado') {
            if (this.SearchPlaca) {
                console.log('culminados', this.SearchPlaca);
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2.filter(item => item.vehiculo_placa.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.origen_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.persona_telefono.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.producto_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()) ||
                    item.destino_nombre.toLowerCase().includes(this.SearchPlaca.toLowerCase()));
            }
            else {
                this.LIST_OF_TRASPORTISTS = this.LIST_OF_TRASPORTISTS_2;
            }
        }
    }
    ngOnInit() {
        this.events.subscribe('despachado', () => {
            this._ListaTrasportistasDespachados(this.segment);
        });
        this.SOLICITUD = this.navParams.get('data');
        console.log(this.SOLICITUD);
        this.events.subscribe('evento-created', () => {
            this._ListaTrasportistasDespachados(this.segment);
        });
    }
    /*-----------------------
    * METODOS POPOVER
    * -----------------------
    * */
    /*ACEPTAR TRANSPORTISTAS ASIGNADOS*/
    acepptPopover(myEvent, data) {
        console.log(data);
        let popover = this.popoverCtrl.create(AcceptPopoverPage, { data: data });
        popover.present({
            ev: myEvent
        });
    }
    /*DESPACHAR TRANSPORTISTAS*/
    eventPopover(myEvent, data) {
        let popover = this.popoverCtrl.create(PopoverDespachoPage, { data: data });
        popover.present({
            ev: myEvent
        });
    }
    /*CULMINAR TRANSPORTISTA EN RUTA*/
    eventPopoverCulminar(myEvent, data) {
        let popover = this.popoverCtrl.create(PopoverculminarPage, { data: data });
        popover.present({
            ev: myEvent
        });
    }
    /*
    * Metodo para Eliminar el Trasportista
    * */
    deleteItem(id, _index, _data) {
        /*  console.log('id_solicitud', id, 'index', _index, 'data', _data);
          console.log("DELTED", id);*/
        let copiaSuperficial = this.TRASPORTISTAS_ENTURNADOS.slice();
        this.TRASPORTISTAS_ENTURNADOS.splice(_index, 1);
        let toast = this.toastCtrl.create({
            message: "Eliminado item",
            duration: 3000,
            showCloseButton: true,
            closeButtonText: "Cancelar"
        });
        toast.onDidDismiss((data, role) => {
            console.log("Toast buton clicked", role, copiaSuperficial, data);
            if (role == "close") {
                console.log('Cerrado', _data, _index);
                this.TRASPORTISTAS_ENTURNADOS = copiaSuperficial;
                console.log(this.TRASPORTISTAS_ENTURNADOS);
            }
            else {
                let trasportistas = {
                    id_solicitud: id,
                    id_usuario: _data.usuario_id
                };
                //--/Limpiando Array de copia.
                copiaSuperficial.splice(0, copiaSuperficial.length);
                this._EliminarTrasportista(trasportistas);
                console.warn('Eliminando', copiaSuperficial);
            }
            ///undo operation
        });
        toast.present().then(() => {
            console.info('Toast,Lanzado');
        });
    }
    /* deleteItem2(id, _index, _data) {
       console.log("DELTED LARRY", id);
       let copiaSuperficial: any = this.LIST_OF_TRASPORTISTS.slice();
       this.LIST_OF_TRASPORTISTS.splice(_index, 1);
  
  
       let toast = this.toastCtrl.create({
         message: "Eliminado item",
         duration: 3000,
         showCloseButton: true,
         closeButtonText: "Cancelar"
       });
  
       toast.onDidDismiss((data, role) => {
         console.log("Toast buton clicked", role, copiaSuperficial, data);
         if (role == "close") {
           console.log('Cerrado', _data, _index);
           this.LIST_OF_TRASPORTISTS = copiaSuperficial;
           console.log(this.LIST_OF_TRASPORTISTS);
         } else {
  
           let trasportistas = {
             id_solicitud: _data.id_solicitud,
             id: _data.id
           };
  
           //--/Limpiando Array de copia.
           copiaSuperficial.splice(0, copiaSuperficial.length);
           this._EliminarTrasportistaRemover2(trasportistas);
           console.warn('Eliminando', copiaSuperficial);
         }
  
         ///undo operation
       });
       toast.present().then(() => {
         console.info('Toast,Lanzado');
       });
  
     }*/
    generateCumplido(data) {
        let popover = this.popoverCtrl.create(GenerateCumplidoPage, { data: data });
        popover.present();
    }
    /**---------------------------------------------------------------
     *  Metodos para Despachar(PUT) y Eliminar Trasportistas(DELETE)
     */
    DespacharTransportista(id, data) {
        console.log('ID_SOLICITUD', id, 'DATA', data);
        const Obj = {
            usuario_id: data.usuario_id,
            despacho_estado: 'DESPACHADO'
        };
        console.log('OBJ:', Obj);
        this.Api._PUT(`/despachos/${id}`, Obj)
            .subscribe((data) => {
            console.log('Despachar', data);
            if (data.success === true) {
                this.Complementos._MessageToast('Transportista Despachado', 5000);
                this._ListaTrasportistasDespachados('asignados');
                // --/Eliminar De la Lista.
                this.TRASPORTISTAS_ENTURNADOS.forEach((item, index) => {
                    if (item.id === id) {
                        this.TRASPORTISTAS_ENTURNADOS.splice(index, 1);
                    }
                });
            }
        }, (err) => {
            console.log('Error:', err);
        }, () => {
        });
    }
    _EliminarTrasportista(ObjAssing) {
        console.info('|ELIMINANDO|', ObjAssing);
        this.Api._DELETE(`/despachos/${ObjAssing.id_solicitud},${ObjAssing.id_usuario}`)
            .subscribe((data) => {
            console.group('Trasportista eliminado de la asignacion:', data);
            this._ListaTrasportistasDespachados('asignados');
            console.groupEnd();
        }, (err) => {
            console.warn('Error de BackEnd:', err);
        }, () => {
            // this._RefrescarData();
        });
    }
    /*_EliminarTrasportistaRemover2(ObjAssing) {
      console.info('||', ObjAssing);
  
      this.Api._POST(`/enturnamiento/remover2`, ObjAssing)
        .subscribe(
          (data) => {
            console.group('Trasportista eliminado de la asignacion:', data);
            console.groupEnd();
          },
          (err) => {
            console.warn('Error de BackEnd:', err)
          },
          () => {
            // this._RefrescarData();
          }
        );
    }*/
    /**----------------------------------------------------------------
     * Fin Metodos para Despachar(POST) y Eliminar Trasportistas(POST)
     *-----------------------------------------------------------------
     */
    _CrearOferta() {
        this.navCtrl.push(AsignarTransportistasPage, { data: this.solicitud });
    }
};
__decorate([
    ViewChild(Searchbar),
    __metadata("design:type", Searchbar)
], DespacharTrasportistasPage.prototype, "searchbar", void 0);
DespacharTrasportistasPage = __decorate([
    Component({
        selector: 'page-despachar-trasportistas',
        templateUrl: 'despachar-trasportistas.html',
        providers: [ApiProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        PopoverController,
        Events,
        ApiProvider,
        ComplementosviewsProvider,
        ToastController])
], DespacharTrasportistasPage);
export { DespacharTrasportistasPage };
//# sourceMappingURL=despachar-trasportistas.js.map