var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import * as moment from "moment";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { distinctUntilChanged, filter, switchMap } from "rxjs/operators";
import { TransportistasListPage } from "../transportistas-list/transportistas-list";
let EditarvehiculoPage = class EditarvehiculoPage {
    constructor(navCtrl, fb, api, events, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.api = api;
        this.events = events;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.placa_repetida = false;
        this.poliza_repetida = false;
        this.myMoment = moment();
        this.TRANSPORTISTA = this.navParams.get('data')[0];
        console.log('datos del carro', this.TRANSPORTISTA);
    }
    getCurrentTime() {
        return moment().format('YYYY-MM-DD');
    }
    ngOnInit() {
        // this.getHeads();
        this.loadData();
        this.initForm();
        console.log(this.TRANSPORTISTA);
        // this.setVehiculoId(this.data);
    }
    initForm() {
        this.form = this.fb.group({
            identificacion: [this.TRANSPORTISTA.identificacion, [Validators.required]],
            nombre: [this.TRANSPORTISTA.nombre, [Validators.required]],
            placa: [this.TRANSPORTISTA.placa, [Validators.required]],
            id_tipovehiculo: [this.TRANSPORTISTA.id_tipovehiculo, [Validators.required]],
            id_carroceria: [this.TRANSPORTISTA.id_carroceria, [Validators.required]],
            id_color: [this.TRANSPORTISTA.id_color, [Validators.required]],
            id_marca: [this.TRANSPORTISTA.id_marca, [Validators.required]],
            id_linea: [this.TRANSPORTISTA.id_linea, [Validators.required]],
            peso: [this.TRANSPORTISTA.peso, [Validators.required]],
            id_aseguradora: [this.TRANSPORTISTA.id_aseguradora, [Validators.required]],
            fechavencesoat: [this.TRANSPORTISTA.fechavencesoat, [Validators.required]],
            poliza: [this.TRANSPORTISTA.poliza],
            fabricacion: [this.TRANSPORTISTA.fabricacion, [Validators.required]],
            id_combustible: [1],
        });
        this.formValueChanges();
    }
    formValueChanges() {
        this.form.get('id_marca').valueChanges.pipe(distinctUntilChanged(), filter((i) => {
            return Boolean(i);
        }), switchMap((v) => {
            return this.listarLinea(v);
        })).subscribe((data) => {
            console.log('SI LLEGO!!!', data);
            this.data_linea = data;
        });
        this.form.get('id_marca').valueChanges
            .pipe(distinctUntilChanged()).subscribe(() => {
            this.form.get('id_linea').reset();
        });
    }
    listarLinea(id) {
        return this.api._GET(`/linea/${id}`);
    }
    loadData() {
        this.api._GET('/tipovehiculo').subscribe(vehiculo => {
            this.data_vehiculo = vehiculo;
            console.log('vehiculo', vehiculo);
        });
        this.api._GET('/aseguradora').subscribe(aseguradora => {
            this.data_aseguradora = aseguradora;
            console.log('aseguradora', aseguradora);
        });
        this.api._GET('/color').subscribe(color => {
            this.data_color = color;
            console.log('color', color);
        });
        this.api._GET('/combustible').subscribe(combustible => {
            this.data_combustible = combustible;
            console.log('combustible', combustible);
        });
        this.api._GET('/carroceria').subscribe(carroceria => {
            this.data_carroceria = carroceria;
            console.log('carroceria', carroceria);
        });
        this.api._GET('/marca').subscribe(marca => {
            this.data_marca = marca;
            console.log('marca', marca);
        });
    }
    /*TYPEAHEAD*/
    _Carroceria(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_carroceria').setValue(datos.data);
        }
        else {
            this.form.get('id_carroceria').setValue('');
        }
    }
    _ColorTrailer(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_color').setValue(datos.data);
        }
        else {
            this.form.get('id_color').setValue('');
        }
    }
    _MarcaVehiculo(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_marca').setValue(datos.data);
        }
        else {
            this.form.get('id_marca').setValue('');
        }
    }
    _TipoLinea(datos) {
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_linea').setValue(datos.data);
        }
        else {
            this.form.get('id_linea').setValue('');
        }
    }
    _Aseguradora(datos) {
        if (datos.data != null) {
            console.log('Output aseguradora', datos);
            this.form.get('id_aseguradora').setValue(datos.data);
        }
        else {
            this.form.get('id_aseguradora').setValue('');
        }
    }
    saveData() {
        console.log(this.form.value);
        this.api._PUT(`/vehiculo/${this.TRANSPORTISTA.id}`, this.form.value).subscribe(() => {
            this.events.publish('list-transport');
            this.navCtrl.setRoot(TransportistasListPage);
            this.Complementos._MessageToast(`Vehiculo editado exitosamente`);
        }, (e) => {
            this.Complementos._MessageToast(`${e.error.mensaje}`);
        });
    }
    /*
    * COMPROBAR CAMPO PLACA Y POLIZA
    * */
    comprobarCampo(campo) {
        switch (campo) {
            case 'placa':
                const placa = this.form.value.placa;
                if (placa !== null) {
                    this.api._GET(`/vehiculo/placa/${placa}`).subscribe((item) => {
                        if (item.existe === 'SI') {
                            this.Complementos._MessageToast('Placa existente');
                            console.log('Aqui el login igual');
                            this.form.get('placa').reset();
                            this.placa_repetida = true;
                        }
                        else {
                            this.placa_repetida = false;
                            console.log('no iguales');
                        }
                    });
                }
                else {
                    this.Complementos._MessageToast('Debes ingresar una placa');
                }
                break;
            case 'poliza':
                const poliza = this.form.value.poliza;
                if (poliza !== null) {
                    this.api._GET(`/vehiculo/poliza/${poliza}`).subscribe((item) => {
                        if (item.existe === 'SI') {
                            console.log('Aqui la licencia igual');
                            this.poliza_repetida = true;
                            this.Complementos._MessageToast('Poliza existente');
                            this.form.get('poliza').reset();
                        }
                        else {
                            this.poliza_repetida = false;
                            console.log('no iguales');
                        }
                    });
                }
                else {
                    this.Complementos._MessageToast('Debes ingresar una poliza');
                }
                break;
        }
    }
};
EditarvehiculoPage = __decorate([
    Component({
        selector: 'page-editarvehiculo',
        templateUrl: 'editarvehiculo.html',
    }),
    __metadata("design:paramtypes", [NavController,
        FormBuilder,
        ApiProvider,
        Events,
        ComplementosviewsProvider,
        NavParams])
], EditarvehiculoPage);
export { EditarvehiculoPage };
//# sourceMappingURL=editarvehiculo.js.map