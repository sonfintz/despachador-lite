import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

@Component({
  selector: 'page-topdespachadores',
  templateUrl: 'topdespachadores.html',
})
export class TopdespachadoresPage {
  public listado: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
    this.listado = this.navParams.get('data');
    console.log(this.listado);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopdespachadoresPage');
  }

}
