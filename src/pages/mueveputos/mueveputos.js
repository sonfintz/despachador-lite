var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { ActivityPage } from "../activity/activity";
import { ApiProvider } from "../../providers/api/api";
import { TopdespachadoresPage } from "../topdespachadores/topdespachadores";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
import { GpsProvider } from "../../providers/gps/gps";
import { TopganadoresPage } from "../topganadores/topganadores";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { zip } from "rxjs/observable/zip";
import { BonusPage } from "../bonus/bonus";
import { RetosPage } from "../retos/retos";
import { LogrosPage } from "../logros/logros";
import { LinksRedirect } from "../../providers/linksRedirect";
let MueveputosPage = class MueveputosPage {
    constructor(navCtrl, Api, events, linksgo, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.Api = Api;
        this.events = events;
        this.linksgo = linksgo;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.ErrorConexion = false;
        this._IMG = false;
        this.events.publish('perfilreload', true);
        this._MuevePuntos();
    }
    get Notificacion() {
        return DataglobalProvider.notificacion;
    }
    get BadConection() {
        return GpsProvider.Band_Conexion;
    }
    _DEFAULT_ImagenPerfil(event) {
        event.target.src = 'assets/imgs/icon/m_f_perfil.svg';
    }
    get ImagenPerfil() {
        return DataglobalProvider.ImagenPerfil;
    }
    get DataPerfil() {
        return DataglobalProvider.dataPerfil;
    }
    _MuevePuntos() {
        this.Api._GET('/estadisticas/rango').subscribe((data) => {
            this.ErrorConexion = false;
            console.log('puntos', data);
            this.Porcentaje = Number(data.porc);
            this.PorcentajeDespacho = Number(data.porcdespacho);
            console.log(this.Porcentaje);
            console.log(this.PorcentajeDespacho);
            this.Puntos = data;
        }, (err) => {
            if (err.status == 0) {
                this.ErrorConexion = true;
            }
            console.log(err);
        });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad MueveputosPage');
    }
    _Notificaciones() {
        this.events.publish('clear-notificaciones');
        this.navCtrl.push(ActivityPage);
    }
    topDespachadores() {
        this.Api._GET('/estadisticas/ranking').subscribe((ranking) => {
            this.Complementos.PresentLoading('cargando', 1000);
            this.navCtrl.push(TopdespachadoresPage, { data: ranking });
        }, (err) => {
            console.log(err);
            this.Complementos._MessageToast(`${err.error.mensaje}`);
        });
    }
    GoGanadores() {
        zip(this.Api._GET('/ganadores'), this.Api._GET('/avances/puntos')).subscribe(([ganadores, avancesPuntos]) => {
            this.Complementos.PresentLoading('cargando', 1000);
            console.log('ganadores', ganadores);
            console.log('avancesPuntos', avancesPuntos);
            this.navCtrl.push(TopganadoresPage, { data: ganadores });
        }, (err) => {
            this.Complementos._MessageToast(`${err.error.mensaje}`);
        });
    }
    goPage(pagina) {
        switch (pagina) {
            case 'logros':
                this.DataZip(LogrosPage, 'Logro');
                break;
            case 'retos':
                this.DataZip(RetosPage, 'Reto MP');
                break;
            case 'bonus':
                this.DataZip(BonusPage, 'Bonus MP');
                break;
        }
    }
    DataZip(page, filter) {
        zip(this.Api._GET('/avances'), this.Api._GET('/avances/puntos')).subscribe(([data, avancesPuntos]) => {
            data = data.filter(i => i.design_tipo === filter);
            this.Complementos.PresentLoading('cargando', 1000);
            console.log(`${filter}`, data);
            console.log('avancesPuntos', avancesPuntos);
            this.navCtrl.push(page, { data: data, value: avancesPuntos });
        }, (err) => {
            this.Complementos._MessageToast(`${err.error.mensaje}`);
        });
    }
    TipoPunto(categoria) {
        let label;
        switch (categoria) {
            case 'GOLD':
                label = 'ORO';
                break;
            case 'SILVER':
                label = 'PLATA';
                break;
            case 'BRONZE':
                label = 'BRONCE';
                break;
        }
        return label;
    }
};
MueveputosPage = __decorate([
    Component({
        selector: 'page-mueveputos',
        templateUrl: 'mueveputos.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        Events,
        LinksRedirect,
        ComplementosviewsProvider,
        NavParams])
], MueveputosPage);
export { MueveputosPage };
//# sourceMappingURL=mueveputos.js.map