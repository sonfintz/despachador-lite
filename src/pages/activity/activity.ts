import {Component} from '@angular/core';
import {App, Events, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {OfertanewdetallePage} from "../ofertanewdetalle/ofertanewdetalle";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {DetalleeventoPage} from "../detalleevento/detalleevento";

@Component({
  selector: 'page-activity',
  templateUrl: 'activity.html',
})
export class ActivityPage {
  private ListActivity: Array<any> = Array();

  get historial() {
    return DataglobalProvider.historial
  }

  get reciente() {
    return DataglobalProvider.reciente
  }

  get NotificationsSetting() {
    return DataglobalProvider.NotificacionesSettings;
  }

  private ErrorConexion: boolean = false;
  private IndexArrayOrder: number;

  constructor(public navCtrl: NavController,
              public Api: ApiProvider,
              public app: App,
              public events: Events,
              public Complements: ComplementosviewsProvider,
              public navParams: NavParams) {
    console.log('NotificationsSetting: ', this.NotificationsSetting);
    this.events.subscribe('notification-reload', () => {
      this.activity();
    });
    // this.activity();
  }

  ionViewWillLeave() {
    console.log('Unsubscribe activity');
    this.events.unsubscribe('notification-reload')
  }

  ionViewWillEnter() {
    this.activity();
  }

  ImgNotificacionLogro(data) {
    let label: string;
    switch (data) {
      case 36:
        label = 'subiste_rango_doradoldpi.svg';
        break;
      case 27:
        label = "rango_plata_subisteldpi.svg";
        break;
      default:
        label = "logro_desbloqueadoldpi.svg";
        break
    }
    return label;
  }

  ImgNotificacion(data) {
    let label: string;
    switch (data) {
      case 1:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 2:
        label = "solicitud_cuenta_validacionldpi.svg";
        break;
      case 3:
        label = "oferta_eliminada.svg";
        break;
      case 4:
        label = "viaje_canceladoldpi.svg";
        break;
      case 5:
        label = "oferta_creadaldpi.svg";
        break;
      case 6:
        label = "transportista_aceptado_verdeldpi.svg";
        break;
      case 7:
        label = "vehiculo_viaje_culminadoldpi.svg";
        break;
      case 11:
        label = "vehiculo_cargado_importanteldpi.svg";
        break;
      case 12:
        label = "vehiculo_en_rutaldpi.svg";
        break;
      case 13:
        label = "vehiculo_cargado_importanteldpi.svg";
        break;
      case 14:
        label = "oferta_a_vencerldpi.svg";
        break;
      case 15:
        label = "nuevo_transportista_postuladoldpi.svg";
        break;
      case 27:
        label = "rango_plata_subisteldpi.svg";
        break;
      case 32:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 33:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 36:
        label = "subiste_rango_doradoldpi.svg";
        break;
      case 39:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 40:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 41:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 52:
        label = "nuevo_transportista_postuladoldpi.svg";
        break;
      case 59:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 60:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 61:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 62:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 63:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 64:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 65:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 66:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 67:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 68:
        label = "reto_v2.svg";
        break;
      case 69:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 70:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 71:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 72:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 73:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 74:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 75:
        label = "viaje_calificado_excelenteldpi.svg";
        break;
      case 76:
        label = "viaje_calificado_buenoldpi.svg";
        break;
      case 77:
        label = "viaje_calificado_aceptableldpi.svg";
        break;
      case 78:
        label = "viaje_calificado_regularldpi.svg";
        break;
      case 91:
        label = "bonus_v2.svg";
        break;
      case 92:
        label = "reto_v2.svg";
        break;
      case 93:
        label = "reto_v2.svg";
        break;
      case 94:
        label = "reto_v2.svg";
        break;
      case 95:
        label = "reto_v2.svg";
        break;
      case 96:
        label = "reto_v2.svg";
        break;
      case 97:
        label = "reto_v2.svg";
        break;
      case 98:
        label = "reto_v2.svg";
        break;
      case 99:
        label = "reto_v2.svg";
        break;
      case 100:
        label = "bonus_v2.svg";
        break;
      case 101:
        label = "bonus_v2.svg";
        break;
      case 102:
        label = "alerta_importantesldpi.svg";
        break;
      case 103:
        label = "alerta_importantesldpi.svg";
        break;
      case 104:
        label = "alerta_importantesldpi.svg";
        break;
      case 105:
        label = "alerta_importantesldpi.svg";
        break;
      case 106:
        label = "alerta_importantesldpi.svg";
        break;
      case 114:
        label = "vehiculo_declino_viajeldpi.svg";
        break;
      case 115:
        label = "oferta_abandonadaldpi.svg";
        break;
      case 116:
        label = "alerta_importantesldpi.svg";
        break;
      case 119:
        label = "oferta_eliminada.svg";
        break;
      default:
        label = "mueve_noticias.svg";
        break;
    }
    return label;
  }

  labelNotification(data) {
    let label: string;
    switch (data) {
      case "logro":
        label = "Logro desbloqueado";
        break;
      default:
        // label = "Notificacion";
        label = `${data}`;
        break;
    }
    return label;
  }

  doRefresh(refresher) {
    this.activity();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  /*NOTIFICACIONES*/
  activity() {
    //this.Complements._PresentLoading();
    this.Api._GET('/actividad').subscribe(
      (data = Array()) => {
        //this.Complements._DismissPresentLoading();
        this.ErrorConexion = false;
        data.map(i => {
          let render = this.ShowRenderView(i.ref);
          return Object.assign(i, render)
        });
        DataglobalProvider.historial = [];
        DataglobalProvider.reciente = data[0];
        console.log(data[0]);
        if (data.length > 1) {
          for (let h = 1; h < data.length; h++) {
            DataglobalProvider.historial.push(data[h]);
          }
          console.log(DataglobalProvider.historial);
          console.log(DataglobalProvider.reciente);
        }

        for (let i = 0; i < 10; i++) {
          if (this.historial[i]) {
            this.IndexArrayOrder = i;
            // console.log(this.historial[i]);
            this.ListActivity.push(this.historial[i]);
            // console.log(this.ListActivity);
          }

        }

      }, (err) => {
        //this.Complements._DismissPresentLoading();
        // this.navCtrl.pop();
        if (err.status == 0) {
          this.ErrorConexion = true;
        } else {
          console.log(err);
          this.Complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error')
        }
      })
  }

  goOfert(data) {
    if (data.evento !== "") {
      this.Api._GET(`/solicitudeseventos/oferta/detalle/${data.evento}`).subscribe(
        (value) => {
          console.log('LA NUEVA RUTA DETALLE EVENTO', value);
          this.navCtrl.push(DetalleeventoPage, {data: data, value: value})
        }, (err) => {
          console.log(err.error)
        })
    }
    if (data.evento == "" && data.oferta !== 0) {
      this.Api._GET(`/oferta/${data.oferta}`).subscribe(
        (data) => {
          console.log(data);
          this.goToOfertaDetail(data);
        }, (err) => {
          this.Complements._MessageToast(`${err.error.mensaje}`);
          console.log(err.error.mensaje)
        });
      console.log('NOTIFICACION', data);
    } else {
      console.log('NO TIENE ID')
    }
  }

  goToOfertaDetail(oferta) {
    this.app.getRootNav().push(OfertanewdetallePage, {data: oferta, value: oferta.oferta_estado});
    // this.navCtrl.push(OfertanewdetallePage, {data: oferta, value: oferta.oferta_estado});
  }

  ShowRenderView(ref) {
    try {
      const Valor = this.NotificationsSetting.find(i => i.design_codigo === ref);
      switch (Valor.design_tipo.toLowerCase()) {
        case `logro`:
          Valor.image = `assets/imgs/icon/${this.ImgNotificacionLogro(Valor.design_codigo)}`;
          break;
        case `reto mp`:
          Valor.image = `assets/imgs/icon/reto_v2.svg`;
          break;
        case  `bonus mp`:
          Valor.image = `assets/imgs/icon/bonus_v2.svg`;
          break;
        case `mueve informa`:
          Valor.image = `assets/imgs/icon/${this.ImgNotificacion(Valor.design_codigo)}`;
          break
      }
      return Valor
    } catch (e) {
      console.error('Error en ShowRenderView', e)
    }
  }

  doInfinite(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        let i = this.IndexArrayOrder + 1;
        let max = i + 10;
        for (i; i < max; i++) {
          if (this.historial[i]) {
            // console.log(this.historial[i]);
            this.IndexArrayOrder = i;
            this.ListActivity.push(this.historial[i]);
          }
        }
        // console.log(this.ListActivity);
        resolve();
      }, 500);
    })
  }


}
