var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DenunciartransportistaPage } from "../denunciartransportista/denunciartransportista";
/**
 * Generated class for the DenunciarpopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
let DenunciarpopoverPage = class DenunciarpopoverPage {
    constructor(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.navParams.get('data');
    }
    _Denunciar() {
        this.navCtrl.push(DenunciartransportistaPage, { data: this.navParams.get('data') });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DenunciarpopoverPage');
    }
};
DenunciarpopoverPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-denunciarpopover',
        templateUrl: 'denunciarpopover.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams])
], DenunciarpopoverPage);
export { DenunciarpopoverPage };
//# sourceMappingURL=denunciarpopover.js.map