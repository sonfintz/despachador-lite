import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DenunciartransportistaPage} from "../denunciartransportista/denunciartransportista";

/**
 * Generated class for the DenunciarpopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-denunciarpopover',
  templateUrl: 'denunciarpopover.html',
})
export class DenunciarpopoverPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
    this.navParams.get('data');
  }

  _Denunciar() {
    this.navCtrl.push(DenunciartransportistaPage, {data: this.navParams.get('data')})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DenunciarpopoverPage');
  }

}
