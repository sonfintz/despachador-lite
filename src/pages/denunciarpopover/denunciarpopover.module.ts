import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DenunciarpopoverPage } from './denunciarpopover';

@NgModule({
  declarations: [
    DenunciarpopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(DenunciarpopoverPage),
  ],
})
export class DenunciarpopoverPageModule {}
