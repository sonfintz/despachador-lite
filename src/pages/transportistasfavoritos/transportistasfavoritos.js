var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams, PopoverController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AddTransportistaPage } from "../add-transportista/add-transportista";
import { EditarvehiculoPage } from "../editarvehiculo/editarvehiculo";
import { EditartrailerPage } from "../editartrailer/editartrailer";
import { TrailerPage } from "../trailer/trailer";
import { VehiculoPage } from "../vehiculo/vehiculo";
import * as moment from "moment";
import { DetalletransportistaPage } from "../detalletransportista/detalletransportista";
import { ActivityPage } from "../activity/activity";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
import { GpsProvider } from "../../providers/gps/gps";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let TransportistasfavoritosPage = class TransportistasfavoritosPage {
    constructor(navCtrl, navParams, popoverCtrl, events, complements, Api) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.complements = complements;
        this.Api = Api;
        this.segment = 'todos';
        this.ErrorConexion = false;
    }
    get Notificacion() {
        return DataglobalProvider.notificacion;
    }
    get BadConection() {
        return GpsProvider.Band_Conexion;
    }
    doRefresh(refresher) {
        this.loadData();
        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
    _Detalletransportista(data) {
        console.log(data);
        this.navCtrl.push(DetalletransportistaPage, { data: data, value: 3 });
    }
    _Notificaciones() {
        this.events.publish('clear-notificaciones');
        this.navCtrl.push(ActivityPage);
    }
    loadTrans() {
        this.Api._GET('/postulacion/aceptados').subscribe((data) => {
            console.log('POSTULADOS---------------------------');
            console.log(data);
        }, (err) => {
            console.log(err);
        });
    }
    validacionTrailer(t) {
        return Boolean(t.trailer_placa);
    }
    validacionVehiculo(t) {
        return Boolean(t.vehiculo_id);
    }
    break_text(text) {
        let b = text.split(' ');
        if (b.length >= 4) {
            return `${b[0]} ${b[1]} <br> ${b[2]} ${b[3]}`;
        }
        if (b.length >= 3) {
            return `${b[0]} ${b[1]} <br> ${b[2]}`;
        }
        if (b.length >= 2) {
            return `${b[0]} <br> ${b[1]}`;
        }
        return b.join(`<br>`);
    }
    ionViewWillEnter() {
        this.loadData();
    }
    ngOnInit() {
        this.events.subscribe('list-transport', () => {
            this.loadData();
        });
    }
    /*CARGAR TRANSPORTISTAS*/
    loadData() {
        this.Api._GET('/postulacion/aceptados').subscribe((data) => {
            if (data) {
                this.ErrorConexion = false;
                this.transportistas = data;
                this.transportistas_2 = data;
                console.log('ASDASD', this.transportistas);
            }
        }, (err) => {
            if (err.status == 0) {
                this.ErrorConexion = true;
            }
            else {
                this.complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error');
            }
            console.log(err);
        });
    }
    _listaTransportistas(value) {
        this.segment = value;
        if (value === 'todos') {
            this.Api._GET('/transportista').subscribe((data) => {
                this.transportistas = data;
                this.transportistas_2 = data;
                console.log(data);
            });
        }
        if (value === 'listos') {
            this.Api._GET('/transportista').subscribe((data) => {
                data = data.filter(i => i.trailer_placa !== 'SIN TRAILER' && i.vehiculo_placa !== 'SIN VEHICULO');
                this.transportistas = data;
                this.transportistas_2 = data;
                console.log(data);
            });
        }
        if (value === 'completar') {
            this.Api._GET('/transportista').subscribe((data) => {
                data = data.filter(i => i.trailer_placa === 'SIN TRAILER' || i.vehiculo_placa === 'SIN VEHICULO');
                this.transportistas = data;
                this.transportistas_2 = data;
                console.log(data);
            });
        }
    }
    updateSchedule() {
        if (this.queryText) {
            console.log('SEARCH', this.queryText);
            this.transportistas =
                this.transportistas_2.filter(item => item.persona_nombre.toLowerCase().includes(this.queryText.toLowerCase())
                    || String(item.persona_telefono).toLowerCase().includes(this.queryText.toLowerCase())
                    || String(item.trailer_placa).toLowerCase().includes(this.queryText.toLowerCase())
                    || String(item.vehiculo_placa).toLowerCase().includes(this.queryText.toLowerCase()));
            console.log('FILTRO', this.transportistas);
        }
        else {
            this.transportistas = this.transportistas_2;
        }
    }
    /*VEHICULO Y TRAILER*/
    _CrearVehiculo(data) {
        console.log('VEHICULO');
        this.navCtrl.push(VehiculoPage, { data: data });
    }
    _CrearTrailer(data) {
        console.log('TRAILER');
        this.navCtrl.push(TrailerPage, { data: data });
    }
    _EditarTrailer(data) {
        console.log('TRAILER');
        this.Api._GET(`/trailer/${data.trailer_placa}`).subscribe((data) => {
            console.log('EDITAR TRAILER', data);
            this.navCtrl.push(EditartrailerPage, { data: data });
        });
    }
    _EditarVehiculo(data) {
        console.log('VEHICULO');
        this.Api._GET(`/vehiculo/${data.vehiculo_placa}`).subscribe((data) => {
            data = data.map((i) => {
                i.fechavencesoat = moment(i.fechavencesoat).format('YYYY-MM-DD');
                return i;
            });
            console.log('EDITAR VEHICULO', data);
            this.navCtrl.push(EditarvehiculoPage, { data: data });
        });
    }
    addTransportista() {
        this.navCtrl.push(AddTransportistaPage, {});
    }
    /*DESTRUIR SOCKET*/
    _DestroySocket() {
        GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
        GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
        if (GpsProvider.EMIT != undefined) {
            console.log(' Disconect Socket', GpsProvider.EMIT);
            GpsProvider.EMIT.disconnect();
            console.log('1 Disconect Socket', GpsProvider.EMIT);
        }
    }
    /*CERRAR GPS*/
    _CloseGPS() {
        console.group('CERRANDO EL EMIT');
        console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
        console.groupEnd();
        clearInterval(GpsProvider.intervalSocket);
        clearInterval(GpsProvider.antiFakeGPS);
        let D = clearInterval(GpsProvider.intervalSocket);
        GpsProvider.intervalSocket = 0;
        console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);
    }
};
TransportistasfavoritosPage = __decorate([
    Component({
        selector: 'page-transportistasfavoritos',
        templateUrl: 'transportistasfavoritos.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        PopoverController,
        Events,
        ComplementosviewsProvider,
        ApiProvider])
], TransportistasfavoritosPage);
export { TransportistasfavoritosPage };
//# sourceMappingURL=transportistasfavoritos.js.map