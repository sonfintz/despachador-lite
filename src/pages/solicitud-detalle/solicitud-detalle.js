var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ChangeDetectorRef, Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { ApiProvider } from "../../providers/api/api";
import { AppModule } from "../../app/app.module";
import { OfertasPage } from "../ofertas/ofertas";
import { AsignarTransportistasPage } from "../asignar-transportistas/asignar-transportistas";
import { DespacharTrasportistasPage } from "../despachar-trasportistas/despachar-trasportistas";
import { OfertaDetallePage } from "../oferta-detalle/oferta-detalle";
import { EstadisticasPage } from "../estadisticas/estadisticas";
let SolicitudDetallePage = class SolicitudDetallePage {
    constructor(dataProvider, Api, events, cd, navCtrl, navParams) {
        this.dataProvider = dataProvider;
        this.Api = Api;
        this.events = events;
        this.cd = cd;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.BASE_URL = AppModule.BASE_URL;
        this.OFERTA_DATA = this.navParams.get('data');
        this.OFERTA_DETALLE = this.navParams.get('detalle');
        console.log(this.navParams.get('data'));
        // console.log('Detalles:', this.OFERTA_DETALLE);
        // this.OFERTA_DATA = this.navParams.get('data');
        // console.group('DETAIL OFERTA:',this.OFERTA_DATA);
        // console.groupEnd();
    }
    ionViewWillEnter() {
        this.updateData();
    }
    doRefresh(refresher) {
        this.updateData();
        setTimeout(() => {
            refresher.complete();
        }, 1000);
    }
    ngOnInit() {
        this.events.subscribe('update-solicitud', () => this.updateData);
        console.log('OFERTA DETALLE', this.navParams.get('detalle'));
        this.loadPostulados();
    }
    loadPostulados() {
        // console.log('SON BY FOR', this.OFERTA_DETALLE[0].oferta_check);
        this.Api._GET(`/oferta/postulados/${this.OFERTA_DETALLE.oferta_check}`).subscribe((data) => {
            this.totalPost = data.postulados;
            // console.log(data, 'LOS POSTULADOS DE MI OFERTA');
        });
    }
    updateData() {
        // this.Complementos.PresentLoading('¡Cargando Solicitud!', 200);
        this.Api._GET(`/solicitudes/despachador/${this.OFERTA_DATA.solicitudes_id}`).subscribe((data) => {
            data = data.map((i) => {
                i.porcentaje = (i.solicitudes_cantidadregistrados * 100) / i.solicitudes_cantidadvehiculo;
                return i;
            });
            this.OFERTA_DETALLE = data[0];
            console.log('por aca', this.OFERTA_DETALLE);
        });
    }
    createOfer() {
        this.Api._GET(`/solicitudes/preoferta/${this.OFERTA_DATA.solicitudes_id}`).subscribe((data) => {
            this.navCtrl.push(OfertasPage, { data: data, value: this.OFERTA_DETALLE });
        });
        // console.log('CREANDO OFERTA')
    }
    consultOfer(oferta) {
        this.navCtrl.push(OfertaDetallePage, { data: oferta });
    }
    _Transportistas() {
        this.navCtrl.push(AsignarTransportistasPage, { data: this.OFERTA_DATA });
    }
    _EnviarTransportistas() {
        this.navCtrl.push(DespacharTrasportistasPage, { data: this.OFERTA_DATA });
    }
    estadistica() {
        this.navCtrl.push(EstadisticasPage, { data: this.OFERTA_DETALLE.solicitudes_id, value: this.OFERTA_DETALLE });
    }
};
SolicitudDetallePage = __decorate([
    Component({
        selector: 'page-session-detail',
        templateUrl: 'solicitud-detalle.html',
        providers: [ApiProvider]
    }),
    __metadata("design:paramtypes", [ConferenceData,
        ApiProvider,
        Events,
        ChangeDetectorRef,
        NavController,
        NavParams])
], SolicitudDetallePage);
export { SolicitudDetallePage };
//# sourceMappingURL=solicitud-detalle.js.map