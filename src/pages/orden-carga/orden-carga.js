var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let OrdenCargaPage = class OrdenCargaPage {
    constructor(navCtrl, fb, api, events, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.api = api;
        this.events = events;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.data_empaquetado = [];
        this.data_operacion = [];
        this.data_remydes = [];
        this.data_aseguradora = [];
        this.municipios = [];
        this.flag = false;
        this.flagList = false;
        this.date = moment().format('YYYY-DD-MM');
        this.segment = 'orden';
        this.data = this.navParams.get('data');
        console.log('ORDEN DE CARGA', this.data);
    }
    getCurrentTime() {
        return moment().format('YYYY-MM-DD');
    }
    ngOnInit() {
        console.log('hey', this.data);
        this.getHeads();
        this.initForm();
    }
    /*
    * FORMULARIO
    * */
    initForm() {
        this.form = this.fb.group({
            id_empaque: [null, [Validators.required]],
            id_operaciontransporte: ['G', [Validators.required]],
            fechallegadacargue: [moment(this.data.solicitudes_fechacarga).format('DD/MM/YYYY'), [Validators.required]],
            cantidad: [null, [Validators.required]],
            pactocarga: [0, [Validators.required]],
            pactoentrega: [0, [Validators.required]],
            horaspactocargue: [12, [Validators.required]],
            minutospactocargue: [0, [Validators.required]],
            horaspactodescargue: [12, [Validators.required]],
            minutospactodescargue: [0, [Validators.required]],
            valorfletepactadoviaje: [null, [Validators.required]],
            // destinatario: [null, [Validators.require],
            porcentaje: [null, [Validators.required]],
            valoranticipomanifiesto: [null, [Validators.required]],
            // remitente: [null, [Validators.require],
            // id_remitente: [null, [Validators.require], // quitarlos
            fechapagosaldomanifiesto: [this.date, [Validators.required]],
            // id_destinatario: [null, [Validators.require], // quitarlos
            fechacarga: [moment(this.data.solicitudes_fechacarga).format('DD/MM/YYYY'), [Validators.required]],
            fechaentrega: [moment(this.data.solicitudes_fechaentrega).format('DD/MM/YYYY'), [Validators.required]],
            horallegadacargueremesa: [moment(this.data.solicitudes_fechacarga).format('DD/MM/YYYY'), [Validators.required]],
            fechaentradacargue: [moment(this.data.solicitudes_fechacarga).format('DD/MM/YYYY'), [Validators.required]],
            horaentradacargueremesa: [moment(this.data.solicitudes_fechacarga).format('DD/MM/YYYY'), [Validators.required]],
            fechasalidacargue: [moment(this.data.solicitudes_fechaentrega).format('DD/MM/YYYY'), [Validators.required]],
            horasalidacargueremesa: ['00:00', [Validators.required]],
            fechacitapactadacargue: [moment(this.data.solicitudes_fechaentrega).format('DD/MM/YYYY'), [Validators.required]],
            horacitapactadacargue: ['00:00', [Validators.required]],
            fechacitapactadadescargue: [moment(this.data.solicitudes_fechaentrega).format('DD/MM/YYYY'), [Validators.required]],
            horacitapactadadescargueremesa: ['00:00', [Validators.required]],
            id_solicitud: [this.data.solicitudes_id, [Validators.required]],
            producto_id: [this.data.producto_id, [Validators.required]],
            unidadmedida_id: [this.data.unidadmedida_id, [Validators.required]],
            // sede_id: [this.data.sede_id],
            duenopoliza: ['E', [Validators.required]],
            // MAnifiesto
            codoperaciontransportemanifiesto: ['G', [Validators.required]],
            // codmunicipioorigenmanifiesto: [null, [Validators.required]],
            // origenmanifiesto: [null, [Validators.required]],
            // codmunicipiodestinomanifiesto: [null, [Validators.required]],
            // destinomanifiesto: [null, [Validators.required]],
            id_usuario: [this.data.usuario_id, [Validators.required]],
            retencionfuentemanifiesto: [1, [Validators.required]],
            retencionicamanifiestocarga: [1, [Validators.required]],
            codmunicipiopagosaldo: [null, [Validators.required]],
            pagosaldo: [null, [Validators.required]],
            codresponsablepagocargue: ['E', [Validators.required]],
            codresponsablepagodescargue: ['E', [Validators.required]],
            observaciones: ['NINGUNA'],
        });
        this.form.get('valorfletepactadoviaje').valueChanges
            .subscribe(i => {
            console.log(i);
            this.v_flete = i;
            this.calcularAnticipo();
        });
        this.form.get('porcentaje').valueChanges
            .subscribe(i => {
            console.log(i);
            this.v_ingresado = i / 100;
            this.calcularAnticipo();
            // this.form.get('valoranticipomanifiesto').setValue(this.v_final);
        });
        // this.formValueChanges();
    }
    /////////////////////////////////////////////////////////////////////////////
    /*TYPEAHEADS*/
    _Empaquetado(datos) {
        console.log(datos);
        if (datos.data) {
            this.form.get('id_empaque').setValue(datos.data);
        }
        else {
            this.form.get('id_empaque').setValue('');
        }
    }
    _Remydes(datos) {
        console.log(datos.data);
        if (datos.data) {
            this.form.get('id_remitente').setValue(datos.data.tercero_id);
        }
        else {
            this.form.get('id_remitente').setValue('');
        }
    }
    _UbicacionOutput(datos, letra) {
        console.log('typeahead', datos);
        if (datos.data) {
            console.log('Output ubicacion', datos, letra);
            this.form.get(`codmunicipio${letra}`).setValue(datos.data);
        }
        else {
            this.form.get(`codmunicipio${letra}`).setValue('');
        }
    }
    /*setTransportista(datos): void {
      this.form.get('numplaca').setValue(datos.data.vehiculo_placa);
      this.form.get('numplaceremolque').setValue(datos.data.trailer_placa);
      this.form.get('numidtitularmanifiesto').setValue(datos.data.persona_identificacion);
      this.form.get('id_usuario').setValue(datos.data.usuario_id);
    }*/
    /* _Aseguradora(datos) {
      console.log(datos.data);
      if (datos.data) {
        this.form.get('nit_aseguradora').setValue(datos.data.nit);
      } else {
        this.form.get('nit_aseguradora').setValue('');
      }
    }*/
    /*FIN TYPEAHEAD*/
    ///////////////////////////////////////////////////////////////////////////////
    /*
    * METODO GUARDAR DATA
    * */
    saveData() {
        let datos = this.form.value;
        const Obj = {
            codtipoempaque: datos.id_empaque,
            codoperaciontransporte: datos.id_operaciontransporte,
            // id_remitente: datos.id_remitente,
            // id_destinatario: datos.id_destinatario,
            fechallegadacargue: datos.fechallegadacargue,
            // fechallegadacargue: moment(datos.fechallegadacargue).format('DD/MM/YYYY'),
            horallegadacargueremesa: '00:00',
            fechaentradacargue: datos.fechaentradacargue,
            // fechaentradacargue: moment(datos.fechaentradacargue).format('DD/MM/YYYY'),
            horaentradacargueremesa: '00:00',
            fechasalidacargue: datos.fechasalidacargue,
            // fechasalidacargue: moment(datos.fechasalidacargue).format('DD/MM/YYYY'),
            horasalidacargueremesa: '00:00',
            fechacitapactadacargue: datos.fechacitapactadacargue,
            // fechacitapactadacargue: moment(datos.fechacitapactadacargue).format('DD/MM/YYYY'),
            horacitapactadacargue: '00:00',
            fechacitapactadadescargue: datos.fechacitapactadadescargue,
            // fechacitapactadadescargue: moment(datos.fechacitapactadadescargue).format('DD/MM/YYYY'),
            horacitapactadadescargueremesa: '00:00',
            pactocarga: datos.pactocarga,
            pactoentrega: datos.pactoentrega,
            horaspactocarga: datos.horaspactocargue,
            minutospactocarga: datos.minutospactocargue,
            horaspactodescargue: datos.horaspactodescargue,
            minutospactodescargue: datos.minutospactodescargue,
            cantidadcargada: datos.cantidad,
            id_solicitud: datos.id_solicitud,
            id_producto: datos.producto_id,
            numplaca: null,
            numplacaremolque: null,
            // sede_id: datos.sede_id,
            duenopoliza: datos.duenopoliza,
            id_usuario: datos.id_usuario,
            codoperaciontransportemanifiesto: datos.codoperaciontransportemanifiesto,
            // codmunicipioorigenmanifiesto: datos.codmunicipioorigenmanifiesto,
            // codmunicipiodestinomanifiesto: datos.codmunicipiodestinomanifiesto,
            valorfletepactadoviaje: datos.valorfletepactadoviaje,
            valoranticipomanifiesto: datos.valoranticipomanifiesto,
            retencionfuentemanifiesto: datos.retencionfuentemanifiesto,
            retencionicamanifiestocarga: datos.retencionicamanifiestocarga,
            codmunicipiopagosaldo: datos.codmunicipiopagosaldo,
            codresponsablepagocargue: datos.codresponsablepagocargue,
            codresponsablepagodescargue: datos.codresponsablepagodescargue,
            fechapagosaldomanifiesto: moment(datos.fechapagosaldomanifiesto).format('DD/MM/YYYY'),
            observaciones: datos.observaciones,
            numidtitularmanifiesto: null
        };
        // console.log('here', this.form.value);
        this.api._POST('/orden', Obj).subscribe((data) => {
            console.log('datos de la orden de carga', data);
            this.Complementos._MessageToast('Registrado correctamente');
            this.events.publish('solicitud-changed', data);
            this.navCtrl.pop();
            this.events.publish('despachado');
            console.log('datos de la orden de carga2', data);
            // this.modalService.show(MainRemesaComponent, {animated: true, class: 'modal-lg', initialState: {datos: data}});
            // this.mainComponent.captureScreen();
        }, (e) => {
            console.log(e);
            this.Complementos._MessageToast(`${e.error.mensaje}`);
        }, () => {
        });
    }
    /*
    * CARGANDO LA DATA
    * */
    getHeads() {
        return __awaiter(this, void 0, void 0, function* () {
            this.data_empaquetado = yield this.api._GET('/empaque').toPromise();
            this.data_operacion = yield this.api._GET('/operaciontransporte').toPromise();
            this.data_remydes = yield this.api._GET('/remitentedestinatario').toPromise();
            this.data_aseguradora = yield this.api._GET('/aseguradora').toPromise();
            this.municipios = yield this.api._GET('/municipio').toPromise();
            this.data_transporte = yield this.api._GET('/transportista').pipe(map(i => i.map((d) => {
                d.injecta = `${d.vehiculo_placa} (${d.persona_nombre})`;
                return d;
            }))).toPromise();
            console.group();
            console.log('la data cargada para los typeahead');
            console.log('MUNICIPIOS', this.municipios);
            console.log('EMPAQUETADO', this.data_empaquetado);
            console.log('OPERACION', this.data_operacion);
            console.log('REMYDES', this.data_remydes);
            console.log('TRANSPORTE', this.data_transporte);
            console.groupEnd();
        });
    }
    /*
    * CAMBIOSELECT
    * */
    cambioSelect(hora) {
        if (hora === 'pactocarga') {
            // @ts-ignore
            this.flag = !this.flag;
            // console.log(this.form.value.pactocarga);
        }
        if (hora === 'pactoentrega') {
            // @ts-ignore
            this.flagList = !this.flagList;
        }
    }
    /*
    * CALCULAR ANTICIPO
    * */
    calcularAnticipo() {
        if (this.v_ingresado !== 0 || this.v_flete !== 0) {
            this.v_final = this.v_ingresado * this.v_flete;
            // console.log('aqui', this.v_final);
            if (this.v_final !== 0) {
                this.form.get('valoranticipomanifiesto').setValue(this.v_final);
            }
        }
        else {
            this.form.get('valoranticipomanifiesto').setValue(0);
        }
    }
    /*
    * CHANGED
    * */
    changed(value) {
        console.log('DONDE VA A PAGAR', value);
        if (value === 'Origen') {
            this.form.get('codmunicipiopagosaldo').setValue(true);
        }
        else if (value === 'Destino') {
            this.form.get('codmunicipiopagosaldo').setValue(false);
        }
    }
};
OrdenCargaPage = __decorate([
    Component({
        selector: 'page-orden-carga',
        templateUrl: 'orden-carga.html',
    }),
    __metadata("design:paramtypes", [NavController,
        FormBuilder,
        ApiProvider,
        Events,
        ComplementosviewsProvider,
        NavParams])
], OrdenCargaPage);
export { OrdenCargaPage };
//# sourceMappingURL=orden-carga.js.map