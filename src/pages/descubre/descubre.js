var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { ActivityPage } from "../activity/activity";
import { DataglobalProvider } from "../../providers/dataglobal/dataglobal";
import { ApiProvider } from "../../providers/api/api";
import { AppModule } from "../../app/app.module";
let DescubrePage = class DescubrePage {
    constructor(navCtrl, navParams, Api, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Api = Api;
        this.events = events;
        this.queryText = '';
    }
    get Notificacion() {
        return DataglobalProvider.notificacion;
    }
    updateSchedule() {
        console.log(this.queryText);
        if (this.queryText) {
            this._Tips = this._Tips2.filter(item => item.titulo.toLowerCase().includes(this.queryText.toLowerCase()));
        }
        else {
            this._Tips = this._Tips2;
        }
    }
    ionViewWillEnter() {
        this.Api._GET('/tips').subscribe((tips) => {
            console.log('TIPS', tips);
            tips = tips.map(i => {
                i.logos = JSON.parse(i.logos);
                return i;
            });
            this._Tips = tips;
            this._Tips2 = tips;
        });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DescubrePage');
    }
    _Notificaciones() {
        this.events.publish('clear-notificaciones');
        this.navCtrl.push(ActivityPage);
    }
    linkhiperv(link) {
        console.log(link.hipervinculo);
        window.open(link.hipervinculo, '_system');
    }
    imagenServer(imagenes) {
        // console.log(AppModule.BASE_URL, '/' + imagenes);
        return AppModule.BASE_URL + '/' + imagenes;
    }
};
DescubrePage = __decorate([
    Component({
        selector: 'page-descubre',
        templateUrl: 'descubre.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        ApiProvider,
        Events])
], DescubrePage);
export { DescubrePage };
//# sourceMappingURL=descubre.js.map