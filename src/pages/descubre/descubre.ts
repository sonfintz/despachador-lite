import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {ActivityPage} from "../activity/activity";
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {ApiProvider} from "../../providers/api/api";
import {AppModule} from "../../app/app.module";

@Component({
  selector: 'page-descubre',
  templateUrl: 'descubre.html',
})
export class DescubrePage {
  queryText = '';
  private _Tips: any;
  private _Tips2: any;

  get Notificacion() {
    return DataglobalProvider.notificacion
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public Api: ApiProvider,
              public events: Events) {
  }

  updateSchedule() {
    console.log(this.queryText);
    if (this.queryText) {
      this._Tips = this._Tips2.filter(item => item.titulo.toLowerCase().includes(this.queryText.toLowerCase()));
    } else {
      this._Tips = this._Tips2;
    }
  }

  ionViewWillEnter() {
    this.Api._GET('/tips').subscribe((tips) => {
      console.log('TIPS', tips);
      tips = tips.map(i => {
        i.logos = JSON.parse(i.logos);
        return i;
      });
      this._Tips = tips;
      this._Tips2 = tips;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescubrePage');
  }

  _Notificaciones() {
    this.events.publish('clear-notificaciones');
    this.navCtrl.push(ActivityPage)
  }

  linkhiperv(link) {
    console.log(link.hipervinculo);
    window.open(link.hipervinculo, '_system')
  }

  imagenServer(imagenes: any) {
    // console.log(AppModule.BASE_URL, '/' + imagenes);
    return AppModule.BASE_URL + '/' + imagenes
  }
}
