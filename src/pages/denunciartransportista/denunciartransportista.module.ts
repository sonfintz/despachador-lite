import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DenunciartransportistaPage } from './denunciartransportista';

@NgModule({
  declarations: [
    DenunciartransportistaPage,
  ],
  imports: [
    IonicPageModule.forChild(DenunciartransportistaPage),
  ],
})
export class DenunciartransportistaPageModule {}
