import {ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {Events, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import * as moment from "moment";
import {ListaOfertasPage} from "../lista-ofertas/lista-ofertas";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {TypeaheadComponent} from "../../components/typeahead/typeahead";
import * as turf from '@turf/turf';
import {DataglobalProvider} from "../../providers/dataglobal/dataglobal";
import {MapaeventoPage} from "../mapaevento/mapaevento";
import {GpsProvider} from "../../providers/gps/gps";

/**
 * Generated class for the CreateOfertPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@Component({
  selector: 'page-create-ofert',
  templateUrl: 'create-ofert.html',
  providers: [ApiProvider]
})
export class CreateOfertPage {
  public Opc: number = 0;
  public Direccioncargue: boolean = true;
  public Direcciondescargue: boolean = true;
  @ViewChild("typeaheadorigen") typeaheadorigen: TypeaheadComponent;
  @ViewChild("typeaheaddestino") typeaheaddestino: TypeaheadComponent;
  public Objs: any = [];
  public BandSolicitud: boolean = false;
  /**--- BANDERA DE SELECT DE SOLICITUD ----**/
  private area = [];
  private area2 = [];
  private area3 = [];
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  Origen: any;
  OrigenAux: any;
  DestinoAux: any;
  Origen2: any;
  Destino: any;
  Destino2: any;
  Origen_lat: any;
  Origen_lng: any;
  distancia_Area: any;
  distancia_Area2: any;
  Solicitudes: any;
  TIPO_VEHICULO: any;
  TIPO_CARGA: any;
  formOferta: FormGroup;
  formOferta1: FormGroup;
  formOferta2: FormGroup;
  Name_cityO: string;
  Name_cityD: string;
  rango_maximo: any;
  rango_minimo: any;
  public vehiculo: any;
  public listado: any;
  public datos: any;
  public long: number;
  public long2: number;
  public lat: number;
  public lat2: number;
  public _Origen: number;
  public date2 = new Date();
  public car_disponible: number;
  public SOLICITUD: any;
  public _OrigenLat: any;
  public _OrigenLng: any;
  public _RangoInicial: any;
  public _RangoFinal: any;
  public _SegundoMarketLat: any;
  public _SegundoMarketLng: any;
  public circle: any;
  public circle2: any;
  public segundoMarket = false;
  public primerMarket = true;
  public ultimaPosicion: any;
  public destino2: any;
  public origen2: any;
  public destino: any;
  public etiquetas: any;
  public markerInvisible: any;
  public _ButtonAux = false;
  public vehiculoCantidad: any;
  private _Destino: { lat: number; lng: number };
  public km: any;
  public km2: any;
  public KM: any;
  public KM2: any;
  referencia = {
    id_referencia: 2,
    id_origen: 2,
    nombre: 'Sardinata'
  };
  private OrigenID: any;
  private DestinoID: any;

  constructor(public navCtrl: NavController,
              public dataGlobal: DataglobalProvider,
              public navParams: NavParams,
              private changeDetector: ChangeDetectorRef,
              private Api: ApiProvider,
              private Complementos: ComplementosviewsProvider,
              public events: Events,
              public formBuilder: FormBuilder,
              public toastCtrl: ToastController) {
    this.vehiculoCantidad = this.navParams.get('data').cupo;
    this.events.subscribe('cargaydescarga', (data) => { // SELECCIONANDO O EDITANDO LUGAR DE CARGUE Y DESCARGUE
      console.log(data);
      if (data.origen == 'carga') {
        console.log('carga', data);
        let Obj = {
          latitud: data.latitud,
          longitud: data.longitud,
          nombre: data.nombre
        };
        this.formOferta1.get('dir_cargue').setValue(JSON.stringify(Obj));
        this.Direccioncargue = false;
      } else if (data.origen == 'descargue') {
        console.log('descarga', data);
        let Obj = {
          latitud: data.latitud,
          longitud: data.longitud,
          nombre: data.nombre
        };
        this.formOferta1.get('dir_descargue').setValue(JSON.stringify(Obj));
        console.log('descarga', data);
        this.Direcciondescargue = false;
      }
    });
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    this.initForm();
    this.Api._GET('/municipio').subscribe((data) => {
      this.Objs = data;
    }, () => {
    }, () => {
    });
  }

  /*LOS VEHICULOS DISPONIBLES PARA LA OFERTA*/
  _VehiculoRequerido() {
    this.Api._GET(`/oferta/restricciones`).subscribe((data) => {
      this.vehiculoCantidad = data.cupo;
    })
  }

  ionViewDidLoad() {
    this._Solicitudes();
    this._TipoVehiculo();
    this._TipoCarga();
    this._Etiquetas();
  }

  _Etiquetas() {
    this.Api._GET('/etiquetas').subscribe(
      (etiquetas) => {
        this.etiquetas = etiquetas;
      }, (err) => {
        console.log(err);
      })
  }

  getResults(keyword: any, opc: number) {
    console.log('Funtion->', keyword);
    if (opc === 1) {
      return this.Objs.filter(item => item['municipio_nombre'].toLowerCase().startsWith(keyword.toLowerCase())).slice(0, 3);
    } else if (opc === 2) {
      return this.Objs.filter(item => item['id_municipio'] === keyword).slice(0, 3);
    }
  }

  initForm() {
    let date = moment().format('YYYY-MM-DD');
    let date2 = moment().add('days', 5).format('YYYY-MM-DD');
    this.formOferta = this.formBuilder.group({
      producto: [''],
      id_tipocarga: ['', [Validators.required]],
      id_tipovehiculo: ['', [Validators.required]],
      cantidad: ['', [Validators.required]],
      id_etiqueta: [null],
    }); // PRIMERA PARTE
    this.formOferta1 = this.formBuilder.group({
      fechacarga: [date, [Validators.required]],
      origenmunicipio: [null, [Validators.required]],
      fechaentrega: [date2, [Validators.required]],
      destinomunicipio: [null, [Validators.required]],
      dir_cargue: [null, [Validators.required]],
      dir_descargue: [null, [Validators.required]],
    }); // SEGUNDA PARTE
    this.formOferta2 = this.formBuilder.group({
      valor: ['', [Validators.required]], // Precio
      observaciones: ['NINGUNA', [Validators.required]],
    }); // TERCERA PARTE
    this.formOferta1.get('fechacarga').valueChanges.subscribe(() => {
      this.formOferta1.get('fechaentrega').reset();
    });
    this.formOferta.get('cantidad').valueChanges.subscribe((data) => {
      if (
        data < 1 ||
        data > this.vehiculoCantidad
      ) {
        this.formOferta.get('cantidad').setErrors({invalidNet: 'cantidad no permitida.'});
      }
    });
    this.formOferta2.get('valor').valueChanges.subscribe((data: any) => {
        console.log(data);
        if (Number(data) < 10000) {
          this.formOferta2.get('valor').setErrors({invalidNet: 'Monto no permitido.'});
        }
        if (Number(data) > 50000000) {
          this.formOferta2.get('valor').setErrors({invalidNet: 'Monto no permitido.'});
        }
      }
    )
  }

  getCurrentTime() {
    return moment().format('YYYY-MM-DD');
  }

  getCurrentTime1() {
    return this.formOferta1.get('fechacarga').value
  }

  _next(valor) {
    if (valor === 1) {
      console.log(valor);
      if (this.Opc < 4) {
        this.Opc = this.Opc + 1;
        this.changeDetector.detectChanges();
      }
      if (this.Opc === 3) {
        this.loadMap();
      }
    }
    if (valor === 2) {
      console.log(valor);
      if (this.formOferta1.get('destinomunicipio').value == null && this.formOferta1.get('origenmunicipio').value == null) {
        this.Complementos._MessageToast('Origen vacio')
      } else {
        if (this.Opc < 4) {
          this.Opc = this.Opc + 1;
          this.changeDetector.detectChanges();
        }
        if (this.Opc === 3) {
          this.segundoMarket = false;
          this.loadMap();
        }
      }
    }
  }

  rootPages() {
    this.navCtrl.setRoot(ListaOfertasPage);
  }

  _Market(value) {
    if (value == 1) {
      /*CREANDO MARKER*/
      if (this.segundoMarket === false) {

        /*VARIABLES*////////////////
        this._ButtonAux = false;
        this.destino.visible = false;
        this.destino.setDraggable(false);
        /*/////////////////////////*/

        /*CREANDO RADIO EN EL TURF*/
        let pt = turf.point([this._SegundoMarketLng, this._SegundoMarketLat]);
        let radioPrincipal = turf.booleanPointInPolygon(pt, this.circle);
        let radioTurf = turf.booleanPointInPolygon(pt, this.circle2);
        console.log('RADIO PRIMARIO', radioPrincipal, 'RADIO SECUNDARIO', radioTurf);
        //////////////////////////////
        let url = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";

        /*2NDO MARKER*////////////////
        this.origen2 = new google.maps.Marker({
          position: {lat: this._SegundoMarketLat, lng: this._SegundoMarketLng},
          map: this.map,
          icon: {
            url: url
          },
          draggable: true,
          animation: google.maps.Animation.DROP,
        });
        /*FIN SEGUNDO MARKER*/////////////////

        /*EVENTO DRAG 2ndo MARKER*///////////////////
        google.maps.event.addListener(this.origen2, 'drag', (data) => {
          //SETEANDO LOCALIZACION DEL 2NDO MARKET
          this._OrigenLat = data.latLng.lat();
          this._OrigenLng = data.latLng.lng();
        });
        /*FIN DRAG*//////////////////////////////////

        /*EVENTO DRAGEND 2ndo MARKER*///////////////////
        google.maps.event.addListener(this.origen2, 'dragend', (data) => {
          this.origen2.setDraggable(false);

          /*GUARDANDO ULTIMA POSICION DE LOCALIZACION EN EL MARKER PARA VALIDAR QUE NO SOBREPASE EL LIMITE DEL RADIO TURF*/
          this.ultimaPosicion = {lat: this._SegundoMarketLat, lng: this._SegundoMarketLng};

          /*PUNTOS DEL TURF*/
          let p4 = turf.point([this.origen2.getPosition().lng(), this.origen2.getPosition().lat()]);
          let p5 = turf.point([data.latLng.lng(), data.latLng.lat()]);

          /*MARKER ORIGEN*/
          let radioPrincipal = turf.booleanPointInPolygon(p4, this.circle);
          let radioTurf = turf.booleanPointInPolygon(p4, this.circle2);

          /*MARKER RADIO*/
          let radioPrincipal2 = turf.booleanPointInPolygon(p5, this.circle);
          let radioTurf2 = turf.booleanPointInPolygon(p5, this.circle2);

          /*VALIDANDO QUE ESTE DENTRO DEL ALCANCE MAXIMO ESTABLECIDO POR EL TURF*/
          console.log('ANTES DEL IF', radioPrincipal, radioTurf, radioPrincipal2, radioTurf2);
          if (radioPrincipal == true && radioTurf == true && radioPrincipal2 == true && radioTurf2 == true) {
            console.log('DESPUES DEL IF', radioPrincipal, radioTurf, radioPrincipal2, radioTurf2);
            this.origen2.setPosition(this.ultimaPosicion);
            this.Complementos._MessageToast('Fuera del alcance permitido');
            this.origen2.setDraggable(true);
          } else if (radioPrincipal == false && radioTurf == false && radioPrincipal2 == false && radioTurf2 == false) {
            console.log('DESPUES DEL IF', radioPrincipal, radioTurf, radioPrincipal2, radioTurf2);
            this.origen2.setPosition(this.ultimaPosicion);
            this.Complementos._MessageToast('Fuera del alcance permitido');
            this.origen2.setDraggable(true);
          } else {
            /*DENTRO DEL RANGO DEL TURF*/
            this.destino2 = new google.maps.Marker({
              position: {lat: data.latLng.lat(), lng: data.latLng.lng()},
              map: this.map,
              // icon: image, //'',
              strokeColor: "blue",
              draggable: true,
              visible: true,
              animation: google.maps.Animation.BOUNCE,
              title: 'Ciudad Origen 2'
            });
            /*SETEANDO LOCALIZACION*/
            this._OrigenLat = data.latLng.lat();
            this._OrigenLng = data.latLng.lng();

            /*EVENTO DRAGEND DEL MARKET PARA GENERAR EL RADIO*/
            google.maps.event.addListener(this.destino2, 'dragend', () => {
              /*DESABILITANDO MARKET AL SOLTAR*/
              this.segundoMarket = true;
              this.destino2.setDraggable(false);
              this.destino2.visible = false;
            });

            /*EVENTO DRAG DEL MARKET PARA GENERAR EL RADIO*/
            google.maps.event.addListener(this.destino2, 'drag', (data) => {
              if (this.segundoMarket == false) {

                /*CALCULANDO PUNTOS DESDE RADIO PRINCIPAL CON RADIO SECUNDARIO*/
                let km2: any = this.distancia(this._OrigenLat, this._OrigenLng, data.latLng.lat(), data.latLng.lng());
                let KM2 = km2 * 1000;
                this.Origen2 = this.origen2.getPosition();
                this.Destino2 = this.destino2.getPosition();
                let p4 = turf.point([this.origen2.getPosition().lng(), this.origen2.getPosition().lat()]);
                let p5 = turf.point([data.latLng.lng(), data.latLng.lat()]);
                // console.log('PUNTOS DE TURF', p4, p5);
                /*MARKER ORIGEN*/
                let respuesta1 = turf.booleanPointInPolygon(p4, this.circle);
                let respuesta2 = turf.booleanPointInPolygon(p4, this.circle2);
                /*MARKER RADIO*/
                let respuesta3 = turf.booleanPointInPolygon(p5, this.circle);
                let respuesta4 = turf.booleanPointInPolygon(p5, this.circle2);
                ////////////////////////////////////////////////////////////////

                this.distancia_Area2 = KM2;
                console.log(respuesta1, respuesta2, respuesta3, respuesta4);

                /*VALIDANDO QUE SE ESTE DENTRO DEL RANGO PERMITIDO*/
                if (respuesta1 == false && respuesta2 == true && respuesta3 == false && respuesta4 == true && KM2 <= 20000) {
                  for (let i in this.area2) {
                    this.area2[i].setMap(null);
                    this.area2[i].strokeColor = "#0c0ce8";
                    this.area2[i].fillColor = "#0c0ce8";
                  }
                  this.area2.push(new google.maps.Circle({
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.5,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.05,
                    visible: true,
                    map: this.map,
                    center: this.origen2.position,
                    radius: KM2
                  }));
                  this._Origen = this.origen2.position;
                  this.lat2 = this.Origen.lat;
                } else {
                  // this.distancia_Area = 200000;
                  this.distancia_Area2 = KM2
                }
              }
            });
          }

        });
      } else {
        this.Complementos._MessageToast('ya existe un segundo marker')
      }
    }
    if (value == 0) {
      /*ELIMINANDO MARKER*/
      if (this.segundoMarket == true) {
        this._ButtonAux = true;
        this.Complementos._MessageToast('Marker eliminado');
        this.origen2.setDraggable(true);
        /*LIMPIANDO REGISTRO DEL NUEVO MARKET*/

        this.destino2 = new google.maps.Marker({
          position: null,
          map: this.map,
          strokeColor: false,
          draggable: true,
          visible: true,
          animation: google.maps.Animation.BOUNCE,
        });

        this.origen2.setMap(null);
        this.destino2.setMap(null);
        this.segundoMarket = false;
        this.destino.visible = true;
        this.destino.setDraggable(true);
        this._OrigenLat = undefined;
        this._OrigenLng = undefined;
        ///////////////////////////////////////
      } else {
        this.Complementos._MessageToast('No existe un segundo marker')
      }
    }
  }

  _ClearInput1(value) {
    console.log('reseteando formulario', value);
    this.formOferta1.get(`${value}`).reset();
  }

  _ClearInput(value) {
    console.log('reseteando formulario', value);
    this.formOferta.get(`${value}`).reset();
  }

  _previous() {
    // console.log('previous', this.Opc);
    if (this.Opc > 0) {
      if (this.Opc === 3) {
        // console.log('estamos en la pagina nro', this.Opc);
        // console.log(this.Origen);
        // console.log(this.OrigenAux);
        this.Origen = this.OrigenAux;
        this.Opc = this.Opc - 1;
      } else {
        // console.log(this.Opc, this.formOferta1.get('origenmunicipio'), this.formOferta1.get('destinomunicipio'));
        this.formOferta1.get('origenmunicipio').reset();
        this.formOferta1.get('destinomunicipio').reset();
        this.Direccioncargue = true;
        this.Direcciondescargue = true;
        this.Opc = this.Opc - 1;
      }
      if (this.Opc === 3) {
        this.loadMap();
        this._Area_kilometros();
      }
    }
  }


  loadMapaCargueyDescargue(data: string) {
    // console.log('ORIGEN DE REFERENCIA', this.Origen);
    if (this.Direccioncargue == true) {
      if (data == 'cargue') {
        let Obj = {
          latitud: this.Origen.lat,
          longitud: this.Origen.lng,
          idOrigen: this.OrigenID
        };
        let Aux = {
          latitud: this.Origen.lat,
          longitud: this.Origen.lng
        };
        console.log('cargando mapa cargue', Obj);
        console.log('viendo mapa');
        this.navCtrl.push(MapaeventoPage, {data: Obj, value: 1, aux: Aux});
      }
    } else {
      if (data == 'cargue') {
        // console.log('editando la direccion');
        // console.log(this.formOferta1.get('dir_cargue').value.latitud, this.formOferta1.get('dir_cargue').value.longitud);
        // console.log('JSON PARSE', JSON.parse(this.formOferta1.get('dir_cargue').value), JSON.parse(this.formOferta1.get('dir_cargue').value));
        let aux = JSON.parse(this.formOferta1.get('dir_cargue').value);
        let Obj = {
          latitud: aux.latitud,
          longitud: aux.longitud,
          idOrigen: this.OrigenID
        };
        let Aux = {
          latitud: this.Origen.lat,
          longitud: this.Origen.lng
        };
        // console.log('cargando mapa cargue', Obj);
        // console.log('viendo mapa');
        this.navCtrl.push(MapaeventoPage, {data: Obj, value: 1, aux: Aux});
      }
    }
    if (this.Direcciondescargue == true) {
      if (data == 'descargue') {
        let Obj = {
          latitud: this._Destino.lat,
          longitud: this._Destino.lng,
          idDestino: this.DestinoID
        };
        let Aux = {
          latitud: this._Destino.lat,
          longitud: this._Destino.lng
        };
        // console.log('viendo mapa', Obj);
        this.navCtrl.push(MapaeventoPage, {data: Obj, value: 2, aux: Aux});
      }
    } else {
      if (data == 'descargue') {
        let aux = JSON.parse(this.formOferta1.get('dir_descargue').value);
        let Obj = {
          latitud: aux.latitud,
          longitud: aux.longitud,
          idDestino: this.DestinoID
        };
        let Aux = {
          latitud: this._Destino.lat,
          longitud: this._Destino.lng
        };
        // console.log('viendo mapa', Obj);
        this.navCtrl.push(MapaeventoPage, {data: Obj, value: 2, aux: Aux});
      }
    }
  }

  loadMap() {
    let mapEle: HTMLElement = document.getElementById('map');

    /*CREANDO MAPA*/
    this.map = new google.maps.Map(mapEle, {
      center: (!this.Origen) ? {lat: 7.913728, lng: -72.5027235} : this.Origen,
      zoom: 10
    });
    ///////////////////////////////////////////////////////////////

    /*SETEANDO VALORES*/
    this.directionsDisplay.setMap(this.map);
    // console.log('this.Origen:', this.Origen);
    this.long = this.Origen.lng;
    this.lat = this.Origen.lat;
    // console.log(this.long);
    // console.log(this.lat);
    /////////////////////////////////////////////////////////////////
    let url = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";

    /*CREANDO MARKER*/////////////////////////////////////////////
    let origen = new google.maps.Marker({
      position: (!this.Origen) ? {lat: 7.913728, lng: -72.5027235} : this.Origen,
      map: this.map,
      strokeColor: "#0c0ce8",
      icon: {
        url: url
      },
      animation: google.maps.Animation.DROP,
    });

    /*turf midiendo distancia*/
    let point = turf.point([this.Origen.lng, this.Origen.lat]);
    let distance = 20;
    let bearing = 180;
    let option2: {} = {units: 'kilometers'};

    let destination = turf.destination(point, distance, bearing, option2);
    // console.log('DISTANCIA DEL PUNTO DE ORIGEN', destination);
    // console.log(destination.geometry.coordinates);
    let secondMark = {
      lng: destination.geometry.coordinates[0],
      lat: destination.geometry.coordinates[1]
    };

    this.DragFunction(secondMark, this.Origen);
    /*FIN*/

    this.destino = new google.maps.Marker({
      position: (!secondMark) ? {lat: 7.913728, lng: -72.5027235} : secondMark,
      map: this.map,
      strokeColor: "blue",
      draggable: true,
      animation: google.maps.Animation.BOUNCE,
      title: 'Ciudad Origen'
    });
    //////////////////////////////////////////////////////////////

    /*PROBANDO 3ER MARKER INVISIBLE QUE MUESTRE EN COLOR LA DISTANCIA QUE PUEDE PONER EL 2NDO MARKER EL USUARIO*/
    this.markerInvisible = new google.maps.Marker({
      position: (!this.Destino) ? {lat: 7.913728, lng: -72.5027235} : this.Destino,
      map: this.map,
      strokeColor: "blue",
      draggable: true,
      visible: false,
      animation: google.maps.Animation.BOUNCE,
      title: 'Ciudad Origen'
    });


    /*EVENTO CLICK DEL MARKER DRAGABLE PARA EL RADIO*/
    this.destino.addListener('click', () => {
    });
    //////////////////////////////////////////////////////////////

    /*EVENTO DRAGEND DEL MARKER DRAGABLE PARA EL RADIO*/
    google.maps.event.addListener(this.destino, 'dragend', () => {
      this._ButtonAux = true;
    });
    ///////////////////////////////////////////////////////////////

    /*EVENTO CLICK DEL MARKER DRAGABLE PARA EL RADIO*/
    google.maps.event.addListener(this.destino, 'drag', () => {

      /*SEGUNDO MARKER INVISIBLE*/
      this.km2 = this.distancia(origen.getPosition().lat(), origen.getPosition().lng(), this.destino.getPosition().lat(), this.destino.getPosition().lng());
      this.KM2 = this.km2 * 1000;
      this.Origen = origen.getPosition();
      this.Destino = this.destino.getPosition();
      this.Origen_lat = origen.getPosition().lat();
      this.Origen_lng = origen.getPosition().lng();
      this.distancia_Area = this.KM2;

      /*SETEANDO VALORES DE LOCALIZACION*/
      this.km = this.distancia(origen.getPosition().lat(), origen.getPosition().lng(), this.destino.getPosition().lat(), this.destino.getPosition().lng());
      this.KM = this.km * 1000;
      this.Origen = origen.getPosition();
      this.Destino = this.destino.getPosition();
      this.Origen_lat = origen.getPosition().lat();
      this.Origen_lng = origen.getPosition().lng();
      this.distancia_Area = this.KM;
      /////////////////////////////////////

      if (this.KM <= 100000) {
        // console.log(this.KM);
        for (let i in this.area) {
          this.area[i].setMap(null);
          this.area[i].strokeColor = "#0c0ce8";
          this.area[i].fillColor = "#0c0ce8";

          this.area3[i].setMap(null);
          this.area3[i].strokeColor = "#0c0ce8";
          this.area3[i].fillColor = "#0c0ce8";
        }

        /*GENERANDO RADIO*/
        this.area.push(new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.05,
          map: this.map,
          center: origen.getPosition(),
          radius: this.KM
        }));

        /*GENERANDO 2NDO RADIO INVISIBLE*/
        this.area3.push(new google.maps.Circle({
          strokeColor: '#0c0ce8',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#0c0ce8',
          fillOpacity: 0.05,
          map: this.map,
          center: origen.getPosition(),
          radius: this.KM + 10000
        }));


        this._SegundoMarketLat = this.destino.getPosition().lat();
        this._SegundoMarketLng = this.destino.getPosition().lng();

        /*GENERANDO PUNTOS TURF*/
        let p1 = turf.point([origen.getPosition().lng(), origen.getPosition().lat()]);
        let radius = this.KM / 1000;
        let radius2 = radius + 10;
        let options: any = {steps: 1000, units: 'kilometers', properties: {foo: 'bar'}};
        this.circle = turf.circle(p1, radius, options);
        this.circle2 = turf.circle(p1, radius2, options);
        /////////////////////////////////////

      } else {
        this.distancia_Area = 100000;
        /*console.log(this._RangoInicial, this._RangoFinal);*/
      }

    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
    });
    /////////////////////////////

    let input = document.getElementById('pac-input');
    let searchBox = new google.maps.places.Autocomplete(input, {});
    searchBox.addListener('place_changed', () => {
      let places = searchBox.getPlace();
      let bounds = new google.maps.LatLngBounds();
      console.log('SearBox', places);
      console.log('Bounds', bounds);
      // console.log('Positions:', bounds.extend(places.geometry.location), places.geometry.location, places.geometry.location.lat());
      // console.log(places.geometry.viewport);
      // console.log(places.geometry.location);
    });
  }

  DragFunction(destino, origen) {
    // console.log('Destino', destino);
    // console.log('Origen', origen);
    // /*PRUEBA*/
    // /*SEGUNDO MARKER INVISIBLE*/
    this.km2 = this.distancia(origen.lat, origen.lng, destino.lat, destino.lng);
    this.KM2 = this.km2 * 1000;
    this.distancia_Area = this.KM2;
    // console.log('DISTANCIA AREA', this.distancia_Area);

    /*GENERANDO RADIO*/
    this.area.push(new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.5,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.05,
      map: this.map,
      center: origen,
      radius: this.KM2
    }));

    /*GENERANDO 2NDO RADIO INVISIBLE*/
    this.area3.push(new google.maps.Circle({
      strokeColor: '#0c0ce8',
      strokeOpacity: 0.5,
      strokeWeight: 2,
      fillColor: '#0c0ce8',
      fillOpacity: 0.05,
      map: this.map,
      center: origen,
      radius: this.KM2 + 10000
    }));
  }

  _Area_kilometros() {

    for (let i in this.area) {
      console.log(this.area);
      this.area[i].setMap(null);
      this.area[i].strokeColor = "#0c0ce8";
      this.area[i].fillColor = "#0c0ce8";
    }

    this.area.push(new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.5,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.05,
      map: this.map,
      center: this.Origen,
      radius: this.distancia_Area
    }));

  }


  distancia(lat1: any, lon1: any, lat2: any, lon2: any) {
    let rad = (x) => {
      return x * Math.PI / 180;
    };
    let R = 6378.137; //Radio de la tierra en km
    let dLat = rad(lat2 - lat1);
    let dLong = rad(lon2 - lon1);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d.toFixed(3); //Retorna tres decimales
  }

  _citysOrigen(datos) {
    // console.log('ID ORIGEN', datos.data);
    this.OrigenID = datos.data;

    if (datos.data != null) {
      let DATA_CITY_ORIGEN: any = this.getResults(datos.data, 2);

      // ORIGEN SELECCIONADO
      // console.log('DATA_CITY_ORIGEN:', DATA_CITY_ORIGEN[0].latitud, DATA_CITY_ORIGEN[0].longitud);

      // verificando cuanto hay de distancia entre tu posición al epicentro de origen
      // console.log(GpsProvider.LATITUDE, GpsProvider.LONGITUDE);
      let from = turf.point([GpsProvider.LONGITUDE, GpsProvider.LATITUDE]);
      let to = turf.point([DATA_CITY_ORIGEN[0].longitud, DATA_CITY_ORIGEN[0].latitud]);
      let options: any = {units: 'kilometers'};
      let distance = turf.distance(from, to, options);
      // console.log('Hay una distancia de : ', Math.round(distance));
      this.OrigenAux = {lat: parseFloat(DATA_CITY_ORIGEN[0].latitud), lng: parseFloat(DATA_CITY_ORIGEN[0].longitud)};
      this.Origen = {lat: parseFloat(DATA_CITY_ORIGEN[0].latitud), lng: parseFloat(DATA_CITY_ORIGEN[0].longitud)};
      this.DestinoAux = {lat: parseFloat(DATA_CITY_ORIGEN[0].latitud), lng: parseFloat(DATA_CITY_ORIGEN[0].longitud)};
      this.Destino = {lat: parseFloat(DATA_CITY_ORIGEN[0].latitud), lng: parseFloat(DATA_CITY_ORIGEN[0].longitud)};
      if (Math.round(distance) > 20) {
        this.Complementos._MessageToast('No estas en la ciudad seleccionada', 3000);
        this.formOferta1.get('dir_cargue').setValue(null);
        this.Direccioncargue = true;
        // console.log(' o por aqui');
        this.formOferta1.get('origenmunicipio').setValue(null);
      } else {
        // console.log('por aqui');
        // console.log('id Ciudad origen', $event.data);
        this.formOferta1.get('origenmunicipio').setValue(datos.data);
      }
    } else {
      this.formOferta1.get('dir_cargue').setValue(null);
      this.Direccioncargue = true;
      // console.log(' o por aqui');
      this.formOferta1.get('origenmunicipio').setValue(null);
    }
  }

  _citysDestino($event) {
    if ($event.data != null) {
      this.DestinoID = $event.data;
      // console.log('ID DESTINO', $event.data);
      // console.log('id Ciudad destino', $event.data);
      this.formOferta1.get('destinomunicipio').setValue($event.data);
      let DATA_CITY_DESTINO: any = this.getResults($event.data, 2);
      // console.log('DATA_CITY_ORIGEN:', DATA_CITY_ORIGEN, DATA_CITY_ORIGEN[0].latitud, DATA_CITY_ORIGEN[0].longitud, '|', parseFloat(DATA_CITY_ORIGEN[0].longitud));
      this._Destino = {lat: parseFloat(DATA_CITY_DESTINO[0].latitud), lng: parseFloat(DATA_CITY_DESTINO[0].longitud)};
      // console.log('EL DESTINO CLICKEADO', this._Destino);
    } else {
      this.formOferta1.get('dir_descargue').setValue(null);
      this.Direcciondescargue = true;
      // console.log(' o por aqui');
      this.formOferta1.get('destinomunicipio').setValue(null);
    }
  }

  _Solicitudes() {
    this.Api._GET('/solicitudes').subscribe((data) => {
      if (data) {
        this.Solicitudes = data;
      }
    }, () => {
    }, () => {
      console.log('/solicitudes', this.Solicitudes)
    });
  }

  _TipoVehiculo() {
    this.Api._GET('/tipovehiculo').subscribe((data) => {
      if (data) {
        this.TIPO_VEHICULO = data;
      }
    }, () => {
    }, () => {
      console.log('/tipovehiculo', this.TIPO_VEHICULO)
    });
  }

  _TipoCarga() {
    this.Api._GET('/tipocarga').subscribe((data) => {
      if (data) {
        this.TIPO_CARGA = data;
      }
    }, () => {
    }, () => {
      console.log('/tipocarga', this.TIPO_CARGA)
    });
  }

  /*RANGO PERSONALIZADO*/
  _REGISTAR_OFERTA() {
    this.listado = [
      {
        latitud: this.lat,
        longitud: this.long,
        radio: this.distancia_Area
      },
    ];
    if (this._OrigenLat !== undefined && this._OrigenLng !== undefined) {
      let Obj2 = {
        latitud: this._OrigenLat,
        longitud: this._OrigenLng,
        radio: this.distancia_Area2
      };
      this.listado.push(Obj2);
    } else {
      console.log('no hay segundo market')
    }
    console.log('LONGITUD Y LATITUD', this.long, this.lat);
    console.log(this.listado);
    let Obj = Object.assign(this.formOferta.value, this.formOferta1.value, this.formOferta2.value, {
      // fechavigencia: moment(this.formOferta.get('fechaentrega').value).add(1, 'days').format("YYYY-MM-DD"),
      listado: this.listado,
      id_solicitud: null
    });
    console.log('VALOR', Number(Obj.valor));
    console.log('BODY', Obj);
    this.Api._POST('/oferta', Obj).subscribe(
      (data) => {

        console.log('response post Oferta', data);
        if (data.success === true) {
          this._MensajeToast('Oferta publicada');
          this.formOferta.reset();
          this.formOferta1.reset();
          this.formOferta2.reset();
          // this.events.publish('list-solicitud');
          this.navCtrl.setRoot(ListaOfertasPage);
        } else {
          this._MensajeToast('¡ERROR OFERTA NO REGISTRADA!');
        }
      },
      (err) => {
        this._MensajeToast(`${err.error.mensaje}`);
      },
      () => {
      }
    );
  }

  /*RANGO RECOMENDADO*/
  _REGISTAR_OFERTA2() {
    if (this.formOferta1.get('destinomunicipio').value == null && this.formOferta1.get('origenmunicipio').value == null && this.formOferta.invalid) {
      this.Complementos._MessageToast('Te faltan campos por llenar')
    } else {
      console.log('LONGITUD Y LATITUD', this.long, this.lat);
      let listado = [{
        latitud: this.Origen.lat,
        longitud: this.Origen.lng,
        radio: 20000
      }];
      let Obj = Object.assign(this.formOferta.value, this.formOferta1.value, this.formOferta2.value, {
        // fechavigencia: moment(this.formOferta.get('fechaentrega').value).add(1, 'days').format("YYYY-MM-DD"),
        listado: listado,
        id_solicitud: null
      });
      console.log('VALOR', Number(Obj.valor));
      console.log('BODY', Obj);
      this.Api._POST('/oferta', Obj).subscribe(
        (data) => {
          console.log('response post Oferta', data);
          if (data.success === true) {
            this._MensajeToast('Oferta publicada');
            this.formOferta.reset();
            this.formOferta1.reset();
            this.formOferta2.reset();
            // this.events.publish('list-solicitud');
            this.navCtrl.setRoot(ListaOfertasPage);
          } else {
            this._MensajeToast('¡ERROR OFERTA NO REGISTRADA!');
          }
        },
        (err) => {
          this._MensajeToast(`${err.error.mensaje}`);
        },
        () => {
        }
      );
    }
  }

  _MensajeToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000
    });

    toast.present().then(() => {
    });
  }
}
