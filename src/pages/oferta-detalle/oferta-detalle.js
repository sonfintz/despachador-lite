var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AppModule } from "../../app/app.module";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let OfertaDetallePage = class OfertaDetallePage {
    constructor(navCtrl, navParams, Api, Complementos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Api = Api;
        this.Complementos = Complementos;
        this.BASE_URL = AppModule.BASE_URL;
        this.Postulantes_2 = Array();
        this.loadDetalle();
        // this.oferta = this.navParams.get('data');
        // this.Postulantes_2 = this.navParams.get('data');
        this.segment = 'todas';
        this.updateSegment('todas');
        // console.log('LA DATA DE LA OFERTA', this.oferta);
    }
    loadDetalle() {
        console.log(this.navParams.get('data').oferta_check);
        this.Api._GET(`/solicitudes/detalleoferta/${this.navParams.get('data').oferta_check}`).subscribe((data) => {
            this.oferta = data;
            this.Postulantes_2 = data;
            console.log(this.oferta);
        });
    }
    _IMG_DEFAULT(event) {
        event.target.src = 'assets/imgs/perfil.png';
    }
    doRefresh(refresher) {
        this.Complementos._MessageToast('Actualizando Oferta');
        this._Postulantes();
        setTimeout(() => {
            // console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
    /**---------------------
     *  Metodo para el cambio en el Segmento
     */
    updateSegment(value) {
        this.segment = value;
        if (this.segment != 'todas') {
            console.log(this.segment);
            this._filter_for_Segment(this.Postulantes_2, this.segment);
        }
        else {
            this.oferta = this.Postulantes_2;
        }
    }
    _filter_for_Segment(ArrayData, item) {
        this.oferta = [];
        console.log('_filter_for_Segment', ArrayData, item);
        this.oferta = ArrayData.filter(i => i.estado === item.toUpperCase());
        console.log(this.oferta);
        /*ArrayData.forEach((i) => {
          console.log('EL ESTADO DE LA OFERTA', i.postulacion_estado.toLowerCase(), item, '|', i.postulacion_estado === item.toUpperCase());
          if (i.postulacion_estado === item.toUpperCase()) {
            // console.log('SI SON IGUALES', i.postulacion_estado.toLowerCase(), item);
            // console.log('ASDASDA', i);
            this.oferta.push(i);
            // console.log(` Las ${item}`, this.POSTULANTES);
          }
        })*/
    }
    /**-----------------------------**/
    /**--------------------------------
     *
     */
    Search_placa_name() {
        if (this.queryText) {
            // this.oferta = this.Postulantes_2.filter(item =>
            //   item.placa.toLowerCase().includes(this.queryText.toLowerCase())).slice(0, 5);
            this.oferta = this.Postulantes_2.filter(i => i.placa.toLowerCase().includes(this.queryText.toLowerCase()) || i.nombre.toLowerCase().includes(this.queryText.toLowerCase()));
            console.log(this.oferta);
        }
        else {
            this.oferta = this.Postulantes_2;
        }
    }
    _Postulantes() {
        console.log('OFERTA_ID', this.navParams.get('data'));
        this.Api._GET(`/solicitudes/detalleoferta/${this.navParams.get('data').ofertaid}`).subscribe((data) => {
            console.log('DATa ID', data);
            this.ofertaDetalle = data;
            this.Postulantes_2 = this.oferta.postulados;
            console.log('POSTULADOS', this.Postulantes_2);
            // this.POSTULANTES = data;
        }, (err) => {
            console.log(err);
        }, () => {
        });
    }
    _EstadoOferta(estado) {
        if ((estado === 'ACEPTADA') || (estado === 'CANCELADA')) {
            // console.log('Comparacion False:',estado);
            return false;
        }
        else {
            // console.log('Comparacion false');
            return true;
        }
    }
    /*
    * ------------------------------------------------
       *  METODOS PARA ACEPTAR O RECHAZAR AL TRANSPORTISTA
       *  -------------------------------------------------
       *
       *  Metodo aceptar ()
    * */
    aceptarTransportistas(id, data) {
        if (this._SOLICITUD.solicitudes_cantidadregistrados >= this._SOLICITUD.solicitudes_cantidadvehiculo) {
            this.Complementos._MessageToast('Cupo de vehiculos completo');
        }
        else {
            console.log('TRASPORTISTA', id);
            const Obj = {
                id_usuario: data.usuario_id,
                estado: 'aceptada'
            };
            this.Api._PUT(`/postulacion/${id}`, Obj)
                .subscribe((data) => {
                console.log('ACEPTADO:', data);
                if (data.success === true) {
                    this.Complementos._MessageToast('¡Transportista aceptado!');
                    this.navCtrl.pop();
                    this._Postulantes();
                    console.log('refresh');
                }
            }, (err) => {
                console.log('ERROR:', err);
                this.Complementos._MessageToast(`${err.error.mensaje}`);
            }, () => {
            });
        }
    }
    /*
    * Metodo rechazar
    * @param item
    * */
    rechazarTransportistas(id, data) {
        console.log('TRASPORTISTA', id);
        const Obj = {
            id_usuario: data.usuario_id,
            estado: 'cancelada'
        };
        this.Api._PUT(`/postulacion/${id}`, Obj)
            .subscribe((data) => {
            console.log('RECHAZADO:', data);
            if (data.success === true) {
                this.Complementos._MessageToast('¡TRANSPORTISTA RECHAZADO!');
                this.navCtrl.pop();
                this._Postulantes();
                console.log('refresh');
            }
        }, (err) => {
            console.log('ERROR:', err);
            this.Complementos._MessageToast(`¡ERROR EN EL SERVIDOR! ${JSON.stringify(err)}`);
        }, () => {
        });
    }
};
OfertaDetallePage = __decorate([
    Component({
        selector: 'page-oferta-detalle',
        templateUrl: 'oferta-detalle.html',
        providers: [ApiProvider, ComplementosviewsProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        ApiProvider,
        ComplementosviewsProvider])
], OfertaDetallePage);
export { OfertaDetallePage };
//# sourceMappingURL=oferta-detalle.js.map