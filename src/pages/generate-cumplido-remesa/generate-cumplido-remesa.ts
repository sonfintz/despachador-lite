import {Component} from '@angular/core';
import {Events, NavController, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';

@Component({
  selector: 'page-generate-cumplido-remesa',
  templateUrl: 'generate-cumplido-remesa.html',
})
export class GenerateCumplidoRemesaPage {
  public solicitud: any;
  public form: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              public events: Events,
              private Api: ApiProvider,
              public fb: FormBuilder,
              private Complementos: ComplementosviewsProvider,
              public toastCtrl: ToastController) {
    this.solicitud = this.navParams.get('data');
  }

  ngOnInit() {
    this.initForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenerateCumplidoRemesaPage');
  }

  initForm() {
    this.form = this.fb.group({
      id_remesa: [this.solicitud.id_remesa],
      // cargue: [null, [Validators.required]], // fecha DD/MM/YYYY
      descargue: [null, [Validators.required]], // fecha DD/MM/YYYY
      cantidad: [null, [Validators.required]],
    })
  }

  getCurrentTime() {
    return moment().format('YYYY-MM-DD')
  }

  saveData() {
    this.Complementos._MessageToast('Generando Cumplido Remesa');
    this.Api._POST(`/orden/cumplirremesa`, this.form.value).subscribe(
      (data) => {
        console.log(data);
        this.Complementos._MessageToast('Cumplido Inicial Generado');
        this.navCtrl.pop();
        this.events.publish('despachado');
      },
      (e) => this.Complementos._MessageToast(`${e.error.mensaje}`));
  }
}
