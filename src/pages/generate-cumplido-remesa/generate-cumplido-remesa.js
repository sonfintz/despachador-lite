var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams, PopoverController, ToastController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';
let GenerateCumplidoRemesaPage = class GenerateCumplidoRemesaPage {
    constructor(navCtrl, navParams, popoverCtrl, events, Api, fb, Complementos, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.Api = Api;
        this.fb = fb;
        this.Complementos = Complementos;
        this.toastCtrl = toastCtrl;
        this.solicitud = this.navParams.get('data');
    }
    ngOnInit() {
        this.initForm();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad GenerateCumplidoRemesaPage');
    }
    initForm() {
        this.form = this.fb.group({
            id_remesa: [this.solicitud.id_remesa],
            // cargue: [null, [Validators.required]], // fecha DD/MM/YYYY
            descargue: [null, [Validators.required]],
            cantidad: [null, [Validators.required]],
        });
    }
    getCurrentTime() {
        return moment().format('YYYY-MM-DD');
    }
    saveData() {
        this.Complementos._MessageToast('Generando Cumplido Remesa');
        this.Api._POST(`/orden/cumplirremesa`, this.form.value).subscribe((data) => {
            console.log(data);
            this.Complementos._MessageToast('Cumplido Inicial Generado');
            this.navCtrl.pop();
            this.events.publish('despachado');
        }, (e) => this.Complementos._MessageToast(`${e.error.mensaje}`));
    }
};
GenerateCumplidoRemesaPage = __decorate([
    Component({
        selector: 'page-generate-cumplido-remesa',
        templateUrl: 'generate-cumplido-remesa.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        PopoverController,
        Events,
        ApiProvider,
        FormBuilder,
        ComplementosviewsProvider,
        ToastController])
], GenerateCumplidoRemesaPage);
export { GenerateCumplidoRemesaPage };
//# sourceMappingURL=generate-cumplido-remesa.js.map