import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {GpsProvider} from "../../providers/gps/gps";
import {DeleteacountopinionPage} from "../deleteacountopinion/deleteacountopinion";

@Component({
  selector: 'page-delete-acount',
  templateUrl: 'delete-acount.html',
})
export class DeleteAcountPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              // private Complementos: ComplementosviewsProvider,
              public Api: ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeleteAcountPage');
  }

  confirmDelete() {
    this.navCtrl.push(DeleteacountopinionPage);
    /*this.Api._DELETE('/despachador/eliminarcuenta').subscribe((data) => {
      console.log(data);
      localStorage.clear();
      AppModule.My_Token = '';
      this._CloseGPS();
      this._DestroySocket();
      this.Complementos._MessageToast('¡Cuenta eliminada!');
      this.navCtrl.setRoot(LoginPage);
    })*/
  }

  /**-- Destruir SOCKET --**/

  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      console.log(' Disconect Socket', GpsProvider.EMIT);
      GpsProvider.EMIT.disconnect();
      console.log('1 Disconect Socket', GpsProvider.EMIT);
    }

  }

  /** CERRAR GPS **/
  _CloseGPS() {
    console.group('CERRANDO EL EMIT');
    console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
    console.groupEnd();
    // GpsProvider.Band_Conexion=false;
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }

}
