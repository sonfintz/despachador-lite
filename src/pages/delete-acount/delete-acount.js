var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { GpsProvider } from "../../providers/gps/gps";
import { DeleteacountopinionPage } from "../deleteacountopinion/deleteacountopinion";
let DeleteAcountPage = class DeleteAcountPage {
    constructor(navCtrl, navParams, 
        // private Complementos: ComplementosviewsProvider,
        Api) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Api = Api;
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DeleteAcountPage');
    }
    confirmDelete() {
        this.navCtrl.push(DeleteacountopinionPage);
        /*this.Api._DELETE('/despachador/eliminarcuenta').subscribe((data) => {
          console.log(data);
          localStorage.clear();
          AppModule.My_Token = '';
          this._CloseGPS();
          this._DestroySocket();
          this.Complementos._MessageToast('¡Cuenta eliminada!');
          this.navCtrl.setRoot(LoginPage);
        })*/
    }
    /**-- Destruir SOCKET --**/
    _DestroySocket() {
        GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
        GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
        if (GpsProvider.EMIT != undefined) {
            console.log(' Disconect Socket', GpsProvider.EMIT);
            GpsProvider.EMIT.disconnect();
            console.log('1 Disconect Socket', GpsProvider.EMIT);
        }
    }
    /** CERRAR GPS **/
    _CloseGPS() {
        console.group('CERRANDO EL EMIT');
        console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
        console.groupEnd();
        // GpsProvider.Band_Conexion=false;
        clearInterval(GpsProvider.intervalSocket);
        clearInterval(GpsProvider.antiFakeGPS);
        let D = clearInterval(GpsProvider.intervalSocket);
        GpsProvider.intervalSocket = 0;
        console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);
    }
};
DeleteAcountPage = __decorate([
    Component({
        selector: 'page-delete-acount',
        templateUrl: 'delete-acount.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        ApiProvider])
], DeleteAcountPage);
export { DeleteAcountPage };
//# sourceMappingURL=delete-acount.js.map