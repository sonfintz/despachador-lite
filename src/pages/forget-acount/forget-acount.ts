import {Component} from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {UserData} from "../../providers/user-data";
import {LoginPage} from "../login/login";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-forget-acount',
  templateUrl: 'forget-acount.html',
})
export class ForgetAcountPage {
  public Opc: number = 0;
  signup = {
    correo: ''
  };
  submitted = false;
  form: FormGroup;
  public _Municipios: any = [];

  constructor(public navCtrl: NavController,
              public Api: ApiProvider,
              public alertController: AlertController,
              public userData: UserData,
              public Complementos: ComplementosviewsProvider,
              public fb: FormBuilder) {
    this.initForm();
  }

  rootPages() {
    console.log('cliqueando');
    this.navCtrl.setRoot(LoginPage);
  }

  initForm() {
    this.form = this.fb.group({
      correo: [null, Validators.required],
    });
  }

  _consultAccount() {
    this.Api._GET(`/despachador/recuperaremail/${this.form.get('correo').value}`).subscribe(
      (data) => {
        console.log('CONSULTA DE EMAIL', data);
        const alert = this.alertController.create({
          title: `Correo enviado`,
          message: `Revisa tu bandeja de entrada o spam.
           Se te ha asignado una nueva contraseña para ingresar a Mueve Logistica,
            recomendamos actualizarla para mayor seguridad.`,
          buttons: [
            {
              text: 'Entendido',
              cssClass: 'botton3',
              role: "cancel"
            }
          ],
          cssClass: 'alertCustomCss3' // <- added this
        });
        alert.present();
      }, () => {
        // this.Complementos._ErrorRequest(err.status, err.statusText, 'Error con el servidor')
      })
  }

  _next() {
    console.log('next', this.Opc);
    if (this.Opc !== 1) {
      this.Opc = this.Opc + 1;
    }
  }

  _previous() {
    console.log('previous', this.Opc)
    if (this.Opc > 0) {
      this.Opc = this.Opc - 1;
      if (this.Opc === 1) {
      }
    }
  }

  _ConsultarCuenta() {
    // this.Api._POST('/registrardespachador', data.value).subscribe((data) => {
    //   console.log(data);
    //   this.Complementos._MessageToast('Usuario creado exitosamente');
    //   this.navCtrl.setRoot(LoginPage)
  }

}
