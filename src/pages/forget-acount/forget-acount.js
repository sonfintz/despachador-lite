var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { ApiProvider } from "../../providers/api/api";
import { UserData } from "../../providers/user-data";
import { LoginPage } from "../login/login";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let ForgetAcountPage = class ForgetAcountPage {
    constructor(navCtrl, Api, alertController, userData, Complementos, fb) {
        this.navCtrl = navCtrl;
        this.Api = Api;
        this.alertController = alertController;
        this.userData = userData;
        this.Complementos = Complementos;
        this.fb = fb;
        this.Opc = 0;
        this.signup = {
            correo: ''
        };
        this.submitted = false;
        this._Municipios = [];
        this.initForm();
    }
    rootPages() {
        console.log('cliqueando');
        this.navCtrl.setRoot(LoginPage);
    }
    initForm() {
        this.form = this.fb.group({
            correo: [null, Validators.required],
        });
    }
    _consultAccount() {
        this.Api._GET(`/despachador/recuperaremail/${this.form.get('correo').value}`).subscribe((data) => {
            console.log('CONSULTA DE EMAIL', data);
            const alert = this.alertController.create({
                title: `Correo enviado`,
                message: `Revisa tu bandeja de entrada o spam.
           Se te ha asignado una nueva contraseña para ingresar a Mueve Logistica,
            recomendamos actualizarla para mayor seguridad.`,
                buttons: [
                    {
                        text: 'Entendido',
                        cssClass: 'botton3',
                        role: "cancel"
                    }
                ],
                cssClass: 'alertCustomCss3' // <- added this
            });
            alert.present();
        }, () => {
            // this.Complementos._ErrorRequest(err.status, err.statusText, 'Error con el servidor')
        });
    }
    _next() {
        console.log('next', this.Opc);
        if (this.Opc !== 1) {
            this.Opc = this.Opc + 1;
        }
    }
    _previous() {
        console.log('previous', this.Opc);
        if (this.Opc > 0) {
            this.Opc = this.Opc - 1;
            if (this.Opc === 1) {
            }
        }
    }
    _ConsultarCuenta() {
        // this.Api._POST('/registrardespachador', data.value).subscribe((data) => {
        //   console.log(data);
        //   this.Complementos._MessageToast('Usuario creado exitosamente');
        //   this.navCtrl.setRoot(LoginPage)
    }
};
ForgetAcountPage = __decorate([
    Component({
        selector: 'page-forget-acount',
        templateUrl: 'forget-acount.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        AlertController,
        UserData,
        ComplementosviewsProvider,
        FormBuilder])
], ForgetAcountPage);
export { ForgetAcountPage };
//# sourceMappingURL=forget-acount.js.map