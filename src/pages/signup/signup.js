var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { ApiProvider } from "../../providers/api/api";
import { LoginPage } from "../login/login";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { TutorialPage } from "../tutorial/tutorial";
import { ValidateFormsProvider } from "../../providers/validate-forms/validate-forms";
import { LinksRedirect } from "../../providers/linksRedirect";
let SignupPage = class SignupPage {
    constructor(navCtrl, Api, userData, linksgo, ValidateForms, Complementos, fb) {
        this.navCtrl = navCtrl;
        this.Api = Api;
        this.userData = userData;
        this.linksgo = linksgo;
        this.ValidateForms = ValidateForms;
        this.Complementos = Complementos;
        this.fb = fb;
        this.Opc = 0;
        this.submitted = false;
        this._Municipios = [];
        this.passwordType = 'password';
        this.passwordType1 = 'password';
        this.passwordShown = false;
        this.passwordShown1 = false;
        this.initForm();
    }
    /*BACK IN PAGE*/
    rootPages() {
        console.log('cliqueando');
        this.navCtrl.setRoot(TutorialPage);
    }
    togglePassword() {
        this.passwordShown ? this.passwordType = 'password' : this.passwordType = 'text';
        this.passwordShown = !this.passwordShown;
    }
    togglePassword1() {
        this.passwordShown1 ? this.passwordType1 = 'password' : this.passwordType1 = 'text';
        this.passwordShown1 = !this.passwordShown1;
    }
    initForm() {
        /*PRIMERA PAGINA*/
        this.form = this.fb.group({
            personanombre: [null, [Validators.required, Validators.pattern(/[a-zA-Z]/)]],
            personanombre1: [null, [Validators.required, Validators.pattern(/[a-zA-Z]/)]],
            personatelefono: [null, [Validators.required], [this.ValidateForms.InputNumber.bind(this)]],
            contacto: [null, [Validators.maxLength(10)], [this.ValidateForms.InputNumber.bind(this)]],
            personaemail: [null, [Validators.required, Validators.email], [this.validar.bind(this, ["correo"])]],
            personaidentificacion: [null, [Validators.required], [this.validar.bind(this, ["identificacion"])]],
        });
        /*TERCERA PAGINA*/
        this.form1 = this.fb.group({
            login: [null, [Validators.required], [this.validar.bind(this, ["username"])]],
            password: [null, [Validators.required]],
            confirmpassword: [null, [Validators.required]],
            referido: ['', [], [this.validar.bind(this, ["referido"])]]
        });
        /*SEGUNDA PAGINA*/
        this.form2 = this.fb.group({
            empresa: [null, [Validators.required]],
            nit: [null, [Validators.required], [this.validar.bind(this, ["nit"])]],
            empresatelefono: [null, [Validators.required], [this.ValidateForms.InputNumber.bind(this)]],
            ref_laboral: [null, [Validators.required]],
            tlf_referencia: [null, [Validators.required], [this.ValidateForms.InputNumber.bind(this)]],
        });
        /*TERMINOS Y CONDICIONES*/
        this.form3 = this.fb.group({
            terminos_condiciones: [null, Validators.required]
        });
        /*VALUE CHANGES*/
        this.form1.get('password').valueChanges.subscribe((data) => {
            console.log(data);
            this.form1.get('confirmpassword').reset();
        });
        this.form1.get('confirmpassword').valueChanges.subscribe((data) => {
            if (data !== this.form1.get('password').value) {
                console.log('no coincide', data);
                this.form1.get('confirmpassword').setErrors({ invalidNet: `No coincide` });
            }
        });
        this.form2.get('nit').valueChanges.subscribe(() => {
            this.form2.get('empresa').reset();
        });
    }
    checkterminos(event) {
        console.log('TERMINOS', event.value);
        let terminos = event.value;
        if (!terminos) {
            this.form3.get('terminos_condiciones').reset();
        }
    }
    /*COMPROBAR CAMPOS------------------------*/
    validar(name_input, control) {
        return new Promise((resolve) => {
            if (name_input[0] == 'identificacion') {
                if (/^([0-9&. ])*$/.test(control.value)) {
                    console.log(control.value);
                    this.Api.GET(`/usuario/identificacion/${control.value}`).subscribe((data) => {
                        if (data && data.existe) {
                            if (data.existe == 'SI') {
                                console.log('IDENTIFICACION ENCONTRADA', data);
                                this.Complementos._MessageToast('Identificacion existente');
                                this.form.get('personaidentificacion').setErrors({ invalidNet: `Identificacion existente` });
                            }
                            else {
                                console.log('NO EXISTE');
                            }
                        }
                    }, (err) => {
                        console.log(err);
                    });
                    resolve(null);
                }
                else {
                    console.log(control);
                    console.log(control.value);
                    console.log({ notNumber: true });
                    control.reset();
                    resolve({ notNumber: true });
                }
            }
            if (name_input[0] == 'correo') {
                if (!this.form.get('personaemail').hasError('pattern')) {
                    this.Api.GET(`/usuario/email/${control.value}`).subscribe((data) => {
                        if (data && data.existe) {
                            if (data.existe == 'SI') {
                                console.log('CORREO ENCONTRADO', data);
                                this.Complementos._MessageToast('Correo existente');
                                // this.form.get('personaemail').reset();
                                this.form.get('personaemail').setErrors({ invalidNet: `Correo existente` });
                            }
                            else {
                                console.log('NO EXISTE');
                            }
                        }
                    });
                }
            }
            if (name_input[0] == 'nit') {
                if (control.value) {
                    if (control.value.length >= 9) {
                        this.Api.GET(`/consultaexterna/nit/${control.value}`).subscribe((data) => {
                            console.log(data);
                            if (data && data.length > 0) {
                                console.log('EMPRESA CONSEGUIDA', data);
                                console.log('NOMBRE DE EMPRESA', data[0].nombre);
                                this.form2.get('empresa').setValue(data[0].nombre);
                            }
                        });
                    }
                    else {
                        console.log('', control.value);
                    }
                }
            }
            if (name_input[0] == 'referido') {
                if (control.value) {
                    if (control.value.length >= 4) {
                        this.Api.GET(`/despachador/verificar/pin/${control.value}`).subscribe((data) => {
                            console.log(data);
                            if (data && data.existe) {
                                if (data.existe == 'SI') {
                                    this.Complementos._MessageToast('PIN válido');
                                    console.log('PIN CONSEGUIDO', data);
                                }
                                else {
                                    this.Complementos._MessageToast('PIN no válido');
                                    this.form1.get('referido').setErrors({ invalidNet: `PIN no existente` });
                                    console.log('PIN NO EXISTE');
                                }
                            }
                        });
                    }
                }
            }
            if (name_input[0] == 'username') {
                this.Api.GET(`/consultaexterna/nit/${control.value}`).subscribe((data) => {
                    if (data) {
                        if (data.length > 0) {
                            console.log('USUARIO ENCONTRADO', data);
                        }
                    }
                });
            }
            resolve(null);
        });
    }
    /*ADELANTE Y ATRAS (PAGINAS)-------------*/
    _next() {
        console.log('next', this.Opc);
        if (this.Opc !== 2) {
            this.Opc = this.Opc + 1;
        }
    }
    _next2() {
        console.log('next', this.Opc);
        if (this.Opc === 1) {
            this.Opc = this.Opc + 1;
        }
    }
    _next3() {
        console.log('next', this.Opc);
        if (this.Opc === 2) {
            this.Opc = this.Opc + 1;
        }
    }
    _previous() {
        console.log('previous', this.Opc);
        if (this.Opc > 0) {
            this.Opc = this.Opc - 1;
            if (this.Opc === 1) {
            }
        }
    }
    linkref() {
        window.open('http://www.grupomueve.com/politica-de-tratamiento-de-datos-personales/', '_system');
    }
    /*GUARDAR LA DATA-----------------------*/
    saveData() {
        let form1 = {
            login: this.form1.value.login,
            password: this.form1.value.password,
            referido: this.form1.value.referido
        };
        let form = {
            personanombre: this.form.value.personanombre + ' ' + this.form.value.personanombre1,
            personatelefono: this.form.value.personatelefono,
            contacto: this.form.value.contacto,
            personaemail: this.form.value.personaemail,
            personaidentificacion: this.form.value.personaidentificacion,
        };
        let OBj;
        OBj = Object.assign(form, form1, this.form2.value);
        console.log('EL OBJETO', OBj);
        this.Api._POST('/despachador', OBj).subscribe((data) => {
            console.log(data);
            this.Complementos._MessageToast('Usuario creado exitosamente');
            this.navCtrl.setRoot(LoginPage);
        }, (err) => {
            this.Complementos._MessageToast(`${err.error.mensaje}`, 2000);
        });
    }
};
SignupPage = __decorate([
    Component({
        selector: 'page-user',
        templateUrl: 'signup.html'
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        UserData,
        LinksRedirect,
        ValidateFormsProvider,
        ComplementosviewsProvider,
        FormBuilder])
], SignupPage);
export { SignupPage };
//# sourceMappingURL=signup.js.map