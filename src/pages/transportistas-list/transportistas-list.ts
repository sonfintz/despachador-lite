import {Component} from '@angular/core';
import {Events, NavController, NavParams, PopoverController, Refresher} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {AddTransportistaPage} from "../add-transportista/add-transportista";
import {TrailerPage} from "../trailer/trailer";
import {VehiculoPage} from "../vehiculo/vehiculo";
import {EditartrailerPage} from "../editartrailer/editartrailer";
import {EditarvehiculoPage} from "../editarvehiculo/editarvehiculo";
import * as moment from "moment";

@Component({
  selector: 'page-transportistas-list',
  templateUrl: 'transportistas-list.html',
})
export class TransportistasListPage {
  public transportistas: any;
  public transportistas_2: any;
  queryText = '';
  public segment = 'todos';


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              public events: Events,
              public Api: ApiProvider) {
  }

  validacionTrailer(t) {
    return Boolean(t.trailer_placa)
  }

  validacionVehiculo(t) {
    return Boolean(t.vehiculo_id)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransportistasListPage');
  }

  doRefresh(refresher: Refresher) {
    this.segment = 'todos';
    this.loadData();
    setTimeout(() => {
      refresher.complete();

    }, 1000);

  }

  ngOnInit() {
    this.events.subscribe('list-transport', () => {
      this.loadData();
    });
  }

  ionViewWillEnter() {
    this.loadData();
  }

  updateSchedule() {
    if (this.queryText) {
      console.log('SEARCH', this.queryText);
      this.transportistas.filter(item => console.log(item.persona_nombre));
      this.transportistas_2.filter(item => console.log(item.persona_nombre));
      this.transportistas = this.transportistas_2.filter(item => item.persona_nombre.toLowerCase().startsWith(this.queryText.toLowerCase())).slice(0, 5);
    } else {
      this.transportistas = this.transportistas_2
    }
    //   if (this.Search) {
    //     console.log('SEARCH', this.Search);
    //     this.LIST_OFERT = this.SOLICITUD_2.filter(item => item.generador_nombre.toLowerCase().startsWith(this.Search.toLowerCase())).slice(0, 5);
    //   } else {
    //     this.LIST_OFERT = this.SOLICITUD_2;
    //   }
  }

  _listaTransportistas(value) {
    this.segment = value;
    if (value === 'todos') {
      this.Api._GET('/transportista').subscribe((data) => {
        this.transportistas = data;
        this.transportistas_2 = data;
        console.log(data);
      })
    }
    if (value === 'listos') {
      this.Api._GET('/transportista').subscribe((data) => {
        data = data.filter(i => i.trailer_placa !== 'SIN TRAILER' && i.vehiculo_placa !== 'SIN VEHICULO');
        this.transportistas = data;
        this.transportistas_2 = data;
        console.log(data);
      })
    }
    if (value === 'completar') {
      this.Api._GET('/transportista').subscribe((data) => {
        data = data.filter(i => i.trailer_placa === 'SIN TRAILER' || i.vehiculo_placa === 'SIN VEHICULO');
        this.transportistas = data;
        this.transportistas_2 = data;
        console.log(data);
      })
    }
  }

  /*VEHICULO Y TRAILER*/
  _CrearVehiculo(data: any) {
    console.log('VEHICULO');
    this.navCtrl.push(VehiculoPage, {data: data});
  }

  _CrearTrailer(data: any) {
    console.log('TRAILER');
    this.navCtrl.push(TrailerPage, {data: data});
    // this.navCtrl.push(OfertasPage, {data: this.navParams.get('data')});
  }

  _EditarTrailer(data: any) {
    console.log('TRAILER');
    this.Api._GET(`/trailer/${data.trailer_placa}`).subscribe((data) => {
      console.log('EDITAR TRAILER', data);
      this.navCtrl.push(EditartrailerPage, {data: data});
    });
  }

  _EditarVehiculo(data: any) {
    console.log('VEHICULO');
    this.Api._GET(`/vehiculo/${data.vehiculo_placa}`).subscribe((data) => {
      data = data.map((i) => {
        i.fechavencesoat = moment(i.fechavencesoat).format('YYYY-MM-DD');
        return i;
      });
      console.log('EDITAR VEHICULO', data);
      this.navCtrl.push(EditarvehiculoPage, {data: data});
    });
  }

  /*presentPopover(myEvent, data: any) {
    let popover = this.popoverCtrl.create(PopoverTransportistaPage, {data: data});
    popover.present({
      ev: myEvent
    });
  }*/

  loadData() {
    // this.Complementos.PresentLoading('¡Cargando Transportistas!', 200);
    this.Api._GET('/transportista').subscribe(
      (data) => {
        this.transportistas = data;
        this.transportistas_2 = data;
        console.log('ASDASD', this.transportistas);
      },
      (err) => {
        if (err.status === 401) {
          // this.events.publish('sesionDuplicada', err)
        } else {
          console.error(err)
        }
      }
    )
  }

  // ngOnInit() {
  //   this.loadData();
  // }

  addTransportista() {
    this.navCtrl.push(AddTransportistaPage, {});
  }

}
