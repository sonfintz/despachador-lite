import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {GpsProvider} from "../../providers/gps/gps";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {FormBuilder} from "@angular/forms";
import {AppModule} from "../../app/app.module";
import {LoginPage} from "../login/login";

@Component({
  selector: 'page-deleteacountopinion',
  templateUrl: 'deleteacountopinion.html',
})
export class DeleteacountopinionPage {
  /*public cuestionario: any;
  form: FormGroup;
  selector: any;
  anotacion: string;*/

  public SectionNum: number = 1;
  // private SendSuccessPOST: boolean = false;
  public items: Array<any> = Array();
  public groupQuestions: Array<any> = Array();
  public comentario: string = null;
  public cuestionario: any;


  constructor(public navCtrl: NavController,
              public Api: ApiProvider,
              public formBuilder: FormBuilder,
              public Complementos: ComplementosviewsProvider,
              public events: Events,
              public Complements: ComplementosviewsProvider,
              public navParams: NavParams) {
    this.Api._GET('/preguntas').subscribe((data) => {
      this.cuestionario = data;
      console.log('CUESTIONARIO', data);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeleteacountopinionPage');
  }

  changeEstateItems(events, id) {
    console.log(events, events._value, id);
    if (events._value) {
      this.groupQuestions.push(id)
    } else {
      let index = this.groupQuestions.indexOf(id);
      this.groupQuestions.splice(index, 1);
    }

    console.log(this.groupQuestions)
  }

  confirmDelete() {
    if (this.groupQuestions.length > 0) {
      let Obj = {
        ids: this.groupQuestions,
        descripcion: this.comentario
      };
      console.log(`/despachador/desvincular/${0}`, Obj);
      this.Api._PUT(`/despachador/desvincular/${localStorage.getItem('idUserMUEVE')}`, Obj).subscribe(
        (data) => {
          console.log(data);
          localStorage.clear();
          AppModule.My_Token = '';
          this._DestroySocket();
          this._CloseGPS();
          this.Complementos._MessageToast('¡Cuenta eliminada!');
          this.navCtrl.setRoot(LoginPage);
        },
        error1 => {
          console.log(error1)
        },
        () => {
        })
    } else {
      this.Complementos.showAlert("Error", "", "No seleccionaste una opción", [{
        text: 'ACEPTAR',
        cssClass: "botton2", role: "cancel"
      }], "alertCustomCss", true)
    }
  }

  /**-- Destruir SOCKET --**/
  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      console.log(' Disconect Socket', GpsProvider.EMIT);
      GpsProvider.EMIT.disconnect();
      console.log('1 Disconect Socket', GpsProvider.EMIT);
    }

  }

  /** CERRAR GPS **/
  _CloseGPS() {
    console.group('CERRANDO EL EMIT');
    console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
    console.groupEnd();
    // GpsProvider.Band_Conexion=false;
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }
}
