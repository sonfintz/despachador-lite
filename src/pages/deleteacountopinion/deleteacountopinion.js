var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { GpsProvider } from "../../providers/gps/gps";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { FormBuilder } from "@angular/forms";
import { AppModule } from "../../app/app.module";
import { LoginPage } from "../login/login";
let DeleteacountopinionPage = class DeleteacountopinionPage {
    constructor(navCtrl, Api, formBuilder, Complementos, events, Complements, navParams) {
        this.navCtrl = navCtrl;
        this.Api = Api;
        this.formBuilder = formBuilder;
        this.Complementos = Complementos;
        this.events = events;
        this.Complements = Complements;
        this.navParams = navParams;
        /*public cuestionario: any;
        form: FormGroup;
        selector: any;
        anotacion: string;*/
        this.SectionNum = 1;
        // private SendSuccessPOST: boolean = false;
        this.items = Array();
        this.groupQuestions = Array();
        this.comentario = null;
        this.Api._GET('/preguntas').subscribe((data) => {
            this.cuestionario = data;
            console.log('CUESTIONARIO', data);
        });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DeleteacountopinionPage');
    }
    changeEstateItems(events, id) {
        console.log(events, events._value, id);
        if (events._value) {
            this.groupQuestions.push(id);
        }
        else {
            let index = this.groupQuestions.indexOf(id);
            this.groupQuestions.splice(index, 1);
        }
        console.log(this.groupQuestions);
    }
    confirmDelete() {
        if (this.groupQuestions.length > 0) {
            let Obj = {
                ids: this.groupQuestions,
                descripcion: this.comentario
            };
            console.log(`/despachador/desvincular/${0}`, Obj);
            this.Api._PUT(`/despachador/desvincular/${localStorage.getItem('idUserMUEVE')}`, Obj).subscribe((data) => {
                console.log(data);
                localStorage.clear();
                AppModule.My_Token = '';
                this._DestroySocket();
                this._CloseGPS();
                this.Complementos._MessageToast('¡Cuenta eliminada!');
                this.navCtrl.setRoot(LoginPage);
            }, error1 => {
                console.log(error1);
            }, () => {
            });
        }
        else {
            this.Complementos.showAlert("Error", "", "No seleccionaste una opción", [{
                    text: 'ACEPTAR',
                    cssClass: "botton2", role: "cancel"
                }], "alertCustomCss", true);
        }
    }
    /**-- Destruir SOCKET --**/
    _DestroySocket() {
        GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
        GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
        if (GpsProvider.EMIT != undefined) {
            console.log(' Disconect Socket', GpsProvider.EMIT);
            GpsProvider.EMIT.disconnect();
            console.log('1 Disconect Socket', GpsProvider.EMIT);
        }
    }
    /** CERRAR GPS **/
    _CloseGPS() {
        console.group('CERRANDO EL EMIT');
        console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
        console.groupEnd();
        // GpsProvider.Band_Conexion=false;
        clearInterval(GpsProvider.intervalSocket);
        clearInterval(GpsProvider.antiFakeGPS);
        let D = clearInterval(GpsProvider.intervalSocket);
        GpsProvider.intervalSocket = 0;
        console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);
    }
};
DeleteacountopinionPage = __decorate([
    Component({
        selector: 'page-deleteacountopinion',
        templateUrl: 'deleteacountopinion.html',
    }),
    __metadata("design:paramtypes", [NavController,
        ApiProvider,
        FormBuilder,
        ComplementosviewsProvider,
        Events,
        ComplementosviewsProvider,
        NavParams])
], DeleteacountopinionPage);
export { DeleteacountopinionPage };
//# sourceMappingURL=deleteacountopinion.js.map