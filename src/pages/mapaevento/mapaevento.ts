import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import * as turf from '@turf/turf';
import {ApiProvider} from "../../providers/api/api";

declare var google;

@Component({
  selector: 'page-mapaevento',
  templateUrl: 'mapaevento.html'
})
export class MapaeventoPage {

  public long: any;
  public lat: any;
  map: any;
  public Origen: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  public _Carga: any;
  public circle: any;
  public circle2: any;
  public dragend = false;

  public referencia = [
    {
      id: 2,
      id_municipio: 3,
      latitud: 7.89807582976537,
      longitud: -72.51189695770262,
      nombre: 'INDIO'
    },
    {
      id: 3,
      id_municipio: 4,
      latitud: 7.889871655547514,
      longitud: -72.50743376190184,
      nombre: 'PARQUE MERCEDEZ'
    },
    {
      id: 4,
      id_municipio: 5,
      latitud: 7.8942500693104485,
      longitud: -72.50116812164305,
      nombre: 'PLAZA BANDERAS'
    },
  ];


  /*TURF*/
  public _SegundoMarketLat: any;
  public _SegundoMarketLng: any;
  private ultimaPosicion: any;
  private dentroRadio: boolean;
  private ciudadOriginal: any;

  constructor(public navCtrl: NavController,
              public events: Events,
              public api: ApiProvider,
              private Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    if (this.navParams.get('value') == 1 || this.navParams.get('value') == 2) {
      this.loadMunicipios();
      this.ciudadOriginal = {lat: this.navParams.get('aux').latitud, lng: this.navParams.get('aux').longitud};
      // this.ciudadOriginal = this.navParams.get('aux');
      console.log('LA CIUDAD ORIGINAL', this.ciudadOriginal);
      console.log(this.navParams.get('value'));
      this.long = this.navParams.get('data').longitud;
      this.lat = this.navParams.get('data').latitud;
      console.log('LA DATA QUE ME LLEVO', this.long, this.lat);
      this.Origen = {lat: Number(this.lat), lng: Number(this.long)};
      this.directionsService = new google.maps.DirectionsService();
      this.directionsDisplay = new google.maps.DirectionsRenderer();
      this.bounds = new google.maps.LatLngBounds();
    } else {
      console.log(this.navParams.get('value'));
      this.long = this.navParams.get('data').longitud;
      this.lat = this.navParams.get('data').latitud;
      this.Origen = {lat: Number(this.lat), lng: Number(this.long)};
      this.directionsService = new google.maps.DirectionsService();
      this.directionsDisplay = new google.maps.DirectionsRenderer();
      this.bounds = new google.maps.LatLngBounds();
    }
  }

  ReferenciaEstablecida(referencia) {
    this.dragend = true;
    // console.log(referencia);
    this.long = referencia.longitud;
    this.lat = referencia.latitud;
    this.Origen = {lat: Number(this.lat), lng: Number(this.long)};
    this.loadMap();
  }

  loadMunicipios() {
    let idLugar = this.navParams.get('data').idOrigen ? this.navParams.get('data').idOrigen : this.navParams.get('data').idDestino;
    this.api._GET(`/referenciamapa/${idLugar}`).subscribe((data) => {
      this.referencia = data;
    })
  }

  addUbication() {
    if (this.navParams.get('value') == 1) {
      // console.log('1');
      if (!this._Carga) {
        // console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
        this.Complementos._MessageToast('No ha establecido una ciudad');
      }
      if (this.dentroRadio == false) {
        // console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
        this.Complementos._MessageToast('Fuera de la ciudad establecida');
      } else {
        // console.log('SI HAY UNA CIUDAD ESTABLECIDA', this._Carga);
        this.events.publish('cargaydescarga', this._Carga);
        this.navCtrl.pop();
      }
    } else if (this.navParams.get('value') == 2) {
      // console.log('2');
      if (!this._Carga) {
        // console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
        this.Complementos._MessageToast('No ha establecido una ciudad');
        return false
      }
      if (this.dentroRadio == false) {
        // console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
        this.Complementos._MessageToast('Fuera de la ciudad establecida');
      } else {
        // console.log('SI HAY UNA CIUDAD ESTABLECIDA', this._Carga);
        this.events.publish('cargaydescarga', this._Carga);
        this.navCtrl.pop();
      }
    }
  }

  /* ionViewCanLeave(): boolean {
     console.log('SALIDA DE INTERFAZ', this._Carga);
     if (this.navParams.get('value') == 1) {
       console.log('1');
       if (!this._Carga) {
         console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
         this.Complementos._MessageToast('No ha establecido una ciudad');
         return false
       }
       if (this.dentroRadio == false) {
         console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
         this.Complementos._MessageToast('Fuera de la ciudad establecida');
         return false
       } else {
         console.log('SI HAY UNA CIUDAD ESTABLECIDA', this._Carga);
         this.events.publish('cargaydescarga', this._Carga);
         return true
       }
     } else if (this.navParams.get('value') == 2) {
       console.log('2');
       if (!this._Carga) {
         console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
         this.Complementos._MessageToast('No ha establecido una ciudad');
         return false
       }
       if (this.dentroRadio == false) {
         console.log('NO HAY UNA CIUDAD ESTABLECIDA', this._Carga);
         this.Complementos._MessageToast('Fuera de la ciudad establecida');
         return false
       } else {
         console.log('SI HAY UNA CIUDAD ESTABLECIDA', this._Carga);
         this.events.publish('cargaydescarga', this._Carga);
         return true
       }
     }
     if (this.navParams.get('value') !== 1 || this.navParams.get('value') !== 2) {
       return true
     }
   }*/

  ionViewDidLoad() {
    this.loadMap();
    // console.log('ionViewDidLoad MapaeventoPage');
  }

  loadMap() {
    // console.log('------------------------------', this.long, this.lat);

    let mapEle: HTMLElement = document.getElementById('map');

    // console.log('1');
    this.map = new google.maps.Map(mapEle, {
      center: this.Origen,
      zoom: 15
    });

    // console.log('2');


    this.directionsDisplay.setMap(this.map);
    // console.log('this.Origen:', this.Origen);
    this.long = this.Origen.lng;
    this.lat = this.Origen.lat;
    // console.log('3');
    // console.log('------------------------------', this.long, this.lat);

    if (this.navParams.get('value') == 1) {
      /*DIRECCION DE CARGUE*/
      // console.log('VALOR DEL VALUE', this.navParams.get('value'));
      let origen = new google.maps.Marker({
        position: this.Origen,
        map: this.map,
        label: 'O',
        strokeColor: "blue",
        draggable: true,
        animation: google.maps.Animation.DROP,
        title: 'Ciudad Cargue'
      });
      console.log('4', origen);

      /*EVENTO DRAG DEL MARKER PARA DIRECCION DE CARGUE*/
      google.maps.event.addListener(origen, 'drag', () => {
        // console.log(data)
      });

      /*EVENTO DRAGEND DEL MARKER DRAGABLE PARA EL RADIO*/
      google.maps.event.addListener(origen, 'dragend', (data) => {

        this.dragend = false;
        /*ULTIMA POSICION EN LA QUE ESTABA*/
        this.ultimaPosicion = turf.point([parseFloat(data.latLng.lng()), parseFloat(data.latLng.lat())]);

        /*GENERANDO PUNTOS TURF*/
        // console.log('EL ORIGEN SETEADO', this.Origen);
        let p1 = turf.point([parseFloat(this.ciudadOriginal.lng), parseFloat(this.ciudadOriginal.lat)]);
        let radius = 50;
        let options: any = {steps: 1000, units: 'kilometers', properties: {foo: 'bar'}};
        this.circle = turf.circle(p1, radius, options);
        /////////////////////////////////////

        /*boolean*/
        this.dentroRadio = turf.booleanPointInPolygon(this.ultimaPosicion, this.circle);
        // console.log(this.dentroRadio);
        /**/

        if (this.dentroRadio == false) {
          this.Complementos._MessageToast('Fuera de la ciudad seleccionada')
        }


        /*DECLARANDO VARIABLES*/
        let geocoder = new google.maps.Geocoder;

        /*GEOCODER*/
        let latlng = {lat: parseFloat(data.latLng.lat()), lng: parseFloat(data.latLng.lng())};
        let latitud = data.latLng.lat();
        let longitud = data.latLng.lng();
        /*INSTANCIANDO EL THIS*/
        let thas = this;
        /*FUNCION GEOCODER LUGAR EXACTA*/
        geocoder.geocode({'location': latlng}, function (results, status) {
          if (status === 'OK') {
            if (results[0]) {
              console.log(results[0]);
              console.log(results[0].formatted_address);

              thas._Carga = {
                latitud: latitud,
                longitud: longitud,
                nombre: results[0].formatted_address,
                origen: 'carga'
              };
              // console.log('CIUDAD A CARGAR', thas._Carga);
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }

        });
        /*FIN*/

        // this._ButtonAux = true;
        // origen.visible = false;
      });
      /////////////////////////////////////////////////

    } else if (this.navParams.get('value') == 2) {


      /*DIRECCION DE DESCARGUE*/
      // console.log('VALOR DEL VALUE', this.navParams.get('value'));

      let origen = new google.maps.Marker({
        position: this.Origen,
        map: this.map,
        label: 'O',
        strokeColor: "blue",
        draggable: true,
        animation: google.maps.Animation.DROP,
        title: 'Ciudad Descargue'
      });
      // console.log('4', origen);

      /*EVENTO DRAG DEL MARKER PARA DIRECCION DE CARGUE*/
      google.maps.event.addListener(origen, 'drag', () => {
        // console.log(data)
      });

      /*EVENTO DRAGEND DEL MARKER DRAGABLE PARA EL RADIO*/
      google.maps.event.addListener(origen, 'dragend', (data) => {
        this.dragend = false;
        /*ULTIMA POSICION EN LA QUE ESTABA*/
        this.ultimaPosicion = turf.point([parseFloat(data.latLng.lng()), parseFloat(data.latLng.lat())]);

        /*GENERANDO PUNTOS TURF*/
        let p1 = turf.point([parseFloat(this.ciudadOriginal.lng), parseFloat(this.ciudadOriginal.lat)]);
        let radius = 50;
        let options: any = {steps: 1000, units: 'kilometers', properties: {foo: 'bar'}};
        this.circle = turf.circle(p1, radius, options);
        /////////////////////////////////////

        /*boolean*/
        this.dentroRadio = turf.booleanPointInPolygon(this.ultimaPosicion, this.circle);
        // console.log(this.dentroRadio);
        /**/

        if (this.dentroRadio == false) {
          this.Complementos._MessageToast('Fuera de la ciudad seleccionada')
        }

        /*DECLARANDO VARIABLES*/
        let geocoder = new google.maps.Geocoder;


        /*GEOCODER*/
        let latlng = {lat: parseFloat(data.latLng.lat()), lng: parseFloat(data.latLng.lng())};
        let latitud = data.latLng.lat();
        let longitud = data.latLng.lng();
        /*INSTANCIANDO EL THIS*/
        let thas = this;
        /*FUNCION GEOCODER LUGAR EXACTA*/
        geocoder.geocode({'location': latlng}, function (results, status) {
          if (status === 'OK') {
            if (results[0]) {

              // console.log(results[0].formatted_address);

              thas._Carga = {
                latitud: latitud,
                longitud: longitud,
                nombre: results[0].formatted_address,
                origen: 'descargue'
              };
              // console.log('CIUDAD A DESCARGUE', thas._Carga);
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }

        });
        /*FIN*/

        // this._ButtonAux = true;
        // origen.visible = false;
      });
      /////////////////////////////////////////////////
    } else {
      let origen = new google.maps.Marker({
        position: this.Origen,
        map: this.map,
        label: 'O',
        strokeColor: "blue",
        //draggable: true ,
        animation: google.maps.Animation.DROP,
        title: 'Ciudad Origen'
      });
      console.log('4', origen);

    }

    /*let destino = new google.maps.Marker({
      position: (!this.Destino) ? {lat: 7.913728, lng: -72.5027235} : this.Destino,
      map: this.map,
      // icon: image, //'',
      draggable: true,
      animation: google.maps.Animation.BOUNCE,
      title: 'Ciudad Origen'
    });
    destino.addListener('click', () => {

    });
    destino.addListener('drag', () => {

    });*/

    /*google.maps.event.addListener(destino, 'drag', () => {

      /!*  let lat  = destino.getPosition().lat();
          let lng  = destino.getPosition().lng(); *!/
      //console.log('Marker',lat,lng);

      let km: any = this.distancia(origen.getPosition().lat(), origen.getPosition().lng(), destino.getPosition().lat(), destino.getPosition().lng());
      let KM = km * 1000;

      this.Origen = origen.getPosition();
      this.Destino = destino.getPosition();
      this.Origen_lat = origen.getPosition().lat();
      this.Origen_lng = origen.getPosition().lng();
      this.distancia_Area = KM;
      if (KM <= 20000) {
        for (let i in this.area) {
          this.area[i].setMap(null);
          this.area[i].strokeColor = "#0c0ce8";
          this.area[i].fillColor = "#0c0ce8";
        }

        this.area.push(new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.5,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.05,
          map: this.map,
          center: origen.getPosition(),
          radius: KM
        }));

      } else {
        this.distancia_Area = 20000
      }


    });*/

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
    });
    // console.log('5');
    let input = document.getElementById('pac-input');
    let searchBox = new google.maps.places.Autocomplete(input, {});
    searchBox.addListener('place_changed', () => {
      let places = searchBox.getPlace();
      let bounds = new google.maps.LatLngBounds();
      console.log('SearBox', places);
      console.log('6');
      console.log('Positions:', bounds.extend(places.geometry.location), places.geometry.location, places.geometry.location.lat());
      // if (places.geometry.viewport) {
      // Only geocodes have viewport.
      console.log(places.geometry.viewport);
      //  } else {
      console.log(places.geometry.location);
      console.log('7');
      // }
    });
  }


}
