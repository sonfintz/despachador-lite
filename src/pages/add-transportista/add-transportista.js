var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
import { TransportistasListPage } from "../transportistas-list/transportistas-list";
import * as moment from 'moment';
let AddTransportistaPage = class AddTransportistaPage {
    constructor(navCtrl, navParams, Api, Complementos, events, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Api = Api;
        this.Complementos = Complementos;
        this.events = events;
        this.formBuilder = formBuilder;
        this._Municipios = [];
        this._Sucursal = [];
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad AddTransportistaPage');
    }
    ngOnInit() {
        this._Ubicaciones();
        this.initForm();
    }
    initForm() {
        this.form = this.formBuilder.group({
            usuario: [null, [Validators.required]],
            clave: [null, [Validators.required]],
            personaidentificacion: [null, [Validators.required]],
            personanombre: [null, [Validators.required]],
            primerapellido: [null, [Validators.required]],
            id_municipio: [null, [Validators.required]],
            personadireccion: [null, [Validators.required]],
            personatelefono: [null, [Validators.required]],
            numerolicencia: [null, [Validators.required]],
            personaemail: [null, [Validators.required]],
            licencia: ['B3', [Validators.required]],
            eps: [null, [Validators.required]],
            id_sucursal: [null, [Validators.required]],
        });
    }
    getCurrentTime() {
        return moment().format('YYYY-MM-DD');
    }
    _UbicacionOutput(datos) {
        console.log('typeahead', datos);
        if (datos.data != null) {
            console.log('Output ubicacion', datos);
            this.form.get('id_municipio').setValue(datos.data);
        }
        else {
            this.form.get('id_municipio').setValue('');
        }
    }
    _SucursalOutput(datos) {
        if (datos.data != null) {
            console.log('Output sucursal', datos);
            this.form.get('id_sucursal').setValue(datos.data);
        }
        else {
            this.form.get('id_sucursal').setValue('');
        }
    }
    _Ubicaciones() {
        this.Api._GET('/municipio')
            .subscribe((data) => {
            console.log('_Municipios:', data);
            this._Municipios = data;
        }, () => {
        }, () => {
        });
        this.Api._GET('/sucursal')
            .subscribe((data) => {
            console.log('_Sucursal:', data);
            this._Sucursal = data;
        }, () => {
        }, () => {
        });
    }
    saveData() {
        let form = this.form.value;
        let Obj = {
            clave: form.clave,
            eps: moment(form.eps).format('YYYY/MM/DD'),
            id_municipio: form.id_municipio,
            id_sucursal: form.id_sucursal,
            licencia: form.licencia,
            numerolicencia: form.numerolicencia,
            personadireccion: form.personadireccion,
            personaemail: form.personaemail,
            personaidentificacion: form.personaidentificacion,
            personanombre: form.personanombre,
            personatelefono: form.personatelefono,
            primerapellido: form.primerapellido,
            usuario: form.usuario
        };
        console.log(Obj);
        this.Api._POST('/transportista', Obj)
            .subscribe((data) => {
            console.log(data);
            this.navCtrl.setRoot(TransportistasListPage);
        }, (e) => this.Complementos._MessageToast(`${e.error.mensaje}`));
    }
};
AddTransportistaPage = __decorate([
    Component({
        selector: 'page-add-transportista',
        templateUrl: 'add-transportista.html',
        providers: [ApiProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        ApiProvider,
        ComplementosviewsProvider,
        Events,
        FormBuilder])
], AddTransportistaPage);
export { AddTransportistaPage };
//# sourceMappingURL=add-transportista.js.map