import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TrailerPage} from "../trailer/trailer";
import {VehiculoPage} from "../vehiculo/vehiculo";

@Component({
  selector: 'page-popover-transportista',
  templateUrl: 'popover-transportista.html',
})
export class PopoverTransportistaPage {
  public TRANSPORTISTA: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.TRANSPORTISTA = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('Transportista', this.TRANSPORTISTA);
  }

  /*TRAILER*/
  _CrearTrailer() {
    console.log('TRAILER');
    this.navCtrl.push(TrailerPage, {data: this.TRANSPORTISTA});
    // this.navCtrl.push(OfertasPage, {data: this.navParams.get('data')});
  }

  /*VEHICULO*/
  _CrearVehiculo() {
    console.log('VEHICULO');
    this.navCtrl.push(VehiculoPage, {data: this.TRANSPORTISTA});
  }

}
