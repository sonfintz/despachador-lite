var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TrailerPage } from "../trailer/trailer";
import { VehiculoPage } from "../vehiculo/vehiculo";
let PopoverTransportistaPage = class PopoverTransportistaPage {
    constructor(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.TRANSPORTISTA = this.navParams.get('data');
    }
    ionViewDidLoad() {
        console.log('Transportista', this.TRANSPORTISTA);
    }
    /*TRAILER*/
    _CrearTrailer() {
        console.log('TRAILER');
        this.navCtrl.push(TrailerPage, { data: this.TRANSPORTISTA });
        // this.navCtrl.push(OfertasPage, {data: this.navParams.get('data')});
    }
    /*VEHICULO*/
    _CrearVehiculo() {
        console.log('VEHICULO');
        this.navCtrl.push(VehiculoPage, { data: this.TRANSPORTISTA });
    }
};
PopoverTransportistaPage = __decorate([
    Component({
        selector: 'page-popover-transportista',
        templateUrl: 'popover-transportista.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams])
], PopoverTransportistaPage);
export { PopoverTransportistaPage };
//# sourceMappingURL=popover-transportista.js.map