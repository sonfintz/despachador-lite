import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {MapaeventoPage} from "../mapaevento/mapaevento";
import * as moment from "moment";

@Component({
  selector: 'page-detalleevento',
  templateUrl: 'detalleevento.html',
})
export class DetalleeventoPage {
  public _Detalleevento: any;
  public _DetalleRuta: any;
  private evidencias = Array();

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
    this._Detalleevento = this.navParams.get('data');
    this._DetalleRuta = this.navParams.get('value');
    console.log('detalle evento DETALLEEVENTO', this._Detalleevento);
    console.log('detalle evento ruta DETALLERUTA', this._DetalleRuta);
    console.log('detalle evento ruta', this._DetalleRuta.data);
    this.evidencias = this._DetalleRuta.data;
    console.log('EVIDENCIAS', this.evidencias)
  }

  MapaEvento() {
    console.log('viendo mapa');
    this.navCtrl.push(MapaeventoPage, {data: this._DetalleRuta, value: 0})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleeventoPage');
  }

  formatFecha(data) {
    return moment(data).format('YYYY-MM-DD HH:mm a')
  }

}
