var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MapaeventoPage } from "../mapaevento/mapaevento";
import * as moment from "moment";
let DetalleeventoPage = class DetalleeventoPage {
    constructor(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.evidencias = Array();
        this._Detalleevento = this.navParams.get('data');
        this._DetalleRuta = this.navParams.get('value');
        console.log('detalle evento DETALLEEVENTO', this._Detalleevento);
        console.log('detalle evento ruta DETALLERUTA', this._DetalleRuta);
        console.log('detalle evento ruta', this._DetalleRuta.data);
        this.evidencias = this._DetalleRuta.data;
        console.log('EVIDENCIAS', this.evidencias);
    }
    MapaEvento() {
        console.log('viendo mapa');
        this.navCtrl.push(MapaeventoPage, { data: this._DetalleRuta, value: 0 });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DetalleeventoPage');
    }
    formatFecha(data) {
        return moment(data).format('YYYY-MM-DD HH:mm a');
    }
};
DetalleeventoPage = __decorate([
    Component({
        selector: 'page-detalleevento',
        templateUrl: 'detalleevento.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams])
], DetalleeventoPage);
export { DetalleeventoPage };
//# sourceMappingURL=detalleevento.js.map