var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ChangeDetectorRef, Component } from '@angular/core';
import { Events, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { FormBuilder, Validators } from "@angular/forms";
import * as moment from "moment";
let OfertasPage = class OfertasPage {
    constructor(navCtrl, navParams, changeDetector, Api, events, formBuilder, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.changeDetector = changeDetector;
        this.Api = Api;
        this.events = events;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.Opc = 0;
        this.Objs = [];
        this.BandSolicitud = false;
        /**--- BANDERA DE SELECT DE SOLICITUD ----**/
        this.area = [];
        this.directionsService = null;
        this.directionsDisplay = null;
        this.bounds = null;
        this.date2 = new Date();
        this.datos = this.navParams.get('data');
        this.SOLICITUD = this.navParams.get('value');
        console.log('LOS DATOS', this.datos[0]);
        console.log('LOS DATOS2', this.navParams.get('value'));
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.bounds = new google.maps.LatLngBounds();
        this.initForm();
        this.Api._GET('/municipio').subscribe((data) => {
            this.Objs = data;
        }, (err) => {
            console.error(err);
        }, () => {
        });
    }
    ionViewDidLoad() {
        // --/ this.loadMap();
        this._Solicitudes();
        this._TipoVehiculo();
        this._TipoCarga();
    }
    /*_NavParams() {
      // ---/ Recibe los parametros de la solicitud
      if (this.navParams.get('data')) {
  
        // let Obj: any = this.navParams.get('data');
        // ---/ Llamar al metodo para cargar la solicitud
        // this._selectSolicitud(Obj);
      }
    }*/
    /*_selectSolicitud($event:any){
      console.log('Select',$event,$event.fecha_carga/!**,$event.fecha_carga.toISOString**!/);
      /!**-- Activando Bandera ---*!/this.BandSolicitud = true;/!**------*!/
      /!**------------
       * Fechas
       *!/
      this.formOferta.get('id_solicitud').setValue($event.id);
      this.formOferta.get('fechacarga').setValue(`${$event.fecha_carga.split('-').reverse().join('-')}`);
      this.formOferta.get('fechaentrega').setValue(`${$event.fecha_entrega.split('-').reverse().join('-')}`);
  
      this.formOferta.get('observaciones').setValue(`${$event.observaciones}`);
      this.formOferta.get('id_tipovehiculo').setValue(`${$event.id_tipovehiculo}`);
      this.formOferta.get('id_tipocarga').setValue(`${$event.id_tipocarga}`);
      this.formOferta.get('producto').setValue(`producto`);
      this.rango_maximo = $event.rango_maximo;
      this.rango_minimo = $event.rango_minimo;
      this.Name_cityO = $event.ciudad_origen;
      this.Name_cityD = $event.ciudad_destino;
      let DATA_CITY_ORIGEN:any = this.getResults($event.ciudad_origen,1);
      let DATA_CITY_DESTINO:any = this.getResults($event.ciudad_origen,1);
  
      this.formOferta.get('lugar_origen').setValue(DATA_CITY_ORIGEN[0].id_municipio);
      this.formOferta.get('lugar_destino').setValue(DATA_CITY_DESTINO[0].id_municipio);
      console.log('DATA_CITY_ORIGEN:',DATA_CITY_ORIGEN,DATA_CITY_ORIGEN[0].latitud,DATA_CITY_ORIGEN[0].longitud,'|',parseFloat(DATA_CITY_ORIGEN[0].longitud));
      this.Origen  = {lat:parseFloat(DATA_CITY_ORIGEN[0].latitud), lng:parseFloat(DATA_CITY_ORIGEN[0].longitud)};
      this.Destino = {lat:parseFloat(DATA_CITY_ORIGEN[0].latitud), lng:parseFloat(DATA_CITY_ORIGEN[0].longitud)};
    }*/
    /**--------------------------------
     *  LATIDU Y LONGITUD DE LA CIUDAD DE ORIGEN
     */
    getResults(keyword, opc) {
        console.log('Funtion->', keyword);
        if (opc === 1) {
            return this.Objs.filter(item => item['municipio_nombre'].toLowerCase().startsWith(keyword.toLowerCase())).slice(0, 3);
        }
        else if (opc === 2) {
            return this.Objs.filter(item => item['id_municipio'] === keyword).slice(0, 3);
        }
    }
    /** -----------------------------
     * |
     * |  FORMULARIO CREAR OFERTA
     * |
     * @private
     */
    initForm() {
        this.car_disponible = this.SOLICITUD.solicitudes_cantidadvehiculo - this.SOLICITUD.solicitudes_cantidadregistrados;
        let date = moment().format('YYYY-MM-DD');
        let date2 = moment().add('days', 5).format('YYYY-MM-DD');
        this.formOferta = this.formBuilder.group({
            origenmunicipio: [this.navParams.get('data')[0].origenmunicipio_id, [Validators.required]],
            destinomunicipio: [this.navParams.get('data')[0].destinomunicipio_id, [Validators.required]],
            fechacarga: [date, [Validators.required]],
            fechaentrega: [date2, [Validators.required]],
            id_tipocarga: ['', [Validators.required]],
            id_tipovehiculo: ['', [Validators.required]],
            cantidad: [this.car_disponible, [Validators.required]],
            valor: ['', [Validators.required]],
            observaciones: [''],
            producto: [this.navParams.get('data')[0].producto_nombre],
            id_solicitud: ['0'],
        });
        this.formOferta.get('cantidad').valueChanges.subscribe((data) => {
            if (data > this.car_disponible) {
                this.formOferta.get('cantidad').setErrors({ invalidNet: `el limite es ${this.car_disponible}.` });
            }
        });
        /*this.formOferta.get('fechaentrega').disable();*/
        this.formOferta.get('fechacarga').valueChanges.subscribe(() => {
            this.formOferta.get('fechaentrega').reset();
        });
        this.formOferta.get('valor').valueChanges.subscribe((data) => {
            if (data < this.navParams.get('data')[0].rango_minimo || data > this.navParams.get('data')[0].rango_maximo) {
                this.formOferta.get('valor').setErrors({ invalidNet: 'monto invalido.' });
            }
        });
    }
    /**-------------------------------
     * | FIN FORMULARIO OFERTA
     *
     * @private
     */
    getCurrentTime() {
        return moment().format('YYYY-MM-DD');
    }
    getCurrentTime1() {
        return this.formOferta.get('fechacarga').value;
    }
    _next() {
        console.log('next', this.Opc);
        if (this.Opc != 2) {
            this.Opc = this.Opc + 1;
            this.changeDetector.detectChanges();
            if (this.Opc === 1) {
                this.loadMap();
            }
        }
    }
    _previous() {
        console.log('previous', this.Opc);
        if (this.Opc > 0) {
            this.Opc = this.Opc - 1;
            this.changeDetector.detectChanges();
            if (this.Opc === 1) {
                this.loadMap();
                this._Area_kilometros();
            }
        }
    }
    loadMap() {
        let mapEle = document.getElementById('map');
        this.map = new google.maps.Map(mapEle, {
            center: (!this.Origen) ? { lat: 7.913728, lng: -72.5027235 } : this.Origen,
            zoom: 15
        });
        this.directionsDisplay.setMap(this.map);
        console.log('this.Origen:', this.Origen);
        this.long = this.navParams.get('data')[0].origenmunicipio_longitud;
        this.lat = this.navParams.get('data')[0].origenmunicipio_latitud;
        let origen = new google.maps.Marker({
            position: (!this.Origen) ? { lat: 7.913728, lng: -72.5027235 } : this.Origen,
            map: this.map,
            label: 'O',
            strokeColor: "blue",
            //draggable: true ,
            animation: google.maps.Animation.DROP,
            title: 'Ciudad Origen'
        });
        let destino = new google.maps.Marker({
            position: (!this.Destino) ? { lat: 7.913728, lng: -72.5027235 } : this.Destino,
            map: this.map,
            // icon: image, //'',
            draggable: true,
            animation: google.maps.Animation.BOUNCE,
            title: 'Ciudad Origen'
        });
        destino.addListener('click', () => {
        });
        destino.addListener('drag', () => {
        });
        google.maps.event.addListener(destino, 'drag', () => {
            /*  let lat  = destino.getPosition().lat();
                let lng  = destino.getPosition().lng(); */
            //console.log('Marker',lat,lng);
            let km = this.distancia(origen.getPosition().lat(), origen.getPosition().lng(), destino.getPosition().lat(), destino.getPosition().lng());
            let KM = km * 1000;
            this.Origen = origen.getPosition();
            this.Destino = destino.getPosition();
            this.Origen_lat = origen.getPosition().lat();
            this.Origen_lng = origen.getPosition().lng();
            this.distancia_Area = KM;
            if (KM <= 20000) {
                for (let i in this.area) {
                    this.area[i].setMap(null);
                    this.area[i].strokeColor = "#0c0ce8";
                    this.area[i].fillColor = "#0c0ce8";
                }
                this.area.push(new google.maps.Circle({
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.5,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.05,
                    map: this.map,
                    center: origen.getPosition(),
                    radius: KM
                }));
                console.log('RADIO', KM);
                console.log('RADIO 2', this.distancia_Area);
            }
            else {
                this.distancia_Area = 20000;
            }
        });
        google.maps.event.addListenerOnce(this.map, 'idle', () => {
            mapEle.classList.add('show-map');
        });
        let input = document.getElementById('pac-input');
        let searchBox = new google.maps.places.Autocomplete(input, {});
        searchBox.addListener('place_changed', () => {
            let places = searchBox.getPlace();
            let bounds = new google.maps.LatLngBounds();
            console.log('SearBox', places);
            console.log('Positions:', bounds.extend(places.geometry.location), places.geometry.location, places.geometry.location.lat());
            // if (places.geometry.viewport) {
            // Only geocodes have viewport.
            console.log(places.geometry.viewport);
            //  } else {
            console.log(places.geometry.location);
            // }
        });
    }
    _Area_kilometros() {
        for (let i in this.area) {
            this.area[i].setMap(null);
            this.area[i].strokeColor = "#0c0ce8";
            this.area[i].fillColor = "#0c0ce8";
        }
        this.area.push(new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.05,
            map: this.map,
            center: this.Origen,
            radius: this.distancia_Area
        }));
        /* let destino2 = new google.maps.Marker({
           position:this.Destino,
           map: this.map,
           draggable: true ,
           animation:google.maps.Animation.BOUNCE,
           title:'Ciudad Origen'
         }); */
    }
    distancia(lat1, lon1, lat2, lon2) {
        let rad = (x) => {
            return x * Math.PI / 180;
        };
        let R = 6378.137; //Radio de la tierra en km
        let dLat = rad(lat2 - lat1);
        let dLong = rad(lon2 - lon1);
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        return d.toFixed(3); //Retorna tres decimales
    }
    _citysOrigen($event) {
        console.log('id Ciudad origen', $event.data);
        this.formOferta.get('origenmunicipio').setValue($event.data);
        let DATA_CITY_ORIGEN = this.getResults($event.data, 2);
        console.log('DATA_CITY_ORIGEN:', DATA_CITY_ORIGEN, DATA_CITY_ORIGEN[0].latitud, DATA_CITY_ORIGEN[0].longitud, '|', parseFloat(DATA_CITY_ORIGEN[0].longitud));
        this.Origen = { lat: this.navParams.get('data')[0].latitud, lng: this.navParams.get('data')[0].longitud };
        this.Destino = { lat: this.navParams.get('data')[0].latitud, lng: this.navParams.get('data')[0].longitud };
    }
    _citysDestino($event) {
        console.log('id Ciudad destino', $event.data);
        this.formOferta.get('destinomunicipio').setValue($event.data);
    }
    _Solicitudes() {
        this.Api._GET('/solicitudes').subscribe((data) => {
            this.Solicitudes = data;
        }, (err) => {
            if (err.status === 401) {
                // this.events.publish('sesionDuplicada', err)
            }
            else {
                console.error(err);
            }
        }, () => {
            console.log('/solicitudes', this.Solicitudes);
        });
    }
    _TipoVehiculo() {
        this.Api._GET('/tipovehiculo').subscribe((data) => {
            this.TIPO_VEHICULO = data;
        }, (err) => {
            if (err.status === 401) {
                // this.events.publish('sesionDuplicada', err)
            }
            else {
                console.error(err);
            }
        }, () => {
            console.log('/tipovehiculo', this.TIPO_VEHICULO);
        });
    }
    _TipoCarga() {
        this.Api._GET('/tipocarga').subscribe((data) => {
            this.TIPO_CARGA = data;
        }, (err) => {
            if (err.status === 401) {
                // this.events.publish('sesionDuplicada', err)
            }
            else {
                console.error(err);
            }
        }, () => {
            console.log('/tipocarga', this.TIPO_CARGA);
        });
    }
    /**-----------------------------
     *  SALVAR DATOS
     * @private
     */
    _REGISTAR_OFERTA() {
        console.log('LONGITUD Y LATITUD', this.long, this.lat);
        let listado = [{
                latitud: this.lat,
                longitud: this.long,
                radio: this.distancia_Area
            }];
        let Obj = Object.assign(this.formOferta.value, {
            // fechavigencia: moment(this.formOferta.get('fechaentrega').value).add(1, 'days').format("YYYY-MM-DD"),
            listado: listado,
            id_solicitud: this.navParams.get('data')[0].solicitudes_id
        });
        console.log('VALOR', Number(Obj.valor));
        console.log('BODY', Obj);
        this.Api._POST('/oferta', Obj).subscribe((data) => {
            console.log('response post Oferta', data);
            if (data.success === true) {
                this._MensajeToast('¡OFERTA REGISTRADA!');
                this.formOferta.reset();
                this.events.publish('list-solicitud');
                this.navCtrl.pop();
            }
            else {
                this._MensajeToast('¡ERROR OFERTA NO REGISTRADA!');
            }
        }, (err) => {
            this._MensajeToast(`${err.error.mensaje}`);
        }, () => {
        });
    }
    /**------------------------------
     * CARGANDO LOS TIPO-VEHICULO
     */
    /**------------------------------
     * FIN DE SALVAR DATOS
     * @private
     */
    /**-----------------------------
     * MENSAJES TOAST
     */
    _MensajeToast(mensaje) {
        let toast = this.toastCtrl.create({
            message: mensaje,
            duration: 3000
        });
        toast.present().then(() => {
        });
    }
};
OfertasPage = __decorate([
    Component({
        selector: 'page-ofertas',
        templateUrl: 'ofertas.html',
        providers: [ApiProvider]
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        ChangeDetectorRef,
        ApiProvider,
        Events,
        FormBuilder,
        ToastController])
], OfertasPage);
export { OfertasPage };
//# sourceMappingURL=ofertas.js.map