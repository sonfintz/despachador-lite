import {Component} from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {File} from "@ionic-native/file";
import {FileOpener} from "@ionic-native/file-opener";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from 'moment';
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {BarcodeScanner, BarcodeScannerOptions} from "@ionic-native/barcode-scanner";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'page-reporte-mr',
  templateUrl: 'reporte-mr.html',
})
export class ReporteMrPage {
  ////////////////////////////////////////
  /*CODIGO QR*/
  options: BarcodeScannerOptions;
  encodText: string = "";
  encodedData: any = {};
  scannedData: any = {};
  ///////////////////////////////////////
  public myAngularxQrCorde: string = null;


  // "value" passed in componentProps
  public value: any;
  public remesas: any;
  public Printremesa: any;
  public PrintOrden: any;
  segment: string = 'despachar';
  pdfObj = null;
  pdfObj1 = null;
  pdfObj2 = null;
  private QR: string;

  constructor(public navCtrl: NavController,
              public Api: ApiProvider,
              private plt: Platform,
              private file: File,
              private barcodeScanner: BarcodeScanner,
              private Complementos: ComplementosviewsProvider,
              private fileOpener: FileOpener,
              public navParams: NavParams) {
    this.value = this.navParams.get('data');
    this.loadData();
    // console.log('MODAL R Y M', this.value);
  }

  reportesMR(value) {
    this.segment = value
  }

  /*
  * ORDEN DE CARGA
  * */
  DownloadPDF() {
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        let blob = new Blob([buffer], {type: 'application/pdf'});

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, {replace: true}).then(() => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

  generatePDF() {
    this.Complementos._MessageToast('¡Orden de carga Generada!', 1000);
    let docDefinition = {
      content: [
        {text: `ORDEN DE CARGA`, alignment: 'center', bold: true},
        {text: `${this.PrintOrden.nomempresa}`, alignment: 'center', bold: true},
        {text: `N.I.T. ${this.PrintOrden.nitempresa}`, alignment: 'center', bold: true},
        {text: `${this.PrintOrden.dirempresa}`, alignment: 'center', bold: true},
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'PLACA VEHICULO:',
                    `${this.PrintOrden.vehiculoplaca}`,
                    `CONDUCTOR: ${this.PrintOrden.nomtransportista}`,
                    'EMPRESA:',
                    `${this.PrintOrden.nomemresa}`,
                  ],
                },
                {
                  stack: [
                    `TRAILER: ${this.PrintOrden.placatrailer}`,
                    `CEDULA / NIT ${this.PrintOrden.nitempresa}`,
                    `TELEFONO: ${this.PrintOrden.tlfempresa}`,
                  ],
                },
                {
                  stack: [
                    `TIPO VEHICULO: ${this.PrintOrden.tipovehiculo}`,
                    `CELULAR: ${this.PrintOrden.tlftransportista}`,
                  ],
                },
              ],
            ],
          },
          margin: [50, 0, 0, 0],
          layout: 'noBorders',
          alignment: 'justify',
        },
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          style: 'tableExample',
          table: {
            body: [
              ['REMESA', 'DESCRIPCION', '', 'PESO NETO', '', '', 'OBSERVACION'],
              [
                {
                  stack: [
                    `00027`
                  ]
                },
                {
                  stack: [
                    'Carbon Coque 10 * 50'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    '34000'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    `ORIGEN: ${this.PrintOrden.origen}`
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    `DESTINO: ${this.PrintOrden.destino}`
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    'PESO TOTAL (TN)'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    '34000'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
            ]
          },
          layout: 'noBorders',
          margin: [50, 0, 0, 0]
        },
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {text: `${this.PrintOrden.nomgenerador}`, alignment: 'left', bold: true, margin: [40, 60, 0, 0]},
        {text: `Cliente ${this.PrintOrden.nomempresa}`, alignment: 'right', bold: true, margin: [0, -15, 60, 0]},
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          text: `Si es victima de algun fraude o conoce de alguna irregularidad en el registro Nacional de Despachos de Carga RNDC denuncielo a la Superintendencia de Puertos y transporte, en la linea gratuita nacional 018000915615 y a traves del correo electronico: atencionciudadano@supertransporte.gov.co`,
          alignment: 'center',
          bold: true,
          margin: [0, 10, 0, 0],
          fontSize: 10
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      defaultStyle: {
        // alignment: 'justify'
      }

      /*content: [

        {
          image: 'imgs/municipios/16.jpg',
          width: 150,
          height: 150,
        },

        {text: 'REMINDER', style: 'header'},
        {text: new Date().toTimeString(), alignment: 'right'},

        {text: 'From', style: 'subheader'},
        {text: this.letterObj.from},

        {text: 'To', style: 'subheader'},
        this.letterObj.to,

        {text: this.letterObj.text, style: 'story', margin: [0, 20, 0, 20]},

        {
          ul: [
            'Bacon',
            'Rips',
            'BBQ',
          ]
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }*/
    };
    this.pdfObj = pdfMake.createPdf(docDefinition);
    this.DownloadPDF();
  }

  /*
  * REMESA
  * */
  DownloadPDF1() {
    if (this.plt.is('cordova')) {
      this.pdfObj1.getBuffer((buffer) => {
        let blob = new Blob([buffer], {type: 'application/pdf'});

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, {replace: true}).then(() => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj1.download();
    }
  }

  generatePDF1() {
    this.Complementos._MessageToast('¡Remesa Generada!', 1000);
    let docDefinition = {
      content: [
        {text: `REMESA TERRESTRE DE CARGA`, alignment: 'center', bold: true},
        {text: `${this.Printremesa.nomempresa}`, alignment: 'center', bold: true},
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          style: 'tableExample',
          table:
            {
              body:
                [
                  [
                    {
                      stack: [
                        `NUMERO AUTORIZACION`,
                        `${this.Printremesa.numeroautorizacion}`,
                        `CONSECUTIVO`,
                        `${this.Printremesa.numremesa}`,
                      ]
                    },
                    {
                      stack: [
                        `NIT: ${this.Printremesa.nitempresa}`,
                        `${this.Printremesa.dirempresa}`,
                        `TELEFONO : ${this.Printremesa.tlfempresa}`,
                      ],
                      alignment: 'left',
                      margin: [53, 0, 0, 0]
                    },
                  ],

                ],
            },
          layout: 'noBorders'
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [
                {
                  stack: [
                    'Viaje',
                    'VIAJE',
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'No de carga',
                    `NO CARGA`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'No Manifiesto',
                    `${this.Printremesa.nummanifiesto}`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'Placa',
                    `${this.Printremesa.placavehiculo}`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'Tipo operacion',
                    `${this.Printremesa.tipooperacion}`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'Tipo empaque',
                    `${this.Printremesa.tipoempaque}`,
                  ],
                  alignment: 'center',
                },

              ],
            ]
          },
          alignment: 'center',
          margin: [20, 0, 0, 8],
          layout: 'noBorders'

        },
        {text: `INFORMACION DE LA CARGA`, alignment: 'center', bold: true},
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {text: `NATURALEZA: ${this.Printremesa.naturaleza} PERMISO INVIAS`},
        {text: `DESCRIPCION: ${this.Printremesa.descripcion}`},
        {text: `CODIGO: ${this.Printremesa.codigo}`},
        {text: `CANTIDAD: ${this.Printremesa.cantidad}`},
        {text: `PROPIETARIO/ CONTRATANTE/ GENERADOR: ${this.Printremesa.nomremitente}`},
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [`REMITENTE/ ${this.Printremesa.munremitente}`, `DESTINATARIO/ ${this.Printremesa.mundestinatario}`],
              [
                {
                  stack: [
                    `NOMBRE: ${this.Printremesa.nomremitente}`,
                    `IDENTIFICACION: NIT ${this.Printremesa.identremitente}`,
                    `SEDE: ${this.Printremesa.sederemitente}`,
                    `DIRECCION: ${this.Printremesa.dirremitente}`,
                    `COORDENADAS LAT ${this.Printremesa.latremitente} LNG ${this.Printremesa.longremitente}`,
                  ],
                },
                {
                  stack: [
                    `NOMBRE: ${this.Printremesa.nombdestinatario}`,
                    `IDENTIFICACION: NIT ${this.Printremesa.identdestinatario}`,
                    `SEDE: ${this.Printremesa.sededestinatario}`,
                    `DIRECCION: ${this.Printremesa.dirdestinatario}`,
                    `COORDENADAS LAT ${this.Printremesa.latdestinatario} LNG ${this.Printremesa.longdestinatario}`,
                  ],
                },
              ],
            ],
          },
          layout: 'noBorders',
          alignment: 'justify',

        },
        {
          style: 'tableExample',
          table: {
            body: [
              [
                {
                  stack: [
                    'Tomador Poliza',
                    `${this.Printremesa.nomempresa}`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'No poliza',
                    `${this.Printremesa.poliza}`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'Aseguradora',
                    `${this.Printremesa.aseguradora}`,
                  ],
                  alignment: 'center',
                },
                {
                  stack: [
                    'Fecha vencimiento',
                    `FECHA VENCIMIENTO`,
                  ],
                  alignment: 'center',
                },
              ],
            ]
          },
          alignment: 'center',
          margin: [50, 0, 0, 8],
          layout: 'noBorders'

        },
        {text: `TIEMPOS LOGISTICOS`, alignment: 'center', bold: true},
        {
          style: 'tableExample',
          table: {
            body: [
              ['', 'FECHA', 'HORA', 'DIFERENCIA EN MINS', '', 'TIEMPO PACTADO', 'TIEMPO EJECUTADO'],
              [
                {
                  stack: [
                    'CITA CARGUE'
                  ]
                },
                {
                  stack: [
                    `${this.Printremesa.citacargue}`
                  ]
                },
                {
                  stack: [
                    `${this.Printremesa.horacargue}`
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    'Cargue'
                  ]
                },
                {
                  stack: [
                    '12 H 0 Min'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    'LLEGADA'
                  ]
                },
                {
                  stack: [
                    `llegada falta`
                  ]
                },
                {
                  stack: [
                    '12:0'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    '0 H 0 Min'
                  ]
                },
              ],
              [
                {
                  stack: [
                    'ENTRADA'
                  ]
                },
                {
                  stack: [
                    '14/03/2019'
                  ]
                },
                {
                  stack: [
                    '12:0'
                  ]
                },
                {
                  stack: [
                    '0'
                  ]
                },
                {
                  stack: [
                    'Descargue'
                  ]
                },
                {
                  stack: [
                    '12 H 0 Min'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    'SALIDA'
                  ]
                },
                {
                  stack: [
                    '14/03/2019'
                  ]
                },
                {
                  stack: [
                    '12:0'
                  ]
                },
                {
                  stack: [
                    '0'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    'CITA'
                  ]
                },
                {
                  stack: [
                    '19/03/2019'
                  ]
                },
                {
                  stack: [
                    '12:0'
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    'DESCARGUE'
                  ]
                },
                {
                  stack: []
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],
              [
                {
                  stack: [
                    'OBSERVACIONES'
                  ]
                },
                {
                  stack: [
                    ``
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
                {
                  stack: [
                    ''
                  ]
                },
              ],

            ]
          },

        },
        {text: `FIRMA Y SELLO EMPRESA`, alignment: 'left', bold: true, margin: [40, 20, 0, 0]},
        {text: `FIRMA CONDUCTOR`, alignment: 'right', bold: true, margin: [0, -17, 60, 0]},
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          text: `Si es victima de algun fraude o conoce de alguna irregularidad en el registro Nacional de Despachos de Carga RNDC denuncielo a la Superintendencia de Puertos y transporte, en la linea gratuita nacional 018000915615 y a traves del correo electronico: atencionciudadano@supertransporte.gov.co`,
          alignment: 'center',
          bold: true,
          margin: [0, 10, 0, 0],
          fontSize: 10
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      defaultStyle: {
        // alignment: 'justify'
      }

      /*content: [

        {
          image: 'imgs/municipios/16.jpg',
          width: 150,
          height: 150,
        },

        {text: 'REMINDER', style: 'header'},
        {text: new Date().toTimeString(), alignment: 'right'},

        {text: 'From', style: 'subheader'},
        {text: this.letterObj.from},

        {text: 'To', style: 'subheader'},
        this.letterObj.to,

        {text: this.letterObj.text, style: 'story', margin: [0, 20, 0, 20]},

        {
          ul: [
            'Bacon',
            'Rips',
            'BBQ',
          ]
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }*/
    };
    this.pdfObj1 = pdfMake.createPdf(docDefinition);
    this.DownloadPDF1();
  }

  /*
  * MANIFIESTO
  * */
  DownloadPDF2() {
    if (this.plt.is('cordova')) {
      this.pdfObj2.getBuffer((buffer) => {
        let blob = new Blob([buffer], {type: 'application/pdf'});

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, {replace: true}).then(() => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj2.download();
    }
  }

  generatePDF2() {
    this.Complementos._MessageToast('¡Manifiesto Generado!', 1000);
    let docDefinition = {
      /* MANIFIESTO CORTO */

      content: [
        {text: `MANIFIESTO ELECTRONICO DE CARGA`, alignment: 'center', bold: true},
        {text: `MINISTERIO DE TRANSPORTE`, alignment: 'center', bold: true},
        {text: `VERSION MOBILE`, alignment: 'center', bold: true},

        {
          style: 'tableExample',
          table: {
            body: [
              [
                {
                  qr: this.QR,
                  fit: 150,
                  with: 150,
                },
                {
                  stack: [
                    'MEC:',
                    `FECHA:`,
                    `PLACA:`,
                    'ORIGEN:',
                    `DESTINO`,
                    `MERCANCIA`,
                    `CONDUCTOR`,
                    `EMPRESA`,
                    `SEGURO`,
                    `OBSERVACION`,
                  ],
                },
                {
                  stack: [
                    `${this.remesas.MEC}`,
                    `${this.remesas.Fecha}`,
                    `${this.remesas.Placa}`,
                    `${this.remesas.Orig}`,
                    `${this.remesas.Dest}`,
                    `${this.remesas.Mercancia}`,
                    `${this.remesas.Conductor}`,
                    `${this.remesas.Empresa}`,
                    `${this.remesas.Obs}`,
                    `${this.remesas.Seguro}`,
                  ],
                },
              ],
            ],
          },
          margin: [50, 20, 0, 0],
          layout: 'noBorders',
          alignment: 'justify',
        },
        {
          text: `_______________________________________________________________________________________________`,
          color: 'orange'
        },
        {
          text: `Si es victima de algun fraude o conoce de alguna irregularidad en el registro Nacional de Despachos de Carga RNDC denuncielo a la Superintendencia de Puertos y transporte, en la linea gratuita nacional 018000915615 y a traves del correo electronico: atencionciudadano@supertransporte.gov.co`,
          alignment: 'center',
          bold: true,
          margin: [0, 10, 0, 0],
          fontSize: 10
        },
      ],

      /* MANIFIESTO LARGO*/
      /*
      content: [
        {text: `MANIFIESTO ELECTRONICO DE CARGA`, alignment: 'center', fontSize: 10,bold:true},
        {text: `MINISTERIO DE TRANSPORTE`, alignment: 'center', bold:true, fontSize: 10},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    { qr: 'text in QR' },
                  ],
                },
                {
                  stack: [
                    'MANIFIESTO',
                    '0000093',
                    'AUTORIZACION',
                    '38231809',
                  ],
                  fontSize: 10
                },
                {
                  stack: [
                    'Nit: 9007319718',
                    'Carrera 71 No 94 127 Apartamento 914 Torre 3',
                    'Tel: 3165235017 Barranquilla Atlantico',
                  ],
                  alignment: 'center',
                  margin: [80,0,0,0],
                  fontSize: 10
                },
              ],
            ],
          },
          layout: 'noBorders',
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'FECHA DE EXPEDICION',
                    '12/03/2019'
                  ],
                },
                {
                  stack: [
                    'TIPO DE MANIFIESTO',
                    'General',
                  ],
                },
                {
                  stack: [
                    'ORIGEN DEL VIAJE',
                    'Margarita Bolivar',
                  ],
                },
                {
                  stack: [
                    'DESTINO DEL VIAJE',
                    'Cucuta Norte de Santander'
                  ],
                },
              ],
            ],
          },
          layout: 'noBorders',
          fontSize: 10,
          margin: [150,-60,0,0]
        },
        {text: `INFORMACION DEL VEHICULO Y CONDUCTORES`, alignment: 'center', bold: true,margin: [0,20,0,0], fontSize: 10},
        {text: `____________________________________________________________________________________________________________________________________________`,color: 'orange'},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'Titular manifiesto',
                    'Larry Fuentes Varela'
                  ],
                },
                {
                  stack: [
                    'Documento identidad',
                    '60345578',
                  ],
                },
                {
                  stack: [
                    'Direccion',
                    'Dir Juan Bautista',
                  ],
                },
                {
                  stack: [
                    'Telefono',
                    '5947263'
                  ],
                },
                {
                  stack: [
                    'Ciudad',
                    'Los Patios Norte de Santander'
                  ],
                },
              ],
            ],
          },
          layout: 'noBorders',
          fontSize: 10,
          margin: [0,20,0,0]
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'Placa',
                    'NFM358'
                  ],
                },
                {
                  stack: [
                    'Marca',
                    'Kamaz',
                  ],
                },
                {
                  stack: [
                    'Placa Semiremolque',
                    'F25048',
                  ],
                },
                {
                  stack: [
                    'Placa Semiremol 2',
                    ''
                  ],
                },
                {
                  stack: [
                    'Configuracion',
                    'S251'
                  ],
                },
                {
                  stack: [
                    'Peso Vacio',
                    '8000'
                  ],
                },
                {
                  stack: [
                    'Peso Vacio Remolque',
                    '3500'
                  ],
                },
                {
                  stack: [
                    'Compañia Seguros SOAT',
                    'LIBERTY SEGUROS'
                  ],
                },
                {
                  stack: [
                    'No Poliza',
                    '8600399880',
                  ],
                },
                {
                  stack: [
                    'F Vencimiento SOAT',
                    '31/12/2019',
                  ],
                },
              ],
            ],
          },
          fontSize: 10,
          layout: 'noBorders',

          margin: [0,10,0,0]
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [
                {
                  stack: [
                    'Conductor',
                    'Juan Bautista Jaimes'
                  ],
                },
                {
                  stack: [
                    'Documento Identificacion',
                    '22683590'
                  ],
                },
                {
                  stack: [
                    'Direccion',
                    'Dir Juan Bautista'
                  ],
                },
                {
                  stack: [
                    'Telefono',
                    '3124440178'
                  ],
                },

                {
                  stack: [
                    'No Licencia',
                    '4-369854710'
                  ],
                },
                {
                  stack: [
                    'Ciudad',
                    'Los Patios Norte de Santander',
                  ],
                },

              ],
            ],
          },
          fontSize: 10,
          layout: 'noBorders',

          margin: [0,5,0,0]
        },
        {
          style: 'tableExample',
          table:
            {
              body: [
                [
                  {
                    stack: [
                      'Conductor Nro 2',
                      'Juan Bautista Jaimes'
                    ],
                  },
                  {
                    stack: [
                      'Documento Identificacion',
                      '22683590'
                    ],
                  },
                  {
                    stack: [
                      'Dir Conductor 2',
                      'Dir Juan Bautista'
                    ],
                  },
                  {
                    stack: [
                      'Telefono',
                      '3124440178'
                    ],
                  },

                  {
                    stack: [
                      'No Licencia',
                      '4-369854710'
                    ],
                  },
                  {
                    stack: [
                      'Ciudad Conductor 2',
                      'Los Patios Norte de Santander',
                    ],
                  },

                ],
              ],
            },
          fontSize: 10,
          layout: 'noBorders',

          margin: [0,5,0,0]
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [
                {
                  stack: [
                    'Poseedor de Vehiculo',
                    'Juan Bautista Jaimes'
                  ],
                },
                {
                  stack: [
                    'Documento Identificacion',
                    '22683590'
                  ],
                },
                {
                  stack: [
                    'Direccion',
                    'Dir Juan Bautista'
                  ],
                },
                {
                  stack: [
                    'Telefono',
                    '3124440178'
                  ],
                },
                {
                  stack: [
                    'Ciudad',
                    'Los Patios Norte de Santander',
                  ],
                },

              ],
            ],
          },
          layout: 'noBorders',
          fontSize: 10
        },
        {text: `INFORMACION DEL VEHICULO Y CONDUCTORES`, alignment: 'center', bold: true,margin: [0,8,0,0],fontSize: 10},
        {text: `____________________________________________________________________________________________________________________________________________`,color: 'orange'},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'Nro Remesa',
                    '000027'
                  ],
                },
                {
                  stack: [
                    'Unidad Medida',
                    'Kilogramos',
                  ],
                },
                {
                  stack: [
                    'Cantidad',
                    '3.500',
                  ],
                },
                {
                  stack: [
                    'Naturaleza',
                    'Carga Normal'
                  ],
                },
                {
                  stack: [
                    'Empaque',
                    'Un Coque 30*50'
                  ],
                },
                {
                  stack: [
                    'Producto Transportado',
                    '004528'
                  ],
                },
                {
                  stack: [
                    'Informacion Remitente Lugar Cargue',
                    'Compañia de Galletas Noel 8110149949 Dir Noel Margarita Bolivar'
                  ],
                },
                {
                  stack: [
                    'Informacion Destinatario Lugar Descargue,',
                    'Ci Minas La Aurora Sas 8070047257 Av. 1e # 11a-13 Barrio Caobos Cucuta Norte de Santander'
                  ],
                },
                {
                  stack: [
                    'Dueño Poliza',
                    'Empresa de Transporte',
                  ],
                },
              ],
            ],

          },
          fontSize: 10,
          layout: 'noBorders',

          margin: [0,10,0,0]
        },
        {text: `Si es victima de algun fraude o conoce de alguna irregularidad en el registro Nacional de Despachos de Carga RNDC denuncielo a la Superintendencia de Puertos y transporte, en la linea gratuita nacional 018000915615 y a traves del correo electronico: atencionciudadano@supertransporte.gov.co`, alignment: 'center', bold: true,margin: [0,10,0,0],fontSize: 10},

        // PAGINA 2 //


        {text: `MANIFIESTO ELECTRONICO DE CARGA`, alignment: 'center', fontSize: 10},
        {text: `MINISTERIO DE TRANSPORTE`, alignment: 'center', bold:true, fontSize: 10},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'MANIFIESTO',
                    '0000093',
                    'AUTORIZACION',
                    '38231809',
                  ],
                  margin: [50,0,0,0],
                },
                {
                  stack: [
                    'Nit: 9007319718',
                    'Carrera 71 No 94 127 Apartamento 914 Torre 3',
                    'Tel: 3165235017 Barranquilla Atlantico',
                  ],
                  margin: [111,0,0,0],
                  fontSize: 12,
                  alignment: 'center'
                },
              ],
            ],
          },
          layout: 'noBorders',

        },
        {text: `____________________________________________________________________________________________________________________________________________`,color: 'orange'},

        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'VALORES',
                  ],
                  margin: [50,0,0,0],
                  bold: true,
                  fontSize: 15,
                },
                {
                  stack: [
                    'OBSERVACIONES',
                  ],
                  margin: [400,0,0,0],
                  bold: true,
                  fontSize: 15,
                },
              ],
            ],
          },
          layout: 'noBorders',
          fontSize: 10,
          margin: [60,40,0,0]
        },
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'VALOR TOTAL DEL VIAJE',
                    'RETENCION EN LA FUENTE',
                    'RETENCION ICA',
                    'VALOR NETO A PAGAR',
                    'VALOR ANTICIPO',
                    'SALDO A PAGAR',
                  ],
                  fontSize: 15,
                },
                {
                  stack: [
                    '120.00.00',
                    '1.00',
                    '120.00',
                    '119.879.00',
                    '18.000.00',
                    '101.879.00',
                  ],
                  fontSize: 15,
                },
                {
                  stack: [
                    'LUGAR DE PAGO',
                    'Bucaramanga Santander',
                    'FECHA',
                    '25/03/2019',
                    'CARGUE PAGADO POR',
                    'Empresa de Transporte',
                    'DESCARGUE PAGADO POR',
                    'Empresa de Transporte'
                  ],
                  fontSize: 11.5,
                  margin: [20,-3,0,0]
                },
              ],
            ],
          },
          layout: 'noBorders',

        },
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'FIRMA Y HUELLA TITULAR MANIFIESTO',
                  ],
                  margin: [50,0,0,0],
                  bold: true,
                  fontSize: 15,
                },
                {
                  stack: [
                    'FIRMA Y HUELLA DEL CONDUCTOR',
                  ],
                  margin: [40,0,0,0],
                  bold: true,
                  fontSize: 15,
                },
              ],
            ],
          },
          layout: 'noBorders',
          fontSize: 10,
          margin: [60,0,0,0]
        },
        {text: `____________________________________________________________________________________________________________________________________________`,margin: [0,100,0,0],color: 'orange'},
        {text: `Si es victima de algun fraude o conoce de alguna irregularidad en el registro Nacional de Despachos de Carga RNDC denuncielo a la Superintendencia de Puertos y transporte, en la linea gratuita nacional 018000915615 y a traves del correo electronico: atencionciudadano@supertransporte.gov.co`, alignment: 'center', bold: true,margin: [0,40,0,0],fontSize: 10},

        // PAGINA 3 //

        {text: `MANIFIESTO ELECTRONICO DE CARGA`, alignment: 'center', fontSize: 10},
        {text: `MINISTERIO DE TRANSPORTE`, alignment: 'center', bold:true, fontSize: 10},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'MANIFIESTO',
                    '0000093',
                    'AUTORIZACION',
                    '38231809',
                  ],
                  margin: [27,0,0,0],
                },
                {
                  stack: [
                    'Nit: 9007319718',
                    'Carrera 71 No 94 127 Apartamento 914 Torre 3',
                    'Tel: 3165235017 Barranquilla Atlantico',
                  ],
                  margin: [134,0,0,0],
                  fontSize: 12,
                  alignment: 'center'
                },
              ],
            ],
          },
          layout: 'noBorders',

        },
        {text: `__________________________________________________________________________________________________________________`,margin: [140,-20,0,0],color: 'orange'},
        {text: `Anexo: Tiempos y Plazos para cargue y descargue Literal 12 Art 8 Decreto 2092 de 2011`, alignment: 'center', fontSize: 12,margin: [0,10,0,0]},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'PLACA VEHICULO',
                    'NFM 358'
                  ],
                  margin: [0,0,0,0],
                  fontSize: 12,
                },
                {
                  stack: [
                    'NOMBRE DEL CONDUCTOR',
                    'Juan Bautista Jaimes',
                  ],
                  margin: [100,0,0,0],
                  fontSize: 12,

                },
                {
                  stack: [
                    'DOCUMENTO IDENTIDAD',
                    '60345578',
                  ],
                  margin: [80,0,0,0],
                  fontSize: 12,

                },
              ],
            ],
          },
          layout: 'noBorders',
          fontSize: 10,
          margin: [100,40,0,0]
        },
        {text: `____________________________________________________________________________________________________________________________________________`, margin: [0,10,0,0],color: 'orange'},
        {
          style: 'tableExample',
          table: {
            body: [
              [

                {
                  stack: [
                    'Numero de Remesa',
                    '000027'
                  ],
                },
                {
                  stack: [
                    'Hrs Pactadas',
                    'Cargue',
                    '__________',
                    'Descargue',
                    '__________',
                  ],
                },
                {
                  stack: [
                    'Llegada al lugar de cargue',
                    'Fecha',
                    '03/02/2019',
                    'Hora',
                    '06:00PM',
                  ],
                },
                {
                  stack: [
                    'Salida del lugar de cargue',
                    'Fecha',
                    '______',
                    'Hora',
                    '______'
                  ],
                },
                {
                  stack: [
                    'Firma del remitente',
                  ],
                },
                {
                  stack: [
                    'Firma del conductor',
                  ],
                },
                {
                  stack: [
                    'Llegada al lugar descargue',
                    'Fecha',
                    '______',
                    'Hora',
                    '______'
                  ],
                },
                {
                  stack: [
                    'Salida del lugar descargue',
                    'Fecha',
                    '______',
                    'Hora',
                    '______'
                  ],
                },
                {
                  stack: [
                    'Firma destinatario',
                    '',
                  ],
                },
                {
                  stack: [
                    'Firma del conductor',
                    '',
                  ],
                },
              ],
            ],
          },
          fontSize: 12,
          layout: 'noBorders',

          margin: [0,10,0,0]
        },
        {text: `____________________________________________________________________________________________________________________________________________`,margin: [0,100,0,0],color: 'orange'},
        {text: `Si es victima de algun fraude o conoce de alguna irregularidad en el registro Nacional de Despachos de Carga RNDC denuncielo a la Superintendencia de Puertos y transporte, en la linea gratuita nacional 018000915615 y a traves del correo electronico: atencionciudadano@supertransporte.gov.co`, alignment: 'center', bold: true,margin: [0,40,0,0],fontSize: 10},
      ],
      */
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        }
      },
      defaultStyle: {
        // alignment: 'justify'
      }

      /*content: [

        {
          image: 'imgs/municipios/16.jpg',
          width: 150,
          height: 150,
        },

        {text: 'REMINDER', style: 'header'},
        {text: new Date().toTimeString(), alignment: 'right'},

        {text: 'From', style: 'subheader'},
        {text: this.letterObj.from},

        {text: 'To', style: 'subheader'},
        this.letterObj.to,

        {text: this.letterObj.text, style: 'story', margin: [0, 20, 0, 20]},

        {
          ul: [
            'Bacon',
            'Rips',
            'BBQ',
          ]
        }
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }*/
    };
    this.pdfObj2 = pdfMake.createPdf(docDefinition);
    this.DownloadPDF2();
  }

  /*generatePDF() {
    this.Complementos._MessageToast('Descargando Manifiesto', 3000);
    /!*const options: DocumentViewerOptions = {
      title: 'My PDF'
    };*!/
    /!*this.document.viewDocument('assets/pdf/nuevo_formato.pdf', 'application/pdf', options);*!/
    // console.log('GENERANDO PDF')

  }*/

  loadData() {
    /*
    this.Api._GET(`/orden/imprimirmanifiesto/${this.value.id_manifiesto}`).subscribe((data) => {
      this.Printmanifiesto = data;
      console.log('DATA PARA MANIFIESTO', this.Printmanifiesto);
    });*/
    this.Api._GET(`/orden/manifiestocorto/${this.value.id_manifiesto}`).subscribe((data) => {
      // console.log('DATA DE REMESAS', data[0]);
      this.remesas = data[0];
      this.QR =
        `MEC: ${this.remesas.MEC}
        Fecha: ${this.remesas.Fecha}
        Placa: ${this.remesas.Placa}
        Remolque: ${this.remesas.Remolque}
        Orig: ${this.remesas.Orig}
        Dest: ${this.remesas.Dest}
        Mercancia: ${this.remesas.Mercancia}
        Conductor: ${this.remesas.MEC}
        Empresa: ${this.remesas.Empresa}
        Obs: ${this.remesas.Obs}
        Config: ${this.remesas.Config}
        Seguro: ${this.remesas.Seguro}`;
      this.myAngularxQrCorde = this.QR;
      // console.log('EL QR', this.myAngularxQrCorde);
      // this.longText = this.remesas.toString();
    });
    this.Api._GET(`/orden/imprimirremesa/${this.value.id_remesa}`).subscribe((data) => {
      this.Printremesa = data[0];
      console.log('DATA PARA REMESA', this.Printremesa);
    });
    this.Api._GET(`/orden/imprimirorden/${this.value.id_orden}`).subscribe((data) => {
      data = data.map((i) => {
        i.fechaorden = moment(i.fechaorden).format('YYYY-MM-DD HH:mm:ss a');
        return i;
      });
      this.PrintOrden = data[0];
      console.log('DATA PARA ORDEN', this.PrintOrden);
    });
  }

  /*CODIGO QR*/
  scanBarcode() {
    this.options = {
      prompt: 'Scan you barcode'
    };
    this.barcodeScanner.scan(this.options).then(barcodeData => {
      this.scannedData = barcodeData;
    }).catch(err => console.log('Error', err))
  }

  encodeBarcode() {
    this.barcodeScanner.encode(this.remesas).then(barcodeData => {
      this.encodedData = barcodeData;
      console.log('lo codificado', this.encodedData);
    }).catch(err => console.log('Error', err))
  }
}


