var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { MessagesPage } from "../messages/messages";
import { AppModule } from "../../app/app.module";
import { GpsProvider } from "../../providers/gps/gps";
let ChatPage = class ChatPage {
    constructor(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.USER_CONECTADOS = Array();
        this.USER_CONECTADOS = GpsProvider.LISTA_CONECTADOS;
        events.subscribe("conectados:chat", (conectados) => {
            this.USER_CONECTADOS = conectados;
        });
    }
    ionViewDidLoad() {
        //--/ console.log('ionViewDidLoad ChatPage');
    }
    get _BASE_URL() { return AppModule.BASE_URL; }
    get id_ofertante() {
        return undefined;
    }
    get empresa_name() {
        return 'Nombre de empresa';
    }
    get contacto_name() {
        return 'Nombre';
    }
    //public get _driverWorking() :any{ /** SET COLOR SI EL CONDUCTOR ESTA LLEVANDO CARGA **/return AppModule.driverWorking;}
    _conectado(id) {
        if (GpsProvider.LISTA_CONECTADOS != undefined) {
            for (let item in GpsProvider.LISTA_CONECTADOS) {
                if (GpsProvider.LISTA_CONECTADOS[item].usuario_id == id) {
                    //return GpsProvider.LISTA_CONECTADOS[item];
                    //console.log('Encontrado el conectado');
                    return true;
                }
            }
        }
    }
    _message(receptor, name) {
        console.log(receptor, name);
        this.navCtrl.push(MessagesPage, { receptor: receptor, contacto: name });
    }
};
ChatPage = __decorate([
    Component({
        selector: 'page-chat',
        templateUrl: 'chat.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Events])
], ChatPage);
export { ChatPage };
//# sourceMappingURL=chat.js.map