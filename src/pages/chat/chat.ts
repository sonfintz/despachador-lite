import { Component } from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {MessagesPage} from "../messages/messages";
import {AppModule} from "../../app/app.module";
import {GpsProvider} from "../../providers/gps/gps";

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  public USER_CONECTADOS = Array();
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public events: Events)
  {
    this.USER_CONECTADOS = GpsProvider.LISTA_CONECTADOS;
      events.subscribe("conectados:chat",(conectados)=>{
          this.USER_CONECTADOS = conectados;
      });
  }

  ionViewDidLoad() {
   //--/ console.log('ionViewDidLoad ChatPage');
  }

  public get _BASE_URL(){ return AppModule.BASE_URL;}

  get id_ofertante(){

      return undefined;
  }


  get empresa_name(){
    return 'Nombre de empresa';
  }
  get contacto_name(){
    return 'Nombre'
  }

  //public get _driverWorking() :any{ /** SET COLOR SI EL CONDUCTOR ESTA LLEVANDO CARGA **/return AppModule.driverWorking;}

   _conectado(id){
    if(GpsProvider.LISTA_CONECTADOS != undefined){
      for(let item in GpsProvider.LISTA_CONECTADOS){
        if(GpsProvider.LISTA_CONECTADOS[item].usuario_id == id){
          //return GpsProvider.LISTA_CONECTADOS[item];
          //console.log('Encontrado el conectado');
          return true;
        }
      }
    }

   }

  _message(receptor:number,name:string){
    console.log(receptor,name);
    this.navCtrl.push(MessagesPage,{receptor:receptor,contacto:name});
  }
}
