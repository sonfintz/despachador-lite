import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-empresaadd',
  templateUrl: 'empresaadd.html',
})
export class EmpresaaddPage {
  public form: FormGroup;
  public PERFIL: any;

  constructor(public navCtrl: NavController,
              public fb: FormBuilder,
              public Api: ApiProvider,
              private Complementos: ComplementosviewsProvider,
              public navParams: NavParams) {
    this.PERFIL = this.navParams.get('data');
    console.log('Data del transportista', this.PERFIL);
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      empresanombre: [(this.PERFIL.empresa_nombre) ? this.PERFIL.empresa_nombre : '']
    })
  }

  _ClearInput(value) {
    console.log('reseteando formulario', value);
    this.form.get(`${value}`).reset();
  }

  saveEmpresa() {
    console.log('actualizando perfil', this.form.value);
    this.Api._PUT(`/despachador/${localStorage.getItem('idUserMUEVE')}`, this.form.value).subscribe((data) => {
      console.log(data);
      this.navCtrl.pop();
      this.Complementos._MessageToast('Empresa actualizada', 800);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmpresaaddPage');
  }

}
