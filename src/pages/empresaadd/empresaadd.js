var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder } from "@angular/forms";
import { ApiProvider } from "../../providers/api/api";
import { ComplementosviewsProvider } from "../../providers/complementosviews/complementosviews";
let EmpresaaddPage = class EmpresaaddPage {
    constructor(navCtrl, fb, Api, Complementos, navParams) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.Api = Api;
        this.Complementos = Complementos;
        this.navParams = navParams;
        this.PERFIL = this.navParams.get('data');
        console.log('Data del transportista', this.PERFIL);
        this.initForm();
    }
    initForm() {
        this.form = this.fb.group({
            empresanombre: [(this.PERFIL.empresa_nombre) ? this.PERFIL.empresa_nombre : '']
        });
    }
    _ClearInput(value) {
        console.log('reseteando formulario', value);
        this.form.get(`${value}`).reset();
    }
    saveEmpresa() {
        console.log('actualizando perfil', this.form.value);
        this.Api._PUT(`/despachador/${localStorage.getItem('idUserMUEVE')}`, this.form.value).subscribe((data) => {
            console.log(data);
            this.navCtrl.pop();
            this.Complementos._MessageToast('Empresa actualizada', 800);
        });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad EmpresaaddPage');
    }
};
EmpresaaddPage = __decorate([
    Component({
        selector: 'page-empresaadd',
        templateUrl: 'empresaadd.html',
    }),
    __metadata("design:paramtypes", [NavController,
        FormBuilder,
        ApiProvider,
        ComplementosviewsProvider,
        NavParams])
], EmpresaaddPage);
export { EmpresaaddPage };
//# sourceMappingURL=empresaadd.js.map