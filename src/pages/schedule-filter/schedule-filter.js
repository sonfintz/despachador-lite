var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
let ScheduleFilterPage = class ScheduleFilterPage {
    constructor(confData, navParams, viewCtrl) {
        this.confData = confData;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.tracks = [];
        // passed in array of track names that should be excluded (unchecked)
        let excludedTrackNames = this.navParams.data;
        this.confData.getTracks().subscribe((trackNames) => {
            trackNames.forEach(trackName => {
                this.tracks.push({
                    name: trackName,
                    isChecked: (excludedTrackNames.indexOf(trackName) === -1)
                });
            });
        });
    }
    resetFilters() {
        // reset all of the toggles to be checked
        this.tracks.forEach(track => {
            track.isChecked = true;
        });
    }
    applyFilters() {
        // Pass back a new array of track names to exclude
        let excludedTrackNames = this.tracks.filter(c => !c.isChecked).map(c => c.name);
        this.dismiss(excludedTrackNames);
    }
    dismiss(data) {
        // using the injected ViewController this page
        // can "dismiss" itself and pass back data
        this.viewCtrl.dismiss(data);
    }
};
ScheduleFilterPage = __decorate([
    Component({
        selector: 'page-schedule-filter',
        templateUrl: 'schedule-filter.html'
    }),
    __metadata("design:paramtypes", [ConferenceData,
        NavParams,
        ViewController])
], ScheduleFilterPage);
export { ScheduleFilterPage };
//# sourceMappingURL=schedule-filter.js.map