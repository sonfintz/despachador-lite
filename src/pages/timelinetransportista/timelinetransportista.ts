import {Component} from '@angular/core';
import {Events, NavController, NavParams} from 'ionic-angular';
import {DetalleeventoPage} from "../detalleevento/detalleevento";
import {ApiProvider} from "../../providers/api/api";
import * as moment from "moment";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";

@Component({
  selector: 'page-timelinetransportista',
  templateUrl: 'timelinetransportista.html',
})
export class TimelinetransportistaPage {
  public Timeline: any;
  public Datatransportista: any;
  public Listado: Array<any> = Array();

  constructor(public navCtrl: NavController,
              public Api: ApiProvider,
              public events: Events,
              public complements: ComplementosviewsProvider,
              public navParams: NavParams) {
    this.EventsSubscribe();
    // console.log('DJKASHDJKASHDJKSAD', this.navParams.get('data'));
    this.Timeline = this.navParams.get('data');
    this.Datatransportista = this.navParams.get('value');
    console.log('TIME LINE 2', this.Timeline);
    console.log('Data transportista', this.Datatransportista)
  }

  EventsSubscribe() {
    this.events.subscribe('timeline', (data) => {
      console.log(data);
      this.Events();
    })
  }

  ionViewWillLeave() {
    console.log('Unsubscribe timeline');
    this.events.unsubscribe('timeline')
  }

  Events() {
    //this.complements._PresentLoading();
    console.log(this.navParams.get('value'));
    this.Api._GET(`/oferta/estados/${this.navParams.get('value').estado.toUpperCase()}/${this.navParams.get('data')[0].id_oferta}`).subscribe(
      (data) => {
        //this.complements._DismissPresentLoading();
        if (Array.isArray(data)) {
          console.log('NUEVA RUTA', data);
          if (data.length == 0) {
            console.log('ESTA VACIO');
            this.navCtrl.pop();
            // this.navCtrl.pop();
          } else {
            this.Listado = data[0].tiempos;
            console.log('ARRAY DE TIEMPOS', this.Listado);
            this.Timeline = this.Listado.map((i) => {
              i.fechahoraregistro = moment(i.fechahoraregistro).format('YYYY-MM-DD HH:mm a');
              return i;
            });
          }
        }
      }, (err) => {
        //this.complements._DismissPresentLoading();
        // this.navCtrl.pop();
        this.complements._MessageToast(`${err.error.mensaje}`)
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimelinetransportistaPage');
  }

  detalleEvento(data) {
    this.complements.PresentLoading('Cargando', 2000);
    // console.log('CLICKEANDO EL DETALLE', data);
    /*this.Api._GET(`/solicitudeseventos/oferta/${data.id_oferta}/usuario/${data.id_usuario}/evento/${data.id_tipoevento}`).subscribe(
      (value) => {
        // console.log('DETALLE EVENTO 1', value);
        this.navCtrl.push(DetalleeventoPage, {data: data, value: value})
      }, (error) => {
        console.error(error);
      });*/
    this.Api._GET(`/solicitudeseventos/oferta/detalle/${data.uuid}`).subscribe(
      (value) => {
        console.log('LA NUEVA RUTA DETALLE EVENTO', value);
        this.navCtrl.push(DetalleeventoPage, {data: data, value: value})
      }, (err) => {
        console.log(err.error);
        this.complements._MessageToast('Ha ocurrido un error con el servidor, Intente nuevamente', 2000)
        this.navCtrl.pop();
      },)
  }

}
