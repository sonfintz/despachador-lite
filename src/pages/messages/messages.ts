import {Component, ViewChild} from '@angular/core';
import {AlertController, Content, Events, NavController, NavParams} from 'ionic-angular';
import {GpsProvider} from "../../providers/gps/gps";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {FileTransfer, FileTransferObject} from '@ionic-native/file-transfer';
import {FileOpener} from '@ionic-native/file-opener';
import {File} from '@ionic-native/file';

import {Subject} from "rxjs/Subject";
import {ApiProvider} from "../../providers/api/api";
import {AppModule} from "../../app/app.module";


@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
  providers:[GpsProvider,ApiProvider]
})
export class MessagesPage {

  mensaje:string;
  @ViewChild(Content) content: Content;
  public receptor: number;
  public conversation = [];
  cont: number;
  image:any;
  _MessagesGet:any;
  public contacto:string;
  private _ListMessage = new Subject();

  tabBarElement:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private Gps:GpsProvider,
              private camera:Camera,
              private transfer: FileTransfer,
              private file: File,
              private fileOpener: FileOpener,
              public events: Events,
              public alertCtrl: AlertController,
              private Api:ApiProvider)
  {
    AppModule.ShowTapss = false;
    this.receptor = this.navParams.get('receptor');
    this.contacto =  this.navParams.get('contacto');
    this.cont = 0;
    this.notify();
    this._MessagesIDuser();

    events.subscribe("mensajes:chat", ()=>{
     //--/ console.log('Leego el Evento',val);
      this._Mensajes_tiempo_real();
    });

  }

  onPageDidEnter()
  {
    AppModule.ShowTapss = false;
    this.tabBarElement.style.display = 'none';

  }

  ionViewDidLoad() {
    //--/ console.log('ionViewDidLoad MessagesPage');
    //console.log('El contene',this.content);
  }

  ionViewWillLeave () {
    this.events.unsubscribe("mensajes:chat");
    AppModule.ShowTapss = true;
    }
  get ShoeTabs(){
    return AppModule.ShowTapss;

  }

  private notify(){ this._ListMessage.next(this.conversation);}


  _Mensajes_chat(){
    this.cont = 0;
    this.conversation.splice(this.conversation.length);

    for(let item in this._MessagesGet){
      let FF = JSON.parse(this._MessagesGet[item].data);
     // console.log(item,'|',FF.emisor,'|',this._MessagesGet[item].data);

      if(parseInt(FF.emisor) == this.receptor || parseInt(FF.receptor) == this.receptor){

        // console.log('Mensaje chat es:',FF,'Receptor:', this.receptor);
        if(FF.mensaje){
          this.conversation[this.cont] = FF;
          this.cont+=1;
        }
      }
    }

    this.notify();
    setTimeout(()=>{ if(this.content._scroll) this.content.scrollToBottom();},200);


    return this.conversation;
  }

  _Mensajes_tiempo_real(){
    for(let item in GpsProvider.MESSAGES){
     if(GpsProvider.MESSAGES[item].visto==="NO")
      if(GpsProvider.MESSAGES[item].emisor == this.receptor || GpsProvider.MESSAGES[item].receptor == this.receptor){

         console.log('Cont 1er:',this.cont,GpsProvider.MESSAGES[item]);
        this.conversation.push(GpsProvider.MESSAGES[item]);
        GpsProvider.MESSAGES[item].visto = "SI";
        GpsProvider.PositionMessage[item]=item;

        console.log('Seteando:',GpsProvider.MESSAGES[item], 'Conversacion:',this.conversation);
      }
    }

    this.notify();
    setTimeout(()=>{
      console.log('Scroll',this.content,'_scroll',this.content._scroll);
      if(this.content._scroll) this.content.scrollToBottom();},200);
    return this.conversation;
  }


  _DeleteMesagesSocket(){
       if(GpsProvider.PositionMessage != undefined){
    console.log('Condicion',GpsProvider.PositionMessage);
      for(let i= 0; i< GpsProvider.PositionMessage.length;i++){
        console.log(i, GpsProvider.PositionMessage[i]);
        //GpsProvider.MESSAGES.splice(parseInt( GpsProvider.PositionMessage[i]),1);
        delete  GpsProvider.MESSAGES[parseInt( GpsProvider.PositionMessage[i])];
      }

     GpsProvider.PositionMessage.splice(GpsProvider.PositionMessage.length);
    }else{console.log('Indefinido')}
    console.groupEnd();


  }

  _ClearMessages(){/** ESTA FUNCION SE EJECUTARA SOLO UNA VEZ AL AENTRAR A COMPONENTE **/

    for(let item in GpsProvider.MESSAGES){

      if((GpsProvider.MESSAGES[item].emisor == this.receptor || GpsProvider.MESSAGES[item].receptor == this.receptor) /*&& GpsProvider.MESSAGES[item].visto=="NO"*/){

        delete GpsProvider.MESSAGES[item];

      }
    }

  }

  get _MiId(){ return window.localStorage.getItem("idUserMUEVE"); }

  getUrl(){ return "url('assets/imgs/fondo_message.jpg')";}

  /** CAPTURAR IMAGENES **/
  _Select_PictureSourceType(){
    let actionSheet = this.alertCtrl.create({
      title: 'Camara',
      message: 'Seleciona ',
      buttons: [{
        text: 'Cargar de la Galeria',
        handler: () => {this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);}
      },{
        text: 'Tomar foto',
        handler: () => {this.getPicture(this.camera.PictureSourceType.CAMERA);}
      },{
        text: 'Cancel',
        role: 'cancel'
      }]
    });
    actionSheet.present();
  }

  getPicture(selectedSourceType:number){
    let options: CameraOptions = {
      sourceType: selectedSourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth:300,
      targetHeight:300,
      quality:70,
      saveToPhotoAlbum:true
    };

    this.camera.getPicture(options)
      .then(imagenData =>{
        this.mensaje = `data:image/jpeg;base64,${imagenData}`;
        this._Emitmensage();
        this.mensaje ='';
      })
      .catch(error => {
        console.log(error);
      });
  }

  /** FIN DE CAPTURA IMAGENES**/


    base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }

    base64ToBlob(base64: string, contentType: string, sliceSize: number = 512): Blob {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    const byteCharacters = atob(base64);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
  }

    download(url) { console.log('Descarga:');
    let cortar = url.indexOf(",")+1;
    url = url.substr(cortar,url.length);

    const fileTransfer: FileTransferObject = this.transfer.create();


    fileTransfer.download(url, this.file.externalRootDirectory+ 'Download/' + 'update_file_name.pdf', true).then((entry) => {
     //--/ console.log('download complete: ' + entry.toURL());
      alert('download complete: ' + entry.toURL());
    }, (error) => {
      // handle error
      alert('Error'+ JSON.stringify(error));
    });
  }

    download2(url){
    let cortar = url.indexOf(",")+1;
    let mimeType : any = this.base64MimeType(url);
    url = url.substr(cortar,url.length);
    let extencionN = mimeType.indexOf("/")+1;
    let extencion = mimeType.substr(extencionN, mimeType.length);

    let downloadPDF: any = url;
    let random = Math.floor((Math.random() * 1000) + 1);
    let nameFile:any ='file.'+random+'.'+extencion;
    console.log(mimeType, extencion,'name:',nameFile);
        fetch(`data:${mimeType};base64,` + downloadPDF,
          {
            method: "GET"
          }).then(res => res.blob()).then(blob => {

            //this.file.externalApplicationStorageDirectory
          this.file.writeFile(this.file.externalRootDirectory+ 'Download/', nameFile, blob, { replace: true }).then(res => {
            this.fileOpener.open(
              res.toInternalURL(),
              mimeType
            ).then(() => {

            }).catch(() => {
              console.log('open error')
            });
          }).catch(()=> {
            console.log('save error')
          });
        }).catch(() => {
          console.log('error')
        });
  }

   _MessagesIDuser(){
    this.Api._GET(`/conversaciones/${localStorage.getItem('idUserMUEVE')}`).subscribe(
      (data)=>{

        this._MessagesGet=data;
      },
      (err)=>{console.log(err)},
      ()=>{
        this._Mensajes_chat();
      }
    );
   }

   _Emitmensage(){
    let hora = new Date();
     GpsProvider.MESSAGES.push({emisor:window.localStorage.getItem("idUserMUEVE"),receptor:this.receptor,mensaje:this.mensaje,
     hora:hora.toLocaleTimeString(),visto:"NO"});

     console.log(GpsProvider.MESSAGES,'Emisor:',this.receptor,this.mensaje);


    if(GpsProvider.Band_Conexion){
      this.Gps._emitMessage(this.receptor,this.mensaje);
      this.events.publish("mensajes:chat",true);
      this.mensaje ='';
    }else{

      this.events.publish("mensajes:chat",true);
      this.mensaje ='';
    }
  }

}
