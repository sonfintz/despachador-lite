import {Component} from '@angular/core';
import {AlertController, Events, NavController, NavParams} from 'ionic-angular';
import {EventosTransportistaPage} from "../eventos-transportista/eventos-transportista";
import {ApiProvider} from "../../providers/api/api";

@Component({
  selector: 'page-eventpopover',
  templateUrl: 'eventpopover.html',
})
export class EventpopoverPage {
  private TRANSPORTISTA: any;

  constructor(public navCtrl: NavController,
              public api: ApiProvider,
              public events: Events,
              public alertController: AlertController,
              public navParams: NavParams) {
    this.TRANSPORTISTA = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventpopoverPage');
  }

  _Evento() {
    this.navCtrl.push(EventosTransportistaPage, {data: this.navParams.get('data')});
    console.log('creando evento')
  }

  _Despachar() {
    console.log('Despachar');
    let solicitudes_id = this.TRANSPORTISTA.solicitudes_id;
    let usuario_id = this.TRANSPORTISTA.usuario_id;
    console.log('Despachar transportista', solicitudes_id, usuario_id);
    const alert = this.alertController.create({
      title: '¿Despachar transportista?',
      message: '<strong>Confirme su opcion</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah', blah);
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            let Obj = {
              observacionesbascula: 0,
              ordenbascula: 0,
              pesoentradabascula: 0,
              pesosalidabascula: 0,
              turnobascula: 0
            };
            console.log('SOLICITUD', solicitudes_id, 'USUARIO', usuario_id);
            this.api._PUT(`/despachos/ruta/${solicitudes_id},${usuario_id}`, Obj).subscribe((data) => {
              console.log(data);
              this.events.publish('despachado');
              this.navCtrl.pop();
            })
          }
        }
      ]
    });
    alert.present();
  }

  _Culminar() {
    let solicitudes_id = this.TRANSPORTISTA.solicitudes_id;
    let usuario_id = this.TRANSPORTISTA.usuario_id;
    console.log('Culminar transportista', solicitudes_id, usuario_id);
    const alert = this.alertController.create({
      title: '¿Culminar transportista?',
      message: '<strong>Confirme su opcion</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah', blah);
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            let Obj = {
              "ordenpuerto": 0,
              "pesopuerto": 0
            };
            console.log('SOLICITUD', solicitudes_id, 'USUARIO', usuario_id);
            this.api._PUT(`/despachos/culminado/${solicitudes_id},${usuario_id}`, Obj).subscribe((data) => {
              console.log(data);
              this.events.publish('evento-created')
              this.navCtrl.pop();
            })
          }
        }
      ]
    });
    alert.present();
    // return this.presentAlertConfirm(solicitudes_id, usuario_id);
  }

  /* async presentAlertConfirm(solicitudes_id, usuario_id) {
     const alert = await this.alertController.create({
       title: '¿Culminar transportista?',
       message: '<strong>Confirme su opcion</strong>!!!',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancel',
           cssClass: 'secondary',
           handler: (blah) => {
             console.log('Confirm Cancel: blah', blah);
           }
         }, {
           text: 'Aceptar',
           handler: () => {
             console.log('SOLICITUD', solicitudes_id, 'USUARIO', usuario_id);
             this.api._PUT(`/despachos/culminado/${solicitudes_id},${usuario_id}`).subscribe((data) => {
               console.log(data)
             })
           }
         }
       ]
     });

     await alert.present();
   }*/

}
