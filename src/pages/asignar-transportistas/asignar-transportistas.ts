import {Component} from '@angular/core';
import {Events, MenuController, NavController, NavParams, Refresher, ToastController} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {AddTransportistaPage} from "../add-transportista/add-transportista";


@Component({
  selector: 'page-asignar-transportistas',
  templateUrl: 'asignar-transportistas.html',
  providers: [ApiProvider]
})
export class AsignarTransportistasPage {
  private ObjAssing: any = {id: '', id_generador: '', listado: ''};
  public solicitud: any;
  public Datos_Solicitud: any;
  public DATOS_TRANSPORTISTA: any;
  public TRASPORTISTAS_SELECCIONADOS: any = [];
  public TRASPORTISTAS_ENTURNADOS: any = [];
  public DATOS_SELECT: any = [];
  public solicitudId: any;
  public id_usuario: any;
  public dataPost: any[];
  public SearchPlaca: string;
  public DATOS_SELECT2: any;
  public vehiculosR: any;

  public _IMG_DEFAULT(event) {
    event.target.src = "assets/imgs/perfil.png";
  }

  constructor(
    private menuCtrl: MenuController,
    public navCtrl: NavController,
    public events: Events,
    public navParams: NavParams,
    private Api: ApiProvider,
    private Complementos: ComplementosviewsProvider,
    public toastCtrl: ToastController,
  ) {
    this.solicitud = this.navParams.get('data');
    this.vehiculosR = this.solicitud.solicitudes_cantidadregistrados;
    console.log('SOLICITUD', this.solicitud, 'Vehiculos', this.vehiculosR);
    // this._SOLICITUDES_TRNASPORTISTA_DATA();
    // this._TRASPORTISTAS_EN_ENTURNAMIENTO();
    this._DATOS_TRANSPORTISTA();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AsignarTransportistasPage', this.navParams.get('data'));
    this.solicitudId = this.navParams.get('data');
    this.ObjAssing.id_solicitud = this.solicitudId.solicitudes_id;
  }

  ionViewWillEnter() {
    this.menuCtrl.close('988');
    this.menuCtrl.swipeEnable(false, '988');


  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true, '988');
  }

  /**----------------------------------------------------------------
   *
   *  Metodos Para cargar los datos de los datos de los trasportistas
   *  Y los trasportistas Asignados para el Despacho de la solicitud
   *
   * -----------------------------------------------------------------
   */

  doRefresh(refresher: Refresher) {
    this._DATOS_TRANSPORTISTA();
    setTimeout(() => {
      refresher.complete();

    }, 1000);

  }

  /*_SOLICITUDES_TRNASPORTISTA_DATA() {
    this.Api._GET(`/solicitudes/${this.solicitud.id}`).subscribe(
      (data) => {
        console.log('SOLICITUDES/ID', data);
        this.Datos_Solicitud = data;
      },
      () => {
      },
      () => {
      }
    );
  }*/

  // _TRASPORTISTAS_EN_ENTURNAMIENTO() {
  //   this.Api._POST('/enturnamiento', {id: this.navParams.get('data').id})
  //     .subscribe((data) => {
  //       console.log('TRASPORTISTAS ENTURNADOS-->', data);
  //       this.TRASPORTISTAS_ENTURNADOS = data;
  //     });
  // }

  _DATOS_TRANSPORTISTA() {
    this.Api._GET('/solicitudes/listadotransportista').subscribe(
      (data) => {

        this.DATOS_TRANSPORTISTA = data;
        console.log('DATOS TRANSPORTISTAS', this.DATOS_TRANSPORTISTA);
      },
      (err) => {
        if (err.status === 401) {
          // this.events.publish('sesionDuplicada', err)
        } else {
          console.error(err)
        }
      },
      () => {
        this._ordenamiento_del_select();
      }
    );
  }

  /** //--------------Metodo para Refrescar la vista **/
  _RefrescarData() {
    // this._TRASPORTISTAS_EN_ENTURNAMIENTO();
    this._DATOS_TRANSPORTISTA();
  }

  /**--------------------------------------------------------------
   *    Fin de los Metodos                                         |
   *---------------------------------------------------------------
   */
  //--------/ Metodo Para agregar los datos que se mostraran en el SELECT
  _ordenamiento_del_select() {
    let ids: any = [];
    this.DATOS_SELECT = [];
    this.TRASPORTISTAS_ENTURNADOS.forEach(
      (e) => {
        // --/ Arrays de los ID de los trasportistas Enturnados
        ids.push(e.id);
      });
    this.DATOS_TRANSPORTISTA.forEach((t) => {
      // --/ Comparar si el ID de los Datos Trasportisas Conciece con los ID Enturnados
      if (!ids.includes(t.id)) {
        //--/ Almacenar el objeto
        this.DATOS_SELECT.push(t);
        this.DATOS_SELECT2 = this.DATOS_SELECT;
      }
    });
  }

  //---------/ Metodo para seleccionar el trasportista
  _selectTransportista(dato: any) {
    console.log('Transportista Seleccionado:', dato);
    this.TRASPORTISTAS_SELECCIONADOS = dato;
    let trasportistas = Array();
    dato.forEach((i) => {
      trasportistas.push({id_usuario: i.usuario_id, id_solicitud: this.solicitudId.solicitudes_id});
      // this.ObjAssing.id_transportista = i.id;
      this.id_usuario = i.usuario_id;
    });

    console.log('TRASPORTISTAS:', trasportistas);
    this.dataPost = trasportistas;
    // this.ObjAssing.id_generador = this.navParams.get('data').id_usuario;
    // this.ObjAssing.id = this.navParams.get('data').id;
    this.ObjAssing.listado = JSON.stringify(trasportistas);
  }

  //----/Metodo para Eliminar el Trasportista
  deleteItem(id, _index, _data) {
    console.log("DELTED", id);
    let copiaSuperficial: any = this.TRASPORTISTAS_ENTURNADOS.slice();
    this.TRASPORTISTAS_ENTURNADOS.splice(_index, 1);


    let toast = this.toastCtrl.create({
      message: "Eliminado item",
      duration: 3000,
      showCloseButton: true,
      closeButtonText: "Cancelar"
    });

    toast.onDidDismiss((data, role) => {
      console.log("Toast buton clicked", role, copiaSuperficial, data);
      if (role == "close") {
        console.log('Cerrado', _data, _index);
        this.TRASPORTISTAS_ENTURNADOS = copiaSuperficial;
        console.log(this.TRASPORTISTAS_ENTURNADOS);
      } else {

        let trasportistas = {
          id_solicitud: this.navParams.get('data').id,
          id_usuario: _data.id
        };

        //--/Limpiando Array de copia.
        copiaSuperficial.splice(0, copiaSuperficial.length);
        this._EliminarTrasportista(trasportistas);
        console.warn('Eliminando', copiaSuperficial);
      }

      ///undo operation
    });
    toast.present().then(() => {
      console.info('Toast,Lanzado');
    });

  }


  /** TODO: AJUSTANDO LA RUTA DE ASIGNAR TRANSPORTISTA....---
   *  Metodos para Agregar(PUT) y Eliminar Trasportistas(POST)
   *
   */
  AsignarTrasportista(value, idx) {
    console.log('DATA DEL SELECCIONADO', value, idx);
    let transportistas = Array();
    transportistas.push({id_usuario: value.usuario_id, id_solicitud: this.solicitudId.solicitudes_id});
    this.id_usuario = value.usuario_id;
    console.log('|', transportistas);
    this.dataPost = transportistas;
    // let data3 = {
    //   data: this.dataPost
    // };
    const data = {data: this.dataPost};
    console.log('LA DATA PARA ASIGNAR', data);
    // this.Complementos.PresentLoading('Enviando Datos', 7000);
    console.log(`/solicitudes/agregartransportista`, JSON.stringify(data));
    this.Api._POST(`/solicitudes/agregartransportista`, JSON.stringify(data))
      .subscribe((data) => {
          console.log('POST:', data);
          if (data.success === true) {
            this.vehiculosR = this.vehiculosR + 1;
            this.Complementos._MessageToast('Transportistas asignados', 7000);
            this.TRASPORTISTAS_SELECCIONADOS.splice(0, this.TRASPORTISTAS_SELECCIONADOS.length);
            this._RefrescarData();
          }
        },
        (err) => {
          if (err.status === 401) {
            // this.events.publish('sesionDuplicada', err)
          } else {
            this.Complementos.loader.dismiss();
            this.Complementos._MessageToast(`${err.error.mensaje}`);
          }
        }, () => {
          this.Complementos.loader.dismiss();
        })
  }

  Search_for_placa() {
    if (this.SearchPlaca) {
      console.log('Filtrando', this.SearchPlaca);
      this.DATOS_SELECT = this.DATOS_SELECT2.filter(item =>
        item.vehiculo_placa.toLowerCase().startsWith(this.SearchPlaca.toLowerCase()) ||
        item.persona_identificacion.toLowerCase().startsWith(this.SearchPlaca.toLowerCase()) ||
        item.trailer_placa.toLowerCase().startsWith(this.SearchPlaca.toLowerCase()) ||
        item.persona_nombre.toLowerCase().startsWith(this.SearchPlaca.toLowerCase()));
    } else {
      this.DATOS_SELECT = this.DATOS_SELECT2;
    }
  }

  addTransportista() {
    this.navCtrl.push(AddTransportistaPage, {});
  }


  _EliminarTrasportista(ObjAssing) {
    console.info('||', ObjAssing);

    this.Api._POST(`/enturnamiento/remover`, ObjAssing)
      .subscribe(
        (data) => {
          console.group('Trasportista eliminado de la asignacion:', data);
          console.groupEnd();
        },
        (err) => {
          if (err.status === 401) {
            // this.events.publish('sesionDuplicada', err)
          } else {
            console.error(err)
          }
        },
        () => {
          this._RefrescarData();
        }
      );
  }

  /**----------------------------------------------------------------
   * Fin Metodos para Agregar(PUT) y Eliminar Trasportistas(POST)
   *-----------------------------------------------------------------
   */



  _Ok_DeleteItem() {
    let toast = this.toastCtrl.create({
      message: "Eliminado item",
      duration: 5000,
      showCloseButton: true,
      closeButtonText: "Cancelar"
    });

    toast.onDidDismiss(() => {
      console.log("Toast buton clicked");

      ///undo operation
    });
    toast.present();
  }
}
