import {Component} from '@angular/core';
import {NavController, NavParams, PopoverController} from 'ionic-angular';
import {DenunciarpopoverPage} from "../denunciarpopover/denunciarpopover";

@Component({
  selector: 'page-reputacion',
  templateUrl: 'reputacion.html',
})
export class ReputacionPage {
  public reputacion: any;

  constructor(public navCtrl: NavController,
              public popoverCtrl: PopoverController,
              public navParams: NavParams) {
    this.navParams.get('data');
    this.reputacion = this.navParams.get('data');
    console.log('REPUTACION TRANSPORTISTA', this.navParams.get('data'))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReputacionPage');
  }

  popoverEdit(myEvent) {
    let popover = this.popoverCtrl.create(DenunciarpopoverPage, {data: this.navParams.get('data')});
    popover.present({
      ev: myEvent,
      animate: true
    });
  }

}
