var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { DenunciarpopoverPage } from "../denunciarpopover/denunciarpopover";
let ReputacionPage = class ReputacionPage {
    constructor(navCtrl, popoverCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.navParams = navParams;
        this.navParams.get('data');
        this.reputacion = this.navParams.get('data');
        console.log('REPUTACION TRANSPORTISTA', this.navParams.get('data'));
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ReputacionPage');
    }
    popoverEdit(myEvent) {
        let popover = this.popoverCtrl.create(DenunciarpopoverPage, { data: this.navParams.get('data') });
        popover.present({
            ev: myEvent,
            animate: true
        });
    }
};
ReputacionPage = __decorate([
    Component({
        selector: 'page-reputacion',
        templateUrl: 'reputacion.html',
    }),
    __metadata("design:paramtypes", [NavController,
        PopoverController,
        NavParams])
], ReputacionPage);
export { ReputacionPage };
//# sourceMappingURL=reputacion.js.map