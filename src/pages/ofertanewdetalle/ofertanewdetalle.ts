import {Component} from '@angular/core';
import {Events, NavController, NavParams, PopoverController} from 'ionic-angular';
import {AppModule} from "../../app/app.module";
import {ApiProvider} from "../../providers/api/api";
import {ComplementosviewsProvider} from "../../providers/complementosviews/complementosviews";
import {PopovereditPage} from "../popoveredit/popoveredit";
import {DetalleactivosPage} from "../detalleactivos/detalleactivos";
import {ReputacionPage} from "../reputacion/reputacion";
import {DetalletransportistaPage} from "../detalletransportista/detalletransportista";
import * as moment from "moment";
import {CallNumber} from "@ionic-native/call-number";
import {GpsProvider} from "../../providers/gps/gps";

@Component({
  selector: 'page-ofertanewdetalle',
  templateUrl: 'ofertanewdetalle.html',
})
export class OfertanewdetallePage {
  public oferta: any;
  public oferta3: any;
  public POSTULANTES: any;
  public segment = 'pendiente';
  public queryText: string;
  BASE_URL = AppModule.BASE_URL;
  public Postulantes_2: any = Array();
  public ofertaDetalle: any;
  public _SOLICITUD: any;
  public _Activos: any;
  public _FechaCarga: any;
  public _FechaEntrega: any;
  public _ActivosCargue: any;
  public _ActivosRuta: any;
  public _ActivosCulminados: any;
  public _alert: boolean;

  get BadConection() {
    return GpsProvider.Band_Conexion;
  }

  constructor(public navCtrl: NavController,
              public popoverCtrl: PopoverController,
              public navParams: NavParams,
              private events: Events,
              private  callNumber: CallNumber,
              public complements: ComplementosviewsProvider,
              private Api: ApiProvider,
              private Complementos: ComplementosviewsProvider) {
    if (this.navParams.get('value') !== 'PUBLICADA') {
      this.segment = 'aceptada';
      console.log('VALOR DEL SEGMENT', this.segment);
    }
  }

  _ConsultarActivos() {
    console.log('ConsultarActivos');
    if (this.segment !== 'activos') {
      this.Api._GET(`/oferta/estadisticas/${this.navParams.get('data').oferta_id}`).subscribe(
        (data) => {
          if (data.cargue.length > 0 || data.ruta.length > 0 || data.culminado.length > 0) {
            this._alert = true;
          } else {
            this._alert = false;
          }
        })
    }
  }

  detalleTransportista(data) {
    this.Complementos._PresentLoading();
    this.Api._GET(`/transportista/detalle/${data.usuario_id}`).subscribe(
      (value) => {
        this.Complementos._DismissPresentLoading();
        if (value) {
          this.navCtrl.push(DetalletransportistaPage,
            {
              data: value,
              value: this.navParams.get('data'),
              ofertadetalle: data
            });
          console.log(value);
        }
      }, (err) => {
        this.Complementos._DismissPresentLoading();
        this.Complementos._MessageToast('Error en la consulta, intenta nuevamente');
        console.log(err)
      })
  }

  _CalcularProgreso(value) {
    if (value > 0) {
      return Math.round(value / this.navParams.get('data').oferta_cantidad * 100);
    } else {
      return 0
    }
  }

  loadNewDetalle() {
    this.Api._GET(`/oferta/estadisticas/${this.navParams.get('data').oferta_id}`).subscribe(
      (data) => {
        if (data) {

          if (data.cargue.length > 0 || data.ruta.length > 0 || data.culminado.length > 0) {
            this._alert = true;
          } else {
            this._alert = false;
          }

          this._FechaCarga = moment(data.fechacarga).format('YYYY-MM-DD HH:mm a') === 'Invalid date' ? 'YYYY-MM-DD' : moment(data.fechacarga).format('YYYY-MM-DD HH:mm a');
          this._FechaEntrega = moment(data.fechaentrega).format('YYYY-MM-DD HH:mm a') === 'Invalid date' ? 'YYYY-MM-DD' : moment(data.fechaentrega).format('YYYY-MM-DD HH:mm a');
          this._Activos = data;
          this._ActivosCargue = data.cargue.length ? Math.round(data.cargue.length) : 0;
          this._ActivosRuta = data.ruta.length ? Math.round(data.ruta.length) : 0;
          this._ActivosCulminados = data.culminado.length ? Math.round(data.culminado.length) : 0;
        }
      }, (err) => {
        console.log(err);
        this.Complementos._MessageToast(`${err.error.mensaje}`)
      });
    // estados de oferta detalle
    this.Api._GET(`/oferta/detalle/${this.navParams.get('data').oferta_id}`).subscribe(
      (data) => {
        if (data) {
          this.Postulantes_2 = data;
          this.oferta = data;
          this.oferta3 = data;
          console.log('oferta1', this.oferta);
          console.log('oferta3', this.oferta3);
          this.filterNewDetalle(this.segment)
        }
      }, (err) => {
        this.complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error')
      });
    // Filtrando la data
  }

  filterNewDetalle(valor) {
    this.segment = valor; // segment será el valor de los filtros..
    if (valor !== 'cancelada') {
      console.log(this.oferta3);
      console.log(valor);
      this.oferta = this.oferta3.filter(i => i.estado === valor.toUpperCase());
    } else {
      this.Postulantes_2 = this.oferta;
      this.oferta = this.oferta3.filter(i => i.estado === valor.toUpperCase());
      if (this.oferta.map(i => i.fuente === 'MANUAL')) {
        this.oferta = this.oferta.filter(i => i.fuente === 'MANUAL');
      } else {
        console.log('No hay rechazos manuales')
      }
    }
  }

  loadofertaDetalle(value: string) {
    if (value !== 'activos')
      this.Api._GET(`/oferta/detalle/${this.navParams.get('data').oferta_id}`).subscribe(
        (data) => {
          if (data) {
            this.Postulantes_2 = data;
            data = data.filter(i => i.estado === this.segment.toUpperCase());
            this.oferta = data;
            console.log(this.oferta);
          }
        }, (err) => {
          this.complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error')
        });
  }

  popoverEdit(myEvent) {
    if (this.navParams.get('data').postulados.length == 0) {
      console.log('NO HAY POSTULADOS');
      let popover = this.popoverCtrl.create(PopovereditPage, {data: this.navParams.get('data'), value: 0});
      popover.present({
        ev: myEvent,
        animate: true
      });
    } else {
      console.log('SI HAY POSTULADOS');
      let popover = this.popoverCtrl.create(PopovereditPage, {data: this.navParams.get('data'), value: 1});
      popover.present({
        ev: myEvent,
        animate: true
      });
    }
  }

  detalleactivos(value) {
    this.Complementos.PresentLoading('cargando', 1000);
    if (value == 'despachos') {
      this.navCtrl.push(DetalleactivosPage, {data: this._Activos.cargue, value: value})
    }
    if (value == 'ruta') {
      this.navCtrl.push(DetalleactivosPage, {data: this._Activos.ruta, value: value})
    }
    if (value == 'ciudad destino') {
      this.navCtrl.push(DetalleactivosPage, {data: this._Activos.ciudad_destino, value: value})
    }
    if (value == 'culminado') {
      this.navCtrl.push(DetalleactivosPage, {data: this._Activos.culminado, value: value})
    }
  }

  doRefresh(refresher) {
    this.Complementos._MessageToast('Refrescando');
    this.loadNewDetalle();
    setTimeout(() => {
      // console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
  }

  updateSegment(value) {
    this.segment = value;
    this._filterNewForSegment(this.Postulantes_2, this.segment)
  }

  _EstadoOferta(estado) {
    if ((estado === 'ACEPTADA') || (estado === 'CANCELADA')) {
      return false;
    } else {
      return true;
    }
  }

  Search_placa_name() {
    if (this.queryText) {
      this.oferta = this.Postulantes_2.filter(i => i.placa.toLowerCase().includes(this.queryText.toLowerCase()) ||
        i.nombre.toLowerCase().includes(this.queryText.toLowerCase()) ||
        i.identificacion.includes(this.queryText.toLowerCase()));
      console.log(this.oferta);
    } else {
      this.oferta = this.Postulantes_2;
    }
  }

  _filterNewForSegment(ArrayData: any, item: string) {
    this.oferta = [];
    console.log('Filter for segment');
    if (item !== 'activos') {
      console.log('_filter_for_Segments', ArrayData, item);
      this.oferta = ArrayData.filter(i => i.estado === item.toUpperCase());
      console.log('filtrado por', item, this.oferta);
    }
  }

  _filter_for_Segment(ArrayData: any, item: string) {
    this.oferta = [];
    console.log('Filter for segment');
    if (item == 'activos') {
      console.log('Data', ArrayData, 'de:', item);
      this.Api._GET(`/oferta/estadisticas/${this.navParams.get('data').oferta_id}`)
        .subscribe(
          (data) => {
            this._FechaCarga = moment(data.fechacarga).format('YYYY-MM-DD HH:mm a') === 'Invalid date' ? 'YYYY-MM-DD' : moment(data.fechacarga).format('YYYY-MM-DD HH:mm a');
            this._FechaEntrega = moment(data.fechaentrega).format('YYYY-MM-DD HH:mm a') === 'Invalid date' ? 'YYYY-MM-DD' : moment(data.fechaentrega).format('YYYY-MM-DD HH:mm a');
            this._Activos = data;
            this._ActivosCargue = data.cargue.length ? Math.round(data.cargue.length) : 0;
            this._ActivosRuta = data.ruta.length ? Math.round(data.ruta.length) : 0;
            this._ActivosCulminados = data.culminado.length ? Math.round(data.culminado.length) : 0;
          }, (err) => {
            this.complements._ErrorRequest(`${err.status}`, `${err.error.message}`, 'Ha ocurrido un error')
          });
      this.oferta = ArrayData.filter(i => i.estado === item.toUpperCase());
    } else {
      console.log('_filter_for_Segments', ArrayData, item);
      this.oferta = ArrayData.filter(i => i.estado === item.toUpperCase());
      console.log('filtrado por', item, this.oferta);
    }
  } // filter NO USADO

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfertanewdetallePage');
  }

  ionViewWillEnter() {
    this.loadNewDetalle();
    this.updateSegment(this.segment);
    this.events.subscribe('activos', (data) => {
      console.log('ACTIVOS CAMBIÓ', data);
      this.loadNewDetalle();
    });
  }

  ionViewWillLeave() {
    console.log('Unsubscribe activos');
    this.events.unsubscribe('activos')
  }

  PhoneCall(Number: string) {
    console.log(Number);
    this.callNumber.callNumber(Number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  break_text(text) {
    let b = text.split(' ');
    if (b.length >= 4) {
      return `${b[0]} ${b[1]} <br> ${b[2]} ${b[3]}`
    }
    if (b.length >= 3) {
      return `${b[0]} ${b[1]} <br> ${b[2]}`
    }
    if (b.length >= 2) {
      return `${b[0]} <br> ${b[1]}`
    }
    return b.join(`<br>`)

  }

  reputacion(data) {
    console.log('reputacion');
    this.navCtrl.push(ReputacionPage, {data: data})
  }

  /*DESTRUIR SOCKET*/
  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      console.log(' Disconect Socket', GpsProvider.EMIT);
      GpsProvider.EMIT.disconnect();
      console.log('1 Disconect Socket', GpsProvider.EMIT);
    }

  }

  /*CERRAR GPS*/
  _CloseGPS() {
    console.group('CERRANDO EL EMIT');
    console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
    console.groupEnd();
    // GpsProvider.Band_Conexion=false;
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }

}
