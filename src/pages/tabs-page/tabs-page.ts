import {Component} from '@angular/core';
import {NavParams} from 'ionic-angular';
import {SolicitudesPage} from '../solicitudes/solicitudes';
import {ListaOfertasPage} from '../lista-ofertas/lista-ofertas';
import {AppModule} from "../../app/app.module";
import {TransportistasListPage} from "../transportistas-list/transportistas-list";

@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = SolicitudesPage;
  tab2Root: any = ListaOfertasPage;
  tab3Root: any = TransportistasListPage;
  // tab4Root: any = ChatPage;
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

  get ShoeTabs() {
    //--/ console.log('GET TABS',AppModule.ShowTapss);
    return AppModule.ShowTapss;

  }

}
