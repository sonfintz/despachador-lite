import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Events, ToastController} from 'ionic-angular';
import {Injectable} from "@angular/core";
import {tap} from "rxjs/operators";


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  SessionError: boolean = false;

  constructor(
    public toastController: ToastController,
    public events: Events) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = (localStorage.getItem('token')) ? localStorage.getItem('token') : false;

    if (token) {
      request = request.clone({
        setHeaders: {
          'Authorization': token
        }
      });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json'
        }
      });
    }

    request = request.clone({
      headers: request.headers.set('Accept', 'application/json')
    });

    /*  return next.handle(request).pipe(
          map((event: HttpEvent<any>) => {
              if (event instanceof HttpResponse) {
                  console.log('event--->>>', event);
              }
              return event;
          }),
          catchError((error: HttpErrorResponse) => {
              if (error.status === 401) {
                  if (error.error.success === false) {
                      this.presentToast('Login failed');
                  } else {
                      this.router.navigate(['login']);
                  }
              }
              return throwError(error);
          })); */

    return next.handle(request).pipe(tap(() => {
      },
      (exeception) => {
        if (exeception instanceof HttpErrorResponse) {
          if (exeception.status === 401) {
            console.log('INTERCEPTOR 401');
            if (!this.SessionError) {
              this.SessionError = true;
              setTimeout(() => {
                this.presentToast('Sesion duplicada');
                this.events.publish('Logout', true);
                this.SessionError = false;
              }, 3000);
            }

          } else {
            console.error('------------------------------')
            console.error(exeception);
            console.log('..................................')
          }
        }
      }));
  }

  //--/ Handdler error


  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }
}
