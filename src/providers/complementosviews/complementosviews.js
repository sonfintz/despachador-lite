var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { AlertController, App, LoadingController, ToastController } from "ionic-angular";
/**
 * Proveedor de omplementos para las vistas en los componentes
 * Toast, Alertas, Preloadings..
 */
let ComplementosviewsProvider = class ComplementosviewsProvider {
    constructor(loadingCtrl, toastCtrl, alertCtrl, app) {
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.navCtrl = this.app.getActiveNav();
    }
    /** ----/ Metodo para lanzar un Toast **/
    _MessageToast(message, duration = 3000, navCtrl = false, page = '') {
        let config = {
            message: message,
            duration: (duration) || 3000
        };
        let toast = this.toastCtrl.create(config);
        toast.present().then(() => {
            if (navCtrl) {
                this.navCtrl.setRoot(page);
            }
        });
    }
    PresentLoading(message, duration) {
        this.loader = this.loadingCtrl.create({
            content: message,
            duration: (duration || 3000)
        });
        this.loader.present();
    }
    showAlert(title, subtitle = '', message = '', buttonsArray = ['OK'], cssClass, enableBackdropDismiss = false) {
        const alert = this.alertCtrl.create({
            title: title,
            subTitle: subtitle,
            message: message,
            buttons: buttonsArray,
            cssClass: cssClass,
            enableBackdropDismiss: enableBackdropDismiss
        });
        alert.present();
    }
    _ErrorRequest(status, statusText, titleError = "¡Error !") {
        let btn = [
            {
                text: "Entendido",
                cssClass: "button2",
                role: "cancel"
            }
        ];
        let message = `<div class='statusError'>${status} <span class='statusText'>${statusText}</span></div>`;
        this.showAlert(titleError, "El servidor no responde.", message, btn, "alertConfig_2_css");
    }
    _PresentLoading() {
        this.loader = this.loadingCtrl.create({
            content: "Cargando",
        });
        this.loader.present();
    }
    _DismissPresentLoading() {
        this.loader.dismiss();
    }
};
ComplementosviewsProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [LoadingController,
        ToastController,
        AlertController,
        App])
], ComplementosviewsProvider);
export { ComplementosviewsProvider };
//# sourceMappingURL=complementosviews.js.map