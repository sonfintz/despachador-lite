import {Injectable} from '@angular/core';
import {AlertController, App, LoadingController, NavController, ToastController} from "ionic-angular";


/**
 * Proveedor de omplementos para las vistas en los componentes
 * Toast, Alertas, Preloadings..
 */
@Injectable()
export class ComplementosviewsProvider {
  private navCtrl: NavController;
  public loader: any;

  constructor(
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private app: App
  ) {
    this.navCtrl = this.app.getActiveNav();
  }


  /** ----/ Metodo para lanzar un Toast **/
  _MessageToast(message: string, duration: number = 3000, navCtrl: boolean = false, page: any = '') {
    let config = {
      message: message,
      duration: (duration) || 3000
    };

    let toast = this.toastCtrl.create(config);
    toast.present().then(() => {
      if (navCtrl) {
        this.navCtrl.setRoot(page);
      }
    });
  }

  PresentLoading(message: string, duration: number) {
    this.loader = this.loadingCtrl.create({
      content: message,
      duration: (duration || 3000)
    });
    this.loader.present();
  }

  showAlert(title: string, subtitle: string = '', message: string = '', buttonsArray: any = ['OK'], cssClass, enableBackdropDismiss = false) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      message: message,
      buttons: buttonsArray,
      cssClass: cssClass,
      enableBackdropDismiss: enableBackdropDismiss
    });
    alert.present();
  }

  _ErrorRequest(status, statusText, titleError = "¡Error !") {
    let btn = [
      {
        text: "Entendido",
        cssClass: "button2",
        role: "cancel"
      }
    ];
    let message = `<div class='statusError'>${status} <span class='statusText'>${statusText}</span></div>`;
    this.showAlert(titleError, "El servidor no responde."
      , message, btn, "alertConfig_2_css")
  }

  _PresentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Cargando",
    });

    this.loader.present();
  }

  _DismissPresentLoading() {
    this.loader.dismiss();
  }

}
