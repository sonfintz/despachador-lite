import {Injectable} from "@angular/core";

@Injectable()
export class Descompilar {

  constructor() {
  }

  public desc(Despachador) {
    Despachador.contenido = this.descompilar(Despachador.contenido);
    for (let row of Despachador.contenido) {
      for (let key of Object.keys(row)) {
        if (key == 'postulados') row[key] = this.descompilar(row[key])
      }
    }
    return Despachador.contenido;
  }

  public descompilar(json) {
    if (typeof json == "object") {
      if (json.hasOwnProperty('cabezera') == true && json.hasOwnProperty('cuerpo') == true) {
        let list = Array();
        for (let i in json.cuerpo) {
          let obj = Object();
          for (let j in json.cuerpo[i]) obj[json.cabezera[j]] = json.cuerpo[i][j];
          list.push(obj)
        }
        json = list
      }
    }
    return json
  }
}
