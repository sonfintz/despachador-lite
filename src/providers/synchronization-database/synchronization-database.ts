import {Injectable} from '@angular/core';
import {Events, Platform} from "ionic-angular";
import {DatabaseProvider} from "../database/database";
import {ApiProvider} from "../api/api";
import {DataglobalProvider} from "../dataglobal/dataglobal";

@Injectable()
export class SynchronizationDatabaseProvider {

  constructor(public events: Events,
              private db: DatabaseProvider,
              public platform: Platform,
              private Api: ApiProvider) {
    this.SubscriptionsEvents()
  }

  SubscriptionsEvents() {
    console.log('LoadDatanotifications');
    this.events.subscribe('user:NotificationsSQL', val => {
      console.log('user:sessions', 'SUBSCRITOOOO', val);
      if (this.platform.is('cordova')) {
        console.log('DESDE ANDROID');
        const IntervalSQL = setInterval(() => {
          if (DatabaseProvider.isOpen) {
            clearInterval(IntervalSQL)
            this.LoadDataNotificationsSettings();

          }
        })

      } else {
        console.log('DESDE LA WEB');
        this.WEBDownloadDataNotificationsAPI();
      }
    });

    this.events.subscribe('ReloadNotifications', (data) => {
      console.log('ReloadNotifications', data);
      this.DownloadData();
    })

  }

  LoadDataNotificationsSettings() {
    console.log(1);
    this.db.listData_table(`notification_settings`)
      .subscribe(
        data => {
          if (data.length == 0) {
            console.log('Esta tabla notification_settings esta vacia');
            this.DownloadDataNotificationsAPI()
          } else {
            console.log('Esta tabla notification_settings no esta vacia', data);
            DataglobalProvider.NotificacionesSettings = data;
          }
        },
        err => {
          console.error('Error al consultar los datos de la tabla notification_settings: ', err)
        }
      )
  }

  DownloadDataNotificationsAPI() {
    this.Api._GET(`/informacion/notificaciones/despachador/`)
      .subscribe(
        data => {
          console.group('Descargando data del API');
          console.log(data);
          console.groupEnd();
          if (Array.isArray(data)) {
            for (let i = 0; i < data.length; i++) {
              this.db.insertData_table(`notification_settings`,
                `design_codigo, design_tipo, design_titulo, design_perfil, design_descripcion, design_mostrar, design_moneda, actualizado, design_descripcion_confirmacion, design_titulo_confirmacion, design_puntos, design_operacion, design_detalle`,
                [data[i].design_codigo, data[i].design_tipo, data[i].design_titulo, data[i].design_perfil, data[i].design_descripcion, data[i].design_mostrar, data[i].design_moneda, data[i].actualizado, data[i].design_descripcion_confirmacion, data[i].design_titulo_confirmacion, data[i].design_puntos, data[i].design_operacion, data[i].design_detalle],
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?`)
                .subscribe(
                  res => {
                    console.log('Resultado del insert notification_settings:', res);
                  },
                  err => {
                    console.error('Error al insertar en la tabla notification_settings', err)
                  }
                );//fin del insert
            }// fin del for
            this.LoadDataNotificationsSettings();
          }
        },
        err => {
          console.error('Error al descargar la data del API ', err)
        }
      );
  }

  WEBDownloadDataNotificationsAPI() {
    this.Api._GET(`/informacion/notificaciones/despachador/`)
      .subscribe(
        data => {
          console.group('Descargando data del API');
          console.log(data);
          console.groupEnd();
          if (Array.isArray(data)) {
            DataglobalProvider.NotificacionesSettings = data;
            console.log('DATAGLOBAL PROVIDER', DataglobalProvider.NotificacionesSettings);
          }
        },
        err => {
          console.error('Error al descargar la data del API ', err)
        }
      );
  }

  DownloadData() {
    console.group('Data actual DATABASE');
    console.log(DataglobalProvider.NotificacionesSettings.length, '<-->', DataglobalProvider.NotificacionesSettings);
    console.groupEnd();
    this.Api._GET(`/informacion/notificaciones/despachador`)
      .subscribe(
        data => {
          console.group('Descargando data del API');
          console.log(data);
          console.groupEnd();
          if (Array.isArray(data)) {
            for (let i = 0; i < data.length; i++) {
              this.db.insertData_tableOR_REPLACE(`notification_settings`,
                `design_codigo, design_tipo, design_titulo, design_perfil, design_descripcion, design_mostrar, design_moneda, actualizado, design_descripcion_confirmacion, design_titulo_confirmacion, design_puntos, design_operacion, design_detalle`,
                [data[i].design_codigo, data[i].design_tipo, data[i].design_titulo, data[i].design_perfil, data[i].design_descripcion,
                  data[i].design_mostrar, data[i].design_moneda, data[i].actualizado, data[i].design_descripcion_confirmacion, data[i].design_titulo_confirmacion, data[i].design_puntos, data[i].design_operacion, data[i].design_detalle], `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?`)
                .subscribe(
                  res => {
                    console.log('Resultado del insert notification_settings:', res)
                  },
                  err => {
                    console.error('Error al insertar en la tabla notification_settings', err)
                  }
                )
            }
            this.events.publish('user:NotificationsSQL')
          }
        },
        err => {
          console.error('Error al descargar la data del API ', err)
        }
      );
  }

}
