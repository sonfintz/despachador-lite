import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import * as io from "socket.io-client";
import {Geolocation} from "@ionic-native/geolocation";
//import {AppModule} from "../../app/app.module";
import {ConferenceApp} from "../../app/app.component";
import {AlertController, App, Events, NavController, Platform, ToastController} from "ionic-angular";
//import {LoginPage} from "../../pages/login/login";
import {Observable} from "rxjs/Observable";
import {LocalNotifications} from '@ionic-native/local-notifications';
import {AppModule} from "../../app/app.module";
import {LoginPage} from "../../pages/login/login";
import * as moment from "moment";
import {ListaOfertasPage} from "../../pages/lista-ofertas/lista-ofertas";
import {ApiProvider} from "../api/api";
import {DataglobalProvider} from "../dataglobal/dataglobal";
import {ComplementosviewsProvider} from "../complementosviews/complementosviews";

declare let cordova;
declare let FCMPlugin;

@Injectable()
export class GpsProvider {

  //private navCtrl: NavController;
  private socket: any;
  public MyLatitude: any;
  public MyLongitude: any;
  public MySpeed: any;
  public TimeLineEvents: any;
  private _LocationGOOGLE: any;
  static switchToLocation: boolean;
  // private BandGoogle: boolean;
  public obj: any = [];
  static Band_Conexion: boolean = false;
  public isAndroid: any;
  static intervalSocket: number;
  static antiFakeGPS: any;
  /** Se Define como el Supervisor de la App
   Detectara las fallas ocacionadas por ubicaciones falsas **/
  public Sup_i: number = 0; // Contador del Supervisor
  public Sup_dis: any; //Distancia recorrida y calculada por el SUpervisor

  static LATITUDE: any;
  static LONGITUDE: any;
  static SPEED: any;
  static EMIT: any;
  subscriptionGP: any;
  public watchPosition: any;
  public Heading: any;
  static HEADING: any;

  static MESSAGES = [];
  static PositionMessage = [];
  /** GUARDA LAS POSICIONES PARA ELIMINAR LOS MESAJES **/
  static LISTA_CONECTADOS = [];
  static LISTA_TURNO = [];
  cola = [];
  alerta = false;

  get NotificationsSetting() {
    return DataglobalProvider.NotificacionesSettings;
  }

  constructor(
    // private app:App,
    private navCtrl: NavController,
    public http: HttpClient,
    public geolocation: Geolocation,
    public alertController: AlertController,
    public platform: Platform,
    // private Complementos: ComplementosviewsProvider,
    private toastCtrl: ToastController,
    private _LNotifications: LocalNotifications,
    public events: Events,
    private Complementos: ComplementosviewsProvider,
    public api: ApiProvider,
    private app: App,
  ) {
    // this.navCtrl = this.app.getActiveNav();
    this.navCtrl = this.app.getActiveNav();
    this.isAndroid = this.platform.is('android');

    console.group('Laamada 1vez this.GET_LOCATION_2');
    this.GET_LOCATION();
    console.groupEnd()

  }

  /** GEOLOCATION **/

  /*_GPS() {
    this.TurnOnGPS();
    this.DiagnosticGPS();

    if (typeof FCMPlugin != 'undefined') {
      FCMPlugin.subscribeToTopic(`notificacion${window.localStorage.getItem("idUserMUEVE")}`);
      if (parseInt(localStorage.getItem("EnturneUserTruck")) === 1) {
        FCMPlugin.subscribeToTopic(`enturneactividad_${window.localStorage.getItem("idUserMUEVE")}`);
      }
    }

    this.GET_LOCATION().subscribe(data => {
      console.log('RESULTADO DE 2: this.GET_LOCATION():--> ', data);
      this.MyLatitude = data.latitude;
      this.MyLongitude = data.longitude;
      this.MySpeed = data.speed;
      this.Heading = data.heading;
      window.localStorage.setItem('MyLatitude', data.latitude.toString());
      window.localStorage.setItem('MyLongitude', data.longitude.toString());
      GpsProvider.LATITUDE = data.latitude;
      GpsProvider.LONGITUDE = data.longitude;
      GpsProvider.SPEED = data.speed;
      GpsProvider.HEADING = data.heading;
    });

    this.subsCripcionGPS();


    this._Socket();


    /!*let options = {
      enableHighAccuracy: false,
      // timeout: 5000,
      maximumAge: 0
    };*!/
    /!** Si es la primera vez que se hace login
     * Llamamos a GOOGLE **!/
    /!*if (OPC === 1) {
      this._LocalizacionGOOGLE().subscribe((data) => {
        console.group('GOOGLE LOCATION');
        console.log(data);
        console.groupEnd();
        this._LocationGOOGLE = data;
        // this.BandGoogle = true;


      }, (err) => {
        /!** SI OCURRE UN ERROR LANZAMOS L PETICION DIRECTA AL GPS **!/
        console.group('ERROR GOOGLE LOCATION');
        console.log(err);
        console.groupEnd();
      }, () => {

        this.MyLatitude = this._LocationGOOGLE.location.lat;
        this.MyLongitude = this._LocationGOOGLE.location.lng;
        /!**------------------- SETEANDO COORDENADAS ------------------------------**!/
        window.localStorage.setItem('MyLatitude', this._LocationGOOGLE.location.lat);
        window.localStorage.setItem('MyLongitude', this._LocationGOOGLE.location.lng);
        /!**
         ----------------------------------------------------------------------------------**!/

        GpsProvider.LATITUDE = this._LocationGOOGLE.location.lat;
        GpsProvider.LONGITUDE = this._LocationGOOGLE.location.lng;
        /!** Lanzamos al SUPERVISOR**!/
        // GpsProvider.antiFakeGPS = setInterval(() => {
        //   this._Supervisor()
        // }, 30000);
        /!** Activamos el Socket**!/
        this._Socket();

      });
    }
    /!** Si el Usuario tiene sesion activa
     * Llamamos al GPS del Movil **!/
    else if (OPC === 2) {

      this._Socket();

      this.geolocation.getCurrentPosition(options).then((resp) => {

        console.log('RESP', resp);
        this.MyLatitude = resp.coords.latitude;
        this.MyLongitude = resp.coords.longitude;
        this.MySpeed = resp.coords.speed;
        /!** LOCAL STORAGE COORDENADAS **!/
        window.localStorage.setItem('MyLatitude', resp.coords.latitude.toString());
        window.localStorage.setItem('MyLongitude', resp.coords.longitude.toString());
        /!** ------------------------ **!/
        GpsProvider.LATITUDE = resp.coords.latitude;
        GpsProvider.LONGITUDE = resp.coords.longitude;
        GpsProvider.SPEED = resp.coords.speed;


      }).catch(
        (err) => {
          console.log('Error', err);
        }
      );
    }*!/
  }*/

  /** GEOLOCATION **/

  _GPS() {
    // this.TurnOnGPS();
    // this.DiagnosticGPS();


    if (typeof FCMPlugin != 'undefined') {
      FCMPlugin.subscribeToTopic(`notificacion${window.localStorage.getItem("idUserMUEVE")}`);
      if (parseInt(localStorage.getItem("EnturneUserTruck")) === 1) {
        FCMPlugin.subscribeToTopic(`enturneactividad_${window.localStorage.getItem("idUserMUEVE")}`);
      }
    }


    this.GET_LOCATION().subscribe(data => {
      console.log('RESULTADO DE 2: this.GET_LOCATION():--> ', data);
      this.MyLatitude = data.latitude;
      this.MyLongitude = data.longitude;
      this.MySpeed = data.speed;
      this.Heading = data.heading;
      window.localStorage.setItem('MyLatitude', data.latitude.toString());
      window.localStorage.setItem('MyLongitude', data.longitude.toString());
      GpsProvider.LATITUDE = data.latitude;
      GpsProvider.LONGITUDE = data.longitude;
      GpsProvider.SPEED = data.speed;
      GpsProvider.HEADING = data.heading;
      console.log(GpsProvider.LATITUDE, GpsProvider.LONGITUDE)
    });
    this.subsCripcionGPS();

    // Llamamos a GOOGLE
    /*   this._LocalizacionGOOGLE().subscribe((data) => {
           console.group('GOOGLE LOCATION');
           console.log(data);
           console.groupEnd();
           this._LocationGOOGLE = data;
           this.BandGoogle = true;


       }, (err) => {
           /// SI OCURRE UN ERROR LANZAMOS L PETICION DIRECTA AL GPS
           console.group('ERROR GOOGLE LOCATION');
           console.log(err);
           console.groupEnd();
           console.log(err.status == 0, err.status)


       }, () => {

           this.MyLatitude = this._LocationGOOGLE.location.lat;
           this.MyLongitude = this._LocationGOOGLE.location.lng;
           //------------------- SETEANDO COORDENADAS ------------------------------
           window.localStorage.setItem('MyLatitude', this._LocationGOOGLE.location.lat);
           window.localStorage.setItem('MyLongitude', this._LocationGOOGLE.location.lng);
           //
            ----------------------------------------------------------------------------------

           GpsProvider.LATITUDE = this._LocationGOOGLE.location.lat;
           GpsProvider.LONGITUDE = this._LocationGOOGLE.location.lng;
           // Lanzamos al SUPERVISOR
         //  GpsProvider.antiFakeGPS = setInterval(() => { this._Supervisor(); this.verifyTurf()}, 30000);



       }); */


    this._Socket();

  }


  GET_LOCATION() {
    console.info('CONSTRUCCION DEL OBSERVER');

    let options = {
      enableHighAccuracy: true//,
      // timeout: 30000,
      // maximumAge: 60000
    };
    return Observable.create((observador) => {
      setInterval(() => {
        this.geolocation.getCurrentPosition(options)
          .then((resp) => {
            try {
              // console.log(this.geolocation, typeof this.geolocation);
              // console.log('GET CURRENT POSITION: ', resp);
              observador.next({
                latitude: resp.coords.latitude,
                longitude: resp.coords.longitude,
                speed: resp.coords.speed
              });
              observador.complete();
            } catch (e) {
              console.log(e)
            }
          }).catch(err => {
          console.error('Error  GET_LOCATION()', err)
        });
      }, 5000)
    })
  }


  subsCripcionGPS() {
    console.log('gps', 1);
    console.log(this.geolocation, 2, typeof this.geolocation);
    try {
      this.subscriptionGP = this.geolocation.watchPosition()
      // .filter((p) => p.coords !== undefined)
      //Filter Out Errors
        .subscribe(position => {
          if (Object.keys(position).length > 0) {
            console.log('POSITION');
            this.watchPosition = position;
            this.MyLatitude = position.coords.latitude;
            this.MyLongitude = position.coords.longitude;
            this.MySpeed = position.coords.speed;
            this.Heading = position.coords.heading;
            window.localStorage.setItem('MyLatitude', position.coords.latitude.toString());
            window.localStorage.setItem('MyLongitude', position.coords.longitude.toString());
            GpsProvider.LATITUDE = position.coords.latitude;
            GpsProvider.LONGITUDE = position.coords.longitude;
            GpsProvider.SPEED = position.coords.speed;
            GpsProvider.HEADING = position.coords.heading;
          }
          //----> console.log('GPS SUBSCRIPCION: ',' longitude:', position.coords.longitude , ' latitude:', position.coords.latitude);
        }, error1 => {
          console.log(error1)
        });
    } catch (e) {
      console.log(e);
    }
  }


  /** POSICIONAMIENTO -LOCALIZACION GOOGLE **/
  _LocalizacionGOOGLE(): Observable<any> {
    return this.http.post('https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAadC_zBxrntzpGJdTVyQnx3U6WH1P2zBo', {});
  }

  /**|FIN POSICIONAMIENTO -LOCALIZACION GOOGLE |**/

  /**---------------------------------------
   * SUPERVISOR **/
  _Supervisor() {

    let Km = this._GetKilometros(this._LocationGOOGLE.location.lat, this._LocationGOOGLE.location.lng, this.MyLatitude, this.MyLongitude);

    let V_promedio = this.Sup_i * 750;
    let metros: number = Km * 1000;

    // console.log('Valor  Promedio:',V_promedio);
    // console.log('Metros:',metros,'Metros parseados:',Math.round(metros),'|',metros);
    if (Math.round(metros) > V_promedio) {

      this._Notification('¡Hemos detectado un posible uso de ubicacion simulada!', 'seguridad', 'file://sounds/car-alarm.mp3');
    }
    this.Sup_i++;
  }

  ViewPuntos(data) {

    /*NOTIFITACIONES*/
    this.events.publish('notificaciones', data);

    /*TIME LINE*/
    this.events.publish('timeline', true);

    // console.log('VIEW PUNTOS', data);

    // console.log('alerta de punto recibido', data);
    // this.events.publish('ofertalist', true);

    /*BUSCANDO EL EVENTO EN LA TABLA DE SQLITE*/
    let evento = this.NotificationsSetting.find(i => i.design_codigo === data.ref);
    console.log('EVENTO CONSEGUIDO GPS', evento.design_mostrar);

    if (evento.design_tipo == 'Mueve Informa') {
      this.events.publish('notification-reload', true);
    }

    /*SI LA REFERENCIA ES DIFERENTE A ESTOS ID, MOSTRARA ALERTA*/
    if (evento.design_mostrar !== '0') {
      // /*OFERTA LIST*/
      // this.events.publish('reloadofertas', true);

      /*const alert = this.alertController.create({
        title: `${evento.design_titulo}`,
        subTitle: `${evento.design_descripcion}`,
        enableBackdropDismiss: false,
        message: `${evento.design_moneda !== '0.00' ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
        buttons: [
          {
            text: '¡Entendido!',
            cssClass: 'botton3',
            handler: () => {
              this.alerta = false;
            }
          }
        ],
        cssClass: 'alertCustomCss2' // <- added this
      });
      alert.present();*/
      if (evento.design_operacion === 'SUMA') {
        console.log('SUMA');
        const alert = this.alertController.create({
          title: `${evento.design_titulo}`,
          subTitle: `${evento.design_descripcion}`,
          enableBackdropDismiss: false,
          message: `${evento.design_moneda !== '0.00' ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
          buttons: [
            {
              text: '¡Entendido!',
              cssClass: 'botton3',
              handler: () => {
                this.alerta = false;
              }
            }
          ],
          cssClass: 'alertCustomCss2' // <- added this
        });
        alert.present();
      }
      if (evento.design_operacion === 'NINGUNO') {
        console.log('NINGUNO');
        const alert = this.alertController.create({
          title: `${evento.design_titulo}`,
          subTitle: `${evento.design_descripcion}`,
          enableBackdropDismiss: false,
          message: `${evento.design_moneda !== '0.00' ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
          buttons: [
            {
              text: '¡Entendido!',
              cssClass: 'botton3',
              handler: () => {
                this.alerta = false;
              }
            }
          ],
          cssClass: 'alertCustomCss2' // <- added this
        });
        alert.present();
      }
      if (evento.design_operacion === 'RESTA') {
        console.log('RESTA');
        this.events.publish('activos', true);
        const alert = this.alertController.create({
          title: `${evento.design_titulo}`,
          subTitle: `${evento.design_descripcion}`,
          enableBackdropDismiss: false,
          message: `${evento.design_moneda !== '0.00' ? evento.design_puntos * Number(evento.design_moneda) + 'MP' : ''}`,
          buttons: [
            {
              text: '¡Entendido!',
              cssClass: 'botton3',
              handler: () => {
                this.alerta = false;
              }
            }
          ],
          cssClass: 'alertCustomCss4' // <- added this
        });
        alert.present();
      }
    } else {
      this.Complementos._MessageToast('Actualizando', 2000);
      this.alerta = false;
    }
    // this.events.publish('actividad', true);
  }

  _GetKilometros(lat1, lon1, lat2, lon2) {
    // console.log('GET Kilometro::',lat1,lon1,lat2,lon2);
    let rad = (x) => {
      return x * Math.PI / 180;
    };
    let R = 6371; //Radio de la Tierra
    let dLat = rad(lat2 - lat1);
    let dLong = rad(lon2 - lon1);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);


    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return d;
  }

  /**----------------------------------------**/
  _SET_GPS_DATA() {
    let options = {
      enableHighAccuracy: false,
      maximumAge: 0
    };
    this.geolocation.getCurrentPosition(options).then((resp) => {
      // console.log('SET Todos los Datos GPS:',resp);
      this.MyLatitude = '';
      this.MyLongitude = '';
      this.MyLatitude = resp.coords.latitude;
      this.MyLongitude = resp.coords.longitude;
      /** LOCAL STORAGE COORDENADAS **/
      window.localStorage.setItem('MyLatitude', resp.coords.latitude.toString());
      window.localStorage.setItem('MyLongitude', resp.coords.longitude.toString());
      /** ------------------------ **/
      this.MySpeed = resp.coords.speed;
      GpsProvider.LATITUDE = resp.coords.latitude;
      GpsProvider.LONGITUDE = resp.coords.longitude;
      GpsProvider.SPEED = resp.coords.speed;

    }).catch();
  }

  /** CONEXION SOCKET.IO **/

  _Socket() {
    let obj = [
      {
        id: window.localStorage.getItem("idUserMUEVE"),
        lat: (this.MyLatitude) || window.localStorage.getItem('MyLatitude'),
        lng: (this.MyLongitude) || window.localStorage.getItem('MyLongitude'),
        token: window.localStorage.getItem("token")/*, speed:this.MySpeed*/
      }
    ];

    /*PRUEBAS DESARROLLO*/
    // this.socket = io('http://192.168.88.151:9601', {
    //   query: {
    //     'token': window.localStorage.getItem('token')
    //   }
    // });
    /*PRODUCCION EXTERNO*/
    // this.socket = io('http://grupomuevesas.ddns.net:9601', {
    //   query: {
    //     'token': window.localStorage.getItem('token')
    //   }
    // });
    /*PRODUCCION INTERNO*/
    this.socket = io('http://192.168.88.59:9601', {
      query: {
        'token': window.localStorage.getItem('token')
      }
    });
    GpsProvider.EMIT = this.socket;


    this.socket.on('connect', () => {
      let thas = this;
      console.log('ESTAMOS CONECTADOS AL SOCKET', obj);
      // this.events.publish('user:sessions', true);
      // this.events.publish('ofertalist', true);
      let toast = thas.toastCtrl.create({
        message: '¡Estás de vuelta! Conectado con Mueve Logística.',
        duration: 1500,
        position: 'bottom'
      });
      toast.present();
      // this.socket.emit('track', obj);
      this.socket.emit('autentificacion', window.localStorage.getItem("idUserMUEVE"));


      GpsProvider.Band_Conexion = true;

      // El tiempo es de 10 segundos se cambiara 30 segundos.

      /** Llama al componente HomPage Luego de enviar la traza del GPS **/
      /**------- DISPARA EL PUBLICADOR DE EVENTOS PARA ENVIAR DATA ALMACENADA EN EL DISPOSITIVO ----------**/
      this.events.publish('connection:SERVER_SEND_DATA', {value: true});
    });
    this.events.publish('tipoUsuario', 'FORANEO');
    this.events.publish('puntos2', window.localStorage.getItem('puntos'));
    console.group('PUBLISH');
    console.log('LINKS');
    console.groupEnd();
    this.events.publish('links', true);
    this.navCtrl.setRoot(ListaOfertasPage);
    /*if (window.localStorage.getItem('tipo') === 'FORANEO') {
      this.events.publish('tipoUsuario', window.localStorage.getItem('tipo'));
      this.events.publish('puntos2', window.localStorage.getItem('puntos'));
      this.navCtrl.setRoot(ListaOfertasPage);
    } else {
      this.events.publish('tipoUsuario', window.localStorage.getItem('tipo'));
      this.navCtrl.setRoot(SolicitudesPage)
    }*/
    this._ON_SOCKET();
    GpsProvider.intervalSocket = setInterval(() => {
      this._GPSinterval()
    }, 30000);

  }

  _GPSinterval() {
    this._SET_GPS_DATA();
    let valores = {
      id_usuario: window.localStorage.getItem("idUserMUEVE"),
      latitud: this.MyLatitude,
      longitud: this.MyLongitude,
      token: window.localStorage.getItem("token"),
      dt_tracker: moment().format('YYYY-MM-DD HH:mm:ss')
    };
    this.obj.push(valores);
    /**this.socket = io('http://192.168.0.39:9000');**/


    if (GpsProvider.Band_Conexion === true) {

      this.socket.emit('gps', this.obj);

      this.obj = [];

    } else {
    }

  }


  _ON_SOCKET() {
    // let thas = this;
    this.socket.on('connect_failed', function (data) {
      console.log('Error Fallo | Connect_failed | la conexion al servidor.', data);
    });
    this.socket.on('reconnect_failed', function (data) {
      console.log('Fallo | Reconnect_failed | la conexion al servidor.', data);
    });
    this.socket.on('connect_error', function (data) {
      /**SI SE DISPARA EDTE ERROR **/GpsProvider.Band_Conexion = false;
      /** ACTIVAMOS LA BANDERA A FALSE**/
      console.log('connection_error');
      /*let toast = thas.toastCtrl.create({
        message: 'Sin conexion',
        duration: 1500,
        position: 'bottom'
      });
      toast.present();*/
      // thas.Complementos._MessageToast('');
      console.log(data);
    });
    let canales = JSON.parse(localStorage.getItem('canales'));
    console.log('LOS CANALES', canales);
    // console.log('TYPEOF', typeof canales);
    for (let canal of canales) {
      // console.log('LOS CANALES 3', canal);
      this.socket.on(canal, (data) => {
        /*CONFIRMAR A BACKEND LA LLEGADA DE LA NOTIFICACION*/
        this.socket.emit('confirmacion', data.uuid);

        console.log('LO QUE ESTOY RECIBIENDO POR SOCKET', data);
        if (Array.isArray(data)) {
          if (data.find(i => i.accion == 'actualizar')) {
            console.log('ACTUALIZAR TABLA');
            data.forEach(i => this.socket.emit('confirmacion', i.uuid));
            this.events.publish('ReloadNotifications', true);
            this.Complementos.PresentLoading('Descargando', 2000);
            setTimeout(() => {
              this.Complementos._MessageToast('Informacion descargada')
            }, 2000);
          } else {
            console.log('NO ES DE ACTUALIZAR');
          }
        }

        /*SI LA CUENTA FUE ACTIVADA*/
        if (data.ref === 1) {
          console.log('CUENTA ACTIVADA', data);
          this.events.publish('cuenta-activada', true)
        }

        if (data.uuid && !data.accion) {

          console.log('PUNTOS');

          /*OFERTA-ACTIVOS*/
          this.events.publish('activos', true);

          /*PUSHEANDO A LA COLA DE NOTIFICACIONES*/
          this.cola.push(data);
          // console.log('NOTIFICACIONES PUSHEADAS', this.cola);
          // this.ViewPuntos(data);

          /*COLA DE NOTIFICACIONES*/
          this.procesarCola();

        }
        this.events.publish('solicitudnueva', data);
        this.events.publish('SubscribeEvents');
        this.events.publish('detalleactivos');
      })
    }

    if (GpsProvider.Band_Conexion === true) {

      this.socket.on('nueva_ofertas', (data) => {
        console.log('ON NUEVA OFERTA', data);

        ConferenceApp.Notifi_ofert = ConferenceApp.Notifi_ofert + 1;

        if (ConferenceApp.Notifi_ofert > 0) {
          ConferenceApp.Col_OfertBtn = 'secondary';
        }

      });

      /** OYENTE DE AUTORIZACION **/
      this.socket.on('autorizacion_' + window.localStorage.getItem("idUserMUEVE"), (data) => {
        console.group('SOCKET AUTORIZATION:', data, '||', localStorage.getItem("token"));
        if (data.token !== window.localStorage.getItem("token")) {
          console.log('CERRANDO LA SESION ON SOCKET', data, data.token, '!=', window.localStorage.getItem("token"));
          if (data.token != window.localStorage.getItem("token")) {

            if (typeof FCMPlugin != 'undefined') {
              FCMPlugin.unsubscribeFromTopic(`notificacion${ConferenceApp.usuario_id}`);
            }
            if (parseInt(localStorage.getItem("EnturneUserTruck")) === 1) {
              FCMPlugin.unsubscribeFromTopic(`enturneactividad_${ConferenceApp.usuario_id}`);
            }


            localStorage.clear();
            localStorage.clear();
            AppModule.My_Token = '';
            ConferenceApp.usuario_id = "";
            this._CloseGPS();
            this._DestroySocket();
            this.navCtrl.setRoot(LoginPage);
            DataglobalProvider.historial = [];

          }

        }
        console.groupEnd();
      });

      this.socket.on('usuario_' + window.localStorage.getItem("idUserMUEVE"), (data) => {
        console.group('SOCKET USUARIO MESSAGE:');
        console.log(data);
        GpsProvider.MESSAGES.push(data);
        this.events.publish("mensajes:chat", true);
        console.groupEnd();
        this._Notification('Nuevo mensaje del chat', 'Chat Kargoo', 'file://sounds/misc189.mp3');

      });

      this.socket.on('chat_actualizar', (data: any) => {
        console.group('LISTA DE CONECTADOS:', data);
        let Users_Con = Array();
        data.forEach((x) => {
          console.log(x);
          if (x.usuario_id != "9") Users_Con.push(x);
        });
        console.log("Users_Con:", Users_Con, "Lista", data);
        GpsProvider.LISTA_CONECTADOS = Users_Con;

        console.groupEnd();
        this.events.publish("conectados:chat", Users_Con);
      });

      /*this.socket.on('disconnect', () => {
        console.log('Socket perdido');
        localStorage.clear();
        this._DestroySocket();
        setTimeout(function () {
          this.navCtrl.setRoot(LoginPage);
        }, 1000);
      });*/

    }
  }

  /*
  *
  *
  * */
  /**-- Destruir SOCKET --**/

  _DestroySocket() {
    GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
    GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
    if (GpsProvider.EMIT != undefined) {
      console.log(' Disconect Socket', GpsProvider.EMIT);
      GpsProvider.EMIT.disconnect();
      console.log('1 Disconect Socket', GpsProvider.EMIT);
    }

  }

  /** CERRAR GPS **/
  _CloseGPS() {
    console.group('CERRANDO EL EMIT');
    console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
    console.groupEnd();
    // GpsProvider.Band_Conexion=false;
    clearInterval(GpsProvider.intervalSocket);
    clearInterval(GpsProvider.antiFakeGPS);
    let D = clearInterval(GpsProvider.intervalSocket);
    GpsProvider.intervalSocket = 0;
    console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);

  }

  /*PROCESAR COLA*/

  procesarCola() {
    // console.log('1', this.alerta);
    const tm = setInterval(() => {
      // console.log('2', this.alerta, this.cola.length);
      if (this.alerta === false && this.cola.length > 0) {
        // console.log('ENTRO AL SET INTERVAL', this.cola);
        this.alerta = true;
        const data = {...this.cola[0]};
        // console.log('ENTRO AL SET INTERVAL', data);
        this.ViewPuntos(data);
        this.cola.splice(0, 1);
      }
      this.procesarCola();
      clearTimeout(tm);
    }, 2000);
  }


  /*FIN PROCESAR COLA*/

  /**------------------**/

  _emitMessage(receptor, message) {
    console.group('Enviando Mensage');
    console.log(message);
    console.groupEnd();
    //this.socket.emit('chat',{emisor:window.localStorage.getItem("idUserMUEVE"),receptor:292,mensaje:message});
    GpsProvider.EMIT.emit('chat', {
      emisor: window.localStorage.getItem("idUserMUEVE"),
      receptor: receptor,
      mensaje: message
    });
  }


  _Notification(message, title, sounds) {
    let number = Math.floor(Math.random() * 100) + 1;
    this._LNotifications.schedule([{
      id: number,
      title: title,
      text: message,
      sound: sounds,
      led: {color: '#29ff34', on: 500, off: 500},
      vibrate: true,
    }]);
  }

  DiagnosticGPS() {
    if (this.platform.is('cordova')) {
      //-->  console.log('DiagnosticGPS() corodva:', cordova)
      if (cordova)
        cordova.plugins.diagnostic.isGpsLocationEnabled(
          res => {
            //  console.log(res)
            if (res) {
              GpsProvider.switchToLocation = false;
            } else {
              if (!GpsProvider.switchToLocation) {
                GpsProvider.switchToLocation = true;
                this.Complementos._MessageToast('EL GPS NO esta Activo')

                let ArrayButtons = [{
                  text: 'OK', handler: () => {
                    cordova.plugins.diagnostic.switchToLocationSettings();
                    GpsProvider.switchToLocation = false;
                  }, cssClass: 'botton-green'
                }];

                this.Complementos.showAlert('Activa el GPS', '', "Activa el gps para seguir usando Mueve Logística",
                  ArrayButtons, 'alertConfig_2_css')

              }

            }

          },
          err => {
            console.error(err)

          })
    }

  }

  TurnOnGPS() {
    if (this.platform.is('cordova')) {
      if (cordova)
        cordova.plugins.locationAccuracy.request(this.SuccessTurnOnGPS, this.FailureTurnOnGPS, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
    }
  }

  SuccessTurnOnGPS(success) {
    console.log("Successfully requested accuracy: " + success.message);
  }

  FailureTurnOnGPS(error) {
    if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {
      if (window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")) {
        cordova.plugins.diagnostic.switchToLocationSettings();
      }


      // let ArrayButtons = [{text: 'OK',
      //     handler: () => {
      //         cordova.plugins.diagnostic.switchToLocationSettings();
      //        // GpsProvider.switchToLocation = false;
      //     }, cssClass: 'botton-green'}];
      //
      // let Message = `Error al establecer automáticamente el Modo de Ubicación en 'Alta Precisión'.
      //  ¿Desea cambiar a la página Configuración de ubicación y hacer esto manualmente?`;
      //
      // this.Complementos.showAlert('Alta Precisión','', Message,
      //     ArrayButtons,'alertConfig_2_css')
    }
  }


}
