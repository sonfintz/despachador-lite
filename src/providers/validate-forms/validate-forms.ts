import {Injectable} from '@angular/core';
import {AbstractControl} from "@angular/forms";
import {ApiProvider} from "../api/api";
import {Observable} from "rxjs/Observable";
import {ComplementosviewsProvider} from "../complementosviews/complementosviews";

@Injectable()
export class ValidateFormsProvider {

  constructor(private Api: ApiProvider,
              public  Complementos: ComplementosviewsProvider) {

  }

  public ValidateLogin(control: AbstractControl) {
    console.log('VALE LOGIN:', control.value);
    return new Promise(resolve => {
      this.Api._GET(`/transportista/login/${control.value.toString().toUpperCase()}`)

        .subscribe(
          data => {
            console.log(data);
            return (data.hasOwnProperty('existe') && data.existe == 'SI') ? resolve({loginTaken: true}) : resolve(null)
          },
          error => {
            console.error('ERROR EN VALIDACION:', error)
          },
          () => {
          }
        );
    });

  }

  public ValidateIsPlacaTrailer2(control: AbstractControl) {
    return Observable.create(observable => {
      console.log('validate trailer', control.value.length)
      // if (control.value.length == 6){
      if (!/^[R]{1}[0-9]{5}$/.test(control.value.toString().toUpperCase())) {
        console.log('trailer no valido', control.value)
        observable.next({notTrailer: "Trailer no valido"});
        observable.complete({notTrailer: "Trailer no valido"})
      } else {
        console.log('de lo contrario');
        observable.next()
        observable.complete()
      }
      // }else {
      //      console.log('XXXXXXXXXX')
      //      observable.next()
      //      observable.complete()
      //  }


    })
  }

  public ValidateIsPlacaTrailer(control: AbstractControl) {
    return new Promise(resolve => {
      console.log('validate trailer', control.value.length)
      // if (control.value.length == 6){
      if (!/^[R]{1}[0-9]{5}$/.test(control.value.toString().toUpperCase())) {
        console.log('trailer no valido', control.value)
        resolve({notTrailer: "Trailer no valido"})
      } else {
        console.log('de lo contrario');
        resolve(null)
      }
    });
  }

  public ValidatePlacaTrailerExist(control: AbstractControl) {
    return Observable.create(observable => {
      if (control.value.length >= 6) {
        this.Api._GET(`/trailer/placa/${control.value.toString().toUpperCase()}`).subscribe(
          data => {
            if (Object.keys(data).length > 0) {
              observable.next({PlacaExiste: "Esta placa ya esta registrada"})
              observable.complete()
            } else {
              observable.next()
              observable.complete()
            }
          },
          () => {
            observable.next({PlacaExiste: "A ocurrido un erro con el servidor no se pudo validar"})
            observable.complete()
          }
        );
      } else {
        observable.next()
        observable.complete()
      }
    });
  }

  public ValidatePinReferido(control: AbstractControl) {
    return Observable.create(observable => {
      console.log('Referido: ', control.value)
      if (control.value != null && control.value.length == 4) {
        this.Api._GET(`/transportista/verificar/pin/${control.value.toString().toUpperCase()}`).subscribe(
          data => {
            console.log(data);
            if (data.hasOwnProperty('existe')) {
              switch (data.existe) {
                case 'SI':
                  observable.next();
                  observable.complete();
                  break;
                case 'NO':
                  //control.setValue(null);
                  observable.next({errorPin: "Pin invalido"});
                  observable.complete();
                  break;
              }
            }

          },
          () => {
            observable.next({PlacaExiste: "A ocurrido un erro con el servidor no se pudo validar"})
            observable.complete()
          }
        );
      } else {
        observable.next()
        observable.complete()
      }
    });
  }

  public ValidateIdentification(name_input, control: AbstractControl) {
    console.log(name_input);
    return new Promise(resolve => {
      if (!/^([0-9])*$/.test(control.value)) {
        //--/ this.myForm.get(name_input[0]).setValue('');
        return resolve({inputTaken: true});
      } else {
        this.Api._GET(`/usuario/identificacion/${control.value}`)
          .subscribe(data => {
              console.log(data)
              return (data.hasOwnProperty('existe') && data.existe == 'SI') ? resolve({cedulaTaken: true}) : resolve(null);
            }, () => {
            },
            () => {
            });
      }
    });

  }

  public InputNumber(control: AbstractControl) {
    return new Promise(resolve => {
      if (control.value != null) {
        if (/^([0-9])*$/.test(control.value)) {
          resolve(null)
        } else {
          console.log({notNumber: true});
          control.reset()
          resolve({notNumber: true})
        }
      } else {
        resolve(null)
      }

    });
  }

  public LicenciaTransportista(control: AbstractControl) {

    return new Promise(resolve => {
      console.log('Control Value:', control.value, 'CONTROLL:', control);
      if ((control.value != null) && (control.value.length >= 3) && (control.value != '')) {
        this.Api._GET(`/transportista/licencia/${control.value}`)
          .subscribe(
            data => {
              console.log(data);
              return (data.hasOwnProperty('existe') && data.existe == 'SI') ? resolve({numlicenciaTaken: true}) : resolve(null);
            },
            () => {
            },
            () => {
            }
          );
      } else return resolve(null);
    });

  }

  public ValidateEmail(control: AbstractControl) {
    return new Promise(resolve => {
      if (/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i.test(control.value)) {
        this.Api._GET(`/transportista/email/${control.value}`)
          .subscribe(data => {
            console.log(data)
            return (data.hasOwnProperty('existe') && data.existe == 'SI') ? resolve({emailTaken: true}) : resolve(null);

          });
      } else {
        resolve({emailtypeError: 'No es un correo valido'})
      }


    });

  }

  //--> Este metodo recibe un array el la propiedad calback, la funcion deveria pasar en la posicion [0]
  public ConsultarPlacaServer(callback, control: AbstractControl) {
    let fcallback = callback[0];
    return new Promise(resolve => {
      if (control.value.length > 5) {
        // this.Complementos._PresentLoading();
        this.Api._GET(`/vehiculo/placa/${control.value}`)
          .subscribe(data => {
              console.log("ValidatePlaca:", data, callback)

              fcallback(data);
              resolve(null)
              //return (data.hasOwnProperty('existe') && data.existe == 'SI' ) ? resolve({placaTaken: true}) : resolve(null);


            }, error1 => {
              console.error(error1)
              resolve({errorServer: true});
              // this.Complementos._DismissPresentLoading()
            },
            () => {
              resolve(null)
              // this.Complementos._DismissPresentLoading()
            });
      } else {
        console.log()
        fcallback({});
        resolve(null)
      }


    });

  }

  public ValidarPeso(PesosArray, control: AbstractControl) {
    let pesoMenor = PesosArray[0];
    let pesoMaximo = PesosArray[1];

    return new Promise(resolve => {
      console.log(control.value, '> ', pesoMenor, ' && ', control.value, ' <=', pesoMaximo, '|', control.value > pesoMenor && control.value <= pesoMaximo)
      if (control.value >= pesoMenor && control.value <= pesoMaximo) {
        resolve(null)
      } else {
        resolve({errorPeso: "El peso no es valido."})
      }
    })
  }

  public CharacterSpecial(control: AbstractControl) {
    return Observable.create(observable => {
      if (/[.*+?¿%"@~`'<>#^${}()|[\]\\/]/g.test(control.value)) {
        console.log('Error caracateres especiales', control.value)
        observable.next({inputCharacter: true});
        observable.complete()
      } else {
        observable.next();
        observable.complete()
      }
    });
  }

  public CharacterSpecialAndNumber(control: AbstractControl) {
    return Observable.create(observable => {
      //--   /^[R]{1}[0-9]{5}$/
      if (/[.*+?¿%"&0-9@~`'<>#^${}()|[\]\\/]/g.test(control.value)) {
        console.log('Error caracateres especiales', control.value)
        observable.next({inputCharacter: true});
        observable.complete()
      } else {
        observable.next();
        observable.complete()
      }
    });
  }

  public LettersAlphabet(control: AbstractControl) {
    return Observable.create(observable => {
      //--   /^[R]{1}[0-9]{5}$/
      const pattern = new RegExp('^[A-Z \s]+$', 'i');
      if (!pattern.test(control.value)) {
        console.log('Error caracateres especiales', control.value)
        control.reset();
        observable.next({inputCharacter: true});
        observable.complete()
      } else {
        observable.next();
        observable.complete()
      }
    });
  }

  public LettersAlphabetAndSpecialCharDirections(control: AbstractControl) {
    return Observable.create(observable => {
      //--   /^[R]{1}[0-9]{5}$/
      const pattern = new RegExp('^[A-Z \s  #0-9\-]+$', 'i');
      if (!pattern.test(control.value)) {
        console.log('Error caracateres especiales', control.value)
        control.reset();
        observable.next({inputCharacter: true});
        observable.complete()
      } else {
        observable.next();
        observable.complete()
      }
    });
  }

}
