import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {AppModule} from "../../app/app.module";

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  private URL_BASE = AppModule.BASE_URL;

  get Token() {
    return localStorage.getItem('token') ? localStorage.getItem('token') : AppModule.My_Token;
  }

  get headers() {
    if (localStorage.getItem('token')) {
      return new HttpHeaders().append('Content-Type', 'application/json')
        .append('Accept', 'application/json')
        .append('Authorization', this.Token);
    } else {
      return new HttpHeaders().append('Content-Type', 'application/json')
        .append('Accept', 'application/json');
    }
  }

  get headers2() {
    return new HttpHeaders().append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
  }

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  _Login(U: any, Pss: any): Observable<any> {

    let params2 = "data=" + U + Pss;
    // --/console.log('params2',params2);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');


    return this.http.post(this.URL_BASE + '', params2, {headers: headers, observe: 'response'});

  }

  _GETMUNICIPIOS(endpoint: any): Observable<any> {
    //---/
    console.log(`${this.URL_BASE}${endpoint}`);

    return this.http.get(`${this.URL_BASE}${endpoint}`);
  }

  _GET(endpoint: any): Observable<any> {
    return this.http.get(this.URL_BASE + endpoint, {headers: this.headers});
  }

  GET(endpoint: any): Observable<any> {
    return this.http.get(this.URL_BASE + endpoint, {headers: this.headers2});
  }

  _POST(endpoint: any, params: any): Observable<any> {
    return this.http.post(this.URL_BASE + endpoint, params, {headers: this.headers});
  }

  _PUT(endpoint: any, params: any): Observable<any> {
    return this.http.put(this.URL_BASE + endpoint, params, {headers: this.headers});
  }


  _DELETE(endpoint: any): Observable<any> {
    return this.http.delete(this.URL_BASE + endpoint, {headers: this.headers});
  }
}
