var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppModule } from "../../app/app.module";
/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let ApiProvider = class ApiProvider {
    constructor(http) {
        this.http = http;
        this.URL_BASE = AppModule.BASE_URL;
        console.log('Hello ApiProvider Provider');
    }
    get Token() {
        return localStorage.getItem('token') ? localStorage.getItem('token') : AppModule.My_Token;
    }
    get headers() {
        if (localStorage.getItem('token')) {
            return new HttpHeaders().append('Content-Type', 'application/json')
                .append('Accept', 'application/json')
                .append('Authorization', this.Token);
        }
        else {
            return new HttpHeaders().append('Content-Type', 'application/json')
                .append('Accept', 'application/json');
        }
    }
    get headers2() {
        return new HttpHeaders().append('Content-Type', 'application/json')
            .append('Accept', 'application/json');
    }
    _Login(U, Pss) {
        let params2 = "data=" + U + Pss;
        // --/console.log('params2',params2);
        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(this.URL_BASE + '', params2, { headers: headers, observe: 'response' });
    }
    _GETMUNICIPIOS(endpoint) {
        //---/
        console.log(`${this.URL_BASE}${endpoint}`);
        return this.http.get(`${this.URL_BASE}${endpoint}`);
    }
    _GET(endpoint) {
        return this.http.get(this.URL_BASE + endpoint, { headers: this.headers });
    }
    GET(endpoint) {
        return this.http.get(this.URL_BASE + endpoint, { headers: this.headers2 });
    }
    _POST(endpoint, params) {
        return this.http.post(this.URL_BASE + endpoint, params, { headers: this.headers });
    }
    _PUT(endpoint, params) {
        return this.http.put(this.URL_BASE + endpoint, params, { headers: this.headers });
    }
    _DELETE(endpoint) {
        return this.http.delete(this.URL_BASE + endpoint, { headers: this.headers });
    }
};
ApiProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [HttpClient])
], ApiProvider);
export { ApiProvider };
//# sourceMappingURL=api.js.map