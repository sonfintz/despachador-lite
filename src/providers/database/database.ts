import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {Observable} from "rxjs/Observable";

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  static db: SQLiteObject;
  static isOpen: boolean;

  constructor(
    public http: HttpClient,
    public storage: SQLite) {
  }


  /*CREAR TABLA SQLITE DATABASE*/
  _CreateTable() {
    if (!DatabaseProvider.isOpen) {
      this.storage = new SQLite();
      this.storage.create({name: "mueve_despachadores_v5.db", location: "default"}).then((db: SQLiteObject) => {
        DatabaseProvider.db = db;
        db.executeSql(`CREATE TABLE IF NOT EXISTS notification_settings (
        design_codigo INT,
        design_tipo TEXT,
        design_titulo TEXT,
        design_perfil TEXT,
        design_descripcion TEXT,
        design_mostrar TEXT,
        design_moneda TEXT,
        design_operacion TEXT,
        actualizado DATETIME,
        design_descripcion_confirmacion TEXT,
        design_titulo_confirmacion TEXT,
        design_puntos INT,
        design_detalle TEXT)`, []);
        db.executeSql("CREATE UNIQUE INDEX idx_positions_title ON notification_settings (design_codigo)", []);//Creando indice unico para la tabla notification_settings campo design_codigo
        DatabaseProvider.isOpen = true;
        console.info('Base de datos creada', DatabaseProvider.db, '|isOpen:', DatabaseProvider.isOpen);
      }).catch((err) => {
        console.log('Error Tabla trace_gps', err);
      });
    }
  }

  /*LISTAR DATA SQLITE*/
  listData_table(table) {
    return Observable.create(observer => {
      let ResValue = Array();
      if (DatabaseProvider.isOpen) {
        DatabaseProvider.db.executeSql(`SELECT *  FROM ${table} `, [])
          .then(data => {
            for (let i = 0; i < data.rows.length; i++) {
              ResValue.push(data.rows.item(i))
            }
            observer.next(ResValue)
          })
      }

    })
  }


  /*INSERT INTO DATABASE SQL*/
  insertData_table(name_table: string, Fields: string, valuesArray: any, VALUES_PREPARATE = '?') {
    return Observable.create(observer => {
      if (DatabaseProvider.isOpen) {
        DatabaseProvider.db.executeSql(`INSERT INTO ${name_table} (${Fields}) VALUES(${VALUES_PREPARATE})`, valuesArray)
          .then(data => {
            observer.next(data)
          }, error => {
            observer.error(error)
          })
      }
    });

  }


  /*FUNCIONES VIEJAS*/
  _listNotificaciones() {
    return new Promise((resolve, reject) => {
      if (DatabaseProvider.isOpen) {
        DatabaseProvider.db.executeSql("SELECT * FROM Notifications_settings", []).then((data) => {
          let arrayNOTIFICACIONES = [];
          console.log('LA DATA NOTIFICACIONES DEL SQLITE', data);
          //console.log('Data',data,'Filas:',data.rows);
          if (data.rows.length > 0) {
            console.log('Si hay data', data);
            for (let i = 0; i < data.rows.length; i++) {
              arrayNOTIFICACIONES.push(data);
            }
          } else {
            console.log('No hay data');
          }
          resolve(arrayNOTIFICACIONES);
        }, (error) => {
          reject(error);
        });
      }
      else {
        reject('Base de Datos No disponible insertar');
      }
    });
  }

  /** ------------------------------
   *  CRUD FAVORITES SOLICITUDES
   */
  insertSolicitudFavorite(solicitud: any, id_user: number) {

    return new Promise((resolve, reject) => {
      if (DatabaseProvider.isOpen) {
        let sql = "INSERT INTO s_favorite (solicitud, id_user) VALUES (?,?)";
        DatabaseProvider.db.executeSql(sql, [solicitud, id_user]).then((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
      } else {
        reject('Base de Datos No disponible List');
      }
    });
  }

  listSolicitudFavorite(id_user: number) {

    return new Promise((resolve, reject) => {
      if (DatabaseProvider.isOpen) {
        DatabaseProvider.db.executeSql("SELECT * FROM s_favorite WHERE id_user=?", [id_user]).then((data) => {
          let SFavorites = [];
          //console.log('Data',data,'Filas:',data.rows);
          if (data.rows.length > 0) {
            for (let i = 0; i < data.rows.length; i++) {
              let obj = {
                id: data.rows.item(i).id,
                solicitud: data.rows.item(i).solicitud,
                id_user: data.rows.item(i).id_user
              };
              SFavorites.push(obj);
            }
          }

          resolve(SFavorites);
        }, (error) => {
          reject(error);
        });
      }
      else {
        reject('Base de Datos No disponible Listar Solicitudes Favoritas');
      }
    });
  }

  DeleteFavorites(id, id_user) {
    if (DatabaseProvider.isOpen) {
      return new Promise((resolve, reject) => {
        DatabaseProvider.db.executeSql("DELETE FROM s_favorite WHERE id=? AND id_user=?", [id, id_user]).then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        })
      });
    }
  }

  DeleteAllFavorites(id_user) {
    if (DatabaseProvider.isOpen) {
      return new Promise((resolve, reject) => {
        DatabaseProvider.db.executeSql("DELETE FROM s_favorite WHERE id_user=?", [id_user]).then((data) => {
          resolve(data);
        }, (err) => {
          reject(err);
        })
      });
    }
  }

  insertData_tableOR_REPLACE(name_table: string, Fields: string, valuesArray: any, VALUES_PREPARATE = '?') {
    return Observable.create(observer => {
      if (DatabaseProvider.isOpen) {
        DatabaseProvider.db.executeSql(`INSERT OR REPLACE INTO ${name_table} (${Fields}) VALUES(${VALUES_PREPARATE})`, valuesArray)
          .then(data => {
            observer.next(data)
          }, error => {
            observer.error(error)
          })
      }
    });

  }

}
