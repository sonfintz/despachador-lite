var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite } from "@ionic-native/sqlite";
import { Observable } from "rxjs/Observable";
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let DatabaseProvider = DatabaseProvider_1 = class DatabaseProvider {
    constructor(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    /*CREAR TABLA SQLITE DATABASE*/
    _CreateTable() {
        if (!DatabaseProvider_1.isOpen) {
            this.storage = new SQLite();
            this.storage.create({ name: "mueve_despachadores_v5.db", location: "default" }).then((db) => {
                DatabaseProvider_1.db = db;
                db.executeSql(`CREATE TABLE IF NOT EXISTS notification_settings (
        design_codigo INT,
        design_tipo TEXT,
        design_titulo TEXT,
        design_perfil TEXT,
        design_descripcion TEXT,
        design_mostrar TEXT,
        design_moneda TEXT,
        design_operacion TEXT,
        actualizado DATETIME,
        design_descripcion_confirmacion TEXT,
        design_titulo_confirmacion TEXT,
        design_puntos INT,
        design_detalle TEXT)`, []);
                db.executeSql("CREATE UNIQUE INDEX idx_positions_title ON notification_settings (design_codigo)", []); //Creando indice unico para la tabla notification_settings campo design_codigo
                DatabaseProvider_1.isOpen = true;
                console.info('Base de datos creada', DatabaseProvider_1.db, '|isOpen:', DatabaseProvider_1.isOpen);
            }).catch((err) => {
                console.log('Error Tabla trace_gps', err);
            });
        }
    }
    /*LISTAR DATA SQLITE*/
    listData_table(table) {
        return Observable.create(observer => {
            let ResValue = Array();
            if (DatabaseProvider_1.isOpen) {
                DatabaseProvider_1.db.executeSql(`SELECT *  FROM ${table} `, [])
                    .then(data => {
                    for (let i = 0; i < data.rows.length; i++) {
                        ResValue.push(data.rows.item(i));
                    }
                    observer.next(ResValue);
                });
            }
        });
    }
    /*INSERT INTO DATABASE SQL*/
    insertData_table(name_table, Fields, valuesArray, VALUES_PREPARATE = '?') {
        return Observable.create(observer => {
            if (DatabaseProvider_1.isOpen) {
                DatabaseProvider_1.db.executeSql(`INSERT INTO ${name_table} (${Fields}) VALUES(${VALUES_PREPARATE})`, valuesArray)
                    .then(data => {
                    observer.next(data);
                }, error => {
                    observer.error(error);
                });
            }
        });
    }
    /*FUNCIONES VIEJAS*/
    _listNotificaciones() {
        return new Promise((resolve, reject) => {
            if (DatabaseProvider_1.isOpen) {
                DatabaseProvider_1.db.executeSql("SELECT * FROM Notifications_settings", []).then((data) => {
                    let arrayNOTIFICACIONES = [];
                    console.log('LA DATA NOTIFICACIONES DEL SQLITE', data);
                    //console.log('Data',data,'Filas:',data.rows);
                    if (data.rows.length > 0) {
                        console.log('Si hay data', data);
                        for (let i = 0; i < data.rows.length; i++) {
                            arrayNOTIFICACIONES.push(data);
                        }
                    }
                    else {
                        console.log('No hay data');
                    }
                    resolve(arrayNOTIFICACIONES);
                }, (error) => {
                    reject(error);
                });
            }
            else {
                reject('Base de Datos No disponible insertar');
            }
        });
    }
    /** ------------------------------
     *  CRUD FAVORITES SOLICITUDES
     */
    insertSolicitudFavorite(solicitud, id_user) {
        return new Promise((resolve, reject) => {
            if (DatabaseProvider_1.isOpen) {
                let sql = "INSERT INTO s_favorite (solicitud, id_user) VALUES (?,?)";
                DatabaseProvider_1.db.executeSql(sql, [solicitud, id_user]).then((data) => {
                    resolve(data);
                }, (error) => {
                    reject(error);
                });
            }
            else {
                reject('Base de Datos No disponible List');
            }
        });
    }
    listSolicitudFavorite(id_user) {
        return new Promise((resolve, reject) => {
            if (DatabaseProvider_1.isOpen) {
                DatabaseProvider_1.db.executeSql("SELECT * FROM s_favorite WHERE id_user=?", [id_user]).then((data) => {
                    let SFavorites = [];
                    //console.log('Data',data,'Filas:',data.rows);
                    if (data.rows.length > 0) {
                        for (let i = 0; i < data.rows.length; i++) {
                            let obj = {
                                id: data.rows.item(i).id,
                                solicitud: data.rows.item(i).solicitud,
                                id_user: data.rows.item(i).id_user
                            };
                            SFavorites.push(obj);
                        }
                    }
                    resolve(SFavorites);
                }, (error) => {
                    reject(error);
                });
            }
            else {
                reject('Base de Datos No disponible Listar Solicitudes Favoritas');
            }
        });
    }
    DeleteFavorites(id, id_user) {
        if (DatabaseProvider_1.isOpen) {
            return new Promise((resolve, reject) => {
                DatabaseProvider_1.db.executeSql("DELETE FROM s_favorite WHERE id=? AND id_user=?", [id, id_user]).then((data) => {
                    resolve(data);
                }, (err) => {
                    reject(err);
                });
            });
        }
    }
    DeleteAllFavorites(id_user) {
        if (DatabaseProvider_1.isOpen) {
            return new Promise((resolve, reject) => {
                DatabaseProvider_1.db.executeSql("DELETE FROM s_favorite WHERE id_user=?", [id_user]).then((data) => {
                    resolve(data);
                }, (err) => {
                    reject(err);
                });
            });
        }
    }
    insertData_tableOR_REPLACE(name_table, Fields, valuesArray, VALUES_PREPARATE = '?') {
        return Observable.create(observer => {
            if (DatabaseProvider_1.isOpen) {
                DatabaseProvider_1.db.executeSql(`INSERT OR REPLACE INTO ${name_table} (${Fields}) VALUES(${VALUES_PREPARATE})`, valuesArray)
                    .then(data => {
                    observer.next(data);
                }, error => {
                    observer.error(error);
                });
            }
        });
    }
};
DatabaseProvider = DatabaseProvider_1 = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [HttpClient,
        SQLite])
], DatabaseProvider);
export { DatabaseProvider };
var DatabaseProvider_1;
//# sourceMappingURL=database.js.map