import {DataglobalProvider} from "./dataglobal/dataglobal";

export class LinksRedirect {
  goRoute(codigo) {
    let ruta;
    ruta = DataglobalProvider.Links.find((i) => {
      if (i.referencia === codigo) {
        return i;
      } else {
        return null
      }
    });
    if (ruta)
      window.open(`${ruta.link}`, '_system');
  }
}
