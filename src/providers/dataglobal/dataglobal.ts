import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {Events} from "ionic-angular";

/*
  Generated class for the DataglobalProvider provider.
  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataglobalProvider {
  static data_combustible: any;
  static data_carroceria: any;
  static data_color: any;
  static data_marca: any;
  static data_unidadmedida: any;
  static data_tipotrailer: any;
  static TIPO_VEHICULO: Array<any> = Array();
  static TIPO_CARGA: any;
  static Objs: any;
  static Perfil: any;
  static reciente: any;
  static dataPerfil: any;
  static notificacion: number = 0;
  static historial: Array<any> = Array();
  static ImagenPerfil: any;
  static NotificacionesSettings: any;
  static ultimUuid: string;
  static ultimUuid2: string;
  static VersionApp = 'v 0.9.2';
  static Localizacion: any;
  static Verificado: string;
  static Links = [];

  constructor(public Api: ApiProvider,
              public events: Events,) {
    this.SetSubscribeEvents();
  }

  /*---------------------------------
  -------EVENTOS SUBSCRIBE-----------
  ----------------------------------*/
  public SetSubscribeEvents() {
    console.log(/*SESION-USER------------------------------------------*/)
    this.events.subscribe('user:sessions', () => {
      console.log('///----> USER INICIADO SET DATA GLOBAL');
      // this.Color();
      this._Perfil();
    });

    /*LINKS*/
    this.events.subscribe('links', (data) => {
      console.log('SUBSCRITO LINK');
      console.log(data);
      this._Links();
    });

    /*RECARGAR DATOS DE PERFIL---------------------------------*/
    this.events.subscribe('perfilreload', (data) => {
      console.log(data);
      this._Perfil();
    });

    /*NOTIFICACIONES--------------------------------------------------*/
    this.events.subscribe('notificaciones', (data) => {
      if (Array.isArray(data) == false) {
        if (DataglobalProvider.ultimUuid !== data.uuid) {
          console.log('SUMANDO NOTIFICACIONES');
          DataglobalProvider.notificacion = DataglobalProvider.notificacion + 1;
          /*RECARGANDO DATA PERFIL*/
          this.events.publish('perfilreload', true);
          /*RECARGANDO DATA OFERTAS*/
          this.events.publish('reloadofertas', true);
          DataglobalProvider.ultimUuid = data.uuid
        }
      }
    });

    /*LIMPIAR NOTIFICACIONES*/
    this.events.subscribe('clear-notificaciones', (data) => {
      console.log(data);
      DataglobalProvider.notificacion = 0;
    });

    this.events.subscribe('imagen-upload', (data) => {
      console.log(data);
      this._Perfil();
    });
  }


  mainData() {
    console.log('-----------------------------------');
    console.log('-------CARGANDO DATA GLOBAL--------');
    console.log('-----------------------------------');
    this.activity();
  }

  _Links() {
    this.Api._GET('/links/perfil/3').subscribe((links) => {
      DataglobalProvider.Links = links;
      console.log('LINKS', links);
    })
  }

  /*COLOR*/
  Color() {
    this.Api._GET('/color').subscribe(color => {
        DataglobalProvider.data_color = color;
      },
      error => {
        console.group("PETICION DE COLOR");
        console.log(error);
        console.groupEnd()
      },
      () => {
      });
  }

  /*COMBUSTIBLE*/
  Combustible() {
    this.Api._GET('/combustible').subscribe(combustible => {
      DataglobalProvider.data_combustible = combustible;
    });
  }

  /*CARROCERIA*/
  Carroceria() {
    this.Api._GET('/carroceria').subscribe(carroceria => {
      DataglobalProvider.data_carroceria = carroceria;
    });
  }

  /*MARCA*/
  Marca() {
    this.Api._GET('/marca').subscribe(marca => {
      DataglobalProvider.data_marca = marca;
    });
  }

  /*UNIDAD MEDIDA*/
  UnidadMedida() {
    this.Api._GET('/unidadmedida').subscribe(unidad => {
      DataglobalProvider.data_unidadmedida = unidad;
    });
  }

  /*TIPO VEHICULO*/
  _TipoVehiculo() {
    this.Api._GET('/tipovehiculo').subscribe((data) => {

      DataglobalProvider.TIPO_VEHICULO = data;

    }, () => {
    }, () => {
      console.log('/tipovehiculo', DataglobalProvider.TIPO_VEHICULO)
    });
  }

  /*TIPO CARGA*/
  _TipoCarga() {
    this.Api._GET('/tipocarga').subscribe((data) => {

      DataglobalProvider.TIPO_CARGA = data;

    }, () => {
    }, () => {
      console.log('/tipocarga', DataglobalProvider.TIPO_CARGA)
    });
  }

  /*MUNICIPIO*/
  _Municipio() {
    this.Api._GET('/municipio').subscribe((data) => {

      DataglobalProvider.Objs = data;

    }, () => {
    }, () => {
    });
  }

  /*PERFIL*/
  _Perfil() {
    let aux = 0;
    console.log('PERFIL GET', aux++);
    /*DATA PARA EL MODULO PERFIL----------------*/
    this.Api._GET(`/despachador/${localStorage.getItem('idUserMUEVE')}`).subscribe(
      (data) => {
        if (data) {
          DataglobalProvider.Perfil = {
            calificaciones: data.reputacion.calificaciones,
            categoria: data.reputacion.categoria,
            despachador: data.reputacion.despachador,
            despachos: data.reputacion.despachos,
            empresa: data.reputacion.empresa,
            puntos: data.reputacion.puntos,
            reputacion: data.reputacion.reputacion,
            telefono: data.reputacion.telefono,
            tiempo: data.reputacion.tiempo
          };
          this.events.publish('data-perfil', DataglobalProvider.Perfil);

          console.log('data del perfil', data);
          DataglobalProvider.dataPerfil = data;
          /*IMAGEN DE PERFIL*/
          DataglobalProvider.ImagenPerfil = data.img;

          DataglobalProvider.Verificado = data.usuario_verificado;
        }
      }, (err) => {
        console.group("PETICION DE PERFIL");
        console.log(err);
        console.groupEnd()
      });

    // this.ImagenPerfil();
  }

  /*NOTIFICACIONES*/
  activity() {
    this.Api._GET('/actividad').subscribe((data = Array()) => {
      data.map(i => {
        let render = this.ShowRenderView(i.ref);
        return Object.assign(i, render)
      });
      DataglobalProvider.historial = [];
      DataglobalProvider.reciente = data[0];
      console.log(data[0]);
      if (data.length > 1) {
        for (let h = 1; h < data.length; h++) {
          DataglobalProvider.historial.push(data[h]);
        }
        console.log(DataglobalProvider.historial);
        console.log(DataglobalProvider.reciente);
      }
    })
  }

  ShowRenderView(ref) {
    const Valor = DataglobalProvider.NotificacionesSettings.find(i => i.design_codigo === ref);
    switch (Valor.design_tipo.toLowerCase()) {
      case `logro`:
        Valor.image = `assets/imgs/icon/logro_desbloqueadoldpi.svg`;
        break;
      case `reto mp`:
        Valor.image = `assets/imgs/icon/reto_v2.svg`;
        break;
      case  `bonus mp`:
        Valor.image = `assets/imgs/icon/bonus_v2.svg`;
        break;
      case `mueve informa`:
        Valor.image = `assets/imgs/icon/${this.ImgNotificacion(Valor.design_codigo)}`;
        break
    }
    return Valor
  }

  ImgNotificacion(data) {
    let label: string;
    switch (data) {
      case 1:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 2:
        label = "solicitud_cuenta_validacionldpi.svg";
        break;
      case 3:
        label = "oferta_eliminada.svg";
        break;
      case 4:
        label = "viaje_canceladoldpi.svg";
        break;
      case 5:
        label = "oferta_creadaldpi.svg";
        break;
      case 6:
        label = "transportista_aceptado_verdeldpi.svg";
        break;
      case 7:
        label = "vehiculo_viaje_culminadoldpi.svg";
        break;
      case 11:
        label = "vehiculo_cargado_importanteldpi.svg";
        break;
      case 12:
        label = "vehiculo_en_rutaldpi.svg";
        break;
      case 13:
        label = "vehiculo_cargado_importanteldpi.svg";
        break;
      case 14:
        label = "oferta_a_vencerldpi.svg";
        break;
      case 15:
        label = "nuevo_transportista_postuladoldpi.svg";
        break;
      case 27:
        label = "rango_plata_subisteldpi.svg";
        break;
      case 32:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 33:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 36:
        label = "subiste_rango_doradoldpi.svg";
        break;
      case 39:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 40:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 41:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 52:
        label = "nuevo_transportista_postuladoldpi.svg";
        break;
      case 59:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 60:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 61:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 62:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 63:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 64:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 65:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 66:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 67:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 68:
        label = "reto_v2.svg";
        break;
      case 69:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 70:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 71:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 72:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 73:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 74:
        label = "logro_desbloqueadoldpi.svg";
        break;
      case 75:
        label = "viaje_calificado_excelenteldpi.svg";
        break;
      case 76:
        label = "viaje_calificado_buenoldpi.svg";
        break;
      case 77:
        label = "viaje_calificado_aceptableldpi.svg";
        break;
      case 78:
        label = "viaje_calificado_regularldpi.svg";
        break;
      case 91:
        label = "bonus_v2.svg";
        break;
      case 92:
        label = "reto_v2.svg";
        break;
      case 93:
        label = "reto_v2.svg";
        break;
      case 94:
        label = "reto_v2.svg";
        break;
      case 95:
        label = "reto_v2.svg";
        break;
      case 96:
        label = "reto_v2.svg";
        break;
      case 97:
        label = "reto_v2.svg";
        break;
      case 98:
        label = "reto_v2.svg";
        break;
      case 99:
        label = "reto_v2.svg";
        break;
      case 100:
        label = "bonus_v2.svg";
        break;
      case 101:
        label = "bonus_v2.svg";
        break;
      case 102:
        label = "alerta_importantesldpi.svg";
        break;
      case 103:
        label = "alerta_importantesldpi.svg";
        break;
      case 104:
        label = "alerta_importantesldpi.svg";
        break;
      case 105:
        label = "alerta_importantesldpi.svg";
        break;
      case 106:
        label = "alerta_importantesldpi.svg";
        break;
      case 114:
        label = "vehiculo_declino_viajeldpi.svg";
        break;
      case 115:
        label = "oferta_abandonadaldpi.svg";
        break;
      case 116:
        label = "alerta_importantesldpi.svg";
        break;
      case 119:
        label = "oferta_eliminada.svg";
        break;
    }
    return label;
  }

  /*FIN NOTIFICACIONES*/

}
