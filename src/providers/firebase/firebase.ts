import {Injectable} from '@angular/core';
import {ConferenceApp} from "../../app/app.component";
import {AlertController, Events, ToastController} from "ionic-angular";
import {GpsProvider} from "../gps/gps";

declare let FCMPlugin;

/*
  Servicio para controlar las acciones de firebase
*/
@Injectable()
export class FirebaseProvider {

  constructor(
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public alertController: AlertController,
    public events: Events,
  ) {
    console.log('Hello FirebaseProvider Provider');
  }

  setPluginFCM() {

    let check_fcm_loaded: any;

    //_  console.log('| ---> setPluginFCM()', typeof check_fcm_loaded === 'undefined')

    if (typeof check_fcm_loaded === 'undefined') {
      check_fcm_loaded = setInterval(() => {
        //_    console.log('Cargando el plugin', typeof FCMPlugin)
        if (typeof FCMPlugin !== 'undefined') {
          console.log(FCMPlugin);
          this.MainFirebase();
          clearInterval(check_fcm_loaded);
        }
      }, 2000);
    }
  }

  WrapPromise(fn: Function) {
    return new Promise((resolve, reject) => {
      fn(resolve, reject);
    })
  }

  async MainFirebase() {
    let Toast = this.toastCtrl;
    let Alert = this.alertCtrl;

    ConferenceApp.FCM = FCMPlugin;

    await this.WrapPromise(FCMPlugin.getToken).then(
      (t) => {
        console.group('OBTENIENDO TOKEN FIREBASE FCM');
        console.log('GET TOKEN FCM:', t);
        console.groupEnd()
      },
      (e) => {
        console.group('OBTENIENDO TOKEN FIREBASE FCM');
        console.log('ERR TOKEN FCM:', e);
        console.groupEnd()

      }
    );

    // FCMPlugin.getToken(
    //   (t) => {
    //     console.group('OBTENIENDO TOKEN FIREBASE FCM');
    //     console.log('GET TOKEN FCM:', t);
    //     console.groupEnd()
    //   },
    //   (e) => {
    //     console.group('OBTENIENDO TOKEN FIREBASE FCM');
    //     console.log('ERR TOKEN FCM:', e);
    //     console.groupEnd()
    //
    //   }
    // );

    /*FCMPlugin.onTokenRefresh((token) => {
      console.group('REFRESCANDO OBTENIENDO TOKEN FIREBASE FCM')
      console.log('GET TOKEN FCM:', token);
      console.groupEnd()
    });*/

    console.log('UserSesionId', localStorage.getItem('idUserMUEVE'));
    console.log(Boolean(FCMPlugin.subscribeToTopic(`notificacion${localStorage.getItem('idUserMUEVE')}`)));
    FCMPlugin.subscribeToTopic(`notificacion${localStorage.getItem('idUserMUEVE')}`);

    FCMPlugin.onNotification((data) => {
      try {
        console.log('--> Data recibida de firebase: ', JSON.stringify(data));
      } catch (e) {
        console.error('Errro al parcear data firebase')
      }

      if (data.wasTapped) {
        console.log(data.wasTapped);
        this.Options(data);
        // --/console.log('If WasTapped: ',data);
        /**  AELERT PARA ENTURNADOR **/
        // if (data.enturnarse) {
        //     let alert = Alert.create({
        //         title: data.title,
        //         message: data.body,
        //         buttons: [
        //             {
        //                 text: 'CANCELAR',
        //                 handler: () => {
        //                     // --/console.log('Cancelado')
        //                 }
        //             },
        //             {
        //                 text: 'DISPONIBLE',
        //                 handler: () => {
        //                     console.log('disponible conductor');
        //                     this.Available_For_Request(data.enturnarse);
        //                    //__/  this.events.publish('conductor:disponible', {op: true, id: data.param2});
        //                 }
        //             }
        //         ]
        //     });
        //     alert.present();
        // }

        /**-----------------
         *  FIN DEL TURNO   |
         *  **/
        if (Object.keys(data).indexOf("termino") > 0) {
          this.events.publish('conductor:carga_finalizada', {op: true, id: data.param2});
          let alert = Alert.create({
            title: data.title,
            message: data.body,
            buttons: [
              {
                text: 'CANCELAR',
                handler: () => {
                  this.events.publish('conductor:carga_finalizada', {
                    op: true,
                    id: data.param2
                  });
                }
              },
              {
                text: 'DISPONIBLE',
                handler: () => {
                  // --/console.log('disponible conductor');
                  this.events.publish('conductor:carga_finalizada', {
                    op: true,
                    id: data.param2
                  });
                }
              }
            ]
          });
          alert.present();
        }

      } else {
        /** SI LA NOTIFICACION LLEGO Y EL MOVIL ESTA EN PRIMER PLANO **/
        this.Options(data);
        // --/console.log('No WasTapped: ',data);
        // if (data.enturnarse) {
        //     let alert = Alert.create({
        //         title: data.title,
        //         message: data.body,
        //         buttons: [
        //             {
        //                 text: 'CANCELAR',
        //                 handler: () => {
        //                     // --/console.log('Cancelado')
        //                 }
        //             },
        //             {
        //                 text: 'DISPONIBLE',
        //                 handler: () => {
        //                     // --/console.log('disponible conductor');
        //                     this.Available_For_Request(data.enturnarse);
        //                     this.events.publish('conductor:disponible', {op: true, id: data.param2});
        //                 }
        //             }
        //         ]
        //     });
        //     alert.present();
        // }
        /**-----------------
         *  FIN DEL TURNO   |
         *  **/
        if (Object.keys(data).indexOf("termino") > 0) {
          this.events.publish('conductor:carga_finalizada', {op: true, id: data.param2});
          let alert = Alert.create({
            title: data.title,
            message: data.body,
            buttons: [
              {
                text: 'CANCELAR',
                handler: () => {
                  this.events.publish('conductor:carga_finalizada', {
                    op: true,
                    id: data.param2
                  });
                }
              },
              {
                text: 'DISPONIBLE',
                handler: () => {
                  // --/console.log('disponible conductor');
                  this.events.publish('conductor:carga_finalizada', {
                    op: true,
                    id: data.param2
                  });
                }
              }
            ]
          });
          alert.present();
        }


        let toast = Toast.create({
          message: data.body,
          duration: 5000,
          position: 'top'
        });

        toast.present().then(() => {
        });
      }
    });
  }


  Available_For_Request(val) {
    GpsProvider.EMIT.emit('repuesta_transportista', {
      usuario_id: localStorage.getItem("idUserMUEVE"),
      latitud: (GpsProvider.LATITUDE) || window.localStorage.getItem('MyLatitude'),
      longitud: (GpsProvider.LONGITUDE) || window.localStorage.getItem('MyLongitude'),
      enturnarse: val
    });
  }


  Options(data) {
    console.log(data);
    // let Toast = this.toastCtrl;
    let Alert = this.alertCtrl;

    if (data.enturnarse) {
      let alert = Alert.create({
        title: 'Despachador',
        subTitle: data.title,
        message: data.body,
        buttons: [
          {
            text: 'CANCELAR',
            handler: () => {
              // --/console.log('Cancelado')
            }
          },
          {
            text: 'DISPONIBLE',
            handler: () => {
              console.log('disponible conductor');
              this.Available_For_Request(data.enturnarse);
              //__/  this.events.publish('conductor:disponible', {op: true, id: data.param2});
            }
          }
        ]
      });
      alert.present();
    }

    if (data.nueva_oferta) {

    }


    if (data.tipo) {
      const tipo = data.tipo;
      switch (tipo) {
        case 'default' :
          // this.Complements.showAlert(`${data.title}`, '', `${data.body}`, btn, 'alertCustomCss2');
          const alert = this.alertController.create({
            title: `${data.title}`,
            message: `${data.body}`,
            buttons: [
              {
                text: '¡Entendido!',
                cssClass: 'botton3',
                handler: () => {
                }
              }
            ],
            cssClass: 'alertCustomCss5' // <- added this
          });
          alert.present();
          break
      }
    }
  }
}
