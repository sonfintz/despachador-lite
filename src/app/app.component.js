var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform, ToastController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { AppModule } from "./app.module";
import { ComplementosviewsProvider } from "../providers/complementosviews/complementosviews";
import { DatabaseProvider } from "../providers/database/database";
import { GpsProvider } from "../providers/gps/gps";
import { PerfilPage } from "../pages/perfil/perfil";
import { SolicitudesPage } from "../pages/solicitudes/solicitudes";
import { TransportistasListPage } from "../pages/transportistas-list/transportistas-list";
import { timer } from "rxjs/observable/timer";
import { ListaOfertasPage } from "../pages/lista-ofertas/lista-ofertas";
import { TutorialPage } from "../pages/tutorial/tutorial";
import { TransportistasfavoritosPage } from "../pages/transportistasfavoritos/transportistasfavoritos";
import { MueveputosPage } from "../pages/mueveputos/mueveputos";
import { DataglobalProvider } from "../providers/dataglobal/dataglobal";
import { AjustesAppPage } from "../pages/ajustes-app/ajustes-app";
import { FirebaseProvider } from "../providers/firebase/firebase";
import { SynchronizationDatabaseProvider } from "../providers/synchronization-database/synchronization-database";
import { HeaderColor } from "@ionic-native/header-color";
import { StatusBar } from '@ionic-native/status-bar';
import { DescubrePage } from "../pages/descubre/descubre";
import { ApiProvider } from "../providers/api/api";
let ConferenceApp = class ConferenceApp {
    // List of pages that can be navigated to from the left menu
    // the left menu only works after login
    // the login page disables the left menu
    constructor(platform, splashScreen, events, userData, menu, serviceDB, confData, storage, firebaseService, toastCtrl, dataGlobal, headerColor, Api, Complementos, Synchronization, statusBar, menuCtrl) {
        this.events = events;
        this.userData = userData;
        this.menu = menu;
        this.serviceDB = serviceDB;
        this.confData = confData;
        this.storage = storage;
        this.firebaseService = firebaseService;
        this.toastCtrl = toastCtrl;
        this.dataGlobal = dataGlobal;
        this.headerColor = headerColor;
        this.Api = Api;
        this.Complementos = Complementos;
        this.Synchronization = Synchronization;
        this.statusBar = statusBar;
        this.menuCtrl = menuCtrl;
        this.rootPage = TutorialPage;
        this.showSplash = true;
        this.menuCtrl.swipeEnable(true, 'leftMenu');
        this.events.subscribe('Logout', () => {
            // console.log(data);
            this._LogOut();
        });
        this.events.subscribe('data-perfil', (data) => {
            // console.log('data Menu perfil', data);
            this.dataPerfil = data;
        });
        this.events.subscribe('tipoUsuario', (data) => {
            if (data === 'FORANEO') {
                // console.log(data);
                this.Ppages = [
                    // {title: 'Solicitudes', component: SolicitudesPage, icon: 'git-pull-request', color: 'azul_claro', push: false},
                    {
                        title: 'Ofertas',
                        component: ListaOfertasPage,
                        color: 'azul_claro',
                        icon: 'kargoo-menu_ofertas',
                        push: false,
                        img: 'menu_ofertas.svg',
                        subtitle: 'Encuentra transportistas'
                    },
                    // {
                    //   title: 'Coord. Logístico.',
                    //   component: CoordinadorLogisticoPage,
                    //   color: 'yellow',
                    //   icon: 'kargoo-coordinador2',
                    //   push: false
                    // },
                    {
                        title: 'Transportistas',
                        component: TransportistasfavoritosPage,
                        color: 'yellow',
                        icon: 'kargoo-menu_transportistas',
                        push: true,
                        img: 'menu_transportistas_v2.svg',
                        subtitle: 'Agenda'
                    },
                    {
                        title: 'Mueve Puntos',
                        component: MueveputosPage,
                        color: 'yellow',
                        icon: 'kargoo-menu_mueve_puntos',
                        push: true,
                        img: 'menu_mp_v2.svg',
                        subtitle: 'Rangos, premios y más'
                    },
                    {
                        title: 'Descubre',
                        component: DescubrePage,
                        color: 'yellow',
                        icon: 'kargoo-menu_mueve_puntos',
                        push: true,
                        img: 'menu_descubre_v2.svg',
                        subtitle: 'Guía y consejos'
                    },
                    {
                        title: 'Ajustes',
                        component: AjustesAppPage,
                        color: 'yellow',
                        icon: 'kargoo-menu_mueve_puntos',
                        push: true,
                        img: 'menu_ajustes_v2.svg',
                        subtitle: 'Configuración general'
                    },
                ];
            }
            if (data === 'NORMAL') {
                console.log(data);
                this.Ppages = [
                    {
                        title: 'Solicitudes',
                        component: SolicitudesPage,
                        icon: 'kargoo-solicitudes',
                        color: 'azul_claro',
                        push: false,
                        img: 'menu_ofertas.svg',
                        subtitle: ''
                    },
                    // {title: 'Ofertas', component: ListaOfertasPage, color: 'azul_claro', icon: 'filing', push: false},
                    // {title: 'Ofertas', component: ListaOfertasPage, icon: 'filing', color: 'azul_claro', push: false},
                    {
                        title: 'Transportistas',
                        component: TransportistasListPage,
                        icon: 'kargoo-transportistas',
                        color: 'azul_claro',
                        push: false,
                        img: 'menu_ofertas.svg',
                        subtitle: ''
                    },
                ];
            }
        });
        // let check_fcm_loaded: any;
        platform.ready().then(() => {
            //Back button handling
            var lastTimeBackPress = 0;
            var timePeriodToExit = 2000;
            this.serviceDB._CreateTable();
            // this.Synchronization.SubscriptionsEvents();
            //this.dataGlobal.SetSubscribeEvents();
            platform.registerBackButtonAction(() => {
                // get current active page
                let view = this.nav.getActive();
                if (view.instance instanceof ListaOfertasPage) {
                    if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                        platform.exitApp(); //Exit from app
                    }
                    else {
                        let toast = this.toastCtrl.create({
                            message: 'Pulsa dos veces para salir.',
                            duration: 2000,
                            position: 'bottom',
                        });
                        toast.present();
                        lastTimeBackPress = new Date().getTime();
                    }
                }
                else {
                    this.nav.pop({}); // go to previous page
                }
            });
            splashScreen.hide();
            this.headerColor.tint('#00AEEA');
            // let status bar overlay webview
            // this.statusBar.overlaysWebView(true);
            // set status bar to white
            this.statusBar.backgroundColorByHexString('#ffffff');
            this.statusBar.styleDefault();
            // this.headerColor.tint('#becb29');
            if (!localStorage.getItem('idUserMUEVE')) {
                timer(10000).subscribe(() => this.showSplash = false);
            }
            else {
                this.showSplash = false;
            }
            if (splashScreen) {
                setTimeout(() => {
                    splashScreen.hide();
                }, 100);
            }
            events.subscribe('usuario:iniciado_cargar_data', val => {
                console.log('usuario:iniciado_cargar_data', val);
                //-- / this.EventosList();
            });
        });
        firebaseService.setPluginFCM();
        // Check if the user has already seen the tutorial
        // decide which menu items should be hidden by current login status stored in local storage
    }
    _IMG_DEFAULT_PERFIL(event) {
        event.target.src = 'assets/imgs/icon/m_f_perfil.svg';
    }
    get ImagenPerfil() {
        return DataglobalProvider.ImagenPerfil;
    }
    break_text(text) {
        let b = text.split(' ');
        if (b.length >= 4) {
            // return `${b[0]} ${b[1]} <br> ${b[2]} ${b[3]}`
            return `<b>${b[0]} ${b[1]}</b>`;
        }
        if (b.length >= 3) {
            // return `${b[0]} ${b[1]} <br> ${b[2]}`
            return `<b>${b[0]} ${b[1]}</b>`;
        }
        if (b.length >= 2) {
            return `<b>${b[0]} ${b[1]}</b>`;
        }
        return b.join(`<br>`);
    }
    get Perfil() {
        return DataglobalProvider.Perfil;
    }
    /** ---------------------------------------*
     *                GETERS
     *------------------------------------------*
     */
    get _user() {
        return this.USER = {
            usuario_empresa: localStorage.getItem("empresa"),
            usuario_nombre: localStorage.getItem("nameUserMUEVE"),
            usuario_id: localStorage.getItem("idUserMUEVE"),
            pin: localStorage.getItem("pin"),
            perfil: localStorage.getItem("perfil"),
            categoria: localStorage.getItem("categoria"),
            puntos: localStorage.getItem("puntos")
        };
    }
    get _BASE_URL() {
        return AppModule.BASE_URL;
    }
    _IMG_DEFAULT(event) {
        event.target.src = 'assets/imgs/perfil.png';
    }
    getUrl() {
        // let idMunicipio = localStorage.getItem("idMunicUserTRUCK");
        // let ruta = `url('${this._BASE_URL}/img/municipios/${idMunicipio}.jpg')`;
        return "url('assets/imgs/bg/src_assets_imgs_bg_fondo-perfil.png')";
    }
    _Ajustes() {
        this.nav.push(AjustesAppPage);
    }
    rootPerfil() {
        this.nav.setRoot(PerfilPage);
    }
    /**-----------------------------------------*
     *              Fin DE GETERS
     * -----------------------------------------*
     */
    openPage(p) {
        if (p.push === false) {
            this.nav.setRoot(p.component);
        }
        else {
            this.nav.push(p.component);
        }
    }
    listenToLoginEvents() {
        this.events.subscribe('user:login', () => {
            this.enableMenu(true);
        });
        this.events.subscribe('user:signup', () => {
            this.enableMenu(true);
        });
        this.events.subscribe('user:logout', () => {
            this.enableMenu(false);
        });
    }
    enableMenu(loggedIn) {
        this.menu.enable(loggedIn, 'loggedInMenu');
        //this.menu.enable(!loggedIn, 'loggedOutMenu');
    }
    isActive(page) {
        let childNav = this.nav.getActiveChildNavs()[0];
        // Tabs are a special case because they have their own navigation
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }
        if (this.nav.getActive() && this.nav.getActive().name === page.name) {
            return 'primary';
        }
        return;
    }
    _Perfil() {
        this.nav.push(PerfilPage);
    }
    categoria(categoria) {
        let label;
        switch (categoria) {
            case 'GOLD':
                label = 'assets/imgs/icon/ico_oro_mp.svg';
                break;
            case 'SILVER':
                label = 'assets/imgs/icon/ico_plata_mp.svg';
                break;
            case 'BRONZE':
                label = 'assets/imgs/icon/ico_bronce_mp.svg';
                break;
        }
        return label;
    }
    _LogOut() {
        console.log('Subscribe');
        try {
            FCMPlugin.unsubscribeFromTopic(`notificacion${localStorage.getItem('idUserMUEVE')}`);
        }
        catch (e) {
            console.log(e);
        }
        // this.Api._GET('/session/cerrar').subscribe(data => console.log(data));
        localStorage.clear();
        AppModule.My_Token = '';
        this._CloseGPS();
        this._DestroySocket();
        this.Complementos._MessageToast('¡Fin de la sesion!');
        this.nav.setRoot(LoginPage);
    }
    /**-- Destruir SOCKET --**/
    _DestroySocket() {
        GpsProvider.LISTA_TURNO.splice(0, GpsProvider.LISTA_TURNO.length);
        GpsProvider.MESSAGES.splice(0, GpsProvider.MESSAGES.length);
        if (GpsProvider.EMIT != undefined) {
            console.log(' Disconect Socket', GpsProvider.EMIT);
            GpsProvider.EMIT.disconnect();
            console.log('1 Disconect Socket', GpsProvider.EMIT);
        }
    }
    /** CERRAR GPS **/
    _CloseGPS() {
        console.group('CERRANDO EL EMIT');
        console.log('Cerrando el Envio de paquetes:', GpsProvider.intervalSocket);
        console.groupEnd();
        // GpsProvider.Band_Conexion=false;
        clearInterval(GpsProvider.intervalSocket);
        clearInterval(GpsProvider.antiFakeGPS);
        let D = clearInterval(GpsProvider.intervalSocket);
        GpsProvider.intervalSocket = 0;
        console.log('Cerrando el Envio de paquetes2', GpsProvider.intervalSocket, 'D:', D);
    }
};
ConferenceApp.Notifi_ofert = 0;
__decorate([
    ViewChild(Nav),
    __metadata("design:type", Nav)
], ConferenceApp.prototype, "nav", void 0);
ConferenceApp = __decorate([
    Component({
        templateUrl: 'app.template.html',
        providers: [DatabaseProvider]
    }),
    __metadata("design:paramtypes", [Platform,
        SplashScreen,
        Events,
        UserData,
        MenuController,
        DatabaseProvider,
        ConferenceData,
        Storage,
        FirebaseProvider,
        ToastController,
        DataglobalProvider,
        HeaderColor,
        ApiProvider,
        ComplementosviewsProvider,
        SynchronizationDatabaseProvider,
        StatusBar,
        MenuController])
], ConferenceApp);
export { ConferenceApp };
//# sourceMappingURL=app.component.js.map