var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ErrorHandler, NgModule } from '@angular/core';
import { Geolocation } from "@ionic-native/geolocation";
import { File } from "@ionic-native/file";
import { FileTransfer } from "@ionic-native/file-transfer";
import { FileOpener } from "@ionic-native/file-opener";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { Camera } from "@ionic-native/camera";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QRCodeModule } from 'angularx-qrcode';
import { IonicStorageModule } from '@ionic/storage';
import { ConferenceApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SolicitudesPage } from '../pages/solicitudes/solicitudes';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SolicitudDetallePage } from '../pages/solicitud-detalle/solicitud-detalle';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { ListaOfertasPage } from '../pages/lista-ofertas/lista-ofertas';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { OfertasPage } from "../pages/ofertas/ofertas";
import { AccordionComponent } from "../components/accordion/accordion";
import { TypeaheadComponent } from "../components/typeahead/typeahead";
import { ApiProvider } from '../providers/api/api';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { PopoverbuttonspagePage } from "../pages/popoverbuttonspage/popoverbuttonspage";
import { AsignarTransportistasPage } from "../pages/asignar-transportistas/asignar-transportistas";
import { DespacharTrasportistasPage } from "../pages/despachar-trasportistas/despachar-trasportistas";
import { ComplementosviewsProvider } from "../providers/complementosviews/complementosviews";
import { EstadisticasPage } from "../pages/estadisticas/estadisticas";
import { ProgressBarComponent } from "../components/progress-bar/progress-bar";
import { OfertaDetallePage } from "../pages/oferta-detalle/oferta-detalle";
import { GpsProvider } from "../providers/gps/gps";
import { ChatPage } from "../pages/chat/chat";
import { MessagesPage } from "../pages/messages/messages";
import { SQLite } from "@ionic-native/sqlite";
import { DatabaseProvider } from "../providers/database/database";
import { PerfilPage } from "../pages/perfil/perfil";
import { AddTransportistaPage } from "../pages/add-transportista/add-transportista";
import { TransportistasListPage } from "../pages/transportistas-list/transportistas-list";
import { PopoverTransportistaPage } from "../pages/popover-transportista/popover-transportista";
import { TrailerPage } from "../pages/trailer/trailer";
import { VehiculoPage } from "../pages/vehiculo/vehiculo";
import { PopoverDespachoPage } from "../pages/popover-despacho/popover-despacho";
import { EventpopoverPage } from "../pages/eventpopover/eventpopover";
import { EventosTransportistaPage } from "../pages/eventos-transportista/eventos-transportista";
import { OrdenCargaPage } from "../pages/orden-carga/orden-carga";
import { PopoverculminarPage } from "../pages/popoverculminar/popoverculminar";
import { AcceptPopoverPage } from "../pages/accept-popover/accept-popover";
import { RemesamanifiestoPage } from "../pages/remesamanifiesto/remesamanifiesto";
import { GenerateCumplidoPage } from "../pages/generate-cumplido/generate-cumplido";
import { GenerateCumplidoManifiestoPage } from "../pages/generate-cumplido-manifiesto/generate-cumplido-manifiesto";
import { GenerateCumplidoRemesaPage } from "../pages/generate-cumplido-remesa/generate-cumplido-remesa";
import { ReporteMrPage } from "../pages/reporte-mr/reporte-mr";
import { EditartrailerPage } from "../pages/editartrailer/editartrailer";
import { EditarvehiculoPage } from "../pages/editarvehiculo/editarvehiculo";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { CreateOfertPage } from "../pages/create-ofert/create-ofert";
import { OfertanewdetallePage } from "../pages/ofertanewdetalle/ofertanewdetalle";
import { TransportistasfavoritosPage } from "../pages/transportistasfavoritos/transportistasfavoritos";
import { ForgetAcountPage } from "../pages/forget-acount/forget-acount";
import { EmpresaaddPage } from "../pages/empresaadd/empresaadd";
import { PerfileditPage } from "../pages/perfiledit/perfiledit";
import { CuentasasociadasPage } from "../pages/cuentasasociadas/cuentasasociadas";
import { DeleteAcountPage } from "../pages/delete-acount/delete-acount";
import { CoordinadorLogisticoPage } from "../pages/coordinador-logistico/coordinador-logistico";
import { DetailvinculatePage } from "../pages/detailvinculate/detailvinculate";
import { OfertaeditPage } from "../pages/ofertaedit/ofertaedit";
import { PopovereditPage } from "../pages/popoveredit/popoveredit";
import { ReputacionPage } from "../pages/reputacion/reputacion";
import { DetalleactivosPage } from "../pages/detalleactivos/detalleactivos";
import { TimelinetransportistaPage } from "../pages/timelinetransportista/timelinetransportista";
import { DetalletransportistaPage } from "../pages/detalletransportista/detalletransportista";
import { PopoverreportPage } from "../pages/popoverreport/popoverreport";
import { ReporttransportistaPage } from "../pages/reporttransportista/reporttransportista";
import { DetalleeventoPage } from "../pages/detalleevento/detalleevento";
import { CalificartransportistaPage } from "../pages/calificartransportista/calificartransportista";
import { MapaeventoPage } from "../pages/mapaevento/mapaevento";
import { ActivityPage } from "../pages/activity/activity";
import { MueveputosPage } from "../pages/mueveputos/mueveputos";
import { TopdespachadoresPage } from "../pages/topdespachadores/topdespachadores";
import { CallNumber } from "@ionic-native/call-number";
import { DataglobalProvider } from '../providers/dataglobal/dataglobal';
import { DeleteacountopinionPage } from "../pages/deleteacountopinion/deleteacountopinion";
import { LostConectionPage } from "../pages/lost-conection/lost-conection";
import { AjustesAppPage } from "../pages/ajustes-app/ajustes-app";
import { DescubrePage } from "../pages/descubre/descubre";
import { TokenInterceptor } from "../providers/interceptors/token.interceptor";
import { FirebaseProvider } from "../providers/firebase/firebase";
import { SocialSharing } from "@ionic-native/social-sharing";
import { SynchronizationDatabaseProvider } from "../providers/synchronization-database/synchronization-database";
import { ReportarProblemaPage } from "../pages/reportar-problema/reportar-problema";
import { EvidenciasPage } from "../pages/evidencias/evidencias";
import { ValidateFormsProvider } from "../providers/validate-forms/validate-forms";
import { HeaderColor } from "@ionic-native/header-color";
import { StatusBar } from "@ionic-native/status-bar";
import { DetailcalificacionPage } from "../pages/detailcalificacion/detailcalificacion";
import { TopganadoresPage } from "../pages/topganadores/topganadores";
import { BonusPage } from "../pages/bonus/bonus";
import { RetosPage } from "../pages/retos/retos";
import { LogrosPage } from "../pages/logros/logros";
import { EtiquetaPage } from "../pages/etiqueta/etiqueta";
import { Descompilar } from "../providers/descopilar/descompilar";
import { LinksRedirect } from "../providers/linksRedirect";
let AppModule = class AppModule {
};
AppModule.BASE_URL = 'http://192.168.88.59:9600';
// static BASE_URL = 'http://grupomuevesas.ddns.net:9600';
// static BASE_URL = 'http://192.168.88.151:9600';
AppModule.My_Token = window.localStorage.getItem("token");
AppModule.ShowTapss = true;
AppModule = __decorate([
    NgModule({
        declarations: [
            ConferenceApp,
            AboutPage,
            AccountPage,
            LoginPage,
            MapPage,
            PopoverPage,
            SolicitudesPage,
            ScheduleFilterPage,
            SolicitudDetallePage,
            SignupPage,
            SpeakerDetailPage,
            ListaOfertasPage,
            TabsPage,
            OfertanewdetallePage,
            TutorialPage,
            SupportPage,
            OfertasPage,
            AccordionComponent,
            AddTransportistaPage,
            TypeaheadComponent,
            ProgressBarComponent,
            DetalletransportistaPage,
            PopoverbuttonspagePage,
            AsignarTransportistasPage,
            DespacharTrasportistasPage,
            EstadisticasPage,
            OfertaDetallePage,
            ChatPage,
            OrdenCargaPage,
            PopoverDespachoPage,
            EventpopoverPage,
            ReputacionPage,
            MessagesPage,
            PopoverculminarPage,
            ReporteMrPage,
            EventosTransportistaPage,
            AcceptPopoverPage,
            PopovereditPage,
            CoordinadorLogisticoPage,
            PerfilPage,
            TransportistasListPage,
            GenerateCumplidoManifiestoPage,
            GenerateCumplidoRemesaPage,
            GenerateCumplidoPage,
            PopoverTransportistaPage,
            TrailerPage,
            VehiculoPage,
            TransportistasfavoritosPage,
            CreateOfertPage,
            OfertaeditPage,
            EmpresaaddPage,
            DetalleactivosPage,
            TimelinetransportistaPage,
            ForgetAcountPage,
            CuentasasociadasPage,
            EditartrailerPage,
            DetailvinculatePage,
            EditarvehiculoPage,
            DeleteAcountPage,
            RemesamanifiestoPage,
            PerfileditPage,
            PopoverreportPage,
            ReporttransportistaPage,
            DetalleeventoPage,
            CalificartransportistaPage,
            MapaeventoPage,
            ActivityPage,
            MueveputosPage,
            TopdespachadoresPage,
            DeleteacountopinionPage,
            LostConectionPage,
            AjustesAppPage,
            DescubrePage,
            ReportarProblemaPage,
            EvidenciasPage,
            DetailcalificacionPage,
            TopganadoresPage,
            BonusPage,
            RetosPage,
            LogrosPage,
            EtiquetaPage
        ],
        imports: [
            BrowserModule,
            HttpModule,
            QRCodeModule,
            HttpClientModule,
            IonicModule.forRoot(ConferenceApp, {}, {
                links: [
                    { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
                    { component: SolicitudesPage, name: 'Schedule', segment: 'schedule' },
                    { component: SolicitudDetallePage, name: 'SessionDetail', segment: 'sessionDetail/:sessionId' },
                    { component: ScheduleFilterPage, name: 'ScheduleFilter', segment: 'scheduleFilter' },
                    { component: ListaOfertasPage, name: 'SpeakerList', segment: 'speakerList' },
                    { component: SpeakerDetailPage, name: 'SpeakerDetail', segment: 'speakerDetail/:speakerId' },
                    { component: MapPage, name: 'Map', segment: 'map' },
                    { component: AboutPage, name: 'About', segment: 'about' },
                    { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
                    { component: SupportPage, name: 'SupportPage', segment: 'support' },
                    { component: LoginPage, name: 'LoginPage', segment: 'login' },
                    { component: AccountPage, name: 'AccountPage', segment: 'account' },
                    { component: SignupPage, name: 'SignupPage', segment: 'signup' }
                ]
            }),
            IonicStorageModule.forRoot()
        ],
        bootstrap: [IonicApp],
        entryComponents: [
            ConferenceApp,
            AboutPage,
            AccountPage,
            CuentasasociadasPage,
            AcceptPopoverPage,
            LoginPage,
            MapPage,
            PopoverPage,
            SolicitudesPage,
            ScheduleFilterPage,
            DeleteAcountPage,
            ReporteMrPage,
            SolicitudDetallePage,
            PerfileditPage,
            SignupPage,
            SpeakerDetailPage,
            DetalletransportistaPage,
            PopovereditPage,
            ReputacionPage,
            TimelinetransportistaPage,
            DetalleactivosPage,
            RemesamanifiestoPage,
            GenerateCumplidoManifiestoPage,
            GenerateCumplidoRemesaPage,
            GenerateCumplidoPage,
            ListaOfertasPage,
            ForgetAcountPage,
            TabsPage,
            TutorialPage,
            OfertaeditPage,
            SupportPage,
            OrdenCargaPage,
            OfertasPage,
            PopoverbuttonspagePage,
            AsignarTransportistasPage,
            DespacharTrasportistasPage,
            EstadisticasPage,
            OfertaDetallePage,
            PopoverDespachoPage,
            CoordinadorLogisticoPage,
            DetailvinculatePage,
            ChatPage,
            PopoverculminarPage,
            MessagesPage,
            OfertanewdetallePage,
            AddTransportistaPage,
            EventosTransportistaPage,
            PerfilPage,
            TransportistasListPage,
            EventpopoverPage,
            PopoverTransportistaPage,
            TrailerPage,
            TransportistasfavoritosPage,
            CreateOfertPage,
            VehiculoPage,
            EmpresaaddPage,
            EditartrailerPage,
            PopoverreportPage,
            EditarvehiculoPage,
            ReporttransportistaPage,
            DetalleeventoPage,
            CalificartransportistaPage,
            MapaeventoPage,
            ActivityPage,
            MueveputosPage,
            TopdespachadoresPage,
            DeleteacountopinionPage,
            LostConectionPage,
            AjustesAppPage,
            DescubrePage,
            ReportarProblemaPage,
            EvidenciasPage,
            DetailcalificacionPage,
            TopganadoresPage,
            BonusPage,
            RetosPage,
            LogrosPage,
            EtiquetaPage
        ],
        providers: [
            { provide: ErrorHandler, useClass: IonicErrorHandler },
            {
                provide: HTTP_INTERCEPTORS,
                useClass: TokenInterceptor,
                multi: true
            },
            ConferenceData,
            Geolocation,
            SQLite,
            File,
            FileTransfer,
            Camera,
            FileOpener,
            LocalNotifications,
            BarcodeScanner,
            UserData,
            InAppBrowser,
            SplashScreen,
            ApiProvider,
            GpsProvider,
            CallNumber,
            ComplementosviewsProvider,
            DatabaseProvider,
            DataglobalProvider,
            FirebaseProvider,
            SocialSharing,
            SynchronizationDatabaseProvider,
            ValidateFormsProvider,
            HeaderColor,
            StatusBar,
            Descompilar,
            LinksRedirect
        ]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map